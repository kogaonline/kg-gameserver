# Koga Online Project - NodeJS

## Game Server

### MySql

Mysql settings (such as server, user and password) are configured in the `.env` file.
If you use a **MySql version greater than 5.6**, you probably will have problem with old password support. Then you might have to run this from MySql CLI:

`ALTER USER 'root'@'localhost' IDENTIFIED BY '';`

`ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'new_password';`

### Sequelize

See the `.env` file to configure the database connection. All database managements and connections were migrated to Sequelize, with all foreign keys as well. We now have all the database structure into script files to recreate or reproduce structure and data from nothing in anywhere. Also all updates will be made in a safer way.

### Socket Pool

Socket Pool is a helper application (actually, a unique-class-application) that intermediates connections between clients and the game servers. It has the cryptography configured in it self, so, when Socket Pool is enabled, all players have their cryptography settings exchanged to it, and the packets get decrypted before it's sent to the Game Server, and get encrypted before it's sent to the client, but no ecryption is set between Socket Pool and the Game Server.

The Socket Pool handles and holds all player connections. This means that the connection is not broken if the Game Server crashes and/or restarts. When it happens, The Socket Pool starts a reconnect process every X seconds, and when the connection with Game Server is reopened, Socket Pool starts transmiting the game packets again. When the Game Server is down, packets are not sent, they get queued and sent after it get online again.

For development, this would be a great time economy, as you need not to wait the login screen load and login again when you restart the server. You can just let the monitor reload the server when code gets updated, and then, you already have the connection established with the Game Server.

You can enable and disable this feature by changing `KOGA_ENV` setting to `dev` or `prod` in the `.env` file.