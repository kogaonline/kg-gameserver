/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50045
Source Host           : localhost:3306
Source Database       : kg

Target Server Type    : MYSQL
Target Server Version : 50045
File Encoding         : 65001

Date: 2018-05-02 01:15:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cq_event_reg
-- ----------------------------
DROP TABLE IF EXISTS `cq_event_reg`;
CREATE TABLE `cq_event_reg` (
  `event` bigint(255) unsigned default NULL,
  `participant` bigint(255) unsigned default NULL,
  `name` varchar(255) default NULL,
  `timestamp` varchar(255) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
