/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50045
Source Host           : localhost:3306
Source Database       : kg

Target Server Type    : MYSQL
Target Server Version : 50045
File Encoding         : 65001

Date: 2019-04-05 00:16:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for accounts
-- ----------------------------
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
  `id` bigint(12) unsigned NOT NULL auto_increment,
  `username` char(25) NOT NULL default '',
  `password` char(16) default NULL,
  `ip` char(15) default NULL,
  `status` tinyint(5) NOT NULL,
  `playerId` bigint(18) NOT NULL,
  `email` varchar(255) default NULL,
  PRIMARY KEY  (`id`,`username`)
) ENGINE=MyISAM AUTO_INCREMENT=137 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;
