/*
 Navicat Premium Data Transfer

 Source Server         : 10.211.55.3
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : 10.211.55.3:3306
 Source Schema         : kg

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 30/08/2019 00:11:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cq_item
-- ----------------------------
DROP TABLE IF EXISTS `cq_item`;
CREATE TABLE `cq_item` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(4) unsigned NOT NULL DEFAULT '0',
  `owner_id` int(4) unsigned NOT NULL DEFAULT '0',
  `player_id` int(4) unsigned NOT NULL DEFAULT '0',
  `amount` smallint(2) unsigned NOT NULL DEFAULT '0',
  `amount_limit` smallint(2) unsigned NOT NULL DEFAULT '0',
  `ident` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `position` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `gem1` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `gem2` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `magic1` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `magic2` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `magic3` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `data` int(4) unsigned NOT NULL DEFAULT '0',
  `reduce_dmg` tinyint(1) unsigned zerofill NOT NULL DEFAULT '0',
  `add_life` tinyint(1) unsigned zerofill NOT NULL DEFAULT '0',
  `anti_monster` tinyint(1) unsigned zerofill NOT NULL DEFAULT '0',
  `chk_sum` int(4) unsigned zerofill NOT NULL DEFAULT '0000',
  `plunder` int(4) unsigned zerofill NOT NULL DEFAULT '0000',
  `SpecialFlag` int(4) unsigned zerofill NOT NULL DEFAULT '0000',
  `color` tinyint(1) unsigned zerofill NOT NULL DEFAULT '3',
  `Addlevel_exp` int(4) unsigned zerofill NOT NULL DEFAULT '0000',
  `Addhole_exp` int(4) unsigned zerofill NOT NULL DEFAULT '0000',
  `monopoly` tinyint(1) unsigned zerofill NOT NULL DEFAULT '0',
  `inscribed` tinyint(1) unsigned zerofill NOT NULL DEFAULT '0',
  `artifact_type` int(4) unsigned NOT NULL DEFAULT '0',
  `artifact_start` int(4) unsigned NOT NULL DEFAULT '0',
  `artifact_expire` int(4) unsigned NOT NULL DEFAULT '0',
  `artifact_stabilization` int(4) unsigned NOT NULL DEFAULT '0',
  `refinery_type` int(4) unsigned NOT NULL DEFAULT '0',
  `refinery_level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `refinery_start` int(4) unsigned NOT NULL DEFAULT '0',
  `refinery_expire` int(4) unsigned NOT NULL DEFAULT '0',
  `refinery_stabilization` int(4) unsigned NOT NULL DEFAULT '0',
  `stack` smallint(2) unsigned NOT NULL DEFAULT '1',
  `expiration` int(4) unsigned NOT NULL DEFAULT '0',
  `bound` smallint(2) unsigned DEFAULT '0',
  `warehouse` smallint(2) unsigned DEFAULT '0',
  `item_status` int(4) unsigned DEFAULT '0',
  `steed_color` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1690 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cq_item
-- ----------------------------
BEGIN;
INSERT INTO `cq_item` VALUES (59, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 3, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 3, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (61, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 3, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (62, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 3, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (63, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 3, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (64, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 3, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (65, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 3, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (66, 421339, 1010113, 1010113, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (67, 421329, 1010113, 1010113, 65535, 65535, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (68, 421329, 1010113, 1010113, 65535, 65535, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (72, 421329, 1010113, 1010113, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (73, 421339, 1010113, 1010113, 65535, 65535, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (74, 421329, 1010113, 1010113, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (75, 421339, 1010113, 1010113, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (76, 421339, 1010113, 1010113, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (77, 421329, 1010113, 1010113, 65535, 65535, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (78, 421329, 1010113, 1010113, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (79, 421339, 1010113, 1010113, 65535, 65535, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (80, 421339, 1010113, 1010113, 65535, 65535, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (81, 421329, 1010113, 1010113, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (82, 421329, 1010113, 1010113, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (83, 421329, 1010113, 1010113, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (84, 115109, 1010113, 1010113, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (85, 118109, 1010113, 1010113, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (86, 135109, 1010113, 1010113, 65535, 65535, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (87, 135109, 1010113, 1010113, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (88, 135109, 1010113, 1010113, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (89, 135109, 1010113, 1010113, 65535, 65535, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (90, 601109, 1010113, 1010113, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (91, 601009, 1010113, 1010113, 65535, 65535, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (92, 601009, 1010113, 1010113, 65535, 65535, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (93, 601109, 1010113, 1010113, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (94, 118109, 1010113, 1010113, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (95, 135109, 1010113, 1010113, 65535, 65535, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (96, 135079, 1010113, 1010113, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (97, 1088000, 1010117, 1010117, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (98, 1088000, 1010117, 1010117, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (101, 1088001, 1010117, 1010117, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (102, 1088001, 1010117, 1010117, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (103, 420339, 1000002, 1000002, 65535, 65535, 0, 4, 0, 0, 0, 0, 12, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (104, 420339, 1000002, 1000002, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (106, 1088000, 1000002, 1000002, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (110, 1088000, 1000002, 1000002, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (111, 1088000, 1000002, 1000002, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `cq_item` VALUES (112, 1088000, 1000002, 1000002, 65535, 65535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0000, 0000, 0000, 0, 0000, 0000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
