# ************************************************************
# Sequel Pro SQL dump
# Versão 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 172.16.5.128 (MySQL 5.7.25-log)
# Base de Dados: kg
# Tempo de Geração: 2019-04-10 13:26:52 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump da tabela cq_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cq_user`;

CREATE TABLE `cq_user` (
  `accountId` int(12) unsigned DEFAULT NULL,
  `uid` bigint(18) unsigned NOT NULL DEFAULT '0',
  `name` varchar(16) NOT NULL DEFAULT '',
  `hairStyle` smallint(12) unsigned DEFAULT '430',
  `class` tinyint(5) unsigned NOT NULL DEFAULT '10',
  `money` bigint(18) unsigned DEFAULT '999999999',
  `cps` bigint(18) unsigned DEFAULT '999999999',
  `cpsBound` bigint(10) unsigned DEFAULT '999999999',
  `treasurePoints` bigint(18) NOT NULL DEFAULT '0',
  `body` smallint(12) unsigned NOT NULL DEFAULT '0',
  `face` smallint(12) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(5) unsigned DEFAULT '1',
  `strength` smallint(12) unsigned DEFAULT '0',
  `agility` smallint(12) unsigned DEFAULT '0',
  `vitality` smallint(12) unsigned DEFAULT '0',
  `spirit` smallint(12) unsigned DEFAULT '0',
  `attrPoints` smallint(12) unsigned DEFAULT '0',
  `hp` mediumint(16) unsigned DEFAULT '500',
  `mana` mediumint(16) unsigned DEFAULT '0',
  `mapId` smallint(12) unsigned DEFAULT '1002',
  `x` smallint(12) unsigned DEFAULT '300',
  `y` smallint(12) unsigned DEFAULT '278',
  `pkPoints` smallint(12) unsigned DEFAULT '0',
  `experience` bigint(255) unsigned DEFAULT '0',
  `quizPoints` mediumint(30) unsigned DEFAULT '0',
  `prevMapId` smallint(12) unsigned DEFAULT '1010',
  `reborn` tinyint(2) unsigned DEFAULT '0',
  `firstClass` tinyint(5) unsigned DEFAULT '10',
  `secondClass` tinyint(2) unsigned DEFAULT '0',
  `firstRebornLevel` tinyint(5) unsigned DEFAULT '0',
  `secondRebornLevel` tinyint(5) unsigned DEFAULT '0',
  `spouse` varchar(16) DEFAULT 'None',
  `whPassword` varchar(16) DEFAULT '',
  `whMoney` bigint(18) unsigned DEFAULT '0',
  `enlightenPoints` bigint(18) unsigned DEFAULT '0',
  `heavenBlessing` bigint(255) unsigned DEFAULT '0',
  `bless` bigint(18) unsigned DEFAULT '0',
  `enlightments` tinyint(5) unsigned DEFAULT '0',
  `enlightmentTime` mediumint(100) unsigned DEFAULT '0',
  `guildId` bigint(18) unsigned DEFAULT '0',
  `guildRank` bigint(18) unsigned DEFAULT '0',
  `guildMoneyDonation` bigint(255) unsigned DEFAULT '0',
  `guildCpsDonation` bigint(255) unsigned DEFAULT '0',
  `vipLevel` tinyint(5) unsigned DEFAULT '0',
  `virtuePoints` bigint(255) unsigned DEFAULT '0',
  `prevX` mediumint(10) DEFAULT '0',
  `prevY` mediumint(10) DEFAULT '0',
  `clanId` int(36) unsigned DEFAULT '0',
  `clanDonation` bigint(64) unsigned DEFAULT '0',
  `clanRank` int(36) unsigned DEFAULT '0',
  `subclass` int(36) unsigned NOT NULL DEFAULT '0',
  `subclassLevel` int(36) unsigned NOT NULL DEFAULT '0',
  `studyPoints` int(36) unsigned DEFAULT '0',
  `lastLogin` bigint(16) DEFAULT '0',
  `title` int(36) NOT NULL DEFAULT '0',
  `firstBuy` smallint(4) unsigned NOT NULL DEFAULT '0',
  `country` smallint(4) DEFAULT NULL,
  `flower` bigint(32) NOT NULL DEFAULT '0',
  `namechange` varchar(16) DEFAULT NULL,
  `namechangeCount` smallint(12) DEFAULT '0',
  `racePoints` bigint(18) NOT NULL DEFAULT '10',
  `multipleExp` bigint(18) unsigned DEFAULT '0',
  `multipleExpTimes` bigint(18) unsigned DEFAULT '100',
  `onlineTrainingExp` bigint(12) unsigned NOT NULL DEFAULT '0',
  `blessedHuntingExp` bigint(12) unsigned NOT NULL DEFAULT '0',
  `royalPoints` bigint(255) unsigned NOT NULL DEFAULT '0',
  `business` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `myIndex` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

LOCK TABLES `cq_user` WRITE;
/*!40000 ALTER TABLE `cq_user` DISABLE KEYS */;

INSERT INTO `cq_user` (`accountId`, `uid`, `name`, `hairStyle`, `class`, `money`, `cps`, `cpsBound`, `treasurePoints`, `body`, `face`, `level`, `strength`, `agility`, `vitality`, `spirit`, `attrPoints`, `hp`, `mana`, `mapId`, `x`, `y`, `pkPoints`, `experience`, `quizPoints`, `prevMapId`, `reborn`, `firstClass`, `secondClass`, `firstRebornLevel`, `secondRebornLevel`, `spouse`, `whPassword`, `whMoney`, `enlightenPoints`, `heavenBlessing`, `bless`, `enlightments`, `enlightmentTime`, `guildId`, `guildRank`, `guildMoneyDonation`, `guildCpsDonation`, `vipLevel`, `virtuePoints`, `prevX`, `prevY`, `clanId`, `clanDonation`, `clanRank`, `subclass`, `subclassLevel`, `studyPoints`, `lastLogin`, `title`, `firstBuy`, `country`, `flower`, `namechange`, `namechangeCount`, `racePoints`, `multipleExp`, `multipleExpTimes`, `onlineTrainingExp`, `blessedHuntingExp`, `royalPoints`, `business`)
VALUES
	(12,1010013,'Belladona[PM]',533,41,135290153,24313754,0,0,2001,448,90,16,53,7,0,790,278,0,1015,717,577,0,5167397,200,1002,1,10,0,140,0,'None','0',0,400,0,0,0,0,4780,499,500000,0,0,0,173,258,0,0,0,0,0,0,636893842112269884,0,0,0,0,NULL,0,0,0,100,0,552830,0,0),
	(13,1010014,'Yuri[PM]',715,11,4497390,1981633,0,0,1003,104,140,185,72,110,0,60,4349,668,1002,231,235,0,70835727240,200,6000,0,10,0,0,0,'None','0',0,400,0,0,0,0,4783,1000,500000,0,1,0,0,0,0,0,0,0,0,0,636371115318304429,0,0,0,0,NULL,0,0,0,100,0,176,0,255),
	(12,1010113,'Adrian[PM]',433,45,25840485,85927,0,0,1003,45,130,0,75,47,245,4200,3334,2725,1015,721,572,0,250375505,200,1015,7,10,0,140,15,'None','0',0,500,4824771,885000,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,636758611927419055,0,0,0,0,NULL,0,0,0,100,18720351,38908677,0,255);

/*!40000 ALTER TABLE `cq_user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
