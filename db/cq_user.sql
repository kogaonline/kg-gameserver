/*
 Navicat Premium Data Transfer

 Source Server         : 10.211.55.3
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : 10.211.55.3:3306
 Source Schema         : kg

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 30/08/2019 00:10:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cq_user
-- ----------------------------
DROP TABLE IF EXISTS `cq_user`;
CREATE TABLE `cq_user` (
  `accountId` int(12) unsigned DEFAULT NULL,
  `uid` bigint(18) unsigned NOT NULL DEFAULT '0',
  `name` varchar(16) NOT NULL DEFAULT '',
  `hairStyle` smallint(12) unsigned DEFAULT '430',
  `class` tinyint(5) unsigned NOT NULL DEFAULT '10',
  `money` bigint(18) unsigned DEFAULT '999999999',
  `cps` bigint(18) unsigned DEFAULT '999999999',
  `cpsBound` bigint(10) unsigned DEFAULT '999999999',
  `treasurePoints` bigint(18) NOT NULL DEFAULT '0',
  `body` smallint(12) unsigned NOT NULL DEFAULT '0',
  `face` smallint(12) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(5) unsigned DEFAULT '1',
  `strength` smallint(12) unsigned DEFAULT '0',
  `agility` smallint(12) unsigned DEFAULT '0',
  `vitality` smallint(12) unsigned DEFAULT '0',
  `spirit` smallint(12) unsigned DEFAULT '0',
  `attrPoints` smallint(12) unsigned DEFAULT '0',
  `hp` mediumint(16) unsigned DEFAULT '93',
  `mana` mediumint(16) unsigned DEFAULT '0',
  `mapId` smallint(12) unsigned DEFAULT '1002',
  `x` smallint(12) unsigned DEFAULT '300',
  `y` smallint(12) unsigned DEFAULT '278',
  `pkPoints` smallint(12) unsigned DEFAULT '0',
  `experience` bigint(255) unsigned DEFAULT '0',
  `quizPoints` mediumint(30) unsigned DEFAULT '200',
  `prevMapId` smallint(12) unsigned DEFAULT '1010',
  `reborn` tinyint(2) unsigned DEFAULT '0',
  `firstClass` tinyint(5) unsigned DEFAULT '10',
  `secondClass` tinyint(2) unsigned DEFAULT '0',
  `firstRebornLevel` tinyint(5) unsigned DEFAULT '0',
  `secondRebornLevel` tinyint(5) unsigned DEFAULT '0',
  `spouse` varchar(16) DEFAULT 'None',
  `whPassword` bigint(16) DEFAULT '0',
  `whMoney` bigint(18) unsigned DEFAULT '0',
  `enlightenPoints` bigint(18) unsigned DEFAULT '0',
  `heavenBlessing` bigint(255) unsigned DEFAULT '0',
  `bless` bigint(18) unsigned DEFAULT '0',
  `enlightments` tinyint(5) unsigned DEFAULT '0',
  `enlightmentTime` mediumint(100) unsigned DEFAULT '0',
  `guildId` bigint(18) unsigned DEFAULT '0',
  `guildRank` bigint(18) unsigned DEFAULT '0',
  `guildMoneyDonation` bigint(255) unsigned DEFAULT '0',
  `guildCpsDonation` bigint(255) unsigned DEFAULT '0',
  `vipLevel` tinyint(5) unsigned DEFAULT '0',
  `virtuePoints` bigint(255) unsigned DEFAULT '0',
  `prevX` mediumint(10) DEFAULT '0',
  `prevY` mediumint(10) DEFAULT '0',
  `clanId` int(36) unsigned DEFAULT '0',
  `clanDonation` bigint(64) unsigned DEFAULT '0',
  `clanRank` int(36) unsigned DEFAULT '0',
  `subclass` int(36) unsigned DEFAULT '0',
  `subclassLevel` int(36) unsigned DEFAULT '0',
  `studyPoints` int(36) unsigned DEFAULT '0',
  `lastLogin` bigint(16) DEFAULT '0',
  `title` int(36) DEFAULT '0',
  `firstBuy` smallint(4) unsigned DEFAULT '0',
  `country` smallint(4) DEFAULT '0',
  `flower` bigint(32) DEFAULT '0',
  `namechange` varchar(16) DEFAULT NULL,
  `namechangeCount` smallint(12) DEFAULT '0',
  `racePoints` bigint(18) DEFAULT '10',
  `multipleExp` bigint(18) unsigned DEFAULT '0',
  `multipleExpTimes` bigint(18) unsigned DEFAULT '100',
  `onlineTrainingExp` bigint(12) unsigned DEFAULT '0',
  `blessedHuntingExp` bigint(12) unsigned DEFAULT '0',
  `royalPoints` bigint(255) unsigned DEFAULT '0',
  `business` tinyint(3) unsigned DEFAULT '0',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `myIndex` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of cq_user
-- ----------------------------
BEGIN;
INSERT INTO `cq_user` VALUES (1, 1000002, 'Pirate', 615, 70, 999999999, 999999999, 999999999, 0, 1003, 1, 130, 504, 500, 505, 504, 0, 16644, 2520, 1002, 301, 283, 0, 0, 200, 1010, 0, 10, 0, 0, 0, 'None', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 10, 0, 100, 0, 0, 0, 0);
INSERT INTO `cq_user` VALUES (2, 1000003, 'Ninja', 518, 50, 999999999, 999999999, 999999999, 0, 2001, 201, 1, 10, 0, 1, 2, 0, 60, 0, 1002, 303, 282, 0, 0, 200, 1010, 0, 10, 0, 0, 0, 'None', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 10, 0, 100, 0, 0, 0, 0);
INSERT INTO `cq_user` VALUES (137, 1000004, 'Monk', 715, 60, 999999999, 999999999, 999999999, 0, 2001, 201, 1, 1, 3, 4, 5, 0, 123, 0, 1002, 300, 288, 0, 0, 200, 1010, 0, 10, 0, 0, 0, 'None', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 10, 0, 100, 0, 0, 0, 0);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
