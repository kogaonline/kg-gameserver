const fs = require('fs');
const moment = require('moment');

function generateSeed(data, count = 10000) {
  let seedData = `'use strict';

    module.exports = {
      up: (queryInterface, Sequelize) => {
        queryInterface.bulkInsert('cq_magictype',
          ${JSON.stringify(data)}
        , {});
        return new Promise((resolve) => resolve(0));
      },
    
      down: (queryInterface, Sequelize) => {
        queryInterface.bulkDelete('cq_magictype', null, {});
        return new Promise((resolve) => resolve(0));
      }
    };`;

  let name = `src/database/sequelize/seeders/${count}-cq_magictype.js`;

  if (fs.existsSync(name))
    fs.unlinkSync(name);

  fs.writeFileSync(name, seedData);
}

if (fs.existsSync('scripts/magictype.txt')) {
  let lines = (fs.readFileSync('scripts/magictype.txt').toLocaleString()).split('\n');

  let items = [];
  let itemsCount = 20000;
  lines.map((_line) => {
    let line = _line.replace('\r', '').replace('�', '');
    let param = line.split('\t');

    let item = {
      id: param[0],
      type: param[1],
      sort: param[2],
      name: param[3],
      pk: param[4],
      ground: param[5],
      multi: param[6],
      target: param[7],
      level: param[8],
      manacost: param[9],
      power: param[10],
      speed: param[11],
      percent: param[12],
      seconds: param[13],
      range: param[14],
      distance: param[15],
      attr: param[16],
      status: param[17],
      req_prof: param[18],
      req_exp: param[19],
      req_level: param[20],
      xp: param[21],
      weapon: param[22],
      times: param[23],
      auto: param[24],
      floor: param[25],
      auto_learn: param[26],
      learn_level: param[27],
      drop_weapon: param[28],
      stamina: param[29],
      weapon_hit: param[30],
      use_item: param[31],
      next_magic: param[32],
      delay: param[33],
      use_item_num: param[34],
      unknown1: param[35],
      unknown2: param[36],
      unknown3: param[37],
      unknown4: param[38],
      unknown5: param[39],
      unknown6: param[40],
      unknown7: param[41],
      unknown8: param[42],
      unknown9: param[43],
      unknown10: param[44],
      unknown11: param[45],
      unknown12: param[46],
      unknown13: param[47],
      upgrade_cost: param[47],
      createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
    };

    if (item.id.length > 0)
      items.push(item);

    if (items.length >= 100) {
      generateSeed(items, itemsCount);
      items = [];
      itemsCount++;
    }
  });

  generateSeed(items, itemsCount);
} else {
  console.log('file not found ->', 'magictype.txt');
}