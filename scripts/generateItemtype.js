const fs = require('fs');
const moment = require('moment');

function generateSeed(data, count = 10000){
    let seedData = `'use strict';

    module.exports = {
      up: (queryInterface, Sequelize) => {
        queryInterface.bulkInsert('cq_itemtype',
          ${JSON.stringify(data)}
        , {});
        return new Promise((resolve) => resolve(0));
      },
    
      down: (queryInterface, Sequelize) => {
        queryInterface.bulkDelete('cq_itemtype', null, {});
        return new Promise((resolve) => resolve(0));
      }
    };`;

    let name = `src/database/sequelize/seeders/${count}-cq_itemtype.js`;

    if (fs.existsSync(name))
        fs.unlinkSync(name);

    fs.writeFileSync(name, seedData);
}

if (fs.existsSync('scripts/itemtype.txt')){
    let lines = (fs.readFileSync('scripts/itemtype.txt').toLocaleString()).split('\n');

    let items = [];
    let itemsCount = 10000;
    lines.map((_line) => {
        let line = _line.replace('\r', '').replace('�', '');
        let param = line.split('\t');

        let item = {
            id: param[0],
            name: param[1],
            class: param[2],
            proficiency: param[3],
            level: param[4],
            gender: param[5],
            strength: param[6],
            agility: param[7],
            vitality: param[8],
            spirit: param[9],
            monopoly: param[10],
            weight: param[11],
            moneyValue: param[12],
            action: param[13],
            maxAttack: param[14],
            minAttack: param[15],
            defense: param[16],
            accuracy: param[17],
            dodge: param[18],
            addHP: param[19],
            addMP: param[20],
            durability: param[21],
            maxDurability: param[22],
            ident: param[23],
            gem1: param[24],
            gem2: param[25],
            magic1: param[26],
            magic2: param[27],
            magic3: param[28],
            unknown0: param[29],
            magicAttack: param[30],
            magicDefense: param[31],
            attackRange: param[32],
            attackSpeed: param[33],
            unknown1: param[34],
            unknown2: param[35],
            unknown3: param[36],
            cpsValue: param[37],
            unknown4: param[38],
            expirationTime: param[39],
            criticalStrike: param[40],
            detoxication: param[41],
            immunity: param[42],
            penetration: param[43],
            block: param[44],
            breakthrough: param[45],
            counterAction: param[46],
            stackSize: param[47],
            metalResist: param[48],
            woodResist: param[49],
            waterResist: param[50],
            fireResist: param[51],
            earthResist: param[52],
            type: param[53],
            description: param[54],
            unknown8: param[55],
            purificationLevel: param[56],
            purificationMeteors: param[57],
            rateLv: param[58],
            unknown10: param[59],
            unknown11: param[60],
            createdAt: moment().format('YYYY-MM-DD HH:mm:ss'),
            updatedAt: moment().format('YYYY-MM-DD HH:mm:ss'),
        };

        if (item.id.length > 0)
          items.push(item);

        if (items.length >= 380){
            generateSeed(items, itemsCount);
            items = [];
            itemsCount++;
        }
    });

    generateSeed(items, itemsCount);
} else {
    console.log('file not found ->', 'itemtype.txt');
}