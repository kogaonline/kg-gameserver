import Player from "../game/player";
import { EDirection } from "../enums/EDirection";
import Character from "../game/character";

export function getRandomInt(): number {
    let min = Math.ceil(0);
    let max = Math.floor(Number.MAX_SAFE_INTEGER - 1);
    return Math.floor(Math.random() * ((max + 1) - min)) + min;
}

export function getRandom(min: number, max: number): number {
    return Math.floor(Math.random() * ((max + 1) - min)) + min;
}

export function printBuffer(_buffer: Buffer) {
    let print = '[';
    for (let x = 0; x < _buffer.length; x++) {
        let b = _buffer[x];
        print = print + b + ',';
    }
    print = print + ']';
    console.log(`Printing packet: ${print}`)
}

export function distanceBetween(x1: number, y1: number, x2: number, y2: number): number {
    return Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
}

export function distanceBetweenPlayers(p1: Player, p2: Player): number {
    return distanceBetween(p1.character.x, p1.character.y, p2.character.x, p2.character.y);
}

export function distanceBetweenCharacters(c1: Character, c2: Character): number {
    return distanceBetween(c1.x, c1.y, c2.x, c2.y);
}

export function sanitizeInt(value: any, radix = 10): number {
    try {
        return Number.parseInt(value, radix)
    } catch (err) {
        console.error(err)
        return 0
    }
}

export function sanitizeUshort(value: any, radix = 10): number {
    try {
        let short = Number.parseInt(value, radix)
        if (short > 65535 || short < -65535)
            short = 0
        return Math.abs(short)
    } catch (err) {
        console.error(err)
        return 0
    }
}


export function getDirection(sx: number, sy: number, tx: number, ty: number): EDirection {
    let direction = 0;

    let AddX = tx - sx;
    let AddY = ty - sy;
    let r = Math.atan2(AddY, AddX);

    if (r < 0) r += Math.PI * 2;

    direction = 360 - (r * 180 / Math.PI);

    let Dir = ((7 - (Math.floor(direction) / 45 % 8)) - 1 % 8);
    return (Math.ceil(Dir % 8) as EDirection);
}

export function probability(chances: number, inTotal: number = 100) {
    let rate = (chances / inTotal) * 1000;
    let rand = Math.random() * 1000;
    let nrate = !!rate && rand;
    return nrate <= rate;
};