declare interface Date {
    addYears: (value: number) => Date;
    addMonths: (value: number) => Date;
    addDays: (value: number) => Date;
    addHours: (value: number) => Date;
    addMinutes: (value: number) => Date;
    addSeconds: (value: number) => Date;
    addMilliseconds: (value: number) => Date;

    subtractYears: (value: number) => Date;
    subtractMonths: (value: number) => Date;
    subtractDays: (value: number) => Date;
    subtractHours: (value: number) => Date;
    subtractMinutes: (value: number) => Date;
    subtractSeconds: (value: number) => Date;
    subtractMilliseconds: (value: number) => Date;
}