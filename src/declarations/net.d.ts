import Player from "../game/player";

declare module "net" {
    interface Socket {
        player: Player;
        client: any;
    }

}