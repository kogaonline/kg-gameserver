declare interface Buffer extends Uint8Array {
    writeUInt64LE(value: number, offset: number, noAssert?: boolean): number;
    readUInt64LE(offset: number, noAssert?: boolean): number;
    writeStringArray(value: string[], offset: number, noAssert?: boolean): number;
}

declare interface JSON {
    decycle: any;
    retrocycle: any;
}

declare interface Console {
    dblog: any;
    dberror: any;
    dbwarn: any;

    chatlog: any;
    itemlog: any;
    tradelog: any;
    playerlog: any;
    
    breakIf: (condition: boolean) => void;
}

declare interface String {
    format: (...args: any[]) => string;
}

declare module "*.json" {
    const value: any;
    export default value;
}

declare interface Array<T> {
    mapAsync<U>(callbackfn: (value: T, index: number, array: T[]) => U, thisArg?: any): Promise<any>
}