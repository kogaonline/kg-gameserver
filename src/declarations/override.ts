const bignum = require('bignum');

// String
String.prototype.format = function (){
    let str: string = this;

    for (let i = 0; i < arguments.length; i++){
        let arg = arguments[i];
        switch (typeof arg){
            case "number": {
                str = str.replace(`%d`, arg.toString());
                break;
            }

            case "string": {
                str = str.replace(`%s`, arg.toString());
                break;
            }
        }
    }

    return str;
}

// Array
Array.prototype.mapAsync = async function <U>(callbackfn: (value: any, index: number, arr: any[]) => U) {
    for (let i = 0; i < this.length; i++){
        await callbackfn(this[i], i, this);
    }
    return this;
};


Buffer.prototype.writeUInt64LE = function (value: number, offset: number, noAssert?: boolean){

    //max safe integer to big number
    let num = bignum(value);
    let buf: Buffer = num.toBuffer({endian:'low',size:8 /*8-byte / 64-bit*/});

    buf.copy(this, offset);
    
    // this.writeInt32LE(value & 0xffffffff, offset);
    // this.writeInt32LE(value / 0xffffffff, offset + 4);

    return 0;
}

Buffer.prototype.readUInt64LE = function (offset: number, noAssert?: boolean){
    let buf: Buffer = Buffer.alloc(8);
    this.copy(buf, 0, offset, offset + 8);
    let num = bignum.fromBuffer(buf, {endian:'low',size:8 /*8-byte / 64-bit*/});

    // let bufInt = (this.readUInt32LE(offset) << 8) + this.readUInt32LE(offset + 4);
    return Number.parseInt(num, 10);
}


Buffer.prototype.writeStringArray = function (value: string[], offset: number, noAssert?: boolean){
    this[offset] = value.length;

    offset++;
    for (let i = 0; i < value.length; i ++){
        let str = value[i];
        this[offset] = str.length;
        this.write(str, offset + 1);
        offset += str.length + 1;
    }
    
    return 0;
}

// Date
//
Date.prototype.addYears = function(value: number){
    this.setFullYear(this.getFullYear()+value);
    return this;
}
Date.prototype.addMonths = function(value: number){
    this.setMonth(this.getMonth()+value);
    return this;
}
Date.prototype.addDays = function(value: number){
    this.setDate(this.getDate()+value);
    return this;
}
Date.prototype.addHours = function(value: number){
    this.setHours(this.getHours()+value);
    return this;
}
Date.prototype.addMinutes = function(value: number){
    this.setMinutes(this.getMinutes()+value);
    return this;
}
Date.prototype.addSeconds = function(value: number){
    this.setSeconds(this.getSeconds()+value);
    return this;
}
Date.prototype.addMilliseconds = function(value: number){
    this.setMillseconds(this.getMilliseconds()+value);
    return this;
}

Date.prototype.subtractYears = function(value: number){
    this.setFullYear(this.getFullYear()-value);
    return this;
}
Date.prototype.subtractMonths = function(value: number){
    this.setMonth(this.getMonth()-value);
    return this;
}
Date.prototype.subtractDays = function(value: number){
    this.setDate(this.getDate()-value);
    return this;
}
Date.prototype.subtractHours = function(value: number){
    this.setHours(this.getHours()-value);
    return this;
}
Date.prototype.subtractMinutes = function(value: number){
    this.setMinutes(this.getMinutes()-value);
    return this;
}
Date.prototype.subtractSeconds = function(value: number){
    this.setSeconds(this.getSeconds()-value);
    return this;
}
Date.prototype.subtractMilliseconds = function(value: number){
    this.setMillseconds(this.getMilliseconds()-value);
    return this;
}