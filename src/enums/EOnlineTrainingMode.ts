export enum EOnlineTrainingMode {
    Show = 0,
    Disable = 1,
    Enable = 2,
    AddPoints = 3,
    Bright = 4,
    Hide = 5,
}