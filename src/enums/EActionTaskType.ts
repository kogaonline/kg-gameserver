export enum EActionTaskType {
    Realize = 1,
    Finish = 2,
    Interrupt = 3,
}