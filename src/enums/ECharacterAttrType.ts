export enum ECharacterAttrType {
    Accuracy = 5,
    Defense = 6,
    Attack = 7,
    FinalAttack = 13,
    Invisibility = 17,
    MoveSpeed = 18,
    Dodge = 21,
    Fly = 22,
    Pray = 25,
    Vortex = 39,
    Strike = 40,
    Poison = 42,
    Toxic = 43,
    FinalDefense = 44,
    DeadLock = 106
}