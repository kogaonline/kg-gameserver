export enum EMapElementType {
    NONE = 0,
    PLAYER = 1,
    MONSTER = 2,
    ITEM = 4,
    OBJECT = 8,
    NPC = 16,
    PORTAL = 32,
    SOBNPC = 64,
}