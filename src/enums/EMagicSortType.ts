export enum EMagicAttrType {
    None = 0,
    Accuracy = 5,
    Attack = 13,
    Defense = 44,
}