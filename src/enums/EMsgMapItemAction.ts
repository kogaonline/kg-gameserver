export enum EMsgMapItemAction {
    Create = 1,
    Delete = 2,
    Pick = 3,
    CastTrap = 10,
    SynchroTrap = 11,
    DropTrap = 12
}