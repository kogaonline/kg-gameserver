export enum ETitleType {
    None = 0,

    GoldenRacer = 11,

    ElitePKChamption_Low = 12,
    ElitePK2ndPlace_Low = 13,
    ElitePK3ndPlace_Low = 14,
    ElitePKTopEight_Low = 15,

    ElitePKChamption_High = 16,
    ElitePK2ndPlace_High = 17,
    ElitePK3ndPlace_High = 18,
    ElitePKTopEight_High = 19,
};