export enum EAppearanceType
{
    Default = 0,
    Garment = 1,
    Artifact = 2,
    Equipment = 3,

    First = Default,
    Last = Equipment + 1
}