export enum EAccountStatus {
    Unabled = 0,
    Enabled = 1,
    Banned = 2,
    BannedPermanently = 3
}