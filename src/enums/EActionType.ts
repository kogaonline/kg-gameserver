export enum EActionType {
    // ACTION_SYS
    /**
     * @type 1
     * @description none.
     */
    ACTION_STATMESSAGE = 1,
    /**
     * @type 100
     * @description Beggining of the system actions.
     */
    ACTION_SYS_FIRST = 100,
    
    /**
     * @type 101
     * @description Shows a text to player. This type can contain text but also can be blank.
     * @params `text` E.g.: Hello, %user_name! Nice to see you!
     */
    ACTION_MENUTEXT = 101,
    
    /**
     * @type 102
     * @description Shows an option to player to choose from dialog.
     * @params `text` E.g.: Nice\~to\~see\~you\~too! `task_id` E.g.: 100090
     * @note `text` must be filled with `~` instead of ` `  (spaces).
     */
    ACTION_MENULINK = 102,

    /**
     * @type 103
     * @description Shows an input box to player to write in dialog.
     * @params `maxsize` E.g.: 16 `task_id` E.g.: 100090 `text` E.g: Write\~your\~name:
     * @note `text` must be filled with `~` instead of ` `  (spaces).
     */
    ACTION_MENUEDIT = 103, // 
    
    /**
     * @type 104
     * @description Shows the face of dialog to player.
     * @params `x` E.g.: 1 `y` E.g.: 1 `face_id` E.g.: 102 `task_id` E.g.: 100090
     */
    ACTION_MENUPIC = 104, // 
    
    /**
     * @type 110
     * @description Shows menu button, the format with hyperlink.
     * @params `text` E.g: http://google.com/
     */
    ACTION_MENUBUTTON = 110, // 

    
    /**
     * @type 111
     * @description Shows menu list item. %iter vars will be filled with players data after executing the task.
     * @params `task_id` E.g.: 100090 `iter` E.g.: %iter_var_data1 `text` E.g: Write\~your\~name:
     */
    ACTION_MENULISTPART = 111, //
    
    /**
     * @type 120
     * @description Create dialog and show it to player.
     * @params
     */
    ACTION_MENUCREATE = 120,
    
    ////ACTION_RAND
    /// <summary>
    ///     <para>
    ///         type: 121
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: Detecta rateio de um random
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         param: data1[vezes] data2[em]
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         Ex.: 10 100 [estes parâmetros dizem que 1 em 10 chances retornará verdadeiro]
    ///     </para>
    /// </summary>
    ACTION_RAND = 121,
    
    ////ACTION_RANDACTION
    /// <summary>
    ///     <para>
    ///         type: 122
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: Escolhe uma action dentre um total de 8
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         param: action0 action1 action2 action3 action4 action5 action6 action7
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         Ex.: 1001 1002 1020 1026 1018 2090 3094 8392
    ///     </para>
    /// </summary>
    ACTION_RANDACTION = 122,
    
    ////ACTION_CHKTIME
    /// <summary>
    ///     <para>
    ///         type: 123,
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: Checa o tempo atual do servidor,
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         data:
    ///         <para>0 - checa data completa com param no formato              Y-M-D h:m Y-M-D h:m,</para>
    ///         <para>1 - checa hora de um dia no mes com param no formato      M-D h:m M-D h:m,</para>
    ///         <para>2 - checa hora de um dia da semana com param no formato   D h:m D h:m,</para>
    ///         <para>3 - checa hora de um dia do mes com param no formato      D h:m D h:m,</para>
    ///         <para>4 - checa hora exata com param no formato                 h:m D h:m,</para>
    ///         <para>5 - checa minutos da hora com param no formato            m m,</para>
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         Ex.: data: 0, param: 2015-10-22 11:04 2015-10-23 11:04
    ///     </para>
    /// </summary>
    ACTION_CHKTIME = 123, // 5 - check the hours "% d % d "(the first few minutes of each hour in the end a few minutes)
    
    ACTION_POSTCMD = 124, // interface to the client to send commands, data for the order number
    ACTION_BROCASTMSG = 125, // full-text news broadcast servers, data for the channel, para content
    
    /**
     * @type 126
     * @description Send a message box with Yes or No options to the player.
     * @data The action id to be executed in case of "Yes" be choose.
     * @params `text` E.g.: "Do you want to participate now?"
     */
    ACTION_MESSAGEBOX = 126,

    //      ############################################################# ATENÇÂO ###############################################              
    // ACTION_MESSAGEBOX = 126 não é essa definição apesar de ser bem interessante esse esquema. A type 126 é como a 1071 de temporização!
    // o que faz a diferença é a 1071 é executada por um player já a 126 é executada pelo sistema.
    //         ############################################################# ATENÇÂO ###############################################   ############################################################# ATENÇÂO ###############################################  

    ////ACTION_SQL_EXE
    /// <summary>
    ///     <para>
    ///         type: 127
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: Executa uma query SQL
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         param: query[sql]
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         Ex.: INSERT INTO cq_log set user_id="%user_id",task_id="5375",profession="%user_pro",level="%user_lev",time="%iter_time"
    ///     </para>
    /// </summary>
    ACTION_SQL_EXE = 127,
    
    ACTION_SYSMESSAGE = 131, // send a system messsage loaded when server start
    TIMER_DATE = 132, // used to execute an action when date is exactly as parameter
    ////ACTION_SYS_LIMIT
    /// <summary>
    ///     <para>
    ///         type: 199
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_SYS_LIMIT = 199,
    
    
    //#################################################################################################//
    ////ACTION_NPC
    ////ACTION_NPC_FIRST
    /// <summary>
    ///     <para>
    ///         type: 200
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_NPC_FIRST = 200,
    
    ////ACTION_NPC_ATTR
    /// <summary>
    ///     <para>
    ///         type: 201,
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: Testa ou modifica propriedades de um NPC. Mínimo 3 parâmetros.
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         data: id do npc
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         param: atributo[lookface, type, task_0, max_life, life, name] 
    ///         operacao[set, -=, +=, &lt;, &gt;, ==] valor[números ou texto]
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         Ex.: data: 1402, param: lookface set 10020
    ///     </para>
    /// </summary>
    ACTION_NPC_ATTR = 201,
    
    // ACTION_NPC_REQUESTSHIFT = 202, // inform the client of a pan NPC. param = "idNextTask".
    // ACTION_NPC_SHIFT = 203, // shift the current NPC. Limited mobility with the map. Limited dynamic NPC.
    ACTION_NPC_ERASE = 205, // delete the current NPC. Limited dynamic NPC. Note: deleted, not to operate this NPC. dwData not to 0, said to delete all of this map type for dwData the NPC. param = "idMap type": the deletion of certain designated map NPC.
    ACTION_NPC_MODIFY = 206, // cross-thread modify the attributes of the tasks assigned to NPC. "npc_id attr opt data". attr choose "lookface "(=)," data ?"(=)," datastr" (=)
    ACTION_NPC_RESETSYNOWNER = 207, // reset gangs map owner. Gang signs only for NPC. Statistics gangs fill the first record OWNER_ID, at the same time to remove all record. Automatically stop all attacks. (Not to suspend the fighting map logo)
    ACTION_NPC_FIND_NEXT_TABLE = 208, // Find a list of items, will ID write TASK_ITERATOR. Only with a list of the NPC. param = "type", corresponding to the type field cq_table.
    ACTION_NPC_ADD_TABLE = 209, // in the list to add one, type and the same will be idKey pre-delete (idKey for 0:00, do not delete). Only with a list of the NPC. param = "type idKey data0 data1 data2 data3 szData", at least two parameters.
    ACTION_NPC_DEL_TABLE = 210, // delete from the list of all eligible, non-existent also return true. Only with a list of the NPC. param = "type idKey data0 data1 data2 data3 szData", at least two parameters to 0 that does not match, all is not to 0 with a list of exactly the same will delete. No param, deleted the current record (iterator designated record), always return true.
    ACTION_NPC_DEL_INVALID = 211, // from the list to remove all expired items, non-existent also return true. Only with a list of the NPC. param = "type idx", idx that date (% date_stamp) in which data stored in. [For example, that the date of idx 3 stored in data3, all data3 the date the item is less than today's date will be deleted. ]
    ACTION_NPC_TABLE_AMOUNT = 212, // check list of items,> = data return false, <data return true. param meaningless.
    ACTION_NPC_SYS_AUCTION = 213, // LW let NPC system auction started, DATA for the NPC's ID, param officially began for the system prompts
    ACTION_NPC_DRESS_SYNCLOTHING = 214, //´©°ïto send clothing
    ACTION_NPC_TAKEOFF_SYNCLOTHING = 215, // off gang clothing
    ACTION_NPC_AUCTIONING = 216, // determine whether there are auction items DATA for the NPC's ID, PARAM: type: 0. That view the system auction items, 1. That the players view the items
    ////ACTION_NPC_LIMIT
    /// <summary>
    ///     <para>
    ///         type: 299
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_NPC_LIMIT = 299,
    
    
    //#################################################################################################//
    ////ACTION_MAP
    ////ACTION_MAP_FIRST
    /// <summary>
    ///     <para>
    ///         type: 300
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_MAP_FIRST = 300,
    
    ACTION_MAP_MOVENPC = 301, // the npc move to the designated map, location (only for fixed NPC), data for a specific npc's ID, param for "idMap nPosX nPosY". Note: to move to the map (0,0) coordinates, in order to hide the NPC.
    ACTION_MAP_MAPUSER = 302, // determine the map of the designated number of users, data for the specified map ID, param for "cmd opt data",
    // Cmd support "map_user" and "alive_user", opt for "==, <=,> =", data for the number of
    ACTION_MAP_BROCASTMSG = 303, // radio news, data for the map id, szParam for broadcasting news
    ACTION_MAP_DROPITEM = 304, // map have designated items, szParam for "idItemType idMap nPosX nPosY"
    ACTION_MAP_SETSTATUS = 305, // set map, and support EVENT. param = "mapid status_bit data", status_bit = (STATUS_WAR = 1,), data = 0 or 1.
    ACTION_MAP_ATTRIB = 306, // check, modify the properties of the map. param = "field opt data idMap", at least three parameters, the default for the current map. field = "synid" (opt ="==","=")¡£ field = "status" (opt = "test", "set", "reset"). field = "type" (opt = "test"). field = "res_lev" (opt ="=","==","<")¡£ field = "mapdoc" (opt ="=","=="), portal0_x (=), portal0_y (=), field = "castle" (opt ="==")
    ACTION_MAP_REGION_MONSTER = 307, // check the map or the current map designation of a number of the region monster. param = "map_id region_x region_y region_cx region_cy monster_type opt data". 0:00 said map_id check for the current map, monster_type of 0 indicated that he did not check the type, opt to support the "==" and "<."
    /// <summary>
    /// Tests the distance of the NPC in relation to player.
    /// </summary>
    ACTION_MAP_DISTANCE_NPC = 308,
    ACTION_MAP_CHANGEWEATHER = 310, // modify where players REGION weather. param = "Type Intensity Dir Color KeepSecs", Type, Intensity = 0 ~ 999, Dir = 0 ~ 359, Color = 0x00RRGGBB, KeepSecs = seconds
    ACTION_MAP_CHANGELIGHT = 311, // modify the brightness of players map. param = "idmap light secs", light = 0xAARRGGBB (0xFFFFFFFF, said the restoration), secs to 0: that a permanent change
    ACTION_MAP_MAPEFFECT = 312, // in the designated map shows the location map of the designated special effects, param = "idMap xy EffectName"
    ACTION_MAP_CREATEMAP = 313, // create a map link to the current NPC's (npc must LINK_NPC), the needs of the target audience. param = "name owner_type owner_id mapdoc type portal_x portal_y reborn_map reborn_portal res_lev". partal refers to the point of entry coordinates, res_lev that map hierarchy (for upgrade).
    ACTION_MAP_FIREWORKS = 314, // put fireworks
    ////ACTION_MAP_LIMIT
    /// <summary>
    ///     <para>
    ///         type: 399
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_MAP_LIMIT = 399,
    
    
    //#################################################################################################//
    ////ACTION_ITEMONLY // Furniture
    ////ACTION_ITEMONLY_FIRST
    /// <summary>
    ///     <para>
    ///         type: 400
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_ITEMONLY_FIRST = 400,
    
    ACTION_ITEMONLY_REQUESTLAYNPC = 401, // notify the client to place a NPC. param = "idNextTask type sort lookface region", at least four parameters. region of the type that cq_region
    ACTION_ITEMONLY_COUNTNPC = 402, // check with the map of the NPC number. param = "field data opt num", field = "name" (by name), "type" (by type), "all" (all NPC), "furniture" (furniture), data = to the name or the type of statistics (all and furniture fill 0), opt ="<","=="¡£
    ACTION_ITEMONLY_LAYNPC = 403, // create a NPC, creating successful, the NPC is the immediate task of NPC, owner_id will be automatically set to gang ID or player ID. param = "name type sort lookface ownertype life region base linkid task0 task0 ... task7 data0 data1 data2 data3 datastr". At least five parameters. Data3 on target in the hierarchy.
    ACTION_ITEMONLY_DELTHIS = 498, // delete the current task items. Note: the need for the last ACTION.
    ////ACTION_ITEMONLY_LIMIT
    /// <summary>
    ///     <para>
    ///         type: 499
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_ITEMONLY_LIMIT = 499,
    
    
    //#################################################################################################//
    ////ACTION_ITEM
    ////ACTION_ITEM_FIRST
    /// <summary>
    ///     <para>
    ///         type: 500
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_ITEM_FIRST = 500,
    
    ACTION_ITEM_ADD = 501, // Add items. data = itemtype_id, param = "amount amount_limit ident gem1 gem2 magic1 magic2 magic3 data warghostexp gemtype availabletime", param can be omitted, all the default value is 0 (that is not revised)
    ACTION_ITEM_DEL = 502, // delete items. data = itemtype_id, param not 0:00, items can be superimposed at the same time to delete a number. Or data for 0, param said you want to delete the items were.
    ACTION_ITEM_CHECK = 503, // Detection items. data = itemtype_id, param not be 0:00, while the number of items to check (or durability), the goods must meet the requirements of the number (or durability) do. Or data for 0, param items that were looking for.
    ACTION_ITEM_HOLE = 504, // weapons holes. param support "ChkHole HoleNum" or "MakeHole HoleNum", Num for 1 or 2
    ACTION_ITEM_REPAIR = 505, // equipmesnt repair. data for the specified location of equipment.
    ACTION_ITEM_MULTIDEL = 506, // delete multiple items, param for "idType0 idType1 num", that is, delete num months idType0-idType1 items.
    ACTION_ITEM_MULTICHK = 507, // detection of a variety of items, param for "idType0 idType1 num", that is, detection num months idType0-idType1 items.
    ACTION_ITEM_LEAVESPACE = 508, // check the remaining space backpack. param = "space weight packtype"
    // Which packtype range of 50 ~ 53
    // 50: ordinary items backpack
    // 51:Ä§»êjewel backpack
    // 52: imaginary animals eggs backpack
    // 53: imaginary animals backpack

    ACTION_ITEM_UPEQUIPMENT = 509, // equipment operation, param format "cmd position",
    // Cmd support "up_lev", "up_quality", "recover_dur"
    // Position for equipment location, defined as follows
    /* ITEMPOSITION_HELMET = 1;
    ITEMPOSITION_NECKLACE = 2;
    ITEMPOSITION_ARMOR = 3;
    ITEMPOSITION_WEAPONR = 4;
    ITEMPOSITION_WEAPONL = 5;
    ITEMPOSITION_RINGR = 6;
    ITEMPOSITION_RINGL = 7;
    ITEMPOSITION_SHOES = 8;
    ITEMPOSITION_MOUNT = 9 
    12 = for mounts ( its not in 5065 0r 5095 the position is there but u cant use it)
    11 = tower
    10 = fan
    1 = headgear
    2 = necklace
    3 = armor
    4 = weapon
    5 = shield
    6 = ring
    7 = talismans
    8 = boots
    9 = Garments */

    ACTION_ITEM_EQUIPTEST = 510, // goods quality inspection,
    // Param "equip_pos cmd opt num",
    // Equip_pos Ibid position definition
    // Cmd support "level", "quality", "durability", "max_dur"
    // Opt to support the "==,> =," = ",
    // Num data, cmd for "durability" and "max_dur" when -1 for the maximum
    ACTION_ITEM_EQUIPEXIST = 511, // the existence of test equipment, data for the equipment location
    ACTION_ITEM_EQUIPCOLOR = 512, // equipment change color, param = "equip_pos color", equip_pos support is as follows
    /* ITEMPOSITION_HELMET = 1;
    ITEMPOSITION_ARMOR = 3;
    ITEMPOSITION_WEAPONL = 5; */
    // / ITEMPOSITION_WEAPONL must only work as a shield
    ACTION_ITEM_FIND = 513, // Find an item, type the existence of user in the iterator. data = itemtype_id. Or data for 0, param that people want to find items.
    ACTION_ENCASH_CHIP = 514, // use of chips for cash, the amount of money in the Item in the Data field
    ACTION_ITEM_EQP_SELECT = 516,
    ACTION_ITEM_EQP_OPT = 517,

    ACTION_ITEM_APPEARANCE = 518, // Set any item as appearance only over any position.


    ACTION_ITEM_ADD_KILL_MST_NUM = 528, // &#8203;?add items (like with the 501, designed to support the running ring Shaguai goods, data = itemid param = "amount amount_limit ident gem1 gem2 magic1 magic2 magic3 data", param may be omitted, the default value is 0 for all (that does not change) 
                                                                                // Parameter which needs Shaguai amount that the number of players; amount_limit monster stc_type corresponding field values; data for the number of players killing monsters, as long as the default to 0 
    ACTION_ITEM_CHK_KILL_MST_NUM = 529, // &#8203;?determine whether the number of items in compliance with killing monsters. If you delete the corresponding items that meet the conditions 
    // Param: "mst_sort data" mst_sort = monster sort data = number of that type if you kill mst_sort strange> = data, returns true, otherwise return false 
    ////ACTION_ITEM_LIMIT
    /// <summary>
    ///     <para>
    ///         type: 599
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_ITEM_LIMIT = 599,
    
    
    //#################################################################################################//
    ////ACTION_NPCONLY // Pets
    ////ACTION_NPCONLY_FIRST
    /// <summary>
    ///     <para>
    ///         type: 600
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_NPCONLY_FIRST = 600,
    
    ACTION_NPCONLY_CREATENEW_PET = 601, // create a MONSTER, OWNERID, OWNERTYPE the same with the NPC. param = "xy generator_id type data name", at least four parameters, if name is changed. monster generator used to control the scope of activities, cq_generator the type meaningless. x, y coordinates of the map's absolute.
    ACTION_NPCONLY_DELETE_PET = 602, // Delete the map all the MONSTER, OWNERID, OWNERTYPE the same with the NPC. param = "type data name", at least one parameter, data does not match the 0 at the same time data, if any name while at the same time matching names.
    ACTION_NPCONLY_MAGICEFFECT = 603, // NPC issued a magic effect. param = "source_id magic_type magic_level target_id data"
    ACTION_NPCONLY_MAGICEFFECT2 = 604, // NPC issued by one magic effect. param = "source_id magic_type magic_level xy target_id data", at least five parameters.
    ////ACTION_NPCONLY_LIMIT
    /// <summary>
    ///     <para>
    ///         type: 699
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_NPCONLY_LIMIT = 699,
    
    
    //#################################################################################################//
    ////ACTION_SYN
    ////ACTION_SYN_FIRST
    /// <summary>
    ///     <para>
    ///         type: 700
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_SYN_FIRST = 700,
    
    ////////////////////////////////////////////////////////
    // Gangs collate Action
    ACTION_SYN_CREATE = 701, // a help, the player must enter the names of gangs. param = "level money leave_money", the three parameters that need grading players need to cash the number of gangs in cash after the establishment of the remaining few.
    ACTION_SYN_DESTROY = 702, // dissolution. The implementation of Action for the head of the players, sub-head when the sub-captain, respectively, said the dissolution of gangs, sub-groups, detachments
    ACTION_SYN_DONATE = 703, // contributions, the need for an input box.
    ACTION_SYN_CREATE_SUB = 708, // a sub-help (Legion long to implement is to create sub-groups, sub-head of the implementation is to create units),
    // Players to enter the gang name (the length of not more than 16BYTE).
    ACTION_SYN_COMBINE_SUB = 710, // merge sub-gangs. Executive Action is the son of the players gang°ïÖ÷, merged into the parent gang
    ACTION_SYN_ATTR = 717, // check and modify the attributes gangs, parameter is not less than 3, the default is the current player ID gang gang ID.
    // Param = "szField szOpt data syn_id", szField Optional:
    // Fund: "money" (opt optional "+=", "<"),
    // Prestige: "repute" (opt optional "+=", "<"),
    // Number: "membernum" (opt for "<"),
    // Father of gang: "fealty" (opt for "=="),
    // Grade: "level" (opt optional "=", "+=", "<", "==")
    ACTION_SYN_ALLOCATE_SYNFUND = 729, // distribution gangs Fund. Players need a specific amount of data (up to no more than 50 percent of total funds)
    ACTION_SYN_RENAME = 731, // rename gangs. Must be sub-gangs, gangs from sub-°ïÖ÷implementation
    ////////////////////////////////////////////////////////

    ACTION_SYN_DEMISE = 704, // shanrang, allowing only a long Legion shanrang, sub-head and sub-captain are not allowed.
    // Players to enter Bangzhong name. param = "level", said players need to accept the demise of the hierarchy
    ACTION_SYN_SET_ASSISTANT = 705, // promoted to vice°ïÖ÷, players have to enter Bangzhong name.
    ACTION_SYN_CLEAR_RANK = 706, // relieved of his duties, the player must enter Bangzhong name.
    ACTION_SYN_PRESENT_MONEY = 707, //ËÍÇ®¸øother gangs. The main input of money to help the number of the ID for other gang TASK_ITERATOR (see ACTION_SYN_FIND_BY_NAME). Money should not be less than 10000
    ACTION_SYN_CHANGE_LEADER = 709, // update the sub-gangs°ïÖ÷.°ïÖ÷and sub-gang new°ïÖ÷team, enter the name of sub-gangs. param = level, the requirements of the new grading°ïÖ÷
    ACTION_SYN_ANTAGONIZE = 711, // enemies, players must enter the names of gangs.
    ACTION_SYN_CLEAR_ANTAGONIZE = 712, // clear the enemies, players must enter the names of gangs.
    ACTION_SYN_ALLY = 713, // alliance, to ask the two team°ïÖ÷
    ACTION_SYN_CLEAR_ALLY = 714, // lift the alliance, the player must enter the names of gangs.
    ACTION_SYN_KICKOUT_MEMBER = 715, // by name expelled Bangzhong, players have to enter Bangzhong name.
    ACTION_SYN_CREATENEW_PET = 716, // (void) to create a gang to protect animals. param = "generator_id type data", at least two parameters, if any accept the name. monster generator used to control the scope of activities, cq_generator the type meaningless.
    ACTION_SYN_CHANGESYN = 718, // Bangzhong lesson mouth. Church mouth to mouth Church, Church to help the mouth and the total conversion. Bangzhong andÌÃÖ÷need (or°ïÖ÷) teams, one to one person. Need to enter into the church I want to name (or gang name). Prior to the past, jobs would be automatically canceled.
    ACTION_SYN_CHANGE_SUBNAME = 719, // modify parishes were limited to the names of more than 6 bytes of parishes. Otherwise return FALSE. (Provisional function)

    ACTION_SYN_FIND_NEXT_SYN = 720, // Find next gangs will ID write TASK_ITERATOR
    ACTION_SYN_FIND_BY_NAME = 721, // by name to find gangs, the names of players to enter the gang. ID will be written into the TASK_ITERATOR
    ACTION_SYN_FIND_NEXT_SYNMEMBER = 722, // Find next Bangzhong will ID write TASK_ITERATOR
    ACTION_SYN_SAINT = 724, // St. Knights of the escalation of the operation "=,> ="
    ACTION_SYN_RANK = 726, // modify RANK, ACCEPT = "rank name". Only modify RANK = 50 and below. param = "RANK50 the level of restrictions RANK40 30 of 20 10", param is empty is not restricted.

    ACTION_SYN_UPMEMBERLEVEL = 728,
    ACTION_SYN_APPLLY_ATTACKSYN = 730, // application to attack gang

    ////ACTION_SYN_LIMIT
    /// <summary>
    ///     <para>
    ///         type: 799
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_SYN_LIMIT = 799,
    
    
    //#################################################################################################//
    ////ACTION_MST
    ////ACTION_MST_FIRST
    /// <summary>
    ///     <para>
    ///         type: 800
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_MST_FIRST = 800,
    
    ACTION_MST_DROPITEM = 801, // monster killed off after the death of goods or money, param "dropitem itemtype" or "dropmoney money"
    // monster killed off the trap of death, param "droptrap traptype lifeperiod".
    ACTION_MST_MAGIC = 802, // check magic.
    // Param "check type" (studied type types of magic),
    // "Check type level" (studied type types of magic, and the rating for a level-class),
    // "Learn type" (Institute of type-type magic, grade for 0),
    // "Uplevel type" (type 1 or type of magic)
    ACTION_MST_SAY = 803,
    ////ACTION_MST_LIMIT
    /// <summary>
    ///     <para>
    ///         type: 899
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_MST_LIMIT = 899,
    
    
    //#################################################################################################//
    ////ACTION_USER
    ////ACTION_USER_FIRST
    /// <summary>
    ///     <para>
    ///         type: 1000,
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: Início das actions de player
    ///     </para>
    ACTION_USER_FIRST = 1000,
    
    ////ACTION_USER_ATTR
    /// <summary>
    ///     <para>
    ///         type: 1001,
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: Testa ou define atributo do player.
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         param: atributo[life, mana, money, exp, pk, profession, level, force, dexterity, speed, health, soul, rank,
    ///         rankshow, iterator, crime, gamecard, gamecard2, xp, metempsychosis, nobility_rank, mercenary_rank, mercenary_exp,
    ///         tutor_exp, tutor_level, syn_proffer, maxeudemon]
    ///         operacao[set,+=,==,&lt;]
    ///         valor[números ou texto]
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         Ex.: profession == 15
    ///     </para>
    /// </summary>
    ACTION_USER_ATTR = 1001,
    

    ACTION_USER_FULL = 1002, // will fill a player's attributes. "attr". attr optional "life", "mana"
    ACTION_USER_CHGMAP = 1003, // cut map param "idMap nPosX nPosY bPrisonChk", bPrisonChk for optional parameters, default can not be a prison, is set to 1 can be a
    ACTION_USER_RECORDPOINT = 1004, // record-keeping point param "idMap nMapX nMapY"
    ACTION_USER_HAIR = 1005, // "color num"
    // "Style num"
    ACTION_USER_CHGMAPRECORD = 1006, // cut map to record points
    ACTION_USER_CHGLINKMAP = 1007, // cut to the NPC link map map. The need for NPC object.
    ACTION_USER_CHKPURECLASS = 1008, // verifies if character is as pure class, param "class".
    /// <summary>
    /// Gives player a buffer.
    /// <list type="Buffers">
    /// Buffers: 
    /// atk (Attack), def (Defense), exp (MultipleExp), etc.
    /// </list>
    /// </summary>
    ACTION_USER_BUFFER = 1009,
    ACTION_USER_TALK = 1010, // message to the players MSGTALK fat. param for the news content, data for the channel,
    // Const unsigned short _TXTATR_NORMAL = 2000;
    // Const unsigned short _TXTATR_ACTION = _TXTATR_NORMAL +2; // action
    // Const unsigned short _TXTATR_SYSTEM = _TXTATR_NORMAL +5; // system
    // Const unsigned short _TXTATR_TALK = _TXTATR_NORMAL +7; // chat
    // Const unsigned short _TXTATR_GM = _TXTATR_NORMAL +11; // GM Channel
    // Const unsigned short _TXTATR_WEBPAGE = _TXTATR_NORMAL +105; // Open URL
    ACTION_USER_MAGIC = 1020, // check magic. param can be as follows:
    // "Check type" (players learned type types of magic),
    // "Check type level" (players learned type types of magic, and the rating for a level-class),
    // "Learn type" (players learn to type type magic, grade for 0),
    // "Uplevel type" (a player's type 1 or type of magic)
    // "Addexp type exp" (a player's type type magic experience points to increase exp)
    ACTION_USER_WEAPONSKILL = 1021, // "check type level", check the weapons of the type and level of skills, whether or not> = grade
    // "Learn type level", specified the type and level of learning skills
    ACTION_USER_LOG = 1022, // Save the specified information to trigger gm log and into the Information (name and id), the information specified in the param
    // For example, "% s to complete the task and gemstones Sky Sword", param in% s is the preservation of the location of the trigger Information
    ACTION_USER_BONUS = 1023, // get a prize.
    ACTION_USER_DIVORCE = 1024, // divorce
    ACTION_USER_MARRIAGE = 1025, // marriage inspection, married to return to 1, unmarried return 0
    ACTION_USER_SEX = 1026, // sex check, M to return to 1, women return 0
    ACTION_USER_EFFECT = 1027, // trigger action figures specified additional effects, param to "opt effect", opt to support the "self", "couple", "team", "target", effect to effect the name of
    ACTION_USER_TASKMASK = 1028, // task mask related to the operation, param to "opt idx", opt for the operation, support for "chk", "add", "clr", idx mission number value (0-31)
    ACTION_USER_MEDIAPLAY = 1029, // media player, param to "opt media", opt to support the "play, broacast", "media" for the media file name
    ACTION_USER_SUPERMANLIST = 1030, // query unique list, start value in the existence of TASK_ITERATOR. param = "idNextTask number", idNextTask next TASK value, number is the number of each list item downlink.
    ACTION_USER_CHKIN_CARD = 1031, // delete the players onto the game card items, add a game card records
    ACTION_USER_CHKOUT_CARD = 1032, // add an item to the card game players, game cards to delete a record
    ACTION_USER_CREATEMAP = 1033, // create a map link to home_id players, the needs of the target audience. param = "name owner_type owner_id mapdoc type portal_x portal_y reborn_map reborn_portal res_lev". partal refers to the point of entry coordinates, res_lev that map hierarchy (for upgrade).
    ACTION_USER_ENTER_HOME = 1034, // return to their home.
    ACTION_USER_ENTER_MATE_HOME = 1035, // back to their spouses home.
    ACTION_USER_CHKIN_CARD2 = 1036, // delete the players onto the game card 2 items, add a game card 2 records
    ACTION_USER_CHKOUT_CARD2 = 1037, // add a game card 2 items to the players, to delete a game card 2 records
    ACTION_USER_FLY_NEIGHBOR = 1038, // on the map to find a group _ROLE_NEIGHBOR_DOOR types of NPC, immediately cut screen to the NPC Office. param = "serial", serial refers data3 value.
    ACTION_USER_UNLEARN_MAGIC = 1039, // reincarnation, the forgotten magic skills, the skills of the future could be "epiphany." param = "type1 type2 ...", at least one parameter, a maximum of 20 parameters.

    ////ACTION_USER_REBIRTH
    /// <summary>
    ///     <para>
    ///         type: 1040,
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: Reincarna o personagem.
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         param: profissão[11,21,41,132,142] corpo[1,2,3,4]
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         Ex.: 21 3
    ///     </para>
    /// </summary>
    ACTION_USER_REBIRTH = 1040,
    
    ACTION_USER_WEBPAGE = 1041, // notify the client to open the page. param = "http:// ....."
    ACTION_USER_BBS = 1042, // in the BBS bulletin boards, add a SYSTEM news channel, message the name of human players. The need for USER objects, retaining only one of each USER. param is the message.
    ACTION_USER_UNLEARN_SKILL = 1043, // reincarnation, the forgotten all weapons skills, the skills of the future could be "epiphany."
    ACTION_USER_DROP_MAGIC = 1044, // reincarnation, deleted magic skills. param = "type1 type2 ...", at least one parameter, a maximum of 20 parameters.
    ACTION_USER_OPEN_DIALOG = 1046, // notify the client to open an interface. data = idDialog. param = "task_id0 task_id1 task_id2 task_id3 ...", can no param, a maximum of 20 task_id, task_id not to 0, allowing the client to choose the next TASK. Non-param when the client can only upload "the client can trigger the TASK". Param when there is, cq_task.client_active must be to 0.
    ACTION_USER_CHGMAP_REBORN = 1047, // reallot attributes
    ACTION_USER_ADD_WPG_BADGE = 1048, // add PK Cup keepsake weeks, according to found the first thing to add token types. The items must be superimposed. A maximum of only two.
    ACTION_USER_DEL_WPG_BADGE = 1049, // delete all week keepsake PK tournament.
    ACTION_USER_CHK_WPG_BADGE = 1050, // check that there is only one player who param types of goods (the number can only have one), no other week PK tournament keepsake. param is empty that it can not have any weeks PK Cup keepsake.
    ACTION_USER_TAKESTUDENTEXP = 1051, // extract contributions apprentice experience. PszAccept specified the need for players to return to the experience of extracting value, automatically deducted from the experience of instructors.

    ACTION_USER_CHGTO_MAINMAP = 1053, // to the main point of the revival of the revival of the map
    ACTION_USER_CHGTO_RANDOMPOS = 1053, // characters randomly fly any of the current map coordinates (the points can not mask)
    
    /// <summary>
    /// Sets player's heaven blessing time.
    /// </summary>
    ACTION_USER_HEAVEN_BLESSING = 1055, 

    ACTION_USER_LOCK_CHK = 1056, // &#8203;?check whether the user returns the current value consistent LOCK, if the current value is 0, is always successful; 
    ACTION_USER_LOCK_SET = 1057, // &#8203;?set the user back to the LOCK value, 0 to remove 

    ACTION_USER_DATAVAR_CHK = 1060, // &#8203;?detection of a register variable, param = "var (x) opt data", x support (0-7), opt to support ">,> =, ==", data as integer values. 
    ACTION_USER_DATAVAR_SET = 1061, // &#8203;?set a register variable, param = "var (x) set data", x support (0-7), data as integer values. 

    ACTION_USER_STRVAR_CHK = 1062, // &#8203;?detection of a register variable, param = "var (x) opt string", x support (0-7), opt to support "==", string to string 
    ACTION_USER_STRVAR_SET = 1063, // &#8203;?set a register variable, param = "var (x) set string", x support (0-7), string to string 

    ACTION_USER_DATAVAR_CAL = 1064, // &#8203;?register variable calculations, param = "var (x) opt data", x support (0-7), 

    ACTION_USER_EQPCHK = 1065, // &#8203;?check player equipment, and equipment to determine the location of a player's equipment is of a certain type. param = "pos item_subtype", 
                                // Pos equipment for the players position (1-8), item_subtype equipment items for the sub-type (type of one hundred thousand, ten thousand, thousand)
    ////ACTION_EXECUTE_ACTION
    /// <summary>
    ///     <para>
    ///         type: 1071,
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: Executa uma action depois de x milisegundos
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         data: id da action
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         param: tempo[em milisegundos]
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         Ex.: data: 140200, param: 5000 [executa a action 140200 em 5 segundos]
    ///     </para>
    /// </summary>
    ACTION_USER_EXECUTE_ACTION = 1071,
    
    // Opt to support "+=", "-=", "*=", "/=", "mod =", "|=", "&=", "^=", data to an integer value. 
    ACTION_USER_SET_STC = 1074, // param 'stc(event_type,data_type) = data data'
    ACTION_USER_CHK_STC = 1073, // param 'stc(event_type,data_type) [==, >=] data'
    
    ACTION_USER_DB_FIELD = 1077, // &#8203;?the specified sql statement to retrieve a specified data field values ??into a string variable to the specified register. 
                                 // Data variable for the register index, param sql statement for the 

    ACTION_USER_QUEST_SET_STC = 1078, // data = Quest ID; param = 'intention, kills, flag or flag2' 'value'
    ACTION_USER_QUEST_CHK_STC = 1079, // data = Quest ID; param = 'intention, kills, flag or flag2' >=,>,<,<=,== 'value'
    /// <summary>
    /// Adds data to the quest; param = Quest_ID data;
    /// </summary>
    ACTION_USER_QUEST_ADD_STC = 1084, 

    ACTION_USER_TASK_MANAGER = 1080, //timestamp(e_type, d_type) set timestamp
    /////////////////////////////////////////////// //
    // // Data: the task number
    // Param: 'new' (to create a new record)
    // 'Delete' (delete records)
    // 'Isexit' (the existence of the task)
    /////////////////////////////////////////////////

    ACTION_USER_TASK_OPE = 1081, //interval(e_type, d_type, 1) opt timestamp
    // data: the task number, if data == -1, then the following operation is carried out against FindNext
    // Param: 'ope opt data', data (value)
    // Ope (phase) opt (> =, ==, +=,=) mission operation phase
    // Ope (completenum) opt (> =, ==, +=,=) task is completed on the number of operations
    // Ope (begintime) opt (> =, ==, +=,=, reset) start time of the mission to operate, for + = time in seconds parameter; for when ">=,==,=" "yyyy -mm-dd hh: mm: ss "for the format
    // Reset the start of the mandate that it will set-up time for the current time
    ACTION_USER_TASK_LOCALTIME = 1082, /////////////////////////////////////////////////////////
    // Data: the task number
    // param: 'seconds', the current time with the task start time comparison action; if the current time with the task start time is greater than the difference between the param, then return true. Otherwise, return false
    //////////////////////////////////////////////////////////////////////

    ACTION_USER_TASK_FIND = 1083, // for gamers tasks inquiries, records are in accordance with the userid, taskid ascending collection
                                  // param: 'find taskid phase completenum'; in accordance with the task ID, phase, the completion of a specific record number of inquiries; phase with cocompletenum at the same time -1, the only record of inquiry in line with the taskid
                                  // 'Findnext'; inquiries under a record
                                  //--- Mandate system to record the details of the task --- end


    // Team part. ¡ï no team will return false. The following ACTION must be triggered by the captain,
    // Operator for each team (usually does not include the captain), team members must be in the framework of a screen.
    // NOTE: All team members must be true are only return true; otherwise return false
    // ¡ï ----------------------------------------------- ----------------

    ////ACTION_USER_CONTRIBUTE
    /// <summary>
    ///     <para>
    ///         type: 1090
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         data: no data.
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: Nenhum parametro. Ao executar esta action, o sistema transforma os RoyalPoints Acumulados em contribuição no ranking de nobreza.
    ///     </para>
    /// </summary>
    ACTION_USER_CONTRIBUTE = 1090,
    
    ////ACTION_USER_PATHFINDING
    /// <summary>
    ///     <para>
    ///         type: 1096
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: Leva o Player para o destino automaticamente,
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         param: mapid[id do mapa destino] posx[posicao x do destino] posy[posicao y do destino] 
    ///         npcid[opcional; id do npc a ser invocado apos encontrar destino.
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         Ex.: 1002 296 288 88868
    ///     </para>
    /// </summary>
    ACTION_USER_PATHFINDING = 1096,
    
    ////ACTION_USER_REALIZE
    /// <summary>
    ///     <para>
    ///         type: 1097
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: Prepara o Player para realizar alguma ação.
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         data: n# action a ser executada após realização 
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         param: texto[pode ser branco; sem espaços, deve-se usar "~" no lugar] tempo[em segundos; até o final da realização] 
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         Ex.: data: 209520, param: Procurando... 5
    ///     </para>
    /// </summary>
    ACTION_USER_REALIZE = 1097,
    
    ////ACTION_USER_LIMIT
    /// <summary>
    ///     <para>
    ///         type: 1099
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_USER_LIMIT = 1099,
    
    
    //#################################################################################################//
    ////ACTION_TEAM
    ////ACTION_TEAM_FIRST
    /// <summary>
    ///     <para>
    ///         type: 1100
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_TEAM_FIRST = 1100,
    
    ACTION_TEAM_BROADCAST = 1101, // to force a message broadcast channels. param = news.
    ACTION_TEAM_ATTR = 1102, // check or operating team attributes.1
    // Param = "field opt data",
    // Field = "money "(+=,<,>,==),
    // Field = "level "(<,>,==),
    // Field = "count" (the number of players including captain ,<,==),
    // Field = "count_near" (the number of players including captain, the map must be alive ,<,==),
    // Field = "mate" (only need to field, must be alive),
    // Field = "friend" (only need to field, must be alive),
    ACTION_TEAM_LEAVESPACE = 1103, // check the remaining space backpack, param = "space weight packtype".
    // Packtype for the need to check the backpack type, range 50 ~ 53
    ACTION_TEAM_ITEM_ADD = 1104, // Add items. data = itemtype_id
    ACTION_TEAM_ITEM_DEL = 1105, // delete items. data = itemtype_id
    ACTION_TEAM_ITEM_CHECK = 1106, // Detection items. data = itemtype_id
    ACTION_TEAM_CHGMAP = 1107, // team all screens (including captain), only for a map with the group cut screen, all to be alive. param = "mapid x y"
    ACTION_TEAM_ISLEADER = 1108, // check whether the captain, no parameters
    ////ACTION_TEAM_LIMIT
    /// <summary>
    ///     <para>
    ///         type: 1199
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_TEAM_LIMIT = 1199,
    
    
    //#################################################################################################//
    ////ACTION_EVENT
    ////ACTION_EVENT_FIRST
    /// <summary>
    ///     <para>
    ///         type: 2000
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_EVENT_FIRST = 2000,
    
    ACTION_EVENT_SETSTATUS = 2001, // set up a state map. param = "mapid status_bit data", status_bit = (STATUS_WAR = 1,), data = 0 or 1.
    ACTION_EVENT_DELNPC_GENID = 2002, // (void) Delete MONSTER. param = "idMap idGen".
    ACTION_EVENT_COMPARE = 2003, // compare various attributes. "data1 opt data2". data1, data2 for belt% of the general parameters, by comparison with several symbols. optional opt "==","<","<="
    ACTION_EVENT_COMPARE_UNSIGNED = 2004, // compare various attributes. "data1 opt data2". data1, data2 for belt% of the general parameters, according to the number of unsigned comparison. optional opt "==","<","<="
    ACTION_EVENT_CHANGEWEATHER = 2005, // modify designated REGION weather. param = "idMap idRegion Type Intensity Dir Color KeepSecs", Type, Intensity = 0 ~ 999, Dir = 0 ~ 359, Color = 0x00RRGGBB, KeepSecs = seconds
    ACTION_EVENT_CREATEPET = 2006, // create a MONSTER. param = "nOwnerType idOwner idMap nPosX nPosY idGen idType nData szName", at least seven parameters, if any accept the name, otherwise named by name. monster generator used to control the scope of activities, cq_generator the type meaningless. idOwner for 0:00, do not save.
    ACTION_EVENT_CREATENEW_NPC = 2007, // create a NPC. param = "name type sort lookface ownertype ownerid mapid posx posy life base linkid task0 task0 ... task7 data0 data1 data2 data3 datastr". At least 9 parameters.
    ACTION_EVENT_COUNTMONSTER = 2008, // check with the number of maps MONSTER. param = "idMap field data opt num", field = "name" (by name), "gen_id" (by type), data = to statistics name or type, opt ="<","=="¡£
    ACTION_EVENT_DELETEMONSTER = 2009, // delete a map of the MONSTER. param = "idMap type data name", at least two parameters. If the data does not match the 0 at the same time data, if a name is at the same time matching name.
    ACTION_EVENT_BBS = 2010, // in the BBS bulletin boards, add a SYSTEM news channel, message man-made "SYSTEM". param is the message.
    ACTION_EVENT_ERASE = 2011, // delete the specified NPC. Limited dynamic NPC. Note: deleted, not to operate on such NPC. param = "idMap type": delete the specified map for the type of all types of NPC.

    ////ACTION_EVENT_INSCRIBE
    /// <summary>
    ///     <para>
    ///         type: 2021
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         data: id do evento.
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: Nenhum parametro. Ao executar esta action, o player será inscrito para a próxima rodada de um evento.
    ///     </para>
    /// </summary>
    ACTION_EVENT_INSCRIBE = 2021,
    
    ////ACTION_EVENT_UNINSCRIBE
    /// <summary>
    ///     <para>
    ///         type: 2022
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         data: id do evento.
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: Nenhum parametro. Ao executar esta action, o player será desinscrito para a próxima rodada de um evento.
    ///     </para>
    /// </summary>
    ACTION_EVENT_UNINSCRIBE = 2022,
    
    ////ACTION_EVENT_CHKINSCRIBE
    /// <summary>
    ///     <para>
    ///         type: 2023
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         data: id do evento.
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         Nenhum parametro: Ao executar esta action, o sistema checa se o player está inscrito para a próxima rodada de um evento.
    ///     </para>
    ///     <para>
    ///         Parametros [time_sec]: O sistema checa se o jogador se inscreveu para um evento há no maximo [time_sec] segundos. 
    ///     </para>
    ///     <para>
    ///         Parametros [time_sec n_players id_action]: O sistema checa se há [n_players] jogadores que se inscreveram para um evento há no maximo [time_sec] segundos e executa a action [id_action]. Retorna falso, se nao encontrar [n_players] jogadores que passem nos parametros.
    ///     </para>
    /// </summary>
    ACTION_EVENT_CHKINSCRIBE = 2023,
    
    ////ACTION_EVENT_LIMIT
    /// <summary>
    ///     <para>
    ///         type: 2099
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_EVENT_LIMIT = 2099,
    
    ////ACTION_EVENT_INIT
    /// <summary>
    ///     <para>
    ///         type: 2024
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         data: id do evento.
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         Nenhum parametro: Ao executar esta action.
    ///     </para>
    /// </summary>
    ACTION_EVENT_INIT = 2024,
    
    ////ACTION_EVENT_END
    /// <summary>
    ///     <para>
    ///         type: 2025
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         data: id do evento.
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         Nenhum parametro: Ao executar esta action.
    ///     </para>
    /// </summary>
    ACTION_EVENT_END = 2025,
    
    
    //#################################################################################################//
    ////ACTION_TRAP
    ////ACTION_TRAP_FIRST
    /// <summary>
    ///     <para>
    ///         type: 2100
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_TRAP_FIRST = 2100,
    
    ACTION_TRAP_CREATE = 2101, // create a trap. param = "type look owner_id map_id pos_x pos_y data".
    ACTION_TRAP_ERASE = 2102, // delete a trap. param = "", delete the current trap. Note: Do not delete after the operation of the trap.
    ACTION_TRAP_COUNT = 2103, // Detection of the type of trap type number, less than the count to return to true. param = "map_id pos_x pos_y pos_cx pos_cy count type".
    ACTION_TRAP_ATTR = 2104, // modify the properties of traps (not save). param = "id field opt num". field: "type" (opt: "="), "look" (opt: "=")¡£
    ////ACTION_TRAP_LIMIT
    /// <summary>
    ///     <para>
    ///         type: 2199
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_TRAP_LIMIT = 2199,
    
    
    //#################################################################################################//
    ////ACTION_WANTED
    ////ACTION_WANTED_FIRST
    /// <summary>
    ///     <para>
    ///         type: 3000
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_WANTED_FIRST = 3000,
    
    ACTION_WANTED_NEXT = 3001, // search for the next reward will be TASK_ITERATOR write idx
    ACTION_WANTED_NAME = 3002, // players to return to reward those who have been designated pszAccept name, and ACTION_WANTED_NEW GC.
    ACTION_WANTED_BONUTY = 3003, // players to return to the amount specified pszAccept, and ACTION_WANTED_NEW GC.
    ACTION_WANTED_NEW = 3004, // through CUser:: m_WantedInfo reward the production of new records, and the joint use of two action.
    ACTION_WANTED_ORDER = 3005, // receive pszAccept specified reward
    ACTION_WANTED_CANCEL = 3006, // 2 times the price of the abolition of pszAccept specified reward
    ACTION_WANTED_MODIFYID = 3007, // players to return to the specified changes pszAccept reward id.
    ACTION_WANTED_SUPERADD = 3008, // players to return to the designated pszAccept additional reward money, with the joint use of ACTION_WANTED_ID.
    ACTION_POLICEWANTED_NEXT = 3010, // search for the next official reward will idx write TASK_ITERATOR
    ACTION_POLICEWANTED_ORDER = 3011, //½Ò°ñ(pszAccept specified number)
    ACTION_POLICEWANTED_CHECK = 3012, // check whether the trigger was wanted by officials
    ////ACTION_WANTED_LIMIT
    /// <summary>
    ///     <para>
    ///         type: 3099
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_WANTED_LIMIT = 3099,
    
    
    //#################################################################################################//
    ////ACTION_MAGIC
    ////ACTION_MAGIC_FIRST
    /// <summary>
    ///     <para>
    ///         type: 4000
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_MAGIC_FIRST = 4000,
    
    ACTION_MAGIC_ATTACHSTATUS = 4001, // additional state, szParam = "status power secs times"

    ACTION_MAGIC_ATTACK = 4002, // magic attacks, data = magictype, szParam = "magiclevel"
    // Request magictype table corresponding data exist
    // Now support magic types are:
    // MAGICSORT_DETACHSTATUS
    // MAGICSORT_STEAL
    ////ACTION_MAGIC_LIMIT
    /// <summary>
    ///     <para>
    ///         type: 4099
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_MAGIC_LIMIT = 4099,
    
    
    //#################################################################################################//
    ////ACTION_ROOM
    ////ACTION_ROOM_FIRST
    /// <summary>
    ///     <para>
    ///         type: 5000
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_ROOM_FIRST = 5000,
    
    ACTION_ROOM_LIST = 5001,
    ACTION_ROOM_CREATE = 5002,
    ACTION_ROOM_QUIT = 5003,
    ACTION_ROOM_JOIN = 5004,
    ACTION_ROOM_NEXTPAGE = 5005,
    ACTION_ROOM_PREVPAGE = 5006,
    ACTION_ROOM_FIRSTPAGE = 5007,
    ACTION_ROOM_LASTPAGE = 5008,
    ACTION_ROOM_FIND = 5009,
    ACTION_ROOM_SET = 5010,
    ACTION_ROOM_FINDPAGE = 5011,
    ACTION_ROOM_START = 5012,
    ACTION_ROOM_OWNER = 5013,
    ////ACTION_ROOM_LIMIT
    /// <summary>
    ///     <para>
    ///         type: 5099
    ///     </para>
    ///     <para>&#160;</para>
    ///     <para>
    ///         description: None
    ///     </para>
    /// </summary>
    ACTION_ROOM_LIMIT = 5099,
    //#################################################################################################//

}