export enum ENobilityRank {
    Serf = 0,
    Knight = 1,
    Baron = 3,
    Earl = 5,
    Duke = 7,
    Prince = 9,
    King = 12
};