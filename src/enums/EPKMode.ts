export enum EPKMode {
    PK = 0,
    Peace = 1,
    Team = 2,
    Capture = 3,
    Revenge = 4,
    Guild = 6,
    Jiang = 7,
};