require('dotenv').config();

import './libs/breakpoint';
import './libs/cycle';
import './declarations/override';

// Database Settings
export const DB_HOST = process.env.DB_HOST;
export const DB_USER = process.env.DB_USER;
export const DB_PASS = process.env.DB_PASS;
export const DB_BASE = process.env.DB_BASE;
export const DB_CLOG = process.env.DB_CLOG === "1";

// GameServer Settings
export const GS_PORT = Number.parseInt(process.env.GS_PORT, 10);
export const AS_PORT = Number.parseInt(process.env.AS_PORT, 10);
export const DS_PORT = Number.parseInt(process.env.DS_PORT, 10);
export const NS_PORT = Number.parseInt(process.env.NS_PORT, 10);

export const DH_KEY_P = "E7A69EBDF105F2A6BBDEAD7E798F76A209AD73FB466431E2E7352ED262F8C558F10BEFEA977DE9E21DCEE9B04D245F300ECCBBA03E72630556D011023F9E857F";
export const DH_KEY_G = "05";
export const DH_KEY_S = "TQServer";

export const SERVER_KEY = "C238xs65pjy7HU9Q";

export const DEV_MODE = process.env.KOGA_ENV === "dev";

// Game Settings
export const GAME_SCREEN_SIZE = Number.parseInt(process.env.GAME_SCREEN_SIZE, 10);
export const GAME_SCREEN_REMOVE = Number.parseInt(process.env.GAME_SCREEN_REMOVE, 10);
export const ITEM_GROUND_TIME = Number.parseInt(process.env.ITEM_GROUND_TIME, 10);
export const MONEY_DROP_RATE = Number.parseInt(process.env.MONEY_DROP_RATE, 10);