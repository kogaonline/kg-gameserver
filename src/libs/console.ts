import * as colors from 'colors'
import * as fs from 'fs'
import { DB_CLOG } from '../settings'
import breakIf from './breakpoint'

export default class ConsoleHandler {
    static getLogFile(type: "main" | "db" | "chat" | "trade" | "item" | "player") {
        let dt = new Date().toLocaleDateString().replace(/\//g, '-')
        let dir = `log`
        let file = `${dir}/kgserver-${type}-${dt}.log`

        if (!fs.existsSync(dir))
            fs.mkdirSync(dir)

        let logFile = fs.createWriteStream(file, {
            flags: 'a+' // 'a' means appending (old data will be preserved)
        })

        return logFile
    }

    static origL: any = console.log
    static origE: any = console.error
    static origW: any = console.warn

    static setup() {
        console.breakIf = breakIf;
        ConsoleHandler.theme()

        const getStrMod = (arg: string) => {
            let msg = `${arg}\n`
            let date = new Date()
            // @ts-ignore
            let dateStr = colors.default(`[${date.toLocaleDateString().replace(/\//g, '-')} ${date.toLocaleTimeString()}]`)
            let final = `${dateStr} ${msg}`
            return final
        }

        console.log = (function () {
            return function () {
                arguments[0] = getStrMod(arguments[0])
                ConsoleHandler.getLogFile(`main`).write(arguments[0])
                arguments[0] = arguments[0].replace(/\n$/g, '')
                ConsoleHandler.origL(...arguments)
            }
        })()

        console.error = (function () {
            return function () {
                arguments[0] = getStrMod(arguments[0])
                ConsoleHandler.getLogFile(`main`).write(arguments[0])
                arguments[0] = arguments[0].replace(/\n$/g, '')
                ConsoleHandler.origE(...arguments)
            }
        })()

        console.warn = (function () {
            return function () {
                arguments[0] = getStrMod(arguments[0])
                ConsoleHandler.getLogFile(`main`).write(arguments[0])
                arguments[0] = arguments[0].replace(/\n$/g, '')
                ConsoleHandler.origW(...arguments)
            }
        })()

        console.dblog = (function () {
            return function () {
                arguments[0] = getStrMod(arguments[0])
                ConsoleHandler.getLogFile(`db`).write(arguments[0])
                arguments[0] = arguments[0].replace(/\n$/g, '')
                if (!DB_CLOG)
                    return;
                ConsoleHandler.origL(...arguments)
            }
        })()

        console.dberror = (function () {
            return function () {
                arguments[0] = getStrMod(arguments[0])
                ConsoleHandler.getLogFile(`db`).write(arguments[0])
                arguments[0] = arguments[0].replace(/\n$/g, '')
                ConsoleHandler.origE(...arguments)
            }
        })()

        console.dbwarn = (function () {
            return function () {
                arguments[0] = getStrMod(arguments[0])
                ConsoleHandler.getLogFile(`db`).write(arguments[0])
                arguments[0] = arguments[0].replace(/\n$/g, '')
                if (!DB_CLOG)
                    return
                ConsoleHandler.origW(...arguments)
            }
        })()


        console.chatlog = (function () {
            return function () {
                arguments[0] = getStrMod(arguments[0])
                ConsoleHandler.getLogFile(`chat`).write(arguments[0])
            }
        })()

        console.itemlog = (function () {
            return function () {
                arguments[0] = getStrMod(arguments[0])
                ConsoleHandler.getLogFile(`item`).write(arguments[0])
            }
        })()

        console.tradelog = (function () {
            return function () {
                arguments[0] = getStrMod(arguments[0])
                ConsoleHandler.getLogFile(`trade`).write(arguments[0])
            }
        })()

        console.playerlog = (function () {
            return function () {
                arguments[0] = getStrMod(arguments[0])
                ConsoleHandler.getLogFile(`player`).write(arguments[0])
                arguments[0] = arguments[0].replace(/\n/g, '')
                ConsoleHandler.origL(...arguments)
            }
        })()
    }

    static theme() {

        colors.setTheme({
            default: 'cyan',
            error: 'red',
            warning: 'yellow',
        })

    }

}

ConsoleHandler.setup();