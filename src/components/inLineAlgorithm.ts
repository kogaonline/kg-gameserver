import { EDirection } from "../enums/EDirection";
import Core from "../core";
import { distanceBetween, getDirection } from "../logical/kernel";

export default class InLineAlgorithm {
    sourceX: number;
    sourceY: number;
    targetX: number;
    targetY: number;
    range: number;

    direction: number;

    locations: {
        x: number,
        y: number
    }[]

    constructor(sourceX: number, sourceY: number, targetX: number, targetY: number, range: number) {
        this.range = range;
        this.sourceX = sourceX;
        this.sourceY = sourceY;
        this.targetX = targetX;
        this.targetY = targetY;

        this.locations = [];
        this.direction = getDirection(sourceX, sourceY, targetX, targetY)

        this.getLineLocations();
    }

    getLineLocations() {

        let xa = this.sourceX;
        let ya = this.sourceY;

        let xb = this.targetX;
        let yb = this.targetY;

        let dx = xb - xa, dy = yb - ya, steps, k;
        let xincrement, yincrement, x = xa, y = ya;

        if (Math.abs(dx) > Math.abs(dy)) steps = Math.abs(dx);
        else steps = Math.abs(dy);

        xincrement = dx / steps;
        yincrement = dy / steps;

        this.locations.push({ x: Math.round(x), y: Math.round(y) });

        for (k = 0; k < this.range; k++) {
            x += xincrement;
            y += yincrement;
            this.locations.push({ x: Math.round(x), y: Math.round(y) });
        }

    }

    isInLine(x: number, y: number) {
        let dist = distanceBetween(this.sourceX, this.sourceY, x, y);

        if (dist <= this.range) {
            for (let i = 0; i < this.locations.length; i++) {
                let loc = this.locations[i];

                if (loc.x === x && loc.y === y)
                    return true;
            }

            return false;
        }
    }
}