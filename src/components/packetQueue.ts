export default class PacketQueue {
    private queue: Buffer[] = new Array<Buffer>();

    public enqueue(packet: Buffer){
        this.queue.push(packet);
    }

    public canDequeue(): boolean {
        return this.queue.length > 0;
    }

    public dequeue(): Buffer {
        let packet = this.queue.shift();
        return packet;
    }

    public get length(){
        return this.queue.length;
    }

    public clear(){
        this.queue = [];
    }
}