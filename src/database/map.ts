import Core from "../core";
import Map from "../game/map";
import DBService from "../services/database";
import { existsSync, createReadStream } from "fs";
import IndexedElement from "../components/indexedElement";

interface IMap {
    id: number;
    name: string;
    description: string;
    doctype: number;
    owner: number;
    ownerType: number;
    group: number;
    weather: number;
    rebirthMap: number;
    rebirthPortal: number;
    portalX: number;
    portalY: number;
    color: number;
    createdAt: string;
    updatedAt: string;
}

export default class RMaps {
    static async load() {
        await this.loadDMaps();

        Core.maps = new IndexedElement();
        let maps = DBService.getResults<IMap>(await DBService.Models.cq_map.findAll());
        await maps.mapAsync(async (m) => {
            let map: Map = new Map(m.id, m.doctype, false);
            await map.loadGround(map.base);
            map.portalX = m.portalX ? m.portalX : 50;
            map.portalY = m.portalY ? m.portalY : 50;
            Core.maps.add(map.id, map);
        });

        console.log(`Loaded ${Core.maps.size} maps.`);
    }

    static new(base: number, id: number) {
        let map: Map = new Map(id, base);
        map.portalX = 0;
        map.portalY = 0;
        Core.maps.add(map.id, map);
    }

    static async loadDMaps() {
        try {
            Core.dmaps = new IndexedElement();
            const gm = process.cwd() + '/ini/GameMap.dat';
            if (existsSync(gm)){
                let data = '';
                let stream = createReadStream(gm, {
                    encoding: 'binary'
                });

                await new Promise((resolve, reject) => {
                    stream.on('data', chunk => data += chunk);
                    stream.on('end', () => {
                        let ptr = 0;
                        let gmBuffer = Buffer.from(data, 'binary');
                        let mapCount = gmBuffer.readUInt32LE(ptr);

                        for (let i = 0; i < mapCount; i++){
                            ptr += 4;

                            let mapid = gmBuffer.readUInt32LE(ptr); ptr += 4;
                            let pathSize = gmBuffer.readUInt32LE(ptr); ptr += 4;
                            let path = data.substr(ptr, pathSize);
                            ptr += pathSize;

                            if (path.endsWith('.7z'))
                                path = `${path.substr(0, pathSize-3)}.dmap`;

                            Core.dmaps.add(mapid, path);
                        }
                        
                        console.log(`Loaded ${mapCount} DMaps successfully!`);
                        resolve(true);
                    });

                });
            } else {
                console.log("GameMap.dat not found!");
            }
        } catch (err){
            console.log(err);
            console.log("Failed to load DMaps.")
        }
    }
}