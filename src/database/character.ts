import Core from "../core";
import MsgRegister from "../network/packets/gamepackets/MsgRegister";
import { cnRes } from "../res/cnRes";
import Player from "../game/player";
import { getRandom } from "../logical/kernel";
import Character from "../game/character";
import DBService from "../services/database";
import AccountTable from "./account";
import RMagic from "./magic";
import { ICharacter } from "./sequelize/models/cq_user";
import RItem from "./item";

interface IWeaponSkillResult {
    playerId: number;
    id: number;
    level: number;
    experience: number;
}

export default class RCharacter {
    static modelMap(c: ICharacter){
        return {
            name: c.name,
            hairStyle: c.hairStyle,
            class: c.class,
            money: c.money,
            cps: c.cps,
            cpsBound: c.cpsBound,
            treasurePoints: c.treasurePoints,
            body: c.body,
            face: c.face,
            level: c.level,
            strength: c.strength,
            agility: c.agility,
            vitality: c.vitality,
            spirit: c.spirit,
            attrPoints: c.attrPoints,
            hp: c.hp,
            mana: c.mana,
            mapId: c.mapId,
            x: c.x,
            y: c.y,
            pkPoints: c.pkPoints,
            experience: c.experience,
            quizPoints: c.quizPoints,
            prevMapId: c.prevMapId,
            reborn: c.reborn,
            firstClass: c.firstClass,
            secondClass: c.secondClass,
            firstRebornLevel: c.firstRebornLevel,
            secondRebornLevel: c.secondRebornLevel,
            spouse: c.spouse,
            whPassword: c.whPassword,
            whMoney: c.whMoney,
            enlightenPoints: c.enlightenPoints,
            enlightmentTime: c.enlightmentTime,
            enlightments: c.enlightments,
            heavenBlessing: c.heavenBlessing,
            luckTime: c.luckTime,
            vipLevel: c.vipLevel,
            virtuePoints: c.virtuePoints,
            prevX: c.prevX,
            prevY: c.prevY,
            studyPoints: c.studyPoints,
            lastLogin: c.lastLogin,
            title: c.title,
            firstBuy: c.firstBuy,
            country: c.country,
            flower: c.flower,
            namechange: c.namechange,
            namechangeCount: c.namechangeCount,
            racePoints: c.racePoints,
            multipleExp: c.multipleExp,
            multipleExpTimes: c.multipleExpTimes,
            onlineTrainingExp: c.onlineTrainingExp,
            blessedHuntingExp: c.blessedHuntingExp,
            royalPoints: c.royalPoints,
            business: c.business
            
        } as ICharacter;
    }

    static async getById(id: number) {
        return DBService.findByUidDecorator<ICharacter>(DBService.Models.cq_user, id, [{ model: DBService.Models.accounts, as: "account" }]);
    }

    static async loadExtras(player: Player) {
        await RMagic.load(player);
        await RMagic.loadWeaponSkills(player);
    }

    static async create(info: MsgRegister, player: Player): Promise<string> {
        let msg = cnRes.ANSWER_OK;
        let { name, profession, body } = info;
        name = name.replace(/\ /g, ``).trim();
        let _name = name.toLowerCase();
        if (_name.length < 3) {
            msg = cnRes.STR_REGISTER_INVALID_NAME;
            return msg;
        }

        if (_name.includes('gm') || _name.includes('pm')) {
            msg = cnRes.STR_REGISTER_INVALID_NAME_CHARACTERS;
            return msg;
        }

        if (await this.nameTaken(name)) {
            msg = cnRes.STR_REGISTER_NAME_TAKEN;
            return msg;
        }


        if (!await this.createCharacter(Core.charaterUIDCounter++, info, player))
            msg = cnRes.STR_REGISTER_UNSUCCESSFULL;

        return msg;
    }

    static async updateCharacter(character: ICharacter): Promise<any> {
        const {uid} = character;
        DBService.Models.cq_user.update(this.modelMap(character), { where: { uid }});
        console.log(`Character (${uid}) updated`);
    }

    static async createCharacter(uid: number, info: MsgRegister, player: Player): Promise<any> {
        try {
            player.account.playerId = uid;
            AccountTable.save(player.account);

            let face = (info.body === 1003 || info.body === 1004) ? 1 : 201;
            let color = getRandom(4, 8);
            let hairStyle = color * 100 + 10 + getRandom(4, 9);
            let attrs = Character.getAttr(1, info.profession);

            let created = await DBService.Models.cq_user.create({
                uid, 
                accountId: player.account.id,
                name: info.name,
                hairStyle,
                class: info.profession,
                level: 1,
                body: info.body,
                face,
                strength: attrs[0],
                agility: attrs[1],
                vitality: attrs[2],
                spirit: attrs[3]
            } as ICharacter);
            
            return created ? true : false;
        } catch (err) {
            throw new Error(`Failed to create new character -> ${err.message}`);
        }
    }

    static async saveCharacter(player: Player) {
        const c = (player.character);
        this.updateCharacter(player.character);
        this.saveExtras(player);
    }

    static async saveExtras(player: Player) {
        RMagic.save(player);
        RMagic.saveWeaponSkills(player);
        RItem.savePlayerItems(player);
    }

    static deleteCharacter(pin: string, player: Player) {
        if (player.character.whPassword === pin) {
            DBService.Models.cq_deluser.create(DBService.Models.cq_user.find({
                where: {
                    uid: player.character.uid
                }
            }));

            DBService.Models.cq_user.destroy({
                where: {
                    uid: player.character.uid
                }
            });

            DBService.Models.accounts.update({
                playerId: 0
            }, {
                where: {
                    playerId: player.character.uid
                }
            });
            return true
        }
        return false
    }

    static async nameTaken(name: string): Promise<boolean> {
        try {
            let existingUser = await DBService.Models.cq_user.findOne({ where: { name } });
            return existingUser ? true : false;
        } catch (err){
            throw new Error(`Failed to check name taken -> ${err.message}`);
        }
    }
}