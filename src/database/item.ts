import Core from "../core";
import Player from "../game/player";
import MsgItemInfo from "../network/packets/gamepackets/MsgItemInfo";
import { EItemPosition } from "../enums/EItemPosition";
import IItemInfo, { IItemComposeInfo } from "../interfaces/IItemInfo";
import DBService from "../services/database";
import { IItemType } from "./itemtype";

export interface IItem {
    itemtype: IItemType;
    
    id: number;
    type: number;
    owner: number;
    player: number;
    durability: number;
    max_durability: number;
    position: number;
    gem1: number;
    gem2: number;
    magic1: number;
    magic2: number;
    magic3: number;
    data: number;
    bless: number;
    enchant: number;
    anti_monster: number;
    color: number;
    progress_gem: number;
    progress_lev: number;
    monopoly: number;
    inscribed: number;
    artifact_start: number;
    artifact_type: number;
    artifact_expire: number;
    artifact_stabilization: number;
    stack: number;
    bound: number;
    warehouse: number;
    expiration: number;
    item_status: number;
}

export default class RItem {
    static modelMap(item: MsgItemInfo, player: Player){
        return {
            id: item.uid,
            type: item.id,
            owner: player.character.uid,
            player: player.character.uid,
            durability: item.durability,
            max_durability: item.maxDurability,
            position: item.position,
            gem1: item.socketOne,
            gem2: item.socketTwo,
            magic1: item.data,
            magic2: 0,
            magic3: item.plus,
            data: item.data,
            bless: item.bless,
            enchant: item.enchant,
            anti_monster: item.lock,
            color: item.color,
            progress_gem: item.socketProgress,
            progress_lev: item.plusProgress,
            monopoly: 0,
            inscribed: item.inscribed,
            artifact_type: 0,
            artifact_start: 0,
            artifact_expire: 0,
            artifact_stabilization: 0,
            stack: item.stackSize,
            bound: item.bound,
            warehouse: 0,
            expiration: 0,
            item_status: 0
        } as IItem;
    }

    static async loadPlayerItems(player: Player): Promise<any> {
        let items = DBService.getResults<IItem>(await DBService.Models.cq_item.findAll({ where: { owner: player.character.uid } }));
        await items.mapAsync((i) => {
            let item = new MsgItemInfo(true);
            item.load(i);

            if (item.position === EItemPosition.Inventory) {
                player.inventory.add(item);
            } else if (item.position >= EItemPosition.Head && item.position <= EItemPosition.AlternateGarment) {
                player.equipment.add(item.position, item);
            }
        });

        player.character.recalculateStats();
    }

    static async savePlayerItems(player: Player): Promise<any> {
        player.inventory.items.map((item) => {
            this.updateItem(item, player);
        });
        console.log(`Saved ${player.character.name}'s items successfully.`);
    }

    static updateItem(item: MsgItemInfo, player: Player): Promise<any> {
        return DBService.Models.cq_item.update(this.modelMap(item, player), { where: { id: item.uid } });
    }

    static createItem(item: MsgItemInfo, player: Player): Promise<any> {
        return DBService.Models.cq_item.create(this.modelMap(item, player));
    }


    static async loadItemInfo(): Promise<any> {
        throw new Error("TODO25");
        // let res: any = await Core.database.executeRaw(`SELECT * FROM \`cq_itemtype\`;`);

        // for (let i = 0; i < res.length; i++) {
        //     let item: IItemInfo = res[i];

        //     Core.itemInfo.add(item.id, item);
        // }
        // console.log(`Loaded ${Core.itemInfo.size} item informations.`);

        // await this.loadItemComposeInfo();
    }


    static async loadItemComposeInfo(): Promise<any> {
        throw new Error("TODO26");
        // let res: any = await Core.database.executeRaw(`SELECT * FROM \`cq_itemcompose\`;`);

        // for (let i = 0; i < res.length; i++) {
        //     let item: IItemComposeInfo = res[i];

        //     Core.itemComposeInfo.add(item.uid, item);
        // }
        // console.log(`Loaded ${Core.itemComposeInfo.size} item compose informations.`);
    }

    static getComposeInfo(id: number, plus: number): IItemComposeInfo {
        let data = null;
        Core.itemComposeInfo.map((info) => {
            let sub = id % 10
            let nid = id - sub
            if (nid === info.id && plus === info.plus) {
                data = info;
            }
        });

        return data;
    }
}