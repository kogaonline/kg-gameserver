import DBService from "../services/database";
import Core from "../core";
import IndexedElement from "../components/indexedElement";
import NPC from "../game/npc";
import { ENPCType } from "../enums/ENPCType";

export interface INpc {
    id: number;
    owner: number;
    player: number;
    name: string;
    type: ENPCType;
    lookface: number;
    map: number;
    x: number;
    y: number;
    task0: number;
    task1: number;
    task2: number;
    task3: number;
    task4: number;
    task5: number;
    task6: number;
    task7: number;
    task8: number;
    task9: number;
    data0: number;
    data1: number;
    data2: number;
    data3: number;
    datastr: string;
    life: number;
    maxlife: number;
    base: number;
    sort: number;
    item: number;
}

export default class RNPC {
    static async load() {
        Core.npcs = new IndexedElement();
        let npcs = DBService.getResults<INpc>(await DBService.Models.cq_npc.findAll());
        Core.maps.map(map => map.removeAllNpc());

        npcs.map((npc) => {
            Core.npcs.add(npc.id, npc);
            let map = Core.maps.get(npc.map);
            if (map){
                let _npc = new NPC();
                _npc.uid = npc.id;
                _npc.mapId = npc.map;
                _npc.x = npc.x;
                _npc.y = npc.y;
                _npc.name = npc.name;
                _npc.type = npc.type;
                _npc.task0 = npc.task0;

                _npc.data0 = npc.data0;

                map.addNpc(_npc);
            }
        });

        console.log(`Loaded ${Core.npcs.size} npc infos.`);
    }

    static async loadNPC(id: number){
        let npc = DBService.getFirstResult<INpc>(await DBService.Models.cq_npc.findOne({ where: id }));
        Core.npcs.remove(id);
        Core.npcs.add(npc.id, npc);

        let map = Core.maps.get(npc.map);
        if (map){
            let _npc = new NPC();
            _npc.uid = npc.id;
            _npc.mapId = npc.map;
            _npc.x = npc.x;
            _npc.y = npc.y;
            _npc.name = npc.name;
            _npc.type = npc.type;
            _npc.task0 = npc.task0;

            _npc.data0 = npc.data0;

            map.removeNpc(_npc);
            map.addNpc(_npc);
        }
    }
}