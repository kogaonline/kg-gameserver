import { EAccountStatus } from "../enums/EAccountStatus";
import { Op } from 'sequelize';
import Core from "../core";
import DBService from "../services/database";
import { IAccountRow } from "./sequelize/models/accounts";

export default class RAccounts {
    static async getById(id: number, include: any[] = []) {
        return DBService.findByIdDecorator<IAccountRow>(DBService.Models.accounts, id, include);
    }

    static async save(acc: IAccountRow){
        await DBService.Models.accounts.update(acc, {
            where: {
                id: acc.id
            }
        })
    }
}