import Player from "../game/player";
import MsgMagicInfo from "../network/packets/gamepackets/MsgMagicInfo";
import DBService from "../services/database";
import MsgWeaponSkill from "../network/packets/gamepackets/MsgWeaponSkill";
import Core from "../core";

export interface IMagic {
    id: number;
    userId: number;
    type: number;
    level: number;
    experience: number;
    createdAt: string;
    updatedAt: string;
}

export interface IWeaponSkill {
    id: number;
    userId: number;
    type: number;
    level: number;
    experience: number;
    createdAt: string;
    updatedAt: string;
}

export default class RMagic {
    static modelMap(magic: MsgMagicInfo, player: Player){
        return {
            id: magic.uid,
            userId: player.character.uid,
            type: magic.type,
            experience: magic.experience,
            level: magic.level,
        } as IMagic;
    }
    static modelMapW(magic: MsgWeaponSkill, player: Player){
        return {
            id: magic.uid,
            userId: player.character.uid,
            type: magic.type,
            experience: magic.experience,
            level: magic.level,
        } as IWeaponSkill;
    }

    static async load(player: Player) {
        let magics = DBService.getResults<IMagic>(await DBService.Models.cq_magic.findAll({ 
            where: { userId: player.character.uid } 
        }));
        await magics.mapAsync(async (m) => {
            let magic = new MsgMagicInfo();
            magic.uid = m.id;
            magic.experience = m.experience;
            magic.level = m.level;
            magic.type = m.type;
            // magic.upgradeLevel = m.LevelHu;
            magic.send(player);

            player.character.addMagic(magic);
        });

        console.log(`Loaded ${player.character.name}'s magic infos.`);
    }

    static async loadWeaponSkills(player: Player) {
        let magics = DBService.getResults<IWeaponSkill>(await DBService.Models.cq_weapon_skill.findAll({ 
            where: { userId: player.character.uid } 
        }));
        await magics.mapAsync(async (m) => {
            let magic = new MsgWeaponSkill();

            let expNeeded = 0;
            let levexp = Core.levexp.filter((lx) => lx.type === 7);
            levexp.map((lx) => {
                if (lx.level === m.level)
                    expNeeded = lx.experience;
            });

            magic.uid = m.id;
            magic.experience = m.experience;
            magic.reqExperience = expNeeded;
            magic.level = m.level;
            magic.type = m.type;
            magic.send(player);

            player.character.addWeaponSkill(magic);
        });

        console.log(`Loaded ${player.character.name}'s magic infos.`);
    }

    static async save(player: Player) {
        player.character.magics.map((magic: MsgMagicInfo) => {
            this.saveMagic(player, magic);
        });
        console.log(`Saved ${player.character.name}'s magics successfully.`);
    }

    static async saveMagic(player: Player, magic: MsgMagicInfo) {
        return DBService.Models.cq_magic.update(this.modelMap(magic, player), { where: { id: magic.uid } });
    }

    static async insertMagic(player: Player, magic: MsgMagicInfo) {
        return DBService.Models.cq_magic.create(this.modelMap(magic, player));
    }

    static async saveWeaponSkill(player: Player, magic: MsgWeaponSkill) {
        return DBService.Models.cq_weapon_skill.update(this.modelMapW(magic, player), { where: { id: magic.uid } });
    }

    static async insertWeaponSkill(player: Player, magic: MsgWeaponSkill) {
        return DBService.Models.cq_weapon_skill.create(this.modelMapW(magic, player));
    }

    static async saveWeaponSkills(player: Player) {
        player.character.weaponSkills.map((magic: MsgWeaponSkill) => {
            this.saveWeaponSkill(player, magic);
        });
        console.log(`Saved ${player.character.name}'s weapon skills successfully.`);
    }
}