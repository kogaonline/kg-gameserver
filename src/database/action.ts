import Core from "../core";
import DBService from "../services/database";
import IndexedElement from "../components/indexedElement";
import ServerAction from "../base/serverActions";

interface IAction {
    id: number;
    id_next: number;
    id_next_fail: number;
    type: number;
    data: number;
    param: string;
    createdAt: string;
    updatedAt: string;
}

export default class RAction {
    static async load() {
        Core.actions = new IndexedElement();
        let actions = DBService.getResults<IAction>(await DBService.Models.cq_action.findAll());
        await actions.mapAsync(async (a) => {
            let sa: ServerAction = new ServerAction();
            sa.id = a.id;
            sa.id_next = a.id_next;
            sa.id_next_fail = a.id_next_fail;
            sa.type = a.type;
            sa.data = a.data;
            sa.param = a.param;

            Core.actions.add(sa.id, sa);
            // TODO: verify actions
        });

        console.log(`Loaded ${Core.actions.size} server actions.`);
    }

    static async loadAction(id: number) {
        let a = DBService.getFirstResult<IAction>(await DBService.Models.cq_action.findOne({where: id}));
        let sa: ServerAction = new ServerAction();
        sa.id = a.id;
        sa.id_next = a.id_next;
        sa.id_next_fail = a.id_next_fail;
        sa.type = a.type;
        sa.data = a.data;
        sa.param = a.param;

        Core.actions.remove(id);
        Core.actions.add(sa.id, sa);

        console.log(`Reloaded action ${id}.`);
    }
}