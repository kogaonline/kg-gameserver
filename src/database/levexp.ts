import Core from "../core";
import DBService from "../services/database";
import IndexedElement from "../components/indexedElement";
import ServerAction from "../base/serverActions";

export interface ILevExp {
    id: number;
    type: number;
    level: number;
    experience: number;
    experience2: number;
    experience3: number;
}

export default class RLevExp {
    static async load() {
        Core.levexp = new IndexedElement();
        let exps = DBService.getResults<ILevExp>(await DBService.Models.cq_levexp.findAll());
        await exps.mapAsync(async (e) => {
            Core.levexp.add(e.id, e);
        });

        console.log(`Loaded ${Core.levexp.size} level experiences.`);
    }
}