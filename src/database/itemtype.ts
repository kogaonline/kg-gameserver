import Core from "../core";
import DBService from "../services/database";
import IndexedElement from "../components/indexedElement";

export interface IItemType {
    id: number;
    name: string;
    class: number;
    proficiency: number;
    level: number;
    gender: number;
    strength: number;
    agility: number;
    vitality: number;
    spirit: number;
    monopoly: number;
    weight: number;
    moneyValue: number;
    action: number;
    maxAttack: number;
    minAttack: number;
    defense: number;
    accuracy: number;
    dodge: number;
    addHP: number;
    addMP: number;
    durability: number;
    maxDurability: number;
    gem1: number;
    gem2: number;
    magic1: number;
    magic2: number;
    magic3: number;
    magicAttack: number;
    magicDefense: number;
    attackRange: number;
    attackSpeed: number;
    cpsValue: number;
    expirationTime:number;
    
    criticalStrike: number;
    detoxication: number;
    immunity: number;
    penetration: number;
    block: number;
    breakthrough: number;
    counterAction: number;
    stackSize: number;
    metalResist: number;
    woodResist: number;
    waterResist: number;
    fireResist: number;
    earthResist: number;
    type: string;
    description: string;
    purificationLevel: number;
    purificationMeteors: number;
    rateLv: number;
}

export default class RItemType {
    static async load() {
        Core.itemInfo = new IndexedElement<IItemType>();
        let items = DBService.getResults<IItemType>(await DBService.Models.cq_itemtype.findAll());
        await items.mapAsync((i) => {
            Core.itemInfo.add(i.id, i);
        });

        console.log(`Loaded ${Core.itemInfo.size} item infos.`);
    }

    static getBaseInfo(id: number): IItemType {
        let data = null;
        Core.itemInfo.map((info) => {
            if (id === info.id) {
                data = info;
            }
        });

        return data;
    }
}