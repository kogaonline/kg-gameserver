import Sequelize, { Model } from 'sequelize';

class cq_itemtype extends Model {
    static init(sequelize: any) {
        super.init(
            {
                id: {
                    allowNull: false,
                    autoIncrement: true,
                    primaryKey: true,
                    type: Sequelize.INTEGER
                },
                name: {
                    allowNull: false,
                    type: Sequelize.STRING
                },
                class: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                proficiency: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                level: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                gender: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                strength: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                agility: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                vitality: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                spirit: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                monopoly: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                weight: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                moneyValue: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                action: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                maxAttack: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                minAttack: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                defense: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                accuracy: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                dodge: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                addHP: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                addMP: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                durability: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                maxDurability: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                ident: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                gem1: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                gem2: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                magic1: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                magic2: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                magic3: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                unknown0: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                magicAttack: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                magicDefense: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                attackRange: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                attackSpeed: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                unknown1: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                unknown2: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                unknown3: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                cpsValue: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                unknown4: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                expirationTime: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                criticalStrike: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                detoxication: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                immunity: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                penetration: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                block: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                breakthrough: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                counterAction: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                stackSize: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                metalResist: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                woodResist: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                waterResist: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                fireResist: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                earthResist: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                type: {
                    allowNull: false,
                    type: Sequelize.STRING
                },
                description: {
                    allowNull: false,
                    type: Sequelize.STRING
                },
                unknown8: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                purificationLevel: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                purificationMeteors: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                rateLv: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                unknown10: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                unknown11: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
            },
            {
                sequelize,
                freezeTableName: true,
                tableName: 'cq_itemtype',
            }
        );

        return this;
    }

    static associate(models: any) {
    }
}

export default cq_itemtype;
