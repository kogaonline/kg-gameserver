import Sequelize, { Model } from 'sequelize';
import DBService from '../../../services/database';

class cq_weapon_skill extends Model {
  static init(sequelize: any) {
    super.init(
      {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        userId: {
            type: Sequelize.BIGINT(16),
            references: {
              model: 'cq_user',
              key: 'uid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'SET NULL',
        },
        type: {
          allowNull: false,
          type: Sequelize.INTEGER.UNSIGNED
        },
        level: {
            allowNull: false,
            type: Sequelize.INTEGER.UNSIGNED
        },
        experience: {
            allowNull: false,
            type: Sequelize.BIGINT.UNSIGNED
        },
      },
      {
        sequelize,
        freezeTableName: true,
        tableName: 'cq_weapon_skill',
      }
    );

    return this;
  }

  static associate(models: any) {
    this.belongsTo(DBService.Models.cq_user, {foreignKey: 'userId', as: 'ownerinfo'});
  }
}

export default cq_weapon_skill;
