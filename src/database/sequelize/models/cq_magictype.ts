import Sequelize, { Model } from 'sequelize';

class cq_magictype extends Model {
    static init(sequelize: any) {
        super.init(
            {
                id: {
                    allowNull: false,
                    autoIncrement: true,
                    primaryKey: true,
                    type: Sequelize.INTEGER
                },
                type: {
                    type: Sequelize.INTEGER.UNSIGNED
                },
                sort: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                name: {
                    allowNull: false,
                    type: Sequelize.STRING
                },
                pk: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                ground: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                multi: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                target: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                level: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                manacost: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                power: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                speed: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                percent: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                seconds: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                range: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                distance: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                attr: {
                    allowNull: false,
                    type: Sequelize.INTEGER
                },
                status: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                req_prof: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                req_exp: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                req_level: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                xp: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                weapon: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                times: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                auto: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                floor: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                auto_learn: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                learn_level: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                drop_weapon: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                stamina: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                weapon_hit: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                use_item: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                next_magic: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                delay: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                use_item_num: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                unknown1: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                unknown2: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                unknown3: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                unknown4: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                unknown5: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                unknown6: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                unknown7: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                unknown8: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                unknown9: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                unknown10: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                unknown11: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                unknown12: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                unknown13: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
                upgrade_cost: {
                    allowNull: false,
                    type: Sequelize.INTEGER.UNSIGNED
                },
            },
            {
                sequelize,
                freezeTableName: true,
                tableName: 'cq_magictype',
            }
        );

        return this;
    }

    static associate(models: any) {
    }
}

export default cq_magictype;
