import Sequelize, { Model } from 'sequelize';
import { EAccountStatus } from "../../../enums/EAccountStatus";

export interface IAccountRow {
    id: number;
    name: string;
    username: string;
    password: string;
    ip: string;
    status: EAccountStatus;
    playerId: number;
    email: string;
    zip: string;
    address: string;
    address2: string;
    createdAt: string;
    updatedAt: string;
}

class accounts extends Model {
    static init(sequelize: any) {
        super.init(
            {
                id: {
                    allowNull: false,
                    autoIncrement: true,
                    primaryKey: true,
                    type: Sequelize.INTEGER
                },
                name: {
                    type: Sequelize.STRING,
                },
                username: {
                    type: Sequelize.STRING,
                },
                ip: {
                    type: Sequelize.STRING,
                },
                status: {
                    type: Sequelize.INTEGER,
                },
                playerId: {
                    type: Sequelize.INTEGER,
                },
                email: {
                    type: Sequelize.STRING,
                },
                zip: {
                    type: Sequelize.STRING,
                },
                address: {
                    type: Sequelize.STRING,
                },
                address2: {
                    type: Sequelize.STRING,
                },
            },
            {
                sequelize,
                freezeTableName: true,
                tableName: 'accounts',
            }
        );

        return this;
    }

    static associate(models: any) {
    }
}

export default accounts;
