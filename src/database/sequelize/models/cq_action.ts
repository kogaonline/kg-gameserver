import Sequelize, { Model } from 'sequelize';

class cq_action extends Model {
  static init(sequelize: any) {
    super.init(
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.BIGINT(16)
            },
            id_next: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT(16)
            },
            id_next_fail: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT(16)
            },
            type: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT(16)
            },
            data: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT(16)
            },
            param: {
                allowNull: false,
                defaultValue: "",
                type: Sequelize.STRING(500)
            },
            createdAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            }
        },
        {
            sequelize,
            freezeTableName: true,
            tableName: 'cq_action',
        }
    );

    return this;
  }

  static associate(models: any) {
  }
}

export default cq_action;
