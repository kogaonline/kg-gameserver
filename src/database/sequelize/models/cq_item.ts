import Sequelize, { Model } from 'sequelize';
import DBService from '../../../services/database';

class cq_item extends Model {
    static init(sequelize: any) {
        super.init(
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            type: {
                type: Sequelize.INTEGER,
                references: {
                model: 'cq_itemtype',
                key: 'id',
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
            },
            owner: {
                type: Sequelize.BIGINT(16),
                references: {
                model: 'cq_user',
                key: 'uid',
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
            },
            player: {
                type: Sequelize.BIGINT(16),
                references: {
                model: 'cq_user',
                key: 'uid',
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
            },
            durability: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            max_durability: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            position: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            gem1: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            gem2: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            magic1: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            magic2: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            magic3: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            data: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            bless: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            enchant: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            anti_monster: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            color: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            progress_gem: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            progress_lev: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            monopoly: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            inscribed: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            artifact_type: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            artifact_start: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            artifact_expire: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            artifact_stabilization: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            stack: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            bound: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            warehouse: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            expiration: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            item_status: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
        },
        {
            sequelize,
            freezeTableName: true,
            tableName: 'cq_item',
        }
        );

        return this;
    }

    static associate(models: any) {
        this.belongsTo(DBService.Models.cq_itemtype, {foreignKey: 'type', as: 'itemtype'});
        this.belongsTo(DBService.Models.cq_user, {foreignKey: 'owner', as: 'ownerinfo'});
    }
}

export default cq_item;
