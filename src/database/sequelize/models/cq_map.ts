import Sequelize, { Model } from 'sequelize';
import DBService from '../../../services/database';

export interface IMap {
}

class cq_map extends Model {
  static init(sequelize: any) {
    super.init(
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            name: {
                allowNull: false,
                type: Sequelize.STRING
            },
            description: {
                allowNull: false,
                type: Sequelize.STRING
            },
            doctype: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            owner: {
                type: Sequelize.BIGINT(16),
                references: {
                  model: 'cq_user',
                  key: 'uid',
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
            },
            ownerType: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            group: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            weather: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            rebirthMap: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            rebirthPortal: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            portalX: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            portalY: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            color: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            createdAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            }
        },
        {
            sequelize,
            freezeTableName: true,
            tableName: 'cq_map',
        }
    );

    return this;
  }

  static associate(models: any) {
    this.belongsTo(DBService.Models.cq_user, {foreignKey: 'owner', as: 'ownerinfo'});
  }
}

export default cq_map;
