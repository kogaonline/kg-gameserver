import Sequelize, { Model } from 'sequelize';

class cq_monstertype extends Model {
  static init(sequelize: any) {
    super.init(
      {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.BIGINT(16)
        },
        name: {
            allowNull: false,
            defaultValue: "Monster",
            type: Sequelize.STRING(16)
        },
        type: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.INTEGER.UNSIGNED
        },
        body: {
            allowNull: false,
            defaultValue: 304,
            type: Sequelize.INTEGER.UNSIGNED
        },
        life: {
            allowNull: false,
            defaultValue: 100,
            type: Sequelize.BIGINT
        },
        mana: {
            allowNull: false,
            defaultValue: 100,
            type: Sequelize.BIGINT
        },
        min_attack: {
            allowNull: false,
            defaultValue: 100,
            type: Sequelize.BIGINT
        },
        max_attack: {
            allowNull: false,
            defaultValue: 100,
            type: Sequelize.BIGINT
        },
        defense: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.INTEGER.UNSIGNED
        },
        dodge: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.INTEGER.UNSIGNED
        },
        helmet: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.INTEGER.UNSIGNED
        },
        armor: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.INTEGER.UNSIGNED
        },
        weapon_right: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.INTEGER.UNSIGNED
        },
        weapon_left: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.INTEGER.UNSIGNED
        },
        view: {
            allowNull: false,
            defaultValue: 1,
            type: Sequelize.INTEGER.UNSIGNED
        },
        range: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.INTEGER.UNSIGNED
        },
        escape_life: {
            allowNull: false,
            defaultValue: 10,
            type: Sequelize.INTEGER.UNSIGNED
        },
        agility: {
            allowNull: false,
            defaultValue: 1000,
            type: Sequelize.INTEGER.UNSIGNED
        },
        speed: {
            allowNull: false,
            defaultValue: 1000,
            type: Sequelize.INTEGER.UNSIGNED
        },
        level: {
            allowNull: false,
            defaultValue: 61,
            type: Sequelize.INTEGER.UNSIGNED
        },
        attack_user: {
            allowNull: false,
            defaultValue: 1,
            type: Sequelize.INTEGER.UNSIGNED
        },
        action: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.BIGINT.UNSIGNED
        },
        drop_money: {
            allowNull: false,
            defaultValue: 500,
            type: Sequelize.BIGINT.UNSIGNED
        },
        drop_item: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.BIGINT.UNSIGNED
        },
        drop_item_rate: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.BIGINT.UNSIGNED
        },
        drop_helmet: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.INTEGER.UNSIGNED
        },
        drop_armor: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.INTEGER.UNSIGNED
        },
        drop_necklace: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.INTEGER.UNSIGNED
        },
        drop_ring: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.INTEGER.UNSIGNED
        },
        drop_weapon: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.INTEGER.UNSIGNED
        },
        drop_shield: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.INTEGER.UNSIGNED
        },
        drop_boots: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.BIGINT.UNSIGNED
        },
        drop_hp: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.BIGINT.UNSIGNED
        },
        drop_mp: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.BIGINT.UNSIGNED
        },
        magic_type: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.BIGINT.UNSIGNED
        },
        magic_def: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.BIGINT.UNSIGNED
        },
        magic_atk: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.BIGINT.UNSIGNED
        },
        magic_rate: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.INTEGER.UNSIGNED
        },
        ai_type: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.INTEGER.UNSIGNED
        },
        stc_type: {
            allowNull: false,
            defaultValue: 0,
            type: Sequelize.BIGINT.UNSIGNED
        },
      },
      {
        sequelize,
        freezeTableName: true,
        tableName: 'cq_monstertype',
      }
    );

    return this;
  }

  static associate(models: any) {
  }
}

export default cq_monstertype;
