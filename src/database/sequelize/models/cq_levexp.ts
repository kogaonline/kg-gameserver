import Sequelize, { Model } from 'sequelize';

class cq_levexp extends Model {
  static init(sequelize: any) {
    super.init(
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.BIGINT(16)
            },
            type: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT(16)
            },
            level: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT(16)
            },
            experience: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT(16)
            },
            experience2: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT(16)
            },
            experience3: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT(16)
            },
            createdAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            }
        },
        {
            sequelize,
            freezeTableName: true,
            tableName: 'cq_levexp',
        }
    );

    return this;
  }

  static associate(models: any) {
  }
}

export default cq_levexp;
