import Sequelize, { Model } from 'sequelize';
import DBService from '../../../services/database';

export interface IMap {
}

class cq_npc extends Model {
  static init(sequelize: any) {
    super.init(
        {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            owner: {
                type: Sequelize.BIGINT(16),
                allowNull: false,
                defaultValue: 0,
            },
            player: {
                type: Sequelize.BIGINT(16),
                allowNull: false,
                defaultValue: 0,
            },
            name: {
                allowNull: false,
                defaultValue: "",
                type: Sequelize.STRING
            },
            type: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            lookface: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            map: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            x: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            y: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            task0: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            task1: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            task2: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            task3: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER
            },
            task4: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            task5: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            task6: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            task7: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            task8: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            task9: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            data0: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            data1: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            data2: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            data3: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            datastr: {
                allowNull: false,
                defaultValue: "",
                type: Sequelize.STRING
            },
            life: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            maxlife: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            base: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            sort: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            item: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            createdAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            }
        },
        {
            sequelize,
            freezeTableName: true,
            tableName: 'cq_npc',
        }
    );

    return this;
  }

  static associate(models: any) {
  }
}

export default cq_npc;
