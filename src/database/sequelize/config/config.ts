require('dotenv').config();

module.exports = {
  "username": process.env.DB_USER,
  "password": process.env.DB_PASS,
  "database": process.env.DB_BASE,
  "host": process.env.DB_HOST,
  "dialect": "mysql",
  "operatorsAliases": 0,
  "logging": console.dblog
}