'use strict';

    module.exports = {
      up: (queryInterface, Sequelize) => {
        queryInterface.bulkInsert('cq_map',
          [
            {
              id: 1010,
              doctype: 1010,
              name: "BirthVillage",
              description: "Place where new warriors are born",
              owner: null,
              ownerType: 0,
              group: 0,
              weather: 0,
              rebirthMap: 0,
              rebirthPortal: 0,
              portalX: 0,
              portalY: 0,
              color: 0,
            },
            {
              id: 1002,
              doctype: 1002,
              name: "TwinCity",
              description: "Main city, center of the Kingdoms",
              owner: null,
              ownerType: 0,
              group: 0,
              weather: 0,
              rebirthMap: 0,
              rebirthPortal: 0,
              portalX: 0,
              portalY: 0,
              color: 0,
            }
          ]
        , {});
        return new Promise((resolve) => resolve(0));
      },
    
      down: (queryInterface, Sequelize) => {
        queryInterface.bulkDelete('cq_map', null, {});
        return new Promise((resolve) => resolve(0));
      }
    };