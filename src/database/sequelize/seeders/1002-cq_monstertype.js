'use strict';

    module.exports = {
      up: (queryInterface, Sequelize) => {
        queryInterface.bulkInsert('cq_monstertype',
          [
            {
                id: 1,
                name: "Pheasant",
                type: 1,
                body: 304,
                life: 33,
                mana: 0,
                min_attack: 4,
                max_attack: 5,
                defense: 1,
                dodge: 10,
                helmet: 0,
                armor: 0,
                weapon_right: 0,
                weapon_left: 0,
                range: 10,
                escape_life: 5,
                agility: 10,
                speed: 100,
                level: 1,
                attack_user: 1,
                action: 0,
                drop_money: 100,
                drop_item: 0,
                drop_item_rate: 0,
                drop_helmet: 0,
                drop_armor: 0,
                drop_necklace: 0,
                drop_ring: 0,
                drop_weapon: 0,
                drop_shield: 0,
                drop_boots: 0,
                drop_hp: 0,
                drop_mp: 0,
                magic_type: 0,
                magic_def: 0,
                magic_atk: 0,
                magic_rate: 0,
                ai_type: 0,
                stc_type: 1,
            }
          ]
        , {});
        return new Promise((resolve) => resolve(0));
      },
    
      down: (queryInterface, Sequelize) => {
        queryInterface.bulkDelete('cq_monstertype', null, {});
        return new Promise((resolve) => resolve(0));
      }
    };