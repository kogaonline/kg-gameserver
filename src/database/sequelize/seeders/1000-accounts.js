'use strict';

    module.exports = {
      up: (queryInterface, Sequelize) => {
        queryInterface.bulkInsert('accounts',
          [
            {
              id: 1,
              name: "adrian",
              username: "adrian",
              password: "0",
              status: 0,
              playerId: 0,
            }
          ]
        , {});
        return new Promise((resolve) => resolve(0));
      },
    
      down: (queryInterface, Sequelize) => {
        queryInterface.bulkDelete('accounts', null, {});
        return new Promise((resolve) => resolve(0));
      }
    };