'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        queryInterface.createTable('cq_generator', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.BIGINT(16)
            },
            mapid: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            startx: {
                allowNull: false,
                defaultValue: 50,
                type: Sequelize.INTEGER.UNSIGNED
            },
            starty: {
                allowNull: false,
                defaultValue: 50,
                type: Sequelize.BIGINT.UNSIGNED
            },
            endx: {
                allowNull: false,
                defaultValue: 50,
                type: Sequelize.BIGINT.UNSIGNED
            },
            endy: {
                allowNull: false,
                defaultValue: 50,
                type: Sequelize.BIGINT.UNSIGNED
            },
            max: {
                allowNull: false,
                defaultValue: 1,
                type: Sequelize.BIGINT.UNSIGNED
            },
            delay: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            peronce: {
                allowNull: false,
                defaultValue: 1,
                type: Sequelize.INTEGER.UNSIGNED
            },
            monstertype: {
                allowNull: false,
                defaultValue: 1,
                type: Sequelize.INTEGER.UNSIGNED
            },
            starttime: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            endtime: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            createdAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            }
        });

        return new Promise((resolve) => resolve(true));
    },
    down: (queryInterface, Sequelize) => {
        queryInterface.dropTable('cq_generator');
        return new Promise((resolve) => resolve(true));
    }
};
