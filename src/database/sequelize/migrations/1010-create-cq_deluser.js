'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        queryInterface.createTable('cq_deluser', {
            uid: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.BIGINT(16)
            },
            accountId: {
                type: Sequelize.INTEGER,
                references: {
                  model: 'accounts',
                  key: 'id',
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
            },
            name: {
                allowNull: false,
                defaultValue: "",
                type: Sequelize.STRING(16)
            },
            hairStyle: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            class: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            money: {
                allowNull: false,
                defaultValue: 100,
                type: Sequelize.BIGINT
            },
            cps: {
                allowNull: false,
                defaultValue: 100,
                type: Sequelize.BIGINT
            },
            cpsBound: {
                allowNull: false,
                defaultValue: 1000,
                type: Sequelize.BIGINT
            },
            treasurePoints: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT
            },
            body: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            face: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            level: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            strength: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            agility: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            vitality: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            spirit: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            attrPoints: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            hp: {
                allowNull: false,
                defaultValue: 100,
                type: Sequelize.INTEGER.UNSIGNED
            },
            mana: {
                allowNull: false,
                defaultValue: 100,
                type: Sequelize.INTEGER.UNSIGNED
            },
            mapId: {
                allowNull: false,
                defaultValue: 1010,
                type: Sequelize.INTEGER.UNSIGNED
            },
            x: {
                allowNull: false,
                defaultValue: 61,
                type: Sequelize.INTEGER.UNSIGNED
            },
            y: {
                allowNull: false,
                defaultValue: 109,
                type: Sequelize.INTEGER.UNSIGNED
            },
            pkPoints: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT.UNSIGNED
            },
            experience: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT.UNSIGNED
            },
            quizPoints: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT.UNSIGNED
            },
            prevMapId: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            reborn: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            firstClass: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            secondClass: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            firstRebornLevel: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            secondRebornLevel: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            spouse: {
                allowNull: false,
                defaultValue: "",
                type: Sequelize.STRING(16)
            },
            whPassword: {
                allowNull: false,
                defaultValue: "",
                type: Sequelize.STRING(16)
            },
            whMoney: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT.UNSIGNED
            },
            enlightenPoints: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT.UNSIGNED
            },
            heavenBlessing: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT.UNSIGNED
            },
            luckTime: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT.UNSIGNED
            },
            enlightments: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT.UNSIGNED
            },
            enlightmentTime: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT.UNSIGNED
            },
            guildId: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            guildRank: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            guildMoneyDonation: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT.UNSIGNED
            },
            guildCpsDonation: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT.UNSIGNED
            },
            vipLevel: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            virtuePoints: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT.UNSIGNED
            },
            prevX: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            prevY: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            clanId: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            clanDonation: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT.UNSIGNED
            },
            clanRank: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            subclass: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            subclassLevel: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            studyPoints: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT.UNSIGNED
            },
            lastLogin: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT.UNSIGNED
            },
            title: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            firstBuy: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            country: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            flower: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            namechange: {
                allowNull: false,
                defaultValue: "",
                type: Sequelize.STRING(16)
            },
            namechangeCount: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            racePoints: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT.UNSIGNED
            },
            multipleExp: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT.UNSIGNED
            },
            multipleExpTimes: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            onlineTrainingExp: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT.UNSIGNED
            },
            blessedHuntingExp: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT.UNSIGNED
            },
            royalPoints: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT.UNSIGNED
            },
            business: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.INTEGER.UNSIGNED
            },
            createdAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            }
        });

        return new Promise((resolve) => resolve(true));
    },
    down: (queryInterface, Sequelize) => {
        queryInterface.dropTable('cq_deluser');
        return new Promise((resolve) => resolve(true));
    }
};
