'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        queryInterface.createTable('cq_item', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            type: {
                type: Sequelize.INTEGER,
                references: {
                  model: 'cq_itemtype',
                  key: 'id',
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
            },
            owner: {
                type: Sequelize.BIGINT(16),
                references: {
                  model: 'cq_user',
                  key: 'uid',
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
            },
            player: {
                type: Sequelize.BIGINT(16),
                references: {
                  model: 'cq_user',
                  key: 'uid',
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
            },
            durability: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            max_durability: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            position: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            gem1: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            gem2: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            magic1: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            magic2: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            magic3: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            data: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            bless: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            enchant: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            anti_monster: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            color: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            progress_gem: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            progress_lev: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            monopoly: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            inscribed: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            artifact_type: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            artifact_start: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            artifact_expire: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            artifact_stabilization: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            stack: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            bound: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            warehouse: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            expiration: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            item_status: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            createdAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            }
        });

        return new Promise((resolve) => resolve(true));
    },
    down: (queryInterface, Sequelize) => {
        queryInterface.dropTable('cq_item');
        return new Promise((resolve) => resolve(true));
    }
};
