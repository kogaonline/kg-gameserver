'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        queryInterface.createTable('cq_action', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.BIGINT(16)
            },
            id_next: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT(16)
            },
            id_next_fail: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT(16)
            },
            type: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT(16)
            },
            data: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT(16)
            },
            param: {
                allowNull: false,
                defaultValue: "",
                type: Sequelize.STRING(500)
            },
            createdAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            }
        });

        return new Promise((resolve) => resolve(true));
    },
    down: (queryInterface, Sequelize) => {
        queryInterface.dropTable('cq_action');
        return new Promise((resolve) => resolve(true));
    }
};
