'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        queryInterface.createTable('cq_itemcompose', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            type: {
                type: Sequelize.INTEGER,
                references: {
                  model: 'cq_itemtype',
                  key: 'id',
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
            },
            plus: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            addHP: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            minAttack: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            maxAttack: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            defense: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            magicAttack: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            magicDefense: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            addAgility: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            addDodge: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            createdAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            }
        });

        return new Promise((resolve) => resolve(true));
    },
    down: (queryInterface, Sequelize) => {
        queryInterface.dropTable('cq_itemcompose');
        return new Promise((resolve) => resolve(true));
    }
};
