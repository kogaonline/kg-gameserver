'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        queryInterface.createTable('cq_levexp', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.BIGINT(16)
            },
            type: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT(16)
            },
            level: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT(16)
            },
            experience: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT(16)
            },
            experience2: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT(16)
            },
            experience3: {
                allowNull: false,
                defaultValue: 0,
                type: Sequelize.BIGINT(16)
            },
            createdAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            }
        });

        return new Promise((resolve) => resolve(true));
    },
    down: (queryInterface, Sequelize) => {
        queryInterface.dropTable('cq_levexp');
        return new Promise((resolve) => resolve(true));
    }
};
