'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        queryInterface.createTable('cq_magic', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            userId: {
                type: Sequelize.BIGINT(16),
                references: {
                  model: 'cq_user',
                  key: 'uid',
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
            },
            type: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            level: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            experience: {
                allowNull: false,
                type: Sequelize.BIGINT.UNSIGNED
            },
            createdAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            }
        });

        return new Promise((resolve) => resolve(true));
    },
    down: (queryInterface, Sequelize) => {
        queryInterface.dropTable('cq_magic');
        return new Promise((resolve) => resolve(true));
    }
};
