'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        queryInterface.createTable('cq_map', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            name: {
                allowNull: false,
                type: Sequelize.STRING
            },
            description: {
                allowNull: false,
                type: Sequelize.STRING
            },
            doctype: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            owner: {
                type: Sequelize.BIGINT(16),
                references: {
                  model: 'cq_user',
                  key: 'uid',
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
            },
            ownerType: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            group: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            weather: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            rebirthMap: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            rebirthPortal: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            portalX: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            portalY: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            color: {
                allowNull: false,
                type: Sequelize.INTEGER.UNSIGNED
            },
            createdAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            }
        });

        return new Promise((resolve) => resolve(true));
    },
    down: (queryInterface, Sequelize) => {
        queryInterface.dropTable('cq_map');
        return new Promise((resolve) => resolve(true));
    }
};
