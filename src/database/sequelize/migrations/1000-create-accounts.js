'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        queryInterface.createTable('accounts', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            name: {
                type: Sequelize.STRING,
            },
            username: {
                type: Sequelize.STRING,
            },
            password: {
                type: Sequelize.STRING,
            },
            ip: {
                type: Sequelize.STRING,
            },
            status: {
                type: Sequelize.INTEGER,
            },
            playerId: {
                type: Sequelize.INTEGER,
            },
            email: {
                type: Sequelize.STRING,
            },
            zip: {
                type: Sequelize.STRING,
            },
            address: {
                type: Sequelize.STRING,
            },
            address2: {
                type: Sequelize.STRING,
            },
            createdAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                defaultValue: "2020-01-01 00:00:00",
                type: Sequelize.DATE
            }
        });

        return new Promise((resolve) => resolve(true));
    },
    down: (queryInterface, Sequelize) => {
        queryInterface.dropTable('accounts');
        return new Promise((resolve) => resolve(true));
    }
};
