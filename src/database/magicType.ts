import Core from "../core";
import IndexedElement from "../components/indexedElement";
import DBService from "../services/database";

export interface IMagicType {
    id: number;
    type: number;
    sort: number;
    name: string;
    pk: number;
    ground: number;
    multi: number;
    target: number;
    level: number;
    manacost: number;
    power: number;
    speed: number;
    percent: number;
    seconds: number;
    range: number;
    distance: number;
    attr: number;
    status: number;
    req_prof: number;
    req_exp: number;
    req_level: number;
    xp: number;
    weapon: number;
    times: number;
    auto: number;
    floor: number;
    auto_learn: number;
    learn_level: number;
    drop_weapon: number;
    stamina: number;
    weapon_hit: number;
    use_item: number;
    next_magic: number;
    delay: number;
    use_item_num: number;
    upgradecost: number;
}

export default class RMagicType {
    static async load() {
        Core.magicInfo = new IndexedElement<IMagicType>();
        let items = DBService.getResults<IMagicType>(await DBService.Models.cq_magictype.findAll());
        await items.mapAsync((i: IMagicType) => {
            Core.magicInfo.add(i.id, i);
        });

        console.log(`Loaded ${Core.magicInfo.size} magic infos.`)
    }

    static getMagicInfo(type: number, level: number): IMagicType {
        let info = null;
        Core.magicInfo.map((mag) => {
            if (mag.type === type && mag.level === level)
                info = mag
        })

        return info
    }
}