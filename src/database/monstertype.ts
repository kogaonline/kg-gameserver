import Core from "../core";
import IndexedElement from "../components/indexedElement";
import DBService from "../services/database";

export interface IMonsterType {
    id: number;
    name: string;
    type: number;
    body: number;
    life: number;
    mana: number;
    min_attack: number;
    max_attack: number;
    defense: number;
    dodge: number;
    helmet: number;
    armor: number;
    weapon_right: number;
    weapon_left: number;
    view: number;
    range: number;
    escape_life: number;
    agility: number;
    speed: number;
    level: number;
    attack_user: number;
    action: number;
    drop_money: number;
    drop_item: number;
    drop_item_rate: number;
    drop_helmet: number;
    drop_armor: number;
    drop_necklace: number;
    drop_ring: number;
    drop_weapon: number;
    drop_shield: number;
    drop_boots: number;
    drop_hp: number;
    drop_mp: number;
    magic_type: number;
    magic_def: number;
    magic_atk: number;
    magic_rate: number;
    ai_type: number;
    stc_type: number;
}

export default class RMonsterType {
    static async load() {
        Core.monsterInfo = new IndexedElement<IMonsterType>();
        let infos = DBService.getResults<IMonsterType>(await DBService.Models.cq_monstertype.findAll());
        await infos.mapAsync((i: IMonsterType) => {
            Core.monsterInfo.add(i.id, i);
        });

        console.log(`Loaded ${Core.monsterInfo.size} monster infos.`);
    }

    static getMonsterInfo(type: number): IMonsterType {
        let info = null;
        Core.monsterInfo.map((inf) => {
            if (inf.id === type)
                info = inf
        });

        return info;
    }
}