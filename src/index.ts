import * as colors from 'colors'
import * as Settings from './settings'
import Core from './core'
import GameServerSocket from './network/sockets/gameSocket'
import {createSnapshot, loadSnapshot} from './services/snapshoting';
import NPCServerSocket from './network/sockets/npcSocket';

export default class App {
    static server = new GameServerSocket();
    static npcserver = new NPCServerSocket();

    static start(){
        new Promise(async (resolve: any, reject: any) => {
            Core.setupConsole();

            console.log(`--------`);
            console.log(colors.bgCyan(`Starting Koga Server (PID ${process.pid})`));

            await Core.prepareDatabase();

            // All ok, start server
            resolve(true);

        }).then(() => {
            if (Settings.DEV_MODE){
                if (process.platform === "win32"){
                    setInterval(createSnapshot, 10000);
                } else {
                    // process.on('SIGKILL', createSnapshot);
                    process.on('SIGINT', createSnapshot);
                    process.on('SIGUSR2', createSnapshot);
                }
                //fakeLoad();
                loadSnapshot();
            }

            let port = Settings.DEV_MODE ? Settings.DS_PORT : Settings.GS_PORT;
            App.server.start(port);
            
            console.log(`Server started at port ${port}. Ready for new connections...`);
            
            let npcport = Settings.NS_PORT;
            App.npcserver.start(npcport);
            
            console.log(`Waiting for NPC Server at port ${npcport}...`);

        }).catch((err) => {
            console.error(err);
        });
    }
}

App.start();