import { ETitleType } from "../enums/ETitleType";
import { EPKMode } from "../enums/EPKMode";
import { EStatusEffect } from "../enums/EStatusEffect";
import Player from "./player";
import { EAppearanceType } from "../enums/EAppearanceType";
import { EDirection } from "../enums/EDirection";
import { ENobilityRank } from "../enums/ENobilityRank";
import ICharacter from "../database/sequelize/models/cq_user";
import { EUpdateType } from "../enums/EUpdateType";
import MsgUserAttrib from "../network/packets/gamepackets/MsgUserAttrib";
import IUpdate from "../interfaces/IUpdate";
import MsgAction from "../network/packets/gamepackets/MsgAction";
import { EMsgActionType } from "../enums/EMsgActionType";
import IPacket from "../interfaces/IPacket";
import Core from "../core";
import MsgMapInfo from "../network/packets/gamepackets/MsgMapInfo";
import Map from "./map";
import MsgWalk from "../network/packets/gamepackets/MsgWalk";
import IMapElement from "../interfaces/IMapElement";
import ICharacterAttr from "../interfaces/ICharacterAttr";
import { EMapElementType } from "../enums/EMapElementType";
import { EAction } from "../enums/EAction";
import { EItemPosition } from "../enums/EItemPosition";
import MsgItemInfo from "../network/packets/gamepackets/MsgItemInfo";
import MsgMapItem from "../network/packets/gamepackets/MsgMapItem";
import DBService from "../services/database";
import IndexedElement from "../components/indexedElement";
import MsgWeaponSkill from "../network/packets/gamepackets/MsgWeaponSkill";
import MsgInteract from "../network/packets/gamepackets/MsgInteract";
import { EInteractionType } from "../enums/EInteractionType";
import MsgMagicInfo from "../network/packets/gamepackets/MsgMagicInfo";
import { sanitizeInt, sanitizeUshort, printBuffer, distanceBetweenCharacters } from "../logical/kernel";
import { IMagicType } from "../database/magicType";
import RMagic from "../database/magic";
import RMaps from "../database/map";
import MsgName from "../network/packets/gamepackets/MsgName";
import { EStringType } from "../enums/EStringType";
import NpcMonsterPos from "../network/packets/npcserver/NpcMonsterPos";
import App from "../";
import { IMonsterType } from "../database/monstertype";
import NpcMonsterKill from "../network/packets/npcserver/NpcMonsterKill";
import { EPostCmdType } from "../enums/EPostCmdType";
import { NPCServerController } from "../network/packets/npcserver/npcscontroller";
import { GAME_SCREEN_SIZE } from "../settings";
import { ECharacterAttrType } from "../enums/ECharacterAttrType";
import { GroundItem } from "../game/map";
import { EActionTaskType } from "../enums/EActionTaskType";
import { cnRes } from "../res/cnRes";
import { dropGold } from "./monster";

const bn = require(`bignum`);

const spawnOffsets = {
    timestamp: 4,
    mesh: 8,
    uid: 12,
    guildId: 16,
    guildRank: 20,
    statusFlag: 26,
    statusFlag2: 34,
    statusFlag3: 42,
    appearanceType: 46,
    head: 48,
    garment: 52,
    armor: 56,
    leftWeapon: 60,
    rightWeapon: 64,
    leftWeaponAccessory: 68,
    rightWeaponAccessory: 72,
    steed: 76,
    mountArmor: 80,
    hp: 90,
    monsterLevel: 96,
    x: 98,
    y: 100,
    hairStyle: 102,
    facing: 104,
    action: 105,
    reborn: 112,
    level: 113,
    windowSpawn: 115,
    away: 116,
    extraBP: 117,
    flowerIcon: 129,
    flowerType: 133,
    nobilityRank: 137,
    armorColor: 141,
    leftWeaponColor: 143,
    rightWeaponColor: 144,
    headColor: 145,
    quizPoints: 146,
    steedPlus: 151,
    steedColor: 157,
    clanUid: 173,
    clanRank: 177,
    title: 185,
    size: 187,
    glow: 196,
    boss: 199,
    headSoul: 200,
    armorSoul: 204,
    leftWeaponSoul: 208,
    rightWeaponSoul: 212,
    activeSubclass: 216,
    firstClass: 225,
    secondClass: 227,
    class: 229,
    country: 231,
    assassinsBP: 237,
    jiangHuTalent: 241,
    jiangHuActive: 242,
    names: 253
};

export default class Character implements ICharacter, IMapElement {
    player: Player;
    accountId: number = 0;

    weaponSkills: IndexedElement<MsgWeaponSkill>;
    magics: IndexedElement<MsgMagicInfo>;

    mapElementType = EMapElementType.PLAYER;
    monsterType: IMonsterType;

    lastAttack: MsgInteract;

    isSpecial: boolean;
    isMonster: boolean;
    isCompanion: boolean;
    invincible: boolean;

    // Properties
    uid: number = 0;
    subclass: number = 0;
    subclassLevel: number = 0;

    title: ETitleType = ETitleType.GoldenRacer;
    private _name: string;
    get name(){
        return this._name;
    }
    set name(val: string){
        if (val.endsWith('[PM]') || val.endsWith('[GM]') || val.startsWith('[PM]') || val.startsWith('[GM]'))
            this.isSpecial = true;
        this._name = val;
    }
    spouse: string = "";
    clanName: string = "";

    extraMagicAttrs: IndexedElement<ICharacterAttr> = new IndexedElement();
    extraItemAttrs: IndexedElement<ICharacterAttr> = new IndexedElement();

    valHolder: any = {}
    
    maxhp: number = 0;
    private _hp: number = 0;
    get hp(): number {
        return this._hp;
    }
    set hp(val: number) {
        if (Number.isNaN(val))
            val = 0;
        if (val < 0)
            val = 0;
        if (val > this.maxhp)
            val = this.maxhp;

        this._hp = val;
        this.sendUpdate(EUpdateType.HITPOINTS, val);
    }

    private _mana: number = 0;
    get mana(): number {
        return this._mana;
    }
    set mana(val: number) {
        if (val < 0)
            val = 0;
        if (!this.isMonster && val > this.maxmana)
            val = this.maxmana;

        this._mana = val;
        this.sendUpdate(EUpdateType.MANA, val);
    }
    maxmana: number = 0;
    addHP: number = 0;
    addMP: number = 0;

    minAttack: number = 0;
    maxAttack: number = 0;
    magicAttack: number = 0;
    magicDefense: number = 0;
    defense: number = 0;
    accuracy: number = 0;
    speed: number = 0;

    reduceDamagePercent: number = 0;
    bonusAttackPercent: number = 0;
    bonusMagicPercent: number = 0;
    dodgePercent: number = 0;


    finalAttack: number = 0;
    finalMagicAttack: number = 0;
    finalDefense: number = 0;
    finalMagicDefense: number = 0;

    private _xpcircle: number = 0;
    get xpcircle(): number {
        return this._xpcircle;
    }
    set xpcircle(val: number) {
        if (Number.isNaN(val))
            val = 0;
        if (val < 0)
            val = 0;

        this._xpcircle = val;
        this.sendUpdate(EUpdateType.XP_CIRCLE, val);
    }

    private _strength: number = 0;
    get strength(): number {
        return this._strength;
    }
    set strength(val: number) {
        this._strength = val;
        this.sendUpdate(EUpdateType.STRENGTH, val);
        this.recalculateStats();
    }

    private _agility: number = 0;
    get agility(): number {
        return this._agility;
    }
    set agility(val: number) {
        this._agility = val;
        this.sendUpdate(EUpdateType.AGILITY, val);
        this.recalculateStats();
    }

    private _spirit: number = 0;
    get spirit(): number {
        return this._spirit;
    }
    set spirit(val: number) {
        this._spirit = val;
        this.sendUpdate(EUpdateType.SPIRIT, val);
        this.recalculateStats();
    }

    private _vitality: number = 0;
    get vitality(): number {
        return this._vitality;
    }
    set vitality(val: number) {
        this._vitality = val;
        this.sendUpdate(EUpdateType.VITALITY, val);
        this.recalculateStats();
    }

    private _attrPoints: number = 0;
    get attrPoints(): number {
        return this._attrPoints;
    }
    set attrPoints(val: number) {
        this._attrPoints = val;
        this.sendUpdate(EUpdateType.ATRIBUTES, val);
    }

    private _attackRange: number = 0;
    get attackRange(): number {
        return this._attackRange;
    }
    set attackRange(val: number) {
        if (val < 1)
            val = 1;
        if (val > 30)
            val = 30;
        this._attackRange = val;
    }
    azureShieldHealth: number = 0;

    detoxication: number = 0;
    immunity: number = 0;
    breakthrough: number = 0;
    criticalStrike: number = 0;
    skillCriticalStrike: number = 0;
    intensification: number = 0;
    block: number = 0;
    penetration: number = 0;
    counteraction: number = 0;

    metalResistance: number = 0;
    woodResistance: number = 0;
    waterResistance: number = 0;
    fireResistance: number = 0;
    earthResistance: number = 0;

    alterantiveEquipment: boolean = false;

    private _transformId: number = 0;
    get transformId(): number {
        return this._transformId;
    }
    set transformId(val: number) {
        this._transformId = val;
        let mesh = this.getMesh();
        this.sendUpdate(EUpdateType.MESH, mesh);
    }
    private _body: number = 0;
    get body(): number {
        return this._body;
    }
    set body(val: number) {
        this._body = val;
        let mesh = this.getMesh();
        this.sendUpdate(EUpdateType.MESH, mesh);
    }
    private _face: number = 0;
    get face(): number {
        return this._face;
    }
    set face(val: number) {
        this._face = val;
        let mesh = this.getMesh();
        this.sendUpdate(EUpdateType.MESH, mesh);
    }

    private _statusFlag: any = bn(0);
    get statusFlag(): any {
        return this._statusFlag;
    }
    set statusFlag(val: any) {
        val = bn(val).abs()
        if (val !== this._statusFlag) {
            this._statusFlag = val;
            this.sendUpdate(EUpdateType.STATUS_FLAG, [
                val.toNumber(),
                this.statusFlag2.toNumber(),
                this.statusFlag3.toNumber()
            ]);
            //this.sendToScreen(this.getSpawnPacket());
        }
    }
    private _statusFlag2: any = bn(0);
    get statusFlag2(): any {
        return this._statusFlag2;
    }
    set statusFlag2(val: any) {
        val = bn(val).abs()
        if (val !== this._statusFlag2) {
            this._statusFlag2 = val;
            this.sendUpdate(EUpdateType.STATUS_FLAG, [
                this.statusFlag,
                val,
                this.statusFlag3
            ]);
            //this.sendToScreen(this.getSpawnPacket());
        }
    }
    private _statusFlag3: any = bn(0);
    get statusFlag3(): any {
        return this._statusFlag3;
    }
    set statusFlag3(val: any) {
        val = bn(val).abs()
        if (val !== this._statusFlag3) {
            this._statusFlag3 = val;
            this.sendUpdate(EUpdateType.STATUS_FLAG, [
                this.statusFlag,
                this.statusFlag2,
                val
            ]);
            //this.sendToScreen(this.getSpawnPacket());
        }
    }

    flowerType: number = 0;

    nobilityRankDonation: number = 0;
    nobilityRank: ENobilityRank = ENobilityRank.King;

    enlightments: number = 0;
    enlightenPoints: number = 0;
    receivedEnlightenPoints: number = 0;
    enlightmentTime: number = 0;

    private _heavenBlessing: number = 0;
    get heavenBlessing(): number {
        return this._heavenBlessing;
    }
    set heavenBlessing(val: number) {
        if (val > 0 && this._heavenBlessing <= val) {
            this._heavenBlessing = val;
            this.addStatus(EStatusEffect.HeavenBlessing);
            this.sendUpdate(EUpdateType.HEAVENS_BLESSING, val);
        }
    }

    xpKillCount: number = 0;

    valueOf: {
        spell: { [key: number]: number },
        attr: { [key: number]: number },
        status: { [key: string]: number },
    }

    timeOf: {
        spell: { [key: number]: number },
        drugsDisabled: number,
        death: number,
        staminaRecharge: number,
        flashingName: number,
        pkPointDiminished: number,
        xpCircleList: number,
        xpList: number,
        lastAttack: number,
    }

    get dead(): boolean {
        return this.hp <= 0;
    }

    pkMode: EPKMode = EPKMode.Capture;
    private _pkPoints: number = 0;
    get pkPoints(): number {
        return this._pkPoints;
    }
    set pkPoints(val: number) {
        if (val < 0)
            val = 0;
        this._pkPoints = val;
        this.sendUpdate(EUpdateType.PK_POINTS, val);
        if (this.fullyLoaded) {
            if (val >= 100) {
                if (!this.hasStatus(EStatusEffect.BlackName)) {
                    this.removeStatus(EStatusEffect.RedName);
                    this.addStatus(EStatusEffect.BlackName);
                }
            } else if (val >= 30) {
                if (!this.hasStatus(EStatusEffect.RedName)) {
                    this.removeStatus(EStatusEffect.BlackName);
                    this.addStatus(EStatusEffect.RedName);
                }
            } else {
                if (this.hasStatus(EStatusEffect.BlackName))
                    this.removeStatus(EStatusEffect.BlackName);
                if (this.hasStatus(EStatusEffect.RedName))
                    this.removeStatus(EStatusEffect.RedName);
            }
        }
    }

    firstClass: number = 0;
    secondClass: number = 0;
    firstRebornLevel: number = 0;
    secondRebornLevel: number = 0;

    private _class: number = 0;
    get class(): number {
        return this._class;
    }
    set class(val: number) {
        this._class = val;
        this.sendUpdate(EUpdateType.CLASS, val);
        this.calculateMaxHp();
    }

    private _reborn: number = 0;
    get reborn(): number {
        return this._reborn;
    }
    set reborn(val: number) {
        this._reborn = val;
        this.sendUpdate(EUpdateType.REBORN, val);
        this.calculateMaxHp();
    }

    private _level: number = 0;
    get level(): number {
        return this._level;
    }
    set level(val: number) {
        this._level = val;
        this.experience = 0;
        this.sendUpdate(EUpdateType.LEVEL, val);

        let update = new MsgAction(true);
        update.uid = this.uid;
        update.id = EMsgActionType.Leveled;
        this.sendToScreen(update);

        this.calculateMaxHp();
    }

    business: number = 0;

    private _experience: number = 0;
    get experience(): number {
        return this._experience;
    }
    set experience(val: number) {
        this._experience = val;
        this.sendUpdate(EUpdateType.EXPERIENCE, val);
    }
    private _money: number = 0;
    get money(): number {
        return this._money;
    }
    set money(val: number) {
        this._money = val;
        this.sendUpdate(EUpdateType.MONEY, val);
    }
    private _cps: number = 0;
    get cps(): number {
        return this._cps;
    }
    set cps(val: number) {
        this._cps = val;
        this.sendUpdate(EUpdateType.CONQUER_POINTS, val);
    }
    private _cpsBound: number = 0;
    get cpsBound(): number {
        return this._cpsBound;
    }
    set cpsBound(val: number) {
        this._cpsBound = val;
        this.sendUpdate(EUpdateType.BOUND_CONQUER_POINTS, [val, 0, 0]);
    }
    quizPoints: number = 0;
    treasurePoints: number = 0;
    private _hairStyle: number = 0;
    get hairStyle(): number {
        return this._hairStyle;
    }
    set hairStyle(val: number) {
        this._hairStyle = val;
        this.sendToScreen(this.getSpawnPacket());
    }
    private _stamina: number = 0;
    get stamina(): number {
        return this._stamina;
    }
    set stamina(val: number) {
        let max = this.heavenBlessing > 0 ? 150 : 100
        if (val < 0)
            val = 0
        if (val > max)
            val = max

        this._stamina = val;
        this.sendUpdate(EUpdateType.STAMINA, val);
    }

    prevMapId: number = 0;
    mapId: number = 0;
    x: number = 0;
    y: number = 0;
    get map(): Map {
        let _map = Core.maps.get(this.mapId);
        return _map ? _map : new Map(9999, 1020);
    }
    _facing: EDirection = EDirection.SouthEast;
    get facing(): EDirection {
        return this._facing;
    }
    set facing(val: EDirection) {
        this._facing = val;
    }
    action: EAction = EAction.None;


    battlePower: number = 0;
    extraBP: number = 0;
    _whPassword: string = "";
    get whPassword(): string {
        return this._whPassword;
    }
    set whPassword(val: string) {
        this._whPassword = val;
    }

    private _whMoney: number = 0;
    get whMoney(): number {
        return this._whMoney;
    }
    set whMoney(val: number) {
        this._whMoney = val;
        this.sendUpdate(EUpdateType.WH_MONEY, val);
    }

    guildId: number = 0;
    guildRank: number = 0;

    appearanceType: EAppearanceType = EAppearanceType.Default;
    away: boolean = false;
    boss: boolean = false;

    namechange: string = "";
    namechangeCount: number = 0;


    luckTime: number = 0;
    guildMoneyDonation: number = 0;
    guildCpsDonation: number = 0;
    private _vipLevel: number = 0;
    get vipLevel(): number {
        return this._vipLevel;
    }
    set vipLevel(val: number) {
        this._vipLevel = val;
        this.sendUpdate(EUpdateType.VIP_LEVEL, val);
    }

    virtuePoints: number = 0;
    prevX: number = 0;
    prevY: number = 0;
    clanId: number = 0;
    clanDonation: number = 0;
    clanRank: number = 0;
    subClass: number = 0;
    subClassLevel: number = 0;
    studyPoints: number = 0;
    lastLogin: number = 0;
    private _firstBuy: number = 0;
    get firstBuy(): number {
        return this._firstBuy;
    }
    set firstBuy(val: number) {
        this._firstBuy = val;
        this.sendUpdate(EUpdateType.FIRST_BUY, val);
    }
    country: number = 0;
    flower: number = 0;
    racePoints: number = 0;
    private _expTime: number = 0;
    get multipleExp(): number {
        return this._expTime;
    }
    set multipleExp(val: number) {
        this._expTime = val;
        this.sendUpdate(EUpdateType.DOUBLE_EXP_TIMER, val);
    }
    multipleExpTimes: number = 0;
    onlineTrainingExp: number = 0;
    blessedHuntingExp: number = 0;
    royalPoints: number = 0;

    fullyLoaded: boolean = false;

    static defaultAttr: any = {
        40: [[7, 0, 2, 1], [9, 0, 3, 1], [11, 0, 4, 1], [13, 0, 4, 2], [15, 0, 5, 2], [18, 0, 5, 2], [20, 0, 6, 2], [21, 0, 7, 3], [23, 0, 8, 3], [26, 0, 8, 3], [28, 0, 8, 4], [30, 0, 9, 4], [32, 0, 10, 4], [34, 0, 10, 5], [35, 0, 12, 5], [38, 0, 12, 5], [40, 0, 12, 6], [42, 0, 13, 6], [45, 0, 13, 6], [46, 0, 14, 7], [49, 0, 14, 7], [51, 0, 15, 7], [53, 0, 16, 7], [55, 0, 16, 8], [57, 0, 17, 8], [59, 0, 17, 9], [61, 0, 18, 9], [63, 0, 19, 9], [65, 0, 19, 10], [67, 0, 20, 10], [70, 0, 20, 10], [72, 0, 21, 10], [74, 0, 22, 10], [76, 0, 22, 11], [78, 0, 23, 11], [80, 0, 23, 12], [82, 0, 24, 12], [84, 0, 25, 12], [87, 0, 25, 12], [90, 0, 25, 12], [91, 0, 26, 13], [93, 0, 27, 13], [95, 0, 28, 13], [97, 0, 29, 13], [99, 0, 29, 14], [101, 0, 29, 15], [103, 0, 30, 15], [105, 0, 31, 15], [107, 0, 31, 16], [109, 0, 32, 16], [112, 0, 32, 16], [114, 0, 33, 16], [116, 0, 34, 16], [118, 0, 34, 17], [120, 0, 35, 17], [122, 0, 35, 18], [124, 0, 36, 18], [126, 0, 37, 18], [128, 0, 37, 19], [130, 0, 38, 19], [133, 0, 38, 19], [135, 0, 39, 19], [137, 0, 40, 19], [139, 0, 40, 20], [141, 0, 41, 20], [143, 0, 41, 21], [145, 0, 42, 21], [147, 0, 43, 21], [149, 0, 43, 22], [150, 0, 45, 22], [153, 0, 45, 22], [156, 0, 45, 22], [158, 0, 46, 22], [160, 0, 46, 23], [162, 0, 47, 23], [164, 0, 48, 23], [166, 0, 48, 24], [168, 0, 49, 24], [170, 0, 49, 25], [172, 0, 50, 25], [174, 0, 51, 25], [177, 0, 51, 25], [179, 0, 52, 25], [181, 0, 52, 26], [183, 0, 53, 26], [185, 0, 54, 26], [187, 0, 54, 27], [189, 0, 55, 27], [191, 0, 55, 28], [193, 0, 56, 28], [195, 0, 57, 28], [198, 0, 57, 28], [200, 0, 58, 28], [202, 0, 58, 29], [204, 0, 59, 29], [206, 0, 60, 29], [208, 0, 60, 30], [210, 0, 61, 30], [213, 0, 60, 31], [215, 0, 60, 32], [217, 0, 61, 32], [219, 0, 62, 32], [221, 0, 63, 32], [223, 0, 64, 32], [225, 0, 65, 32], [227, 0, 66, 32], [229, 0, 66, 33], [231, 0, 67, 33], [233, 0, 67, 34], [235, 0, 68, 34], [237, 0, 69, 34], [240, 0, 69, 34], [242, 0, 70, 34], [244, 0, 70, 35], [246, 0, 71, 35], [249, 0, 71, 35], [250, 0, 72, 36], [252, 0, 73, 36], [254, 0, 73, 37], [256, 0, 74, 37]],
        100: [[2, 5, 0, 3], [3, 6, 0, 4], [3, 8, 0, 5], [4, 9, 0, 6], [4, 11, 0, 7], [5, 12, 0, 8], [6, 14, 0, 8], [6, 16, 0, 9], [7, 17, 0, 10], [7, 19, 0, 11], [8, 20, 0, 12], [9, 21, 0, 13], [9, 23, 0, 14], [10, 24, 0, 15], [10, 25, 0, 17], [11, 27, 0, 17], [12, 29, 0, 17], [12, 32, 0, 17], [13, 34, 0, 17], [14, 36, 0, 17], [14, 38, 0, 18], [15, 40, 0, 18], [15, 43, 0, 18], [16, 44, 0, 19], [16, 47, 0, 19], [17, 49, 0, 19], [18, 51, 0, 19], [18, 53, 0, 20], [19, 55, 0, 20], [20, 57, 0, 20], [20, 59, 0, 21], [21, 61, 0, 21], [21, 64, 0, 21], [22, 66, 0, 21], [22, 68, 0, 22], [23, 70, 0, 22], [24, 72, 0, 22], [25, 74, 0, 22], [25, 77, 0, 22], [25, 80, 0, 22], [26, 81, 0, 23], [27, 82, 0, 24], [27, 85, 0, 24], [28, 87, 0, 24], [28, 89, 0, 25], [29, 91, 0, 25], [30, 93, 0, 25], [30, 95, 0, 26], [31, 97, 0, 26], [31, 100, 0, 26], [32, 102, 0, 26], [32, 104, 0, 27], [33, 106, 0, 27], [34, 108, 0, 27], [34, 110, 0, 28], [35, 112, 0, 28], [36, 114, 0, 28], [36, 116, 0, 29], [37, 118, 0, 29], [38, 120, 0, 29], [38, 122, 0, 30], [39, 124, 0, 30], [39, 127, 0, 30], [40, 129, 0, 30], [40, 131, 0, 31], [41, 133, 0, 31], [42, 135, 0, 31], [43, 137, 0, 31], [43, 139, 0, 32], [45, 140, 0, 32], [45, 143, 0, 32], [45, 145, 0, 33], [45, 148, 0, 33], [46, 150, 0, 33], [46, 152, 0, 34], [47, 154, 0, 34], [48, 156, 0, 34], [48, 158, 0, 35], [49, 160, 0, 35], [50, 162, 0, 35], [50, 164, 0, 36], [51, 166, 0, 36], [51, 169, 0, 36], [52, 171, 0, 36], [52, 173, 0, 37], [53, 175, 0, 37], [54, 177, 0, 37], [54, 179, 0, 38], [55, 181, 0, 38], [55, 184, 0, 38], [56, 186, 0, 38], [56, 188, 0, 39], [57, 190, 0, 39], [58, 192, 0, 39], [58, 194, 0, 40], [59, 196, 0, 40], [60, 198, 0, 40], [60, 200, 0, 41], [60, 203, 0, 41], [60, 205, 0, 42], [61, 207, 0, 42], [62, 209, 0, 42], [63, 211, 0, 42], [64, 213, 0, 42], [64, 215, 0, 43], [65, 217, 0, 43], [65, 219, 0, 44], [65, 221, 0, 45], [65, 223, 0, 46], [65, 225, 0, 47], [66, 227, 0, 47], [67, 229, 0, 47], [67, 232, 0, 47], [68, 234, 0, 47], [69, 236, 0, 47], [70, 238, 0, 47], [71, 240, 0, 47], [72, 242, 0, 47], [73, 244, 0, 47], [75, 245, 0, 47]],
        10: [[2, 0, 5, 3], [2, 0, 7, 4], [3, 0, 8, 5], [4, 0, 10, 5], [5, 0, 11, 6], [5, 0, 13, 7], [6, 0, 14, 8], [6, 0, 16, 9], [7, 0, 17, 10], [7, 0, 19, 11], [8, 0, 20, 12], [8, 0, 22, 13], [9, 0, 23, 14], [10, 0, 25, 14], [10, 0, 28, 14], [11, 0, 28, 16], [12, 0, 29, 17], [12, 0, 31, 18], [13, 0, 32, 19], [13, 0, 34, 20], [14, 0, 35, 21], [14, 0, 37, 22], [15, 0, 38, 23], [15, 0, 40, 24], [16, 0, 41, 25], [17, 0, 43, 25], [18, 0, 44, 26], [18, 0, 46, 27], [19, 0, 47, 28], [19, 0, 49, 29], [20, 0, 50, 30], [20, 0, 52, 31], [21, 0, 53, 32], [21, 0, 55, 33], [22, 0, 56, 34], [23, 0, 58, 34], [24, 0, 59, 35], [24, 0, 61, 36], [25, 0, 62, 37], [25, 0, 65, 37], [26, 0, 65, 39], [26, 0, 67, 40], [27, 0, 68, 41], [27, 0, 70, 42], [28, 0, 71, 43], [29, 0, 73, 43], [30, 0, 74, 44], [30, 0, 76, 45], [31, 0, 77, 46], [31, 0, 79, 47], [32, 0, 80, 48], [32, 0, 82, 49], [33, 0, 83, 50], [33, 0, 85, 51], [34, 0, 86, 52], [35, 0, 88, 52], [36, 0, 89, 53], [36, 0, 91, 54], [37, 0, 92, 55], [37, 0, 94, 56], [38, 0, 95, 57], [38, 0, 97, 58], [39, 0, 98, 59], [39, 0, 100, 60], [40, 0, 101, 61], [41, 0, 103, 61], [42, 0, 104, 62], [42, 0, 106, 63], [42, 0, 108, 64], [42, 0, 110, 65], [44, 0, 110, 66], [44, 0, 112, 67], [45, 0, 113, 68], [45, 0, 115, 69], [46, 0, 116, 70], [47, 0, 118, 70], [48, 0, 119, 71], [48, 0, 121, 72], [49, 0, 122, 73], [49, 0, 124, 74], [50, 0, 125, 75], [50, 0, 127, 76], [51, 0, 128, 77], [51, 0, 130, 78], [52, 0, 131, 79], [53, 0, 133, 79], [54, 0, 134, 80], [54, 0, 136, 81], [55, 0, 137, 82], [55, 0, 139, 83], [56, 0, 140, 84], [56, 0, 142, 85], [57, 0, 143, 86], [57, 0, 145, 87], [58, 0, 146, 88], [59, 0, 148, 88], [60, 0, 149, 89], [60, 0, 151, 90], [60, 0, 153, 91], [60, 0, 155, 92], [61, 0, 156, 93], [62, 0, 157, 94], [63, 0, 158, 95], [63, 0, 160, 96], [64, 0, 161, 97], [65, 0, 163, 97], [66, 0, 164, 98], [66, 0, 166, 99], [67, 0, 167, 100], [67, 0, 170, 100], [68, 0, 170, 102], [68, 0, 172, 103], [69, 0, 173, 104], [69, 0, 175, 105], [70, 0, 176, 106], [71, 0, 178, 106], [72, 0, 179, 107], [72, 0, 181, 108], [73, 0, 182, 109], [72, 0, 185, 110]],
        20: [[2, 0, 5, 3], [2, 0, 7, 4], [3, 0, 8, 5], [4, 0, 10, 5], [5, 0, 11, 6], [5, 0, 13, 7], [6, 0, 14, 8], [6, 0, 16, 9], [7, 0, 17, 10], [7, 0, 19, 11], [8, 0, 20, 12], [8, 0, 22, 13], [9, 0, 23, 14], [10, 0, 25, 14], [10, 0, 28, 14], [11, 0, 28, 16], [12, 0, 29, 17], [12, 0, 32, 17], [13, 0, 34, 17], [14, 0, 36, 17], [14, 0, 38, 18], [15, 0, 40, 18], [15, 0, 43, 18], [16, 0, 44, 19], [16, 0, 47, 19], [17, 0, 49, 19], [18, 0, 51, 19], [18, 0, 53, 20], [19, 0, 55, 20], [20, 0, 57, 20], [20, 0, 59, 21], [21, 0, 61, 21], [21, 0, 64, 21], [22, 0, 66, 21], [22, 0, 68, 22], [23, 0, 70, 22], [24, 0, 72, 22], [25, 0, 74, 22], [25, 0, 77, 22], [25, 0, 80, 22], [26, 0, 81, 23], [27, 0, 82, 24], [27, 0, 85, 24], [28, 0, 87, 24], [28, 0, 89, 25], [29, 0, 91, 25], [30, 0, 93, 25], [30, 0, 95, 26], [31, 0, 97, 26], [31, 0, 100, 26], [32, 0, 102, 26], [32, 0, 104, 27], [33, 0, 106, 27], [34, 0, 108, 27], [34, 0, 110, 28], [35, 0, 112, 28], [36, 0, 114, 28], [36, 0, 116, 29], [37, 0, 118, 29], [38, 0, 120, 29], [38, 0, 122, 30], [39, 0, 124, 30], [39, 0, 127, 30], [40, 0, 129, 30], [40, 0, 131, 31], [41, 0, 133, 31], [42, 0, 135, 31], [43, 0, 137, 31], [43, 0, 139, 32], [45, 0, 140, 32], [45, 0, 143, 32], [45, 0, 145, 33], [45, 0, 148, 33], [46, 0, 150, 33], [46, 0, 152, 34], [47, 0, 154, 34], [48, 0, 156, 34], [48, 0, 158, 35], [49, 0, 160, 35], [50, 0, 162, 35], [50, 0, 164, 36], [51, 0, 166, 36], [51, 0, 169, 36], [52, 0, 171, 36], [52, 0, 173, 37], [53, 0, 175, 37], [54, 0, 177, 37], [54, 0, 179, 38], [55, 0, 181, 38], [55, 0, 184, 38], [56, 0, 186, 38], [56, 0, 188, 39], [57, 0, 190, 39], [58, 0, 192, 39], [58, 0, 194, 40], [59, 0, 196, 40], [60, 0, 198, 40], [60, 0, 200, 41], [60, 0, 203, 41], [60, 0, 205, 42], [61, 0, 207, 42], [62, 0, 209, 42], [63, 0, 211, 42], [64, 0, 213, 42], [64, 0, 215, 43], [65, 0, 217, 43], [65, 0, 219, 44], [65, 0, 221, 45], [65, 0, 223, 46], [65, 0, 225, 47], [66, 0, 227, 47], [67, 0, 229, 47], [67, 0, 232, 47], [68, 0, 234, 47], [69, 0, 236, 47], [70, 0, 238, 47], [71, 0, 240, 47], [72, 0, 242, 47], [73, 0, 244, 47], [75, 0, 245, 47]],
        50: [[8, 0, 1, 1], [10, 0, 1, 2], [12, 0, 2, 2], [15, 0, 2, 2], [17, 0, 2, 3], [20, 0, 2, 3], [21, 0, 3, 4], [24, 0, 3, 4], [25, 0, 4, 5], [28, 0, 4, 5], [30, 0, 4, 6], [33, 0, 4, 6], [34, 0, 5, 7], [37, 0, 5, 7], [39, 0, 6, 7], [41, 0, 6, 8], [44, 0, 6, 8], [46, 0, 6, 9], [49, 0, 6, 9], [50, 0, 7, 10], [53, 0, 7, 10], [55, 0, 7, 11], [57, 0, 8, 11], [59, 0, 8, 12], [62, 0, 8, 12], [65, 0, 8, 12], [66, 0, 9, 13], [69, 0, 9, 13], [71, 0, 9, 14], [73, 0, 10, 14], [75, 0, 10, 15], [78, 0, 10, 15], [79, 0, 11, 16], [82, 0, 11, 16], [84, 0, 11, 17], [87, 0, 11, 17], [89, 0, 12, 17], [91, 0, 12, 18], [94, 0, 12, 18], [97, 0, 12, 18], [98, 0, 13, 19], [100, 0, 13, 20], [102, 0, 14, 20], [104, 0, 14, 21], [107, 0, 14, 21], [110, 0, 14, 21], [111, 0, 15, 22], [114, 0, 15, 22], [116, 0, 15, 23], [118, 0, 16, 23], [120, 0, 16, 24], [123, 0, 16, 24], [124, 0, 17, 25], [127, 0, 17, 25], [129, 0, 17, 26], [132, 0, 17, 26], [134, 0, 18, 26], [136, 0, 18, 27], [139, 0, 18, 27], [140, 0, 19, 28], [143, 0, 19, 28], [145, 0, 19, 29], [147, 0, 20, 29], [149, 0, 20, 30], [152, 0, 20, 30], [155, 0, 20, 30], [156, 0, 21, 31], [159, 0, 21, 31], [161, 0, 21, 32], [163, 0, 22, 32], [165, 0, 22, 33], [168, 0, 22, 33], [169, 0, 23, 34], [172, 0, 23, 34], [174, 0, 23, 35], [176, 0, 24, 35], [179, 0, 24, 35], [181, 0, 24, 36], [184, 0, 24, 36], [185, 0, 25, 37], [188, 0, 25, 37], [190, 0, 25, 38], [192, 0, 26, 38], [194, 0, 26, 39], [197, 0, 26, 39], [199, 0, 27, 39], [201, 0, 27, 40], [204, 0, 27, 40], [206, 0, 27, 41], [208, 0, 28, 41], [210, 0, 28, 42], [213, 0, 28, 42], [214, 0, 29, 43], [217, 0, 29, 43], [219, 0, 29, 44], [221, 0, 30, 44], [224, 0, 30, 44], [226, 0, 30, 45], [229, 0, 30, 45], [231, 0, 30, 46], [234, 0, 30, 46], [235, 0, 31, 47], [238, 0, 31, 47], [239, 0, 32, 48], [242, 0, 32, 48], [244, 0, 33, 48], [246, 0, 33, 49], [249, 0, 33, 49], [251, 0, 33, 50], [253, 0, 34, 50], [255, 0, 34, 51], [258, 0, 34, 51], [259, 0, 35, 52], [262, 0, 35, 52], [264, 0, 35, 53], [267, 0, 35, 53], [269, 0, 36, 53], [271, 0, 36, 54], [274, 0, 36, 54], [275, 0, 37, 55]],
        60: [[1, 2, 3, 4], [1, 3, 4, 5], [1, 4, 5, 6], [2, 4, 7, 6], [2, 5, 7, 8], [2, 6, 9, 8], [3, 7, 9, 9], [3, 8, 11, 9], [3, 8, 11, 12], [3, 9, 13, 12], [4, 10, 14, 12], [4, 10, 15, 14], [4, 11, 16, 15], [5, 12, 17, 15], [5, 12, 19, 16], [5, 13, 19, 18], [6, 14, 20, 18], [6, 16, 21, 18], [6, 17, 22, 19], [6, 18, 23, 20], [7, 19, 24, 20], [7, 20, 25, 21], [7, 21, 26, 22], [7, 22, 28, 22], [8, 23, 28, 23], [8, 24, 30, 23], [9, 25, 30, 24], [9, 26, 32, 24], [9, 27, 32, 26], [9, 28, 34, 26], [10, 29, 35, 26], [10, 30, 36, 27], [10, 32, 37, 27], [10, 33, 38, 28], [11, 34, 39, 28], [11, 35, 40, 29], [12, 36, 41, 29], [12, 37, 42, 30], [12, 38, 43, 31], [12, 39, 45, 31], [13, 40, 45, 32], [13, 41, 46, 33], [13, 42, 47, 34], [13, 43, 49, 34], [14, 44, 49, 35], [14, 45, 51, 35], [15, 46, 51, 36], [15, 47, 53, 36], [15, 49, 53, 37], [15, 50, 55, 37], [16, 51, 56, 37], [16, 52, 57, 38], [16, 53, 58, 39], [16, 54, 59, 40], [17, 55, 60, 40], [17, 56, 61, 41], [18, 57, 62, 41], [18, 58, 63, 42], [18, 59, 64, 43], [18, 60, 65, 44], [19, 61, 66, 44], [19, 62, 67, 45], [19, 63, 68, 46], [19, 64, 70, 46], [20, 65, 70, 47], [20, 66, 72, 47], [21, 67, 72, 48], [21, 68, 74, 48], [21, 69, 75, 49], [21, 70, 77, 49], [22, 71, 77, 50], [22, 72, 78, 51], [22, 74, 79, 51], [22, 75, 80, 52], [23, 76, 81, 52], [23, 77, 82, 53], [24, 78, 83, 53], [24, 79, 84, 54], [24, 80, 85, 55], [24, 81, 86, 56], [25, 82, 87, 56], [25, 83, 88, 57], [25, 84, 89, 58], [25, 85, 91, 58], [26, 86, 91, 59], [26, 87, 93, 59], [27, 88, 93, 60], [27, 89, 95, 60], [27, 91, 95, 61], [27, 92, 97, 61], [28, 93, 98, 61], [28, 94, 99, 62], [28, 95, 100, 63], [28, 96, 101, 64], [29, 97, 102, 64], [29, 98, 103, 65], [30, 99, 104, 65], [30, 100, 105, 66], [30, 101, 107, 66], [30, 102, 108, 67], [30, 103, 109, 68], [31, 104, 109, 69], [31, 105, 110, 70], [31, 106, 112, 70], [32, 107, 112, 71], [32, 108, 114, 71], [33, 109, 114, 72], [33, 110, 116, 72], [33, 112, 116, 73], [33, 112, 119, 73], [34, 113, 119, 74], [34, 114, 120, 75], [34, 116, 121, 75], [34, 117, 122, 76], [35, 118, 123, 76], [35, 119, 124, 77], [36, 120, 125, 77], [36, 121, 126, 78], [36, 122, 127, 79], [36, 122, 129, 80]],
        70: [[3, 0, 4, 3], [4, 0, 5, 4], [6, 0, 5, 5], [7, 0, 7, 5], [8, 0, 8, 6], [9, 0, 9, 7], [11, 0, 9, 8], [11, 0, 11, 9], [12, 0, 12, 10], [14, 0, 12, 11], [15, 0, 13, 12], [16, 0, 14, 13], [17, 0, 15, 14], [18, 0, 17, 14], [19, 0, 19, 14], [20, 0, 19, 16], [22, 0, 19, 17], [23, 0, 20, 18], [24, 0, 21, 19], [25, 0, 22, 20], [26, 0, 23, 21], [28, 0, 23, 22], [29, 0, 24, 23], [30, 0, 25, 24], [31, 0, 26, 25], [32, 0, 28, 25], [33, 0, 29, 26], [34, 0, 30, 27], [35, 0, 31, 28], [36, 0, 32, 29], [38, 0, 32, 30], [39, 0, 33, 31], [40, 0, 34, 32], [41, 0, 35, 33], [42, 0, 36, 34], [44, 0, 37, 34], [45, 0, 38, 35], [46, 0, 39, 36], [47, 0, 40, 37], [49, 0, 41, 37], [50, 0, 41, 39], [51, 0, 42, 40], [52, 0, 43, 41], [53, 0, 44, 42], [54, 0, 45, 43], [55, 0, 47, 43], [56, 0, 48, 44], [57, 0, 49, 45], [58, 0, 50, 46], [59, 0, 51, 47], [61, 0, 51, 48], [62, 0, 52, 49], [63, 0, 53, 50], [64, 0, 54, 51], [66, 0, 54, 52], [67, 0, 56, 52], [68, 0, 57, 53], [69, 0, 58, 54], [70, 0, 59, 55], [71, 0, 60, 56], [73, 0, 60, 57], [74, 0, 61, 58], [75, 0, 62, 59], [76, 0, 63, 60], [77, 0, 64, 61], [78, 0, 66, 61], [79, 0, 67, 62], [80, 0, 68, 63], [81, 0, 69, 64], [82, 0, 70, 65], [84, 0, 70, 66], [85, 0, 71, 67], [86, 0, 72, 68], [88, 0, 72, 69], [89, 0, 73, 70], [90, 0, 75, 70], [91, 0, 76, 71], [92, 0, 77, 72], [93, 0, 78, 73], [94, 0, 79, 74], [95, 0, 80, 75], [97, 0, 80, 76], [98, 0, 81, 77], [99, 0, 82, 78], [100, 0, 83, 79], [101, 0, 85, 79], [102, 0, 86, 80], [103, 0, 87, 81], [105, 0, 87, 82], [106, 0, 88, 83], [107, 0, 89, 84], [108, 0, 90, 85], [110, 0, 90, 86], [111, 0, 91, 87], [112, 0, 92, 88], [113, 0, 94, 88], [114, 0, 95, 89], [115, 0, 96, 90], [117, 0, 96, 91], [118, 0, 97, 92], [119, 0, 98, 93], [120, 0, 99, 94], [121, 0, 100, 95], [122, 0, 101, 96], [123, 0, 102, 97], [124, 0, 104, 97], [125, 0, 105, 98], [127, 0, 105, 99], [128, 0, 106, 100], [129, 0, 108, 100], [130, 0, 108, 102], [132, 0, 108, 103], [133, 0, 109, 104], [134, 0, 110, 105], [135, 0, 111, 106], [136, 0, 113, 106], [137, 0, 114, 107], [138, 0, 115, 108], [139, 0, 116, 109], [140, 0, 117, 110]]
    };

    static getAttr(level: number, profession: number) {
        return this.defaultAttr[profession][level];
    }

    calculateMaxHp() {
        if (this.isMonster)
            return;

        const hpMultiplier = 24, manaMultiplier = 5, manaExtraMultiplier = 25;

        let baseHP =
            (this.vitality * hpMultiplier) +
            (this.spirit * 3) +
            (this.agility * 3) +
            (this.strength * 3);

        switch (this.class) {
            case 11: baseHP = (baseHP * 1.05); break;
            case 12: baseHP = (baseHP * 1.08); break;
            case 13: baseHP = (baseHP * 1.10); break;
            case 14: baseHP = (baseHP * 1.12); break;
            case 15: baseHP = (baseHP * 1.15); break;
            default: baseHP = (baseHP * 1.00); break;
        }

        let baseMana =
            (this.spirit * manaMultiplier);

        let pclass = this.class - 100;
        switch (pclass) {
            case 42:
            case 32: baseMana = (baseMana * 3); break;
            case 43:
            case 33: baseMana = (baseMana * 4); break;
            case 44:
            case 34: baseMana = (baseMana * 5); break;
            case 45:
            case 35: baseMana = (baseMana * 6); break;
        }

        this.maxhp = this.fullyLoaded ? baseHP + this.addHP : 65535;
        this.maxmana = this.fullyLoaded ? baseMana + this.addMP : 65535;
        this.hp = this.hp;

    }

    getMesh() {
        let mesh = this.transformId * 10000000 + this.face * 10000 + this.body;
        return mesh;
    }

    recalculateBP() {
        this.battlePower += this.level;
        this.battlePower += this.extraBP;
    }

    recalculateStats() {
        this.resetStatus();

        this.speed += ((this.agility / 10) + (this.agility % 10 > 5 ? 1 : 0));
        this.minAttack += (this.strength);
        this.maxAttack += (this.strength) * 1.3;
        this.attackRange += 3;

        this.player.equipment.items.map((item) => {
            this.loadItemStats(item);
        });

        this.recalculateBP();
        this.calculateMaxHp();
    }

    calcItemBP(item: MsgItemInfo) {
        let quality = item.id % 10;

        let bp = 0;
        if (quality === 9)
            bp += 4;
        if (quality === 8)
            bp += 3;
        if (quality === 7)
            bp += 2;
        if (quality === 6)
            bp += 1;

        if (item.socketOne != 0) {
            bp++;
            if (item.socketOne % 10 === 3)
                bp++;
        }
        if (item.socketTwo != 0) {
            bp++;
            if (item.socketOne % 10 === 3)
                bp++;
        }

        bp += item.plus;

        if (item.isTwoHanded())
            bp *= 2;

        this.battlePower += bp;
    }

    loadItemStats(item: MsgItemInfo) {
        let id = item.id;
        let info = DBService.ItemTypesTable.getBaseInfo(id);
        let composeInfo = DBService.ItemsTable.getComposeInfo(id, item.plus);
        let pos = item.position;

        this.calcItemBP(item);

        if (info) {
            if (pos === EItemPosition.AttackTalisman) {
                this.finalAttack += info.minAttack;
                this.finalMagicAttack += info.magicAttack;
            }
            else if (pos === EItemPosition.DefenseTalisman) {
                this.finalDefense += info.defense;
                this.finalMagicDefense += info.magicDefense;
            }
            else if (pos === EItemPosition.LeftWeapon) {
                this.minAttack += info.minAttack * 0.5;
                this.maxAttack += info.maxAttack * 0.5;
            }
            else if (pos === EItemPosition.RightWeapon) {
                this.minAttack += info.minAttack;
                this.maxAttack += info.maxAttack;
                this.attackRange += info.attackRange;
                this.magicAttack += info.magicAttack;
            } else {
                this.minAttack += info.minAttack;
                this.maxAttack += info.maxAttack;
                this.defense += info.defense;
                this.magicAttack += info.magicAttack;
                this.magicDefense += info.magicDefense;
            }

            this.minAttack += info.minAttack;
            this.maxAttack += info.maxAttack;
            this.attackRange += info.attackRange;
            this.magicAttack += info.magicAttack;
            this.dodgePercent += info.dodge;
            this.addHP += info.addHP + item.enchant;
            //this.addMP += info.addMP;
            this.reduceDamagePercent += item.bless;

            if (item.plus > 0 && composeInfo) {
                if (pos === EItemPosition.AttackTalisman) {
                    this.finalAttack += composeInfo.minAttack;
                    this.finalMagicAttack += composeInfo.magicAttack;
                }
                else if (pos === EItemPosition.DefenseTalisman) {
                    this.finalDefense += composeInfo.defense;
                    this.finalMagicDefense += composeInfo.magicDefense;
                } else {
                    this.addHP += composeInfo.addHP;
                    //this.addMP += composeInfo.addMP;
                    this.speed += composeInfo.addAgility;
                    this.accuracy += composeInfo.addAccuracy;
                    this.minAttack += composeInfo.minAttack;
                    this.maxAttack += composeInfo.maxAttack;
                    this.defense += composeInfo.defense;
                    this.magicAttack += composeInfo.magicAttack;
                    this.magicDefense += composeInfo.magicDefense;
                }
            }
        }
    }

    resetStatus() {
        this.battlePower = 0;
        this.speed = 0;
        this.attackRange = 1;
        this.defense = 0;
        this.minAttack = 0;
        this.maxAttack = 0;
        this.magicAttack = 0;
        this.magicDefense = 0;
        this.finalAttack = 0;
        this.finalDefense = 0;
        this.finalMagicAttack = 0;
        this.finalMagicDefense = 0;

        this.accuracy = 0;
        this.dodgePercent = 0;
        this.reduceDamagePercent = 0;
        this.bonusAttackPercent = 0;
        this.bonusMagicPercent = 0;

        this.criticalStrike = 0;
        this.skillCriticalStrike = 0;
        this.block = 0;
        this.counteraction = 0;
        this.detoxication = 0;
        this.breakthrough = 0;
        this.intensification = 0;
        this.penetration = 0;
        this.immunity = 0;

        this.metalResistance = 0;
        this.waterResistance = 0;
        this.fireResistance = 0;
        this.woodResistance = 0;
        this.earthResistance = 0;
    }

    receiveExp(value: number){
        let levexp = Core.levexp.filter((lx) => lx.type === 0);
        let expNeeded = 0;

        levexp.map((lx) => {
            if (lx.level === this.level)
                expNeeded = lx.experience;
        });

        if (expNeeded > 0){
            let cexp = this.experience + value;
            while (cexp > 0 && this.level < 130){
                if (cexp > expNeeded){
                    cexp -= expNeeded;
                    this.level++;
                } else {
                    this.experience = cexp;
                    cexp = 0;
                }
            }
        } else this.experience += value;
    }

    receiveProfExp(type: number, value: number){
        let levexp = Core.levexp.filter((lx) => lx.type === 7);
        let expNeeded = 0;

        let wepSkill = this.getWeaponSkill(type);
        if (wepSkill){
            levexp.map((lx) => {
                if (lx.level === 1)
                    expNeeded = lx.experience;
            });

            if (expNeeded){
                let cexp = wepSkill.experience + value;
                while (cexp > 0){
                    if (cexp > expNeeded){
                        cexp -= expNeeded;
                        wepSkill.level++;
                    } else {
                        wepSkill.experience = cexp;
                        cexp = 0;
                    }
                }
                wepSkill.send(this.player);
            }
        } else {
            levexp.map((lx) => {
                if (lx.level === 1)
                    expNeeded = lx.experience;
            });

            wepSkill = new MsgWeaponSkill();
            wepSkill.type = type;
            wepSkill.level = 1;
            wepSkill.experience = value;
            wepSkill.reqExperience = expNeeded;
            wepSkill.send(this.player);

            this.addWeaponSkill(wepSkill);
        }
    }

    receiveMagicExp(type: number, value: number){
        let magic = this.getMagic(type);
        if (magic){
            let info: IMagicType = null;
            Core.magicInfo.map((m) => {
                if (m.type === type)
                    info = m;
            });

            if (info){
                let expNeeded = info.req_exp;
                if (expNeeded){
                    let cexp = magic.experience + value;
                    while (cexp > 0){
                        if (cexp > expNeeded){
                            cexp -= expNeeded;
                            magic.level++;
                        } else {
                            magic.experience = cexp;
                            cexp = 0;
                        }
                    }
                    magic.send(this.player);
                }
            }
        } 
    }

    receiveCure(attacker: Character, attack: MsgInteract, magic: boolean = false) {
        attack.target = this.uid;
        attack.x = this.x;
        attack.y = this.y;

        this.hp += attack.value;

        if (magic) {
            attack.value = 0;
        }

        attacker.sendToScreen(attack);
    }

    receiveDamage(attacker: Character, attack: MsgInteract, magic: boolean = false) {
        if (!this.invincible) {
            attack.target = this.uid;
            attack.x = this.x;
            attack.y = this.y;

            let expVal = attack.value;

            if (this.hp <= attack.value) {
                expVal = this.hp;
                this.hp = 0;
                attacker.lastAttack = null;
            } else {
                if (this.stamina > 0)
                    if (this.action === EAction.Sit)
                        this.stamina -= 25;

                this.hp -= attack.value;
                this.action = EAction.None;
            }

            if (!attacker.isMonster){
                attacker.receiveExp(expVal * this.level);
                attacker.receiveProfExp(attack.weaponTypeR, expVal);
                attacker.receiveMagicExp(attack.magic, expVal);
                if (attack.weaponTypeR === attack.weaponTypeL || attack.weaponTypeR === 0)
                    attacker.receiveProfExp(attack.weaponTypeL, expVal);
            }

            if (magic) {
                attack.value = 0;
            }
        }

        attacker.sendToScreen(attack);

        if (this.dead){
            attacker.xpKillCount++;

            if (this.isCompanion) {
                // TODO: Companion Die
            } else if (this.isMonster) {
                this.dieAsMonster();
            } else {
                this.dieAsPlayer();
                attacker.pkPoints += 10;
                // TODO: Enemies or guild enemies gains half pk points
            }

            this.dieAnimation(attacker, this.isMonster ? attacker.xpKillCount : 0);
        }
    }

    dieAnimation(attacker: Character, ko: number = 0, value: number = 0) {
        let kill = new MsgInteract();
        kill.type = EInteractionType.Kill;
        kill.x = this.x;
        kill.y = this.y;
        kill.sender = attacker.uid;
        kill.target = this.uid;
        kill.value = value;
        if (ko)
            kill.ko = ko;

        if (attacker.player)
            attacker.player.send(kill);
        this.sendToScreen(kill, false, attacker.player);
    }

    dieAsPlayer() {
        let target = this;
        this.timeOf.death = Date.now();
        target.addStatus(EStatusEffect.Dead);

        let kill = new NpcMonsterKill();
        kill.uid = this.uid;
        kill.mapid = this.mapId;
        kill.send(App.npcserver);

        setTimeout(() => {
            target.addStatus(EStatusEffect.Ghost);
            this.transformId = (this.body % 10 < 3) ? 99 : 98;
        }, 3000);
    }

    dieAsMonster() {
        let target = this;
        this.timeOf.death = Date.now();
        target.addStatus(EStatusEffect.Dead | EStatusEffect.FadeAway);

        //this.sendToScreen(this.getSpawnPacket());
        this.map.removeMonster(this);
        dropGold(this);

        let kill = new NpcMonsterKill();
        kill.uid = this.uid;
        kill.mapid = this.mapId;
        kill.send(App.npcserver);

        setTimeout(() => {
            target.removeSpawn();
        }, 4500);
    }

    revive() {
        this.removeStatus(EStatusEffect.Dead);
        this.transformId = 0;
        this.removeStatus(EStatusEffect.Ghost);
        this.action = EAction.None;
        this.hp = this.maxhp;

        NPCServerController.sendPlayerInfo();
    }

    constructor(player: Player, isMonster: boolean, isCompanion: boolean = false) {
        this.player = player;
        this.isMonster = isMonster;
        this.isCompanion = isCompanion;
        this.invincible = false;
        this.isSpecial = true; // TODO: remove this;

        if (this.isMonster || this.isCompanion)
            this.mapElementType = EMapElementType.MONSTER;
        else {
            this.weaponSkills = new IndexedElement();
            this.magics = new IndexedElement();
            this.mapElementType = EMapElementType.PLAYER;

    
            this.valueOf = {
                spell: {},
                attr: {},
                status: {},
            };
        }

        let now = Date.now();
        this.timeOf = {
            spell: {},
            drugsDisabled: 0,
            death: 0,
            flashingName: 0,
            staminaRecharge: now,
            pkPointDiminished: now,
            xpCircleList: now,
            xpList: now,
            lastAttack: now,
        };
    }

    addMagicAttrValue(magic: number, type: ECharacterAttrType, value: number) {
        let attr = {
            type,
            value,
            magic,
            item: 0
        };

        this.extraMagicAttrs.add(magic, attr)
    }

    getExtraAttrVal(type: ECharacterAttrType): number {
        let val = 0;
    
        let mAttrs = this.extraMagicAttrs.filter((a) => a.type === type);
        mAttrs.map((e, i, o) => {
            val += e.value;
        });
    
        let iAttrs = this.extraItemAttrs.filter((a) => a.type === type);
        iAttrs.map((e, i, o) => {
            val += e.value;
        });
    
        return val;
    }

    removeMagicAttrValue(magic: number){
        this.extraMagicAttrs.remove(magic);
    }

    addItemAttrValue(item: number, type: ECharacterAttrType, value: number) {
        let attr = {
            type,
            value,
            magic: 0,
            item
        };

        this.extraMagicAttrs.add(item, attr)
    }

    removeItemAttrValue(item: number){
        this.extraItemAttrs.remove(item);
    }

    observe(): Buffer {
        let packet = this.getSpawnPacket();
        packet[spawnOffsets.windowSpawn] = 1;
        return packet;
    }

    getStatusEffects(packet: Buffer) {
        let sf1 = Buffer.alloc(8)
        let statusFlag: any = (this.statusFlag.toBuffer() as Buffer).reverse()
        statusFlag.copy(sf1, 0);
        sf1.copy(packet, spawnOffsets.statusFlag);

        let sf2 = Buffer.alloc(8)
        let statusFlag2: any = (this.statusFlag2.toBuffer() as Buffer).reverse()
        statusFlag2.copy(sf2, 0);
        sf2.copy(packet, spawnOffsets.statusFlag2);

        let sf3 = Buffer.alloc(8)
        let statusFlag3: any = (this.statusFlag3.toBuffer() as Buffer).reverse()
        statusFlag3.copy(sf3, 0);
        sf3.copy(packet, spawnOffsets.statusFlag3);
    }

    getSpawnPacket(): Buffer {
        let len = 253 + 8 + this.name.length + this.clanName.length + 5;

        let packet: Buffer = Buffer.alloc(len);

        packet.writeUInt16LE(len - 8, 0);
        packet.writeUInt16LE(10014, 2);

        packet.writeUInt32LE(this.getMesh(), spawnOffsets.mesh);
        packet.writeUInt32LE(this.uid, spawnOffsets.uid);

        packet.writeUInt32LE(this.guildId, spawnOffsets.guildId);
        packet.writeUInt16LE(this.guildRank, spawnOffsets.guildRank);
        packet.writeUInt16LE(this.appearanceType, spawnOffsets.appearanceType);

        this.getStatusEffects(packet)

        packet.writeUInt16LE(this.hp, spawnOffsets.hp);
        packet[spawnOffsets.monsterLevel] = this.isMonster ? this.level & 0xFF : 0;

        packet.writeUInt16LE(sanitizeUshort(this.x), spawnOffsets.x);
        packet.writeUInt16LE(sanitizeUshort(this.y), spawnOffsets.y);

        packet.writeUInt16LE(this.hairStyle, spawnOffsets.hairStyle);
        packet[spawnOffsets.facing] = this.facing;

        packet.writeUInt16LE(this.action, spawnOffsets.action);
        packet[spawnOffsets.reborn] = this.reborn;
        packet[spawnOffsets.level] = this.isMonster ? 0 : this.level;
        packet[spawnOffsets.away] = this.away ? 1 : 0; // ?
        packet[spawnOffsets.extraBP] = this.extraBP; // ?

        packet.writeUInt32LE(this.flowerType, spawnOffsets.flowerType);
        packet[spawnOffsets.nobilityRank] = 0; // TODO: Nobility
        packet.writeUInt32LE(this.quizPoints, spawnOffsets.quizPoints);
        packet[spawnOffsets.title] = 0; // TODO: Title;
        packet[spawnOffsets.boss] = this.boss ? 1 : 0;

        packet[spawnOffsets.activeSubclass] = this.subclass;
        packet[spawnOffsets.firstClass] = this.firstClass;
        packet[spawnOffsets.secondClass] = this.secondClass;
        packet[spawnOffsets.class] = this.class;

        this.writeEquips(packet);
        this.writeNames(packet);

        return packet;
    }

    writeEquips(packet: Buffer) {
        if (!this.isMonster && !this.isCompanion) {
            this.player.equipment.items.map((item) => {
                switch (item.position) {
                    case EItemPosition.Head: {
                        packet.writeUInt32LE(item.id, spawnOffsets.head);
                        packet[spawnOffsets.headColor] = item.color;
                        break;
                    }
                    case EItemPosition.RightWeapon: {
                        packet.writeUInt32LE(item.id, spawnOffsets.rightWeapon);
                        packet[spawnOffsets.rightWeaponColor] = item.color;
                        break;
                    }
                    case EItemPosition.LeftWeapon: {
                        packet.writeUInt32LE(item.id, spawnOffsets.leftWeapon);
                        packet[spawnOffsets.leftWeaponColor] = item.color;
                        break;
                    }
                    case EItemPosition.Armor: {
                        packet.writeUInt32LE(item.id, spawnOffsets.armor);
                        packet[spawnOffsets.armorColor] = item.color;
                        break;
                    }
                    case EItemPosition.Garment: {
                        packet.writeUInt32LE(item.id, spawnOffsets.garment);
                        break;
                    }
                }
            });
        }

        return packet;
    }

    writeNames(packet: Buffer) {

        let arr = [this.name, '', '', '', '', this.clanName];
        if (this.isMonster)
            arr = [this.name, '', '', ''];
        packet.writeStringArray(arr, spawnOffsets.names);

        return packet;
    }

    prepare(data: ICharacter) {
        try {
            Object.keys(data).map((key: string) => {
                (this as any)[key] = (data as any)[key];
            });
        } catch (err) {
            console.error(err);
            console.log(new Error().stack);
        }
    }

    finalizeLoad() {
        //await this.player.inventory.loadItems();
        this.recalculateStats();

        if (this.hp < 1) {
            this.hp = 1;
            // TODO: teleport to default map location
        }

        this.teleport(this.mapId, this.x, this.y);
    }

    setLocation(x: number, y: number) {
        this.x = x;
        this.y = y;

        let pos = new NpcMonsterPos();
        pos.uid = this.uid;
        pos.x = x;
        pos.y = y;
        pos.mapid = this.mapId;

        App.npcserver.send(pos);
    }

    teleport(mapid: number, x: number, y: number) {
        let currentMap = Core.maps.get(this.mapId);

        if (!Core.maps.contains(mapid))
            RMaps.new(mapid, mapid);
        let map = Core.maps.get(mapid);

        this.removeSpawn();

        if (currentMap && mapid !== currentMap.id) {
            currentMap.players.remove(this.player.id);
        }

        map.addPlayer(this.player);

        let teleport = new MsgAction(true);
        teleport.uid = this.uid;
        teleport.id = EMsgActionType.Teleport;
        teleport.map = map.base;
        teleport.x = x;
        teleport.y = y;

        this.mapId = mapid;
        this.x = x;
        this.y = y;

        this.setLocation(this.x, this.y);
        this.lastAttack = null;

        teleport.send(this.player);

        let mapInfo = new MsgMapInfo();
        mapInfo.id = map.id;
        mapInfo.baseId = map.base;
        mapInfo.send(this.player);

        this.sendToScreen(this.getSpawnPacket());

        this.player.screen.clearAll();
        this.player.screen.reload();
    }

    move(packet: MsgWalk) {
        try {
            let _x = this.x;
            let _y = this.y;

            this.facing = packet.direction;
            let xdirs = [0, -1, -1, -1, 0, 1, 1, 1];
            let ydirs = [1, 1, 0, -1, -1, -1, 0, 1];
            let offset = packet.direction % 8;
            let X = xdirs[offset];
            let Y = ydirs[offset];

            let x = this.x + X;
            let y = this.y + Y;

            this.cancelActionTask();
            this.lastAttack = null;

            if (this.player) {
                this.setLocation(x, y);
                this.map.ground.setInvalidLocation(EMapElementType.PLAYER, x, y);
                this.map.ground.setValidLocation(EMapElementType.PLAYER, _x, _y);
                this.sendToScreen(packet);
                //this.player.screen.clearAll();
                this.player.screen.reload(null);
            } else if (this.isMonster) {
                if (this.map.ground.validLocation(EMapElementType.MONSTER, x, y)) {
                    this.setLocation(x, y);
                    this.map.ground.setInvalidLocation(EMapElementType.MONSTER, x, y);
                    this.map.ground.setValidLocation(EMapElementType.MONSTER, _x, _y);
                    this.action = EAction.None;
                    this.facing = packet.direction;

                    this.sendToScreen(packet);
                }
            }

        } catch (err) {
            console.log(err);
        }
    }

    addStatus(statusEffect: EStatusEffect) {
        this.removeStatus(statusEffect);
        let num = bn(statusEffect);
        let aux = bn(this.statusFlag).or(num);
        this.statusFlag = aux;
    }

    hasStatus(statusEffect: EStatusEffect) {
        let num = bn(statusEffect);
        let aux = bn(this.statusFlag).and(num.add(1).neg());
        let hasStatus = !(aux.toString() === this.statusFlag.toString());
        return hasStatus;
    }

    removeStatus(statusEffect: EStatusEffect) {
        let num = bn(statusEffect);
        if (this.hasStatus(num)) {
            let aux = bn(this.statusFlag).and(num.add(1).neg());
            this.statusFlag = aux;
        }
    }

    addStatus2(statusEffect: EStatusEffect) {
        this.removeStatus2(statusEffect);
        let num = bn(statusEffect);
        let aux = bn(this.statusFlag2).or(num);
        this.statusFlag2 = aux;
    }

    hasStatus2(statusEffect: EStatusEffect) {
        let num = bn(statusEffect);
        let aux = bn(this.statusFlag2).and(num.add(1).neg());
        let hasStatus = !(aux.toString() === this.statusFlag2.toString());
        return hasStatus;
    }

    removeStatus2(statusEffect: EStatusEffect) {
        let num = bn(statusEffect);
        if (this.hasStatus(num)) {
            let aux = bn(this.statusFlag2).and(num.add(1).neg());
            this.statusFlag2 = aux;
        }
    }

    addStatus3(statusEffect: EStatusEffect) {
        this.removeStatus3(statusEffect);
        let num = bn(statusEffect);
        let aux = bn(this.statusFlag3).or(num);
        this.statusFlag3 = aux;
    }

    hasStatus3(statusEffect: EStatusEffect) {
        let num = bn(statusEffect);
        let aux = bn(this.statusFlag3).and(num.add(1).neg());
        let hasStatus = !(aux.toString() === this.statusFlag3.toString());
        return hasStatus;
    }

    removeStatus3(statusEffect: EStatusEffect) {
        let num = bn(statusEffect);
        if (this.hasStatus(num)) {
            let aux = bn(this.statusFlag3).and(num.add(1).neg());
            this.statusFlag3 = aux;
        }
    }

    sendGroundItemSpawn(item: GroundItem) {
        if (item) {
            if (this.player) {
                let spawn = new MsgMapItem();
                spawn.uid = item.uid;
                spawn.type = item.type;
                spawn.color = item.color;
                spawn.x = item.x;
                spawn.y = item.y;
                spawn.action = item.action;

                this.player.send(spawn);
            }
        }
    }

    sendSpawn(character: Character) {
        if (character) {
            if (this.player && this.player.screen.add(character)) {
                this.player.send(character.getSpawnPacket());
                //this.player.sendMsg(`Added ${character.name} to screen.`);
            }
        }
    }

    removeSpawn() {
        let data = new MsgAction(true);
        data.uid = this.uid;
        data.id = EMsgActionType.RemoveEntity;

        this.sendToScreen(data);
    }

    removeScreenSpawn(player: Player) {
        let data = new MsgAction(true);
        data.uid = this.uid;
        data.id = EMsgActionType.RemoveEntity;
        player.send(data);
        player.screen.remove(this);
    }

    sendUpdate(type: EUpdateType, value: any | number | number[]) {
        if (this.fullyLoaded || this.isMonster) {
            let update = new MsgUserAttrib();
            update.uid = this.uid;

            let valArray = Array.isArray(value);

            let u: IUpdate = {
                type,
                value1: valArray ? value[0] : value,
                value2: valArray ? value[1] : 0,
                value3: valArray ? value[2] : 0,
            };

            update.addUpdate(u);
            this.sendToScreen(update, false);
            if (this.player)
                update.send(this.player);
        }
    }

    sendToScreen(buffer: IPacket | Buffer, sendMyself = true, exclude: Player = null) {
        // TODO: check all elements in screen
        if (this.player && this.player.screen) {
            this.player.screen.elements.map((obj) => {
                if (obj.mapElementType === EMapElementType.PLAYER) {
                    let player = (obj as Character).player;
                    if (player.character.uid !== this.player.character.uid) {
                        player.send(buffer);
                    }
                }
            });

            if (sendMyself)
                this.player.send(buffer);
        } else {
            // monsters screen range
            this.map.players.map(player => {
                let dist = distanceBetweenCharacters(player.character, this);
                if (dist <= GAME_SCREEN_SIZE) {
                    if (!exclude || player.character.uid !== exclude.character.uid)
                        player.send(buffer);
                }
            });
        }
    }

    sendEffect(name: string) {
        let effect = new MsgName(true, EStringType.RoleEffect);
        effect.uid = this.uid;
        effect.addString(name);
        effect.prepare();
        this.sendToScreen(effect);
    }

    sendSpawnEffect() {
        let action = new MsgAction(true);
        action.uid = this.uid;
        action.id = 134;
        action.facing = this.facing;
        this.sendToScreen(action);
    }

    sendPostCmd(type: EPostCmdType, data1?: number, data2?: number, data3?: number, data4?: number, data5?: number) {
        let open = new MsgAction(true);
        open.uid = this.uid;
        open.id = EMsgActionType.PostCommand;
        open.x = this.x;
        open.y = this.y;
        open.param = type;
        open.param2 = data1 ? data1 : 0;
        open.aParam1 = data2 ? data2 : 0;
        open.aParam2 = data3 ? data3 : 0;
        // open.aParam5 = data4 ? data4 : 0;
        // open.aParam6 = data5 ? data5 : 0;
        this.player ? this.player.send(open) : this.sendToScreen(open);
    }

    addWeaponSkill(skill: MsgWeaponSkill) {
        if (this.fullyLoaded) {
            if (!this.weaponSkills.contains(skill.type)) {
                RMagic.insertWeaponSkill(this.player, skill);
            } else
                this.weaponSkills.remove(skill.type);

            this.weaponSkills.add(skill.type, skill);
            skill.send(this.player);
        }
    }

    hasWeaponSkill(id: number, minLevel: number = 1) {
        let success = false;
        this.weaponSkills.map((ws) => {
            if (ws.type === id && ws.level >= minLevel)
                success = true;
        })

        return success;
    }

    getWeaponSkill(type: number): MsgWeaponSkill {
        let magic = null;
        this.weaponSkills.map((mag) => {
            if (mag.type === type)
                magic = mag;
        })

        return magic;
    }

    addMagic(magic: MsgMagicInfo) {
        if (this.fullyLoaded) {
            if (!this.magics.contains(magic.type)) {
                RMagic.insertMagic(this.player, magic);
            } else {
                let oldMagic = this.magics.get(magic.type);
                magic.uid = oldMagic.uid;
                this.magics.remove(magic.type);
                RMagic.saveMagic(this.player, magic);
            }
        }

        this.magics.add(magic.type, magic);
        magic.send(this.player);
    }

    addMagicExp(value: number, magic: MsgMagicInfo, magicInfo: IMagicType) {
        magic.experience += value

        if (magicInfo.req_exp > 0 && magic.experience >= magicInfo.req_exp) {
            magic.level++
            magic.experience = 0
        }

        this.addMagic(magic)
    }

    hasMagic(type: number, minLevel: number = 0) {
        let success = false;
        this.magics.map((mag) => {
            if (mag.type === type && mag.level >= minLevel)
                success = true;
        })

        return success;
    }

    getMagic(type: number): MsgMagicInfo {
        let magic = null;
        this.magics.map((mag) => {
            if (mag.type === type)
                magic = mag;
        })

        return magic;
    }

    findFirstLocation() {
        let map = Core.maps.get(this.mapId);
        let location = map.ground.getNextValid(EMapElementType.PLAYER, map.portalX, map.portalY);
        if (location){
            this.teleport(this.mapId, location.x, location.y);
        } else {
            this.player.sendMsg(cnRes.STR_LOCATION_INVALID);
        }
    }

    actionTaskTimeout: any = null;
    inActionTask: boolean = false;
    startActionTask(type: EActionTaskType, playerAction: EAction, seconds: number, action: number, actionFail: number, text: string, doneCallback?: () => void) {
        let data = new MsgAction(true, text);
        data.id = EMsgActionType.ActionTask;
        data.uid = this.uid;
        data.param = playerAction;
        data.param3 = type;
        data.aParam3 = seconds;

        if (type === EActionTaskType.Finish){
            clearTimeout(this.actionTaskTimeout);
            this.inActionTask = false;
        } else if (type === EActionTaskType.Interrupt){
            clearTimeout(this.actionTaskTimeout);
            this.inActionTask = false;
            if (actionFail){
                // TODO: call actionFail
            }
        } else {
            this.inActionTask = true;
        }

        let character = this;
        this.actionTaskTimeout = setTimeout(() => {
            // TODO: call action

            character.finishActionTask();
            if (doneCallback)
                doneCallback();
        }, seconds * 1000);

        this.player.send(data);
    }

    cancelActionTask(text: string = "Interrupted"){
        if (this.inActionTask)
            this.startActionTask(EActionTaskType.Interrupt, 0, 0, 0, 0, text);
    }

    finishActionTask(){
        if (this.inActionTask)
            this.startActionTask(EActionTaskType.Finish, 0, 0, 0, 0, 'Done!');
    }
}