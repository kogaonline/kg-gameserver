import Player from "./player";
import IndexedElement from "../components/indexedElement";
import MsgItemInfo from "../network/packets/gamepackets/MsgItemInfo";
import MsgItem from "../network/packets/gamepackets/MsgItem";
import { EItemActionType } from "../enums/EItemAction";
import DBService from "../services/database";
import { cnRes } from "../res/cnRes";
import { EItemPosition } from "../enums/EItemPosition";

export default class Inventory {
    player: Player;
    items: IndexedElement<MsgItemInfo>;

    constructor(player: Player) {
        this.player = player;
        this.items = new IndexedElement<MsgItemInfo>();
    }

    add(item: MsgItemInfo) {
        if (this.items.size >= 40) {
            this.player.sendMsg(cnRes.STR_INVENTORY_FULL);
            return;
        }
        item.position = 0;
        this.items.add(item.uid, item);
        item.send(this.player);
        if (item.justCreated) {
            DBService.ItemsTable.createItem(item, this.player);
        } else {
            DBService.ItemsTable.updateItem(item, this.player);
        }
    }

    equip(item: MsgItemInfo) {
        this.items.map((_item) => {
            if (_item === item) {
                this.items.remove(item.uid);

                let iu = new MsgItem(true);
                iu.uid = item.uid;
                iu.action = EItemActionType.RemoveInventory;
                iu.send(this.player);

                iu.action = EItemActionType.EquipItem;
                iu.send(this.player);
            }
        });
    }

    remove(item: MsgItemInfo) {
        this.items.map((_item) => {
            if (_item === item) {
                this.items.remove(item.uid);

                let iu = new MsgItem(true);
                iu.uid = item.uid;
                iu.action = EItemActionType.RemoveInventory;
                iu.send(this.player);

                item.position = EItemPosition.Remove;
                DBService.ItemsTable.updateItem(item, this.player);
            }
        });
    }

    removeUID(uid: number) {
        this.items.map((_item) => {
            if (_item.uid === uid) {
                this.items.remove(uid);

                let iu = new MsgItem(true);
                iu.uid = uid;
                iu.action = EItemActionType.RemoveInventory;
                iu.send(this.player);

                _item.position = EItemPosition.Remove;
                DBService.ItemsTable.updateItem(_item, this.player);
            }
        });
    }

    contains(id: number, quantity: number = 1) {
        let count = 0;
        this.items.map((item) => {
            if (item.id === id) {
                count += item.stackSize;
            }
        })

        return count >= quantity;
    }

    containsUID(uid: number) {
        this.items.map((item) => {
            if (item.uid === uid) {
                return false;
            }
        })

        return false;
    }

    getUID(uid: number): MsgItemInfo {
        let _item = null;
        this.items.map((item) => {
            if (item.uid === uid) {
                _item = item;
            }
        });

        return _item;
    }

    async loadItems() {
        await DBService.ItemsTable.loadPlayerItems(this.player);
    }

    clear() {
        this.items.map((item) => {
            this.remove(item);
        });
    }
}