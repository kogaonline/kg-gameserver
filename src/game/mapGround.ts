import { EMapElementType } from "../enums/EMapElementType";

interface IMapLocation {
    flag: number;
}

export default class MapGround {
    private lastAngle = 0;
    
    size: {
        width: number,
        height: number,
    };

    objects: {
        [x: number] : {
            [y: number]: IMapLocation
        }
    }

    constructor(width: number, height: number){
        this.size = { width, height };
        this.objects = {};
    }

    setValidLocation(type:EMapElementType, x: number, y: number){
        this.removeFlag(type, x, y);
    }

    setInvalidLocation(type:EMapElementType, x: number, y: number){
        this.addFlag(type, x, y);
    }

    validLocation(type: EMapElementType, x: number, y: number){
        if (x > this.size.width || y > this.size.height || x < 0 || y < 0)
            return false;

        let loc = this.getXY(x, y);
        if (!loc)
            return false;
            
        let aux = loc.flag & ((type + 1) * -1);
        let hasflag = !(aux.toString() === loc.flag.toString());

        return !hasflag;
    }

    getNextValid(type: EMapElementType, x:number, y:number): false | {x: number, y: number} {
        if (x > this.size.width || y > this.size.height)
            return false;

        if (this.validLocation(type, x, y)){
            return {x, y};
        }

        let sx = x, sy = y;

        for (let i = this.lastAngle; i < 8; i++){
            let _angle = i;

            for (let a = 0; a < 8; a++){
                let angle = a;
                let _x = x, _y = y;
                let update = this.updateAngle(angle, _x, _y);
                _x = update.x, _y = update.y;

                if (this.validLocation(type, _x, _y)){
                    return update;
                }
            }
            let updateangle = this.updateAngle(_angle, sx, sy);
            sx = updateangle.x, sy = updateangle.y;

            if (_angle === 7){
                let updatestart = this.updateAngle(_angle, sx, sy);
                sx = updatestart.x, sy = updatestart.y;
            }

        }

        this.lastAngle++;
        if (this.lastAngle === 8)
            this.lastAngle = 0;
        return this.getNextValid(type, sx, sy);
    }

    updateAngle(angle: number, x: number, y: number): {x: number, y: number} {
        let xi = 0, yi =0;
        switch (angle){
            case 0: yi = 1; break;
            case 1: xi = -1; yi = 1; break;
            case 2: xi = -1; break;
            case 3: xi = yi = -1; break;
            case 4: yi = -1; break;
            case 5: xi = 1; yi = -1; break;
            case 6: xi = 1; break;
            case 7: xi = yi = 1; break;
        }

        return { x: x+xi, y: y+yi };
    }

    getXY(x: number, y: number): IMapLocation {
        try {
            return this.objects[x][y];
        } catch { return null;}
    }

    private addLocation(x: number, y: number){
        if (!this.objects[x])
            this.objects[x] = {};
            this.objects[x][y] = { flag: EMapElementType.NONE };
        return this.getXY(x, y);
    }

    private addFlag(flag:EMapElementType, x: number, y: number): IMapLocation {
        let loc = this.getXY(x, y);
        if (!loc || loc.flag)
            loc = this.addLocation(x, y);

        let aux = loc.flag |= flag;
        loc.flag = aux;
        return loc;
    }

    private removeFlag(flag: EMapElementType, x: number, y: number): IMapLocation {
        let loc = this.getXY(x, y);
        if (!loc || loc.flag)
            loc = this.addLocation(x, y);

        let aux = loc.flag & ((flag * -1)-1);
        loc.flag = aux;
        return loc;
    }
}