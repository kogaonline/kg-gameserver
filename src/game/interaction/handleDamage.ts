import MsgInteract from "../../network/packets/gamepackets/MsgInteract";
import { IMagicType } from "../../database/magicType";
import { getRandom, probability } from "../../logical/kernel";
import Character from "../character";
import { EInteractionActivationType } from "../../enums/EInteractionActivationType";
import { EMagicAttrType } from "../../enums/EMagicSortType";
import { ECharacterAttrType } from "../../enums/ECharacterAttrType";

export function getDamage(attacker: Character, target: Character, attack: MsgInteract, magicInfo: IMagicType | null = null): number {
    let damage = 1;

    try {
        if (magicInfo && !magicInfo.weapon_hit)
            damage = getMagicDamage(damage, attacker, target, attack, magicInfo);
        else
            damage = getMeleeDamage(damage, attacker, target, attack);
    } catch (err) {
        console.error(err);
    }

    if (damage < 1 && !attack.hasStatus(EInteractionActivationType.Block))
        damage = 1;

    return damage;
}

export function getMeleeDamage(damage: number, attacker: Character, target: Character, attack: MsgInteract): number {
    damage = applyMeleeBaseDamage(damage, attacker, target, attack);
    damage = applyBattlePowerAlgorithm(damage, attacker, target, attack);
    damage = applyMeleeFinalDamage(damage, attacker, target, attack);
    damage = applyMeleeDodgeAccuracyDamage(damage, attacker, target, attack);

    return damage;
}

function applyMeleeBaseDamage(damage: number, attacker: Character, target: Character, attack: MsgInteract): number {
    damage = getRandom(attacker.minAttack, attacker.maxAttack);
    damage = damage - target.defense;

    let eatk = attacker.getExtraAttrVal(ECharacterAttrType.Attack);
    if (eatk > 0) {
        let pct = damage / 100;
        damage = pct * eatk;
    }

    if (target.isMonster)
        damage *= 2;

    return damage;
}

function applyMeleeFinalDamage(damage: number, attacker: Character, target: Character, attack: MsgInteract): number {
    damage = damage + attacker.finalAttack;

    let eatk = attacker.getExtraAttrVal(ECharacterAttrType.FinalAttack);
    if (eatk > 0) {
        let pct = damage / 100;
        damage = pct * eatk;
    }

    damage = damage - target.finalDefense;

    return damage;
}

function applyMeleeDodgeAccuracyDamage(damage: number, attacker: Character, target: Character, attack: MsgInteract): number {
    let min = attacker.accuracy - target.dodgePercent;
    if (min < 1)
        min = 1;

    let evaded = probability(min, 100);
    if (evaded && !target.isMonster) {
        damage = 0;
        attack.addStatus(EInteractionActivationType.Block);
    }

    return damage;
}

function applyBattlePowerAlgorithm(damage: number, attacker: Character, target: Character, attack: MsgInteract): number {
    if (attacker.battlePower > target.battlePower)
        damage *= 1.3;
    else if (attacker.battlePower < target.battlePower)
        damage *= 0.3;

    return damage;
}

export function getMagicDamage(damage: number, attacker: Character, target: Character, attack: MsgInteract, magicInfo: IMagicType): number {
    damage = applyMagicInfoBaseDamage(damage, magicInfo);
    damage = applyMagicBaseDamage(damage, attacker, target, attack);
    damage = applyBattlePowerAlgorithm(damage, attacker, target, attack);
    damage = applyMagicFinalDamage(damage, attacker, target, attack);

    return damage;
}

function applyMagicInfoBaseDamage(damage: number, magicInfo: IMagicType): number {
    damage += magicInfo.power;
    damage *= (magicInfo.percent / 100);

    return damage;
}

function applyMagicBaseDamage(damage: number, attacker: Character, target: Character, attack: MsgInteract): number {
    damage = damage + attacker.magicAttack;
    damage = damage - target.magicDefense;

    if (target.isMonster)
        damage *= 2;

    return damage;
}

function applyMagicFinalDamage(damage: number, attacker: Character, target: Character, attack: MsgInteract): number {
    damage = damage + attacker.finalMagicAttack;
    damage = damage - target.finalMagicDefense;

    return damage;
}