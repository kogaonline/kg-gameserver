import Player from "../player";
import MsgInteract from "../../network/packets/gamepackets/MsgInteract";
import { EInteractionType } from "../../enums/EInteractionType";
import { handleAttack, handleMagicAttack } from "./attack";
import Core from "../../core";
import Character from "../character";

export default function handleInteraction(data: MsgInteract, sender: Character) {
    // if (sender.player)
    //     sender.player.sendMsg(`Received new interaction of type (${data.type}) to the target (${data.target}).`);

    switch (data.type) {
        case EInteractionType.Attack: {
            let found: Character = null;

            if (Core.playerPool.contains(data.target)) found = Core.playerPool.get(data.target).character;
            if (sender.map.monsters.contains(data.target)) found = sender.map.monsters.get(data.target);

            if (found && !found.dead)
                handleAttack(sender, found, data);
            else if (sender.player)
                sender.player.sendMsg(`Target ${data.target} not found or is dead.`);

            break;
        }

        case EInteractionType.MagicAttack: {
            let magicType = getMagicType(data, sender);
            let coords = getMagicCoords(data, sender);
            let target = getMagicTarget(data, sender);

            data.x = coords.x;
            data.y = coords.y;
            data.target = target;
            data.type = magicType;

            let found: Character = null;

            if (Core.playerPool.contains(data.target)) found = Core.playerPool.get(data.target).character;
            if (sender.map.monsters.contains(data.target)) found = sender.map.monsters.get(data.target);

            if ((found && !found.dead) || data.target === 0)
                handleMagicAttack(sender, found, data);
            else if (sender.player)
                sender.player.sendMsg(`Target ${data.target} not found or is dead.`);

            break;
        }

        case EInteractionType.Shoot: {
            let found: Character = null;

            if (Core.playerPool.contains(data.target)) found = Core.playerPool.get(data.target).character;
            if (sender.map.monsters.contains(data.target)) found = sender.map.monsters.get(data.target);

            if (found)
                handleAttack(sender, found, data);
            else if (sender.player)
                sender.player.sendMsg(`Target ${data.target} not found.`);

            break;
        }
    }
}

function getMagicType(data: MsgInteract, sender: Character): number {
    let _mid = Buffer.alloc(8);
    _mid.writeUInt32LE(data.value, 0);
    let _cid = Buffer.alloc(8);
    _cid.writeUInt32LE(sender.uid, 0);

    let ushort = new Uint16Array(5);

    ushort[0] = _mid[0] | _mid[1] << 8;
    ushort[1] = ushort[0] ^ 37213;
    ushort[2] = ushort[1] ^ _cid.readUInt16LE(0);
    ushort[3] = (ushort[2] << 3 | ushort[2] >> 13);
    ushort[4] = ushort[3] - 60226;

    let magicType = ushort[4];
    return magicType;
}

function getMagicCoords(data: MsgInteract, sender: Character): { x: number, y: number } {
    let _x = Buffer.alloc(4);
    _x.writeUInt32LE(data.x, 0);
    let _y = Buffer.alloc(4);
    _y.writeUInt32LE(data.y, 0);

    let x = _x[0] | _x[1] << 8;
    x ^= (data.sender & 0xffff) ^ 11990;
    x = ((x << 1) | ((x & 32768) >> 15)) & 0xffff;
    x = ((x | 0xffff0000) - 0xffff8922) & 0xffff;
    x -= 39372;

    let y = _y[0] | _y[1] << 8;
    y ^= (data.sender & 0xffff) ^ 47515;
    y = ((y << 5) | ((y & 63488) >> 11)) & 0xffff;
    y = ((y | 0xffff0000) - 0xffff8922) & 0xffff;

    return { x, y };
}

function getMagicTarget(data: MsgInteract, sender: Character): number {
    /*
        Target = ((uint)attack.ToArray()[12 + 4] & 0xFF) | (((uint)attack.ToArray()[13 + 4] & 0xFF) << 8) | (((uint)attack.ToArray()[14 + 4] & 0xFF) << 16) | (((uint)attack.ToArray()[15 + 4] & 0xFF) << 24);
        Target = ((((Target & 0xffffe000) >> 13) | ((Target & 0x1fff) << 19)) ^ 0x5F2D2463 ^ attacker.UID) - 0x746F4AE6;
    */
    let _tid = Buffer.alloc(8);
    _tid.writeUInt32LE(data.target, 0);

    let target = ((_tid[0]) | (_tid[1] << 8) | (_tid[2] << 16) | (_tid[3] << 24)) >>> 0;
    target = (((((target & 0xffffe000) >>> 13) | ((target & 0x1fff) << 19)) ^ 0x5F2D2463 ^ data.sender) - 0x746F4AE6) >>> 0;

    return target;
}
