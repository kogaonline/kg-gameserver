import MsgInteract from "../../../network/packets/gamepackets/MsgInteract";
import { IMagicType } from "../../../database/magicType";
import MsgMagicEffect, { EffectTarget } from "../../../network/packets/gamepackets/MsgMagicEffect";
import Character from "../../character";
import { cnRes } from "../../../res/cnRes";

export function handleReviveMagic(attacker: Character, target: Character, attack: MsgInteract, magicInfo: IMagicType, magicEffect: MsgMagicEffect) {
    if (validateMagicTarget(attacker, target, magicInfo)) {
        let et = new EffectTarget();
        et.X = target.x;
        et.Y = target.y;
        et.targetUID = target.uid;
        et.hit = true;
        magicEffect.addTarget(et);
        attacker.sendToScreen(magicEffect, true);
        // attack.value = magicInfo.power;
        // target.receiveDamage(attacker, attack, true);

        target.revive();
    }

}

function validateMagicTarget(attacker: Character, target: Character, magicInfo: IMagicType) {
    if (attacker && !attacker.dead && target.dead)
        if (attacker.level >= magicInfo.req_level) {
            return true;
        } else {
            if (attacker.player)
                attacker.player.sendMsg(cnRes.STR_USE_MAGIC_LEVEL_NOT_ENOUGH);
        }
    return false;
}