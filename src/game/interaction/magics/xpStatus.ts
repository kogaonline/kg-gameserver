import MsgInteract from "../../../network/packets/gamepackets/MsgInteract";
import { IMagicType } from "../../../database/magicType";
import MsgMagicEffect, { EffectTarget } from "../../../network/packets/gamepackets/MsgMagicEffect";
import Character from "../../character";

export function handleXpStatusMagic(attacker: Character, target: Character, attack: MsgInteract, magicInfo: IMagicType, magicEffect: MsgMagicEffect) {
    if (validateMagicTarget(attacker, target, magicInfo)) {
        let et = new EffectTarget();
        et.X = target.x;
        et.Y = target.y;
        et.targetUID = target.uid;
        et.hit = true;
        magicEffect.addTarget(et);
        attacker.sendToScreen(magicEffect, true);
        // attack.value = magicInfo.power;
        // target.receiveDamage(attacker, attack, true);

        let now = Date.now();
        target.addStatus(magicInfo.status);
        target.timeOf.spell[magicInfo.type] = now += magicInfo.seconds * 1000;
        target.addMagicAttrValue(magicInfo.type, magicInfo.attr, magicInfo.power - 30000);
        target.valueOf.spell[magicInfo.type] = magicInfo.id;

    }

}

function validateMagicTarget(attacker: Character, target: Character, magicInfo: IMagicType) {
    if (attacker && !attacker.dead && !target.dead)
        if (attacker.level >= magicInfo.req_level) {
            return true;
        }
    return false;
}