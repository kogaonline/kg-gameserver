import Player from "../../../game/player";
import MsgInteract from "../../../network/packets/gamepackets/MsgInteract";
import { IMagicType } from "../../../database/magicType";
import MsgMagicEffect, { EffectTarget } from "../../../network/packets/gamepackets/MsgMagicEffect";
import { getDamage } from "../handleDamage";
import { distanceBetweenCharacters } from "../../../logical/kernel";
import Character from "../../../game/character";
import { cnRes } from "../../../res/cnRes";
import { validatePK } from "../attack";

export function handleSingleTargetMagic(attacker: Character, target: Character, attack: MsgInteract, magicInfo: IMagicType, magicEffect: MsgMagicEffect) {
    if (validateMagicTarget(attacker, target, magicInfo)) {
        let et = new EffectTarget();
        et.X = target.x;
        et.Y = target.y;
        et.damage = getDamage(attacker, target, attack, magicInfo);
        et.damage = et.damage * (magicInfo.percent / 100);
        attack.value = et.damage;
        target.receiveDamage(attacker, attack, true);

        et.activationFlag = attack.getActivation();
        et.targetUID = target.uid;
        et.hit = true;

        magicEffect.addTarget(et);
    }

    attacker.sendToScreen(magicEffect, true);
}

function validateMagicTarget(attacker: Character, target: Character, magicInfo: IMagicType) {
    if (attacker && !attacker.dead && !target.dead) {
        if (attacker.level >= magicInfo.req_level) {
            if (attacker.uid !== target.uid) {
                if (validatePK(attacker, target)){
                    let dist = distanceBetweenCharacters(attacker, target);
                    if (dist <= magicInfo.distance) return true;
                }
            } else {
                if (attacker.player)
                    attacker.player.sendMsg(cnRes.STR_USE_MAGIC_SELF_FORBIDDEN);
            }
        } else {
            if (attacker.player)
                attacker.player.sendMsg(cnRes.STR_USE_MAGIC_LEVEL_NOT_ENOUGH);
        }
    } else {
        if (attacker.player)
            attacker.player.sendMsg(cnRes.STR_YOU_ALREADY_DIE);
    }
    return false;
}