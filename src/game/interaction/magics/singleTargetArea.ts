import Player from "../../player";
import MsgInteract from "../../../network/packets/gamepackets/MsgInteract";
import { IMagicType } from "../../../database/magicType";
import MsgMagicEffect, { EffectTarget } from "../../../network/packets/gamepackets/MsgMagicEffect";
import { getDamage } from "../handleDamage";
import { distanceBetweenCharacters } from "../../../logical/kernel";
import Character from "../../../game/character";
import { validatePK } from "../attack";
import { cnRes } from "../../../res/cnRes";

export function handleSingleTargetAreaMagic(attacker: Character, target: Character, attack: MsgInteract, magicInfo: IMagicType, magicEffect: MsgMagicEffect) {
    attacker.map.players.map(m => {
        if (magicEffect.targets.size >= 30) return;
        
        let p = m.character;
        if (validateMagicTarget(attacker, p, magicInfo)) {
            let et = new EffectTarget();
            et.X = p.x;
            et.Y = p.y;
            et.damage = getDamage(attacker, p, attack, magicInfo);
            et.damage = et.damage * (magicInfo.percent / 100);
            et.activationFlag = attack.getActivation();
            et.targetUID = p.uid;
            et.hit = true;

            magicEffect.addTarget(et);

            attack.value = et.damage;
            p.receiveDamage(attacker, attack, true);
        }
    });

    attacker.map.monsters.map(m => {
        if (magicEffect.targets.size >= 30) return;
        
        if (validateMagicTarget(attacker, m, magicInfo)) {
            let et = new EffectTarget();
            et.X = m.x;
            et.Y = m.y;
            et.damage = getDamage(attacker, m, attack, magicInfo);
            et.damage = et.damage * (magicInfo.percent / 100);
            et.activationFlag = attack.getActivation();
            et.targetUID = m.uid;
            et.hit = true;

            magicEffect.addTarget(et);

            attack.value = et.damage;
            m.receiveDamage(attacker, attack, true);
        }
    });

    attacker.sendToScreen(magicEffect, true);
}

function validateMagicTarget(attacker: Character, target: Character, magicInfo: IMagicType) {
    if (attacker && !attacker.dead && !target.dead){
        if (attacker.level >= magicInfo.req_level){
            if (attacker.uid !== target.uid){
                if (validatePK(attacker, target)){ 
                    let dist = distanceBetweenCharacters(attacker, target);
                    if (dist <= magicInfo.distance) return true;
                }
            } else {
                if (attacker.player)
                    attacker.player.sendMsg(cnRes.STR_USE_MAGIC_SELF_FORBIDDEN);
            }
        } else {
            if (attacker.player)
                attacker.player.sendMsg(cnRes.STR_USE_MAGIC_LEVEL_NOT_ENOUGH);
        }
    } else {
        if (attacker.player)
            attacker.player.sendMsg(cnRes.STR_YOU_ALREADY_DIE);
    }
    return false;
}