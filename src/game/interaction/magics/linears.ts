import Player from "../../../game/player";
import MsgInteract from "../../../network/packets/gamepackets/MsgInteract";
import InLineAlgorithm from "../../../components/inLineAlgorithm";
import { IMagicType } from "../../../database/magicType";
import MsgMagicEffect, { EffectTarget } from "../../../network/packets/gamepackets/MsgMagicEffect";
import { getDamage } from "../handleDamage";
import { validatePK } from "../attack";
import Character from "../../../game/character";
import IndexedElement from "../../../components/indexedElement";
import { cnRes } from "../../../res/cnRes";

export function handleLinearMagic(attacker: Character, attack: MsgInteract, magicInfo: IMagicType, magicEffect: MsgMagicEffect) {
    let la = new InLineAlgorithm(attacker.x, attacker.y, attack.x, attack.y, magicInfo.range);
    let targets: IndexedElement<Character> = new IndexedElement();

    attacker.map.players.map(m => {
        let p = m.character;

        if (validateLinearMagicTarget(p, attacker, magicInfo)) {
            if (la.isInLine(p.x, p.y)) {
                targets.add(p.uid, p);
            }
        }
    });

    attacker.map.monsters.map(m => {
        if (validateLinearMagicTarget(m, attacker, magicInfo)) {
            if (la.isInLine(m.x, m.y)) {
                targets.add(m.uid, m);
            }
        }
    });

    targets.map((p) => {
        if (magicEffect.targets.size >= 30) return;
        
        let et = new EffectTarget();
        et.X = p.x;
        et.Y = p.y;
        et.damage = getDamage(attacker, p, attack);

        if (magicInfo.power === 0)
            et.damage = et.damage * (magicInfo.percent / 100);
        et.damage += magicInfo.power;

        et.activationFlag = attack.getActivation();
        et.targetUID = p.uid;
        et.hit = true;

        magicEffect.addTarget(et);

        attack.value = et.damage;
        p.receiveDamage(attacker, attack, true);
    });

    attacker.sendToScreen(magicEffect, true);
}

function validateLinearMagicTarget(p: Character, attacker: Character, magicInfo: IMagicType) {
    if (p && !p.dead){
        if (p.uid !== attacker.uid){
            if (attacker.level >= magicInfo.req_level){
                if (validatePK(attacker, p))
                    return true;
            } else {
                if (attacker.player)
                    attacker.player.sendMsg(cnRes.STR_USE_MAGIC_LEVEL_NOT_ENOUGH);
            }
        } else {
            if (attacker.player)
                attacker.player.sendMsg(cnRes.STR_USE_MAGIC_SELF_FORBIDDEN);
        } 
    } else {
        if (attacker.player)
            attacker.player.sendMsg(cnRes.STR_YOU_ALREADY_DIE);
    }
    return false;
}