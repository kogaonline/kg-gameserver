import Player from "../../player";
import MsgInteract from "../../../network/packets/gamepackets/MsgInteract";
import { IMagicType } from "../../../database/magicType";
import MsgMagicEffect, { EffectTarget } from "../../../network/packets/gamepackets/MsgMagicEffect";
import { getDamage } from "../handleDamage";
import { distanceBetweenCharacters } from "../../../logical/kernel";
import Character from "../../character";
import { cnRes } from "../../../res/cnRes";

export function handleSingleTargetCureMagic(attacker: Character, target: Character, attack: MsgInteract, magicInfo: IMagicType, magicEffect: MsgMagicEffect) {
    if (validateMagicTarget(attacker, target, magicInfo)) {
        let et = new EffectTarget();
        et.X = target.x;
        et.Y = target.y;
        et.damage = magicInfo.power;
        attack.value = et.damage;
        target.receiveCure(attacker, attack, true);

        // et.activationFlag = attack.getActivation();
        et.targetUID = target.uid;
        et.hit = true;

        magicEffect.addTarget(et);
    }

    attacker.sendToScreen(magicEffect, true);
}

function validateMagicTarget(attacker: Character, target: Character, magicInfo: IMagicType) {
    if (attacker && !attacker.dead)
        if (attacker.level >= magicInfo.req_level) {
            let dist = distanceBetweenCharacters(attacker, target);
            if (dist <= magicInfo.distance) return true;
        } else {
            if (attacker.player)
                attacker.player.sendMsg(cnRes.STR_USE_MAGIC_LEVEL_NOT_ENOUGH);
        }
    return false;
}