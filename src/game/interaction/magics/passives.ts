import Player from "../../../game/player";
import Core from "../../../core";
import MsgMagicInfo from "../../../network/packets/gamepackets/MsgMagicInfo";
import MsgInteract from "../../../network/packets/gamepackets/MsgInteract";
import { handleMagicAttack, handleMagicEffect } from "../attack";
import { probability } from "../../../logical/kernel";
import Character from "../../../game/character";

export function handlePassiveMagics(attacker: Character, target: Character, attack: MsgInteract): boolean {
    if (attacker.isMonster) return;
    
    let success = false
    Core.magicInfo.map((info) => {
        let magic: MsgMagicInfo = null

        if (info.auto === 4)
            if ((magic = attacker.magics.get(info.type)) && magic.level === info.level) {
                if (attacker.level < info.req_level)
                    return

                if (probability(info.percent)) {
                    attack.target = 0
                    attack.value = 0
                    attack.x = target.x
                    attack.y = target.y
                    handleMagicEffect(attacker, target, attack, magic, info)
                    success = true
                }
            }
    })

    return success
}