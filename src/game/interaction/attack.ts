import MsgInteract from "../../network/packets/gamepackets/MsgInteract";
import { getDirection, distanceBetweenCharacters } from "../../logical/kernel";
import DBService from "../../services/database";
import { IMagicType } from "../../database/magicType";
import MsgMagicEffect, { EffectTarget } from "../../network/packets/gamepackets/MsgMagicEffect";
import { handleLinearMagic } from "./magics/linears";
import { EMagicSort } from "../../enums/EMagicSort";
import { handleSingleTargetMagic } from "./magics/singleTarget";
import { handleSingleTargetCureMagic } from "./magics/singleCure";
import { getDamage } from "./handleDamage";
import { handlePassiveMagics } from "./magics/passives";
import MsgMagicInfo from "../../network/packets/gamepackets/MsgMagicInfo";
import { EStatusEffect } from "../../enums/EStatusEffect";
import { EPKMode } from "../../enums/EPKMode";
import Character from "../character";
import { handleSingleTargetAreaMagic } from "./magics/singleTargetArea";
import { handleAreaMagic } from "./magics/area";
import { handleXpStatusMagic } from "./magics/xpStatus";
import { handleReviveMagic } from "./magics/revive";
import { EItemPosition } from "../../enums/EItemPosition";

export function handleAttack(attacker: Character, target: Character, attack: MsgInteract) {
    if (firstThingsFirst(attacker, target, attack)) {
        attacker.lastAttack = attack;
        attacker.timeOf.lastAttack = Date.now();

        extractWeaponData(attacker, attack);

        attack.value = getDamage(attacker, target, attack);
        if (handlePassiveMagics(attacker, target, attack))
            return;
        target.receiveDamage(attacker, attack);
    }
}

export function handleMagicAttack(attacker: Character, target: Character, attack: MsgInteract) {
    if (attacker.hasMagic(attack.type)) {
        attack.value = 0;

        let magicData = attacker.getMagic(attack.type);
        let magicInfo = DBService.MagicsTypeTable.getMagicInfo(magicData.type, magicData.level);
        attack.magic = attack.type;

        attacker.lastAttack = attack;

        if (firstThingsFirst(attacker, target, attack, magicInfo, true)) {
            handleMagicEffect(attacker, target, attack, magicData, magicInfo);
            attacker.timeOf.lastAttack = Date.now();
        }
    }
}

export function handleMagicEffect(attacker: Character, target: Character, attack: MsgInteract, magicData: MsgMagicInfo, magicInfo: IMagicType) {
    let magicEffect = new MsgMagicEffect();
    magicEffect.sender = attacker.uid;
    magicEffect.id = magicData.type;
    magicEffect.level = magicData.level;
    magicEffect.x = attack.x;
    magicEffect.y = attack.y;

    let dir = getDirection(attacker.x, attacker.y, attack.x, attack.y);
    attacker.facing = dir;

    switch (magicInfo.sort) {
        case EMagicSort.Linear:
            handleLinearMagic(attacker, attack, magicInfo, magicEffect);
            break;
        case EMagicSort.Single:
            handleSingleTargetMagic(attacker, target, attack, magicInfo, magicEffect);
            break;
        case EMagicSort.SingleArea:
            handleSingleTargetAreaMagic(attacker, target, attack, magicInfo, magicEffect);
            break;
        case EMagicSort.SingleCure:
            handleSingleTargetCureMagic(attacker, target, attack, magicInfo, magicEffect);
            break;
        case EMagicSort.XPStatus:
            handleXpStatusMagic(attacker, target, attack, magicInfo, magicEffect);
            break;
        case EMagicSort.Revive:
            handleReviveMagic(attacker, target, attack, magicInfo, magicEffect);
            break;
        case EMagicSort.Area:
            handleAreaMagic(attacker, attack, magicInfo, magicEffect);
            break;
        default: {
            if (attacker.player)
                attacker.player.sendMsg(`Magic Sort: ${magicInfo.sort}`);
            break;
        }
    }
    
    consumeMagic(attacker, magicInfo);
}

function firstThingsFirst(attacker: Character, target: Character, attack: MsgInteract, magicInfo: IMagicType = null, isMagic: boolean = false): boolean {
    let now = Date.now();
    let agiCoefx = (attacker.agility * 5) + attacker.speed;

    if (!isMagic) {
        if (target)
            if (distanceBetweenCharacters(attacker, target) <= attacker.attackRange)
                if (!target.dead)
                    if (now > attacker.timeOf.lastAttack + (1200 - agiCoefx))
                        return true;
            
    } else if (magicInfo) {
        if (attack.target === 0){
            attacker.lastAttack = null;
            if (validateMagicConsumption(attacker, magicInfo)) 
                if (now > attacker.timeOf.lastAttack + (1500 - agiCoefx))
                    return true;
        }
        if (target)
            if (distanceBetweenCharacters(attacker, target) <= magicInfo.distance)
                if (!target.dead)
                    if (validateMagicConsumption(attacker, magicInfo)) 
                        if (now > attacker.timeOf.lastAttack + (1500 - agiCoefx))
                            return true;
    }

    attacker.lastAttack = null;
    return false;
}

function validateMagicConsumption(attacker: Character, magicInfo: IMagicType) {
    if (attacker.stamina >= magicInfo.stamina)
        if (attacker.mana >= magicInfo.manacost)
            if (magicInfo.xp === 1) {
                if (attacker.hasStatus(EStatusEffect.XPList))
                    return true
                else return false
            }
            else
                return true

    return false
}

function consumeMagic(attacker: Character, magicInfo: IMagicType) {
    if (magicInfo.manacost > 0)
        attacker.mana -= magicInfo.manacost
    if (magicInfo.stamina > 0)
        attacker.stamina -= magicInfo.stamina

    if (magicInfo.xp) {
        if (attacker.hasStatus(EStatusEffect.XPList))
            attacker.removeStatus(EStatusEffect.XPList)
    }
}

export function validatePK(attacker: Character, attacked: Character) {
    if (attacked.isMonster) return true;

    if (attacker.pkMode === EPKMode.Peace) {
        return false;
    }

    if (attacker.pkMode === EPKMode.PK) {
        attacker.timeOf.flashingName = Date.now();
        attacker.addStatus(EStatusEffect.FlashingName);
        return true;
    }

    if (attacker.pkMode === EPKMode.Capture && attacked.hasStatus(EStatusEffect.FlashingName)) {
        return true;
    }

    if (attacker.pkMode === EPKMode.Revenge) {
        return true;
    }

    if (attacker.pkMode === EPKMode.Team) {
        return true;
    }

    if (attacker.pkMode === EPKMode.Guild) {
        return true;
    }

    return false;
}

export function extractWeaponData(attacker: Character, attack: MsgInteract){
    if (!attacker.player) return;

    let wep1 = attacker.player.equipment.getPos(EItemPosition.RightWeapon);
    let wep2 = attacker.player.equipment.getPos(EItemPosition.LeftWeapon);

    if (wep1){
        let typeId = Math.round(wep1.id / 1000);
        attack.weaponTypeR = typeId;
    }

    if (wep2){
        let typeId = Math.round(wep2.id / 1000);
        attack.weaponTypeL = typeId;
    }
}