import Player from "./player"
import IndexedElement from "../components/indexedElement"
import MsgItemInfo from "../network/packets/gamepackets/MsgItemInfo"
import MsgItem from "../network/packets/gamepackets/MsgItem"
import { EItemActionType, EItemMode } from "../enums/EItemAction"
import DBService from "../services/database"
import { EItemPosition } from "../enums/EItemPosition"
import { cnRes } from "../res/cnRes"
import { sanitizeInt } from "../logical/kernel";

export default class Equipment {
    player: Player
    items: IndexedElement<MsgItemInfo>

    constructor(player: Player) {
        this.player = player
        this.items = new IndexedElement<MsgItemInfo>()
    }

    add(position: EItemPosition, item: MsgItemInfo) {
        item.position = position
        this.items.add(position, item)
        this.player.inventory.equip(item)
        item.send(this.player)
        this.player.character.sendToScreen(this.player.character.getSpawnPacket())
        this.player.character.recalculateStats()
        this.sendEquips()
        DBService.ItemsTable.updateItem(item, this.player);
    }

    remove(position: EItemPosition) {
        let success = false
        this.items.map((_item) => {
            if (_item.position === position) {
                if (this.player.inventory.items.size < 40) {
                    this.items.remove(position)
                    this.player.inventory.add(_item)
                    _item.position = EItemPosition.Inventory

                    let iu = new MsgItem(true)
                    iu.param1 = position
                    iu.uid = _item.uid
                    iu.action = EItemActionType.UnequipItem
                    iu.send(this.player)
                    _item.send(this.player)

                    this.player.character.sendToScreen(this.player.character.getSpawnPacket())
                    this.player.character.recalculateStats()
                    this.sendEquips()
                    DBService.ItemsTable.updateItem(_item, this.player);
                } else {
                    this.player.sendMsg(cnRes.STR_EQUIPMENT_UNEQUIP_INVENTORY_FULL)
                }
            }
        })

        return success
    }

    occupied(position: EItemPosition) {
        let success = false
        this.items.map((_item, i, index) => {
            let pos = sanitizeInt(index)
            if (pos === position) {
                success = true
            }
        })

        return success
    }

    /**
     * Returns the item at position `position`. Returns null if slot is empty.
     * @param position is the position of equipment.
     */
    getPos(position: EItemPosition): MsgItemInfo {
        let item = null
        this.items.map((_item) => {
            if (_item.position === position) {
                item = _item
            }
        })

        return item
    }

    getUID(uid: number): MsgItemInfo {
        let _item = null
        this.items.map((item) => {
            if (item.uid === uid) {
                _item = item
            }
        })

        return _item
    }

    sendEquips() {

        let rweapon = this.getPos(EItemPosition.RightWeapon)
        let lweapon = this.getPos(EItemPosition.LeftWeapon)
        let head = this.getPos(EItemPosition.Head)
        let armor = this.getPos(EItemPosition.Armor)
        let boots = this.getPos(EItemPosition.Boots)
        let garment = this.getPos(EItemPosition.Garment)
        let ring = this.getPos(EItemPosition.Ring)
        let necklace = this.getPos(EItemPosition.Necklace)
        let bottle = this.getPos(EItemPosition.Bottle)

        let aHead = this.getPos(EItemPosition.AlternateHead)
        let aArmor = this.getPos(EItemPosition.AlternateArmor)
        let aRing = this.getPos(EItemPosition.AlternateRing)
        let aBoots = this.getPos(EItemPosition.AlternateBoots)
        let aNecklace = this.getPos(EItemPosition.AlternateNecklace)
        let aGarment = this.getPos(EItemPosition.AlternateGarment)
        let aBottle = this.getPos(EItemPosition.AlternateBottle)
        let aRWeapon = this.getPos(EItemPosition.AlternateRightWeapon)
        let aLWeapon = this.getPos(EItemPosition.AlternateLeftWeapon)

        let items = new MsgItem(true)
        items.uid = this.player.character.uid
        items.action = EItemActionType.SendEquips
        items.alternativeEquipment = this.player.character.alterantiveEquipment ? 1 : 0

        items.rightWeapon = rweapon ? rweapon.uid : 0
        items.leftWeapon = lweapon ? lweapon.uid : 0
        items.head = head ? head.uid : 0
        items.armor = armor ? armor.uid : 0
        items.boots = boots ? boots.uid : 0
        items.garment = garment ? garment.uid : 0
        items.ring = ring ? ring.uid : 0
        items.necklace = necklace ? necklace.uid : 0
        items.bottle = bottle ? bottle.uid : 0

        items.rightWeapon = aRWeapon ? aRWeapon.uid : items.rightWeapon
        items.leftWeapon = aLWeapon ? aLWeapon.uid : items.leftWeapon
        items.head = aHead ? aHead.uid : items.head
        items.armor = aArmor ? aArmor.uid : items.armor
        items.boots = aBoots ? aBoots.uid : items.boots
        items.garment = aGarment ? aGarment.uid : items.garment
        items.ring = aRing ? aRing.uid : items.ring
        items.necklace = aNecklace ? aNecklace.uid : items.necklace
        items.bottle = aBottle ? aBottle.uid : items.bottle

        items.send(this.player)
    }

    static getPosition(id: number): EItemPosition {
        let pos: EItemPosition = EItemPosition.Inventory
        let s = id % 1000
        let type = ((id - s) / 1000)

        if ((type >= 111 && type <= 118) || (type >= 141 && type <= 144) || (type === 123)) {
            pos = EItemPosition.Head
        }

        if ((type >= 120 && type <= 121)) {
            pos = EItemPosition.Necklace
        }

        if ((type >= 130 && type <= 139)) {
            pos = EItemPosition.Armor
        }

        if ((type >= 150 && type <= 152)) {
            pos = EItemPosition.Ring
        }

        if ((type === 160)) {
            pos = EItemPosition.Ring
        }

        if ((type >= 181 && type <= 194)) {
            pos = EItemPosition.Garment
        }

        if ((type === 200)) {
            pos = EItemPosition.SteedArmor
        }

        if ((type === 201)) {
            pos = EItemPosition.AttackTalisman
        }

        if ((type === 202)) {
            pos = EItemPosition.DefenseTalisman
        }

        if ((type === 203)) {
            pos = EItemPosition.SteedTalisman
        }

        if ((type === 300)) {
            pos = EItemPosition.Steed
        }

        if ((type >= 350 && type <= 370)) {
            pos = EItemPosition.RightWeaponAccessory
        }

        if ((type === 380)) {
            pos = EItemPosition.LeftWeaponAccessory
        }

        if ((type >= 410 && type <= 490) || (type >= 500 && type <= 580) || (type >= 601 && type <= 616)) {
            pos = EItemPosition.RightWeapon
        }

        if ((type === 1050 || type == 900)) {
            pos = EItemPosition.LeftWeapon
        }

        if ((type === 2100)) {
            pos = EItemPosition.Bottle
        }

        return pos
    }
}