import IMapElement from "../interfaces/IMapElement";
import { EMapElementType } from "../enums/EMapElementType";
import { ENPCType, EMsgNpcActionType } from "../enums/ENPCType";
import Player from "./player";
import MsgNpcInfo from "../network/packets/gamepackets/MsgNpcInfo";
import MsgNpc from "../network/packets/gamepackets/MsgNpc";

export default class NPC implements IMapElement {
    uid: number;
    mapId: number;
    x: number;
    y: number;
    mapElementType: EMapElementType;

    name: string;
    type: ENPCType;
    lookface: number;
    task0: number;
    task1: number;
    task2: number;
    task3: number;
    task4: number;
    task5: number;
    task6: number;
    task7: number;
    task8: number;
    task9: number;
    data0: number;
    data1: number;
    data2: number;
    data3: number;
    datastr: string;
    life: number;
    maxlife: number;
    base: number;
    sort: number;
    item: number;

    constructor(){
        this.mapElementType = EMapElementType.NPC;
    }

    sendSpawn(player: Player){
        let spawn = new MsgNpcInfo();
        spawn.uid = this.uid;
        spawn.type = this.type;
        spawn.lookface = this.lookface;
        spawn.x = this.x;
        spawn.y = this.y;
        spawn.name = this.name;

        player.send(spawn);
    }

    removeSpawn(player: Player){
        let spawn = new MsgNpc();
        spawn.uid = this.uid;
        spawn.action = EMsgNpcActionType.DeleteNpc;

        player.send(spawn);
    }
}