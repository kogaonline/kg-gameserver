import Character from './character';
import { MONEY_DROP_RATE } from '../settings';
import { probability, getRandom, getRandomInt } from '../logical/kernel';
import { getMoneyItemID } from './functions';
import { EMapElementType } from '../enums/EMapElementType';
import { GroundItem } from './map';
import { EMsgMapItemAction } from '../enums/EMsgMapItemAction';
import Core from '../core';

export function dropGold(monster: Character){
    let gRate = MONEY_DROP_RATE;
    if (gRate < 0) gRate = 1;

    if (probability(gRate, 1000)){
        let amount = getRandom(1, 2);
        if (probability(2)) amount = 5;

        for (let i = 0; i < amount; i++){
            let monsterDrop = monster.monsterType.drop_money;
            let dropCoefx = Math.ceil(monsterDrop + 10 + ((monster.monsterType.level * getRandom(1, 25)) / 2));
            let goldAmount = getRandom(monsterDrop, dropCoefx);
            if (probability(1, 50)) goldAmount *= getRandom(2, 5);

            if (goldAmount === 0) return;

            let itemID = getMoneyItemID(goldAmount);
            let location = monster.map.ground.getNextValid(EMapElementType.ITEM, monster.x, monster.y);
            if (location){
                let drop = new GroundItem();
                drop.x = location.x;
                drop.y = location.y;
                drop.type = itemID;
                drop.action = EMsgMapItemAction.Create;
                drop.mapElementType = EMapElementType.ITEM;
                drop.mapId = monster.mapId;
                drop.moneyValue = goldAmount;
                drop.uid = Core.itemUIDCounter++;
                drop.valueItem = true;

                monster.map.showGroundItem(drop);
            }

        }
    }
}