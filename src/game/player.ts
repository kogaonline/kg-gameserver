import * as net from 'net';
import * as Settings from '../settings';
import DHKey from '../network/packets/gamepackets/MsgDH';
import Writer from '../network/packets/BufferWriter';
import GameCryptography from '../network/cryptography/gameCryptography';
import PacketQueue from '../components/packetQueue';
import IPacket from '../interfaces/IPacket';
import Character from './character';
import MsgTalk from '../network/packets/gamepackets/MsgTalk';
import MsgData from '../network/packets/gamepackets/MsgData';
import MsgTaskDialog from '../network/packets/gamepackets/MsgTaskDialog';
import Core from '../core';
import Screen from './screen';
import Inventory from './inventory';
import Equipment from './equipment';
import CharacterTable from '../database/character';
import { EMsgTalkType } from '../enums/EMsgTalkType';
import { IAccountRow } from '../database/sequelize/models/accounts';
import GameServerSocket from '../network/sockets/gameSocket';

export default class Player {
    socket: net.Socket;
    id: number;

    account: IAccountRow;

    cryptography: GameCryptography;
    dKeyExchange: DHKey;

    queue: PacketQueue;
    recqueue: PacketQueue;
    bkpqueue: PacketQueue;

    exchanged: boolean = false;
    playerAction: number = 0;

    character: Character;
    screen: Screen;
    inventory: Inventory;
    equipment: Equipment;

    bypass: boolean;

    taskDialogs: MsgTaskDialog[];
    taskDialogOptions: number[];

    iters: any;

    loginhr: any;

    constructor(connector: net.Socket, bypass?: boolean) {
        this.socket = connector;
        let key = Buffer.from(Settings.SERVER_KEY);

        this.cryptography = new GameCryptography(key);
        this.dKeyExchange = new DHKey();

        this.queue = new PacketQueue();
        this.recqueue = new PacketQueue();
        this.bkpqueue = new PacketQueue();

        this.bypass = bypass;

        this.screen = new Screen(this);
        this.inventory = new Inventory(this);
        this.equipment = new Equipment(this);

        this.taskDialogs = [];
        this.taskDialogOptions = [0];

        this.iters = {};

        // start send & receive
        // this.startReceiver();
        // this.startSender();
    }

    /*async startSender(){
        let _this = this;
        setInterval(() => {
            while (_this.queue.canDequeue()){
                let _buffer = _this.queue.dequeue();
                _this.socket.write(_buffer);
            }
        }, 0);
    }

    async startReceiver(){
        let _this = this;
        setInterval(() => {
            while (_this.recqueue.canDequeue()){
                let _buffer = _this.recqueue.dequeue();
                GameServerSocket.handleData(_buffer, _this.socket);
            }
        }, 0);
    }*/

    send(packet: Buffer | IPacket) {
        if (this.socket && this.socket.writable && !this.socket.destroyed) {
            if (packet instanceof Buffer) {
                let len = packet.readUInt16LE(0);
                //let id = packet.readUInt16LE(2);

                if (len === 0)
                    Writer.WriteUInt16(packet.length - 8, 0, packet);
                packet.write(Settings.DH_KEY_S, packet.length - 8);

                // console.log('Sent before encrypt');
                // printBuffer(packet);
                let _buffer: any = Buffer.from(packet);
                if (!this.bypass)
                    _buffer = this.cryptography.encrypt(_buffer);

                // console.log('Sent after encrypt');
                // printBuffer(_buffer);

                this.socket.write(_buffer);
                // this.queue.enqueue(_buffer);
            } else
                this.send(packet.toArray());
        }
    }

    sendDate(){
        let now = new Date();
        let date = new MsgData();
        date.year = now.getFullYear() - 1900;
        date.month = now.getMonth();
        date.dayOfYear = 115; // Not implemented
        date.day = now.getDate();
        date.hour = now.getHours();
        date.minute = now.getMinutes();
        date.second = now.getSeconds();
        this.send(date);
    }

    sendMsg(text: string, type: EMsgTalkType = EMsgTalkType.Talk) {
        let msg = new MsgTalk(text);
        msg.type = type;
        this.send(msg);
    }

    async disconnect() {
        await Core.playerPool.remove(this.id);

        this.queue.clear();
        this.recqueue.clear();

        if (this.character) {
            this.character.removeSpawn();
            this.character.map.players.remove(this.character.uid);
            await this.save();
        }

        if (this.socket) {
            this.socket.end();
            this.socket.destroy();
        }
    }

    loadFromSnapshot(p: Player) {
        this.character = new Character(this, false, false);
        for (var k in p.character) {
            if (k === "map" ||
                k === "magics" ||
                k === "weaponSkills")
                continue;
            (this.character as any)[k] = (p.character as any)[k];
        };

        Core.playerPool.remove(this.id);
        Core.playerPool.add(this.id, this);

        this.inventory.clear();
        this.inventory.loadItems();
    }

    async save() {
        await CharacterTable.saveCharacter(this);
    }
}