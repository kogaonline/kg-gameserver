import IMapElement from "../interfaces/IMapElement";
import IndexedElement from "../components/indexedElement";
import { GAME_SCREEN_SIZE, GAME_SCREEN_REMOVE } from "../settings";
import Core from "../core";
import { distanceBetween, distanceBetweenPlayers, distanceBetweenCharacters } from "../logical/kernel";
import Player from "./player";
import IPacket from "../interfaces/IPacket";
import { EMapElementType } from "../enums/EMapElementType";
import Character from "./character";
import MsgAction from "../network/packets/gamepackets/MsgAction";
import { EMsgActionType } from "../enums/EMsgActionType";
import { GroundItem } from "./map";
import NPC from "./npc";

export default class Screen {
    elements: IMapElement[];
    entities: IndexedElement<Character>;
    items: IndexedElement<GroundItem>;
    npcs: IndexedElement<NPC>;
    player: Player;

    constructor(player: Player) {
        this.elements = [];
        this.entities = new IndexedElement();
        this.items = new IndexedElement();
        this.npcs = new IndexedElement();
        this.player = player;
    }

    updateElements(){
        this.elements = [];
        this.entities.map(e => this.elements.push(e));
        this.items.map(e => this.elements.push(e));
        this.npcs.map(e => this.elements.push(e));
    }

    add(obj: IMapElement): boolean {
        if (obj){
            switch (obj.mapElementType){
                case EMapElementType.MONSTER:
                case EMapElementType.PLAYER: {
                    if (!this.entities.contains(obj.uid))
                        if (distanceBetween(this.player.character.x, this.player.character.y, obj.x, obj.y) <= GAME_SCREEN_SIZE) {
                            this.entities.add(obj.uid, (obj as Character));
                            this.player.character.sendSpawn(obj as Character);
                            (obj as Character).sendSpawn(this.player.character);
                        }
                    break;
                }
                case EMapElementType.ITEM: {
                    if (!this.items.contains(obj.uid))
                        if (distanceBetween(this.player.character.x, this.player.character.y, obj.x, obj.y) <= GAME_SCREEN_SIZE) {
                            this.items.add(obj.uid, (obj as GroundItem));
                        }
                    break;
                }
                case EMapElementType.NPC: {
                    if (!this.npcs.contains(obj.uid))
                        if (distanceBetween(this.player.character.x, this.player.character.y, obj.x, obj.y) <= GAME_SCREEN_SIZE) {
                            this.npcs.add(obj.uid, (obj as NPC));
                            (obj as NPC).sendSpawn(this.player);
                        }
                    break;
                }
            }

            this.updateElements();
            return true;
        }
        return false;
    }

    remove(obj: IMapElement): boolean {
        if (obj) {
            switch (obj.mapElementType){
                case EMapElementType.MONSTER:
                case EMapElementType.PLAYER: {
                    if (this.entities.contains(obj.uid))
                        if (distanceBetween(this.player.character.x, this.player.character.y, obj.x, obj.y) >= GAME_SCREEN_REMOVE) {
                            this.entities.remove(obj.uid);
                            this.sendRemove(obj.uid, this.player);
                            if (obj.player){
                                obj.player.screen.remove(this.player.character);
                            }
                            (obj as Character).removeScreenSpawn(this.player);
                            return true;
                        }
                    break;
                }
                case EMapElementType.ITEM: {
                    if (this.items.contains(obj.uid))
                        if (distanceBetween(this.player.character.x, this.player.character.y, obj.x, obj.y) >= GAME_SCREEN_REMOVE) {
                            this.items.remove(obj.uid);
                            return true;
                        }
                    break;
                }
                case EMapElementType.NPC: {
                    if (this.npcs.contains(obj.uid))
                        if (distanceBetween(this.player.character.x, this.player.character.y, obj.x, obj.y) >= GAME_SCREEN_REMOVE) {
                            this.npcs.remove(obj.uid);
                            return true;
                            //(obj as NPC).removeSpawn(this.player);
                        }
                    break;
                }
            }

            this.updateElements();
        }
        return false;
    }

    sendRemove(uid: number, player: Player) {
        let data = new MsgAction(true);
        data.uid = uid;
        data.id = EMsgActionType.RemoveEntity;
        player.send(data);
    }

    reload(withPacket?: IPacket) {
        if (!this.player)
            return;

        this.clean();
        if (this.player.character.fullyLoaded){
            this.reloadPlayers();
            this.reloadMonsters();
            this.reloadNpcs();
            this.reloadItems();
        }
    }

    async reloadPlayers() {
        Core.playerPool.map((p) => {
            if (!p || !p.character || !p.character.sendSpawn || !this.player.character || p.character.uid === this.player.character.uid)
                return;
            if (p.character.mapId === this.player.character.mapId)
                this.add(p.character);
        });
    }

    async reloadMonsters() {
        let map = Core.maps.get(this.player.character.mapId);
        if (map) {
            map.monsters.map((mm: IMapElement) => {
                let m = mm as Character;
                if (!m || !m.isMonster || !m.sendSpawn)
                    return;
                this.add(m);
            });
        }
    }

    async reloadNpcs() {
        let map = Core.maps.get(this.player.character.mapId);
        if (map) {
            map.npcs.map((mm: IMapElement) => {
                let npc = mm as NPC;
                if (!npc)
                    return;

                this.add(npc);
            });
        }
    }

    async reloadItems() {
        let map = Core.maps.get(this.player.character.mapId);
        if (map) {
            map.groundItems.map((i) => {
                if (!i) return;
                this.add(i);
            });
        }
    }

    clean() {
        try {
            this.elements.map((element) => {
                this.remove(element);
            });
        } catch (err) {
            console.log(err);
            console.trace(err);
        }
    }

    clearAll() {
        try {
            //this.clean();
            this.entities = new IndexedElement();
            this.items = new IndexedElement();
            this.npcs = new IndexedElement();
            
            this.updateElements();
        } catch (err) {
            console.log(err);
        }
    }
}