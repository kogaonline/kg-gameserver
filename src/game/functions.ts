export function  getMoneyItemID(value: number)
{
    if (value < 100)
        return 1090000;
    else if (value < 399)
        return 1090010;
    else if (value < 5099)
        return 1090020;
    else if (value < 8099)
        return 1091000;
    else if (value < 12099)
        return 1091010;
    else
        return 1091020;
}