import Player from "./player";
import IndexedElement from "../components/indexedElement";
import Character from "./character";
import MapGround from "./mapGround";
import NPC from "./npc";
import Core from "../core";
import { existsSync, createReadStream } from "fs";
import { EMapElementType } from "../enums/EMapElementType";
import { EMsgMapItemAction } from "../enums/EMsgMapItemAction";
import MsgItemInfo from "../network/packets/gamepackets/MsgItemInfo";
import IMapElement from "../interfaces/IMapElement";
import { distanceBetween } from "../logical/kernel";
import { GAME_SCREEN_SIZE } from "../settings";

interface Portal {
    portalX: number;
    portalY: number;
}

export class GroundItem implements IMapElement {
    uid: number;
    type: number;
    color: number;
    info: MsgItemInfo;
    mapId: number;
    x: number;
    y: number;
    mapElementType: EMapElementType;
    action: EMsgMapItemAction;
    timestamp: number;

    moneyValue: number;
    emoneyValue: number;
    valueItem: boolean;
}

export default class Map {

    id: number;
    base: number;

    portalX: number;
    portalY: number;

    portals: IndexedElement<Portal>;
    players: IndexedElement<Player>;
    monsters: IndexedElement<Character>;
    groundItems: IndexedElement<GroundItem>;
    npcs: IndexedElement<NPC>;

    ground: MapGround;

    constructor(id: number = 0, base: number = 0, autoload = true) {
        this.id = id;
        this.base = base;

        this.players = new IndexedElement();
        this.monsters = new IndexedElement();
        this.portals = new IndexedElement();
        this.groundItems = new IndexedElement();
        this.npcs = new IndexedElement();

        if (autoload)
            this.loadGround(base);
    }

    addMonster(monster: Character){
        this.monsters.add(monster.uid, monster);
        if (this.ground)
            this.ground.setInvalidLocation(EMapElementType.MONSTER, monster.x, monster.y);
    }

    removeMonster(monster: Character){
        this.monsters.remove(monster.uid);
        if (this.ground)
            this.ground.setValidLocation(EMapElementType.MONSTER, monster.x, monster.y);
    }

    addPlayer(p: Player){
        this.players.add(p.character.uid, p);
        if (this.ground)
            this.ground.setInvalidLocation(EMapElementType.PLAYER, p.character.x, p.character.y);
    }

    removePlayer(p: Player){
        this.players.remove(p.character.uid);
        if (this.ground)
            this.ground.setValidLocation(EMapElementType.PLAYER, p.character.x, p.character.y);
    }

    addGroundItem(i: MsgItemInfo, x: number, y: number){
        let groundItem = new GroundItem();
        groundItem.uid = i.uid;
        groundItem.type = i.id;
        groundItem.mapId = this.id,
        groundItem.x = x;
        groundItem.y = y;
        groundItem.color = i.color;
        groundItem.info = i;
        groundItem.action = EMsgMapItemAction.Create;

        this.showGroundItem(groundItem);
    }

    showGroundItem(i: GroundItem){
        i.timestamp = Date.now();
        this.groundItems.add(i.uid, i);
        this.players.map(p => {
            if (distanceBetween(p.character.x, p.character.y, i.x, i.y) <= GAME_SCREEN_SIZE)
                p.character.sendGroundItemSpawn(i);
        });

        if (this.ground)
            this.ground.setInvalidLocation(EMapElementType.ITEM, i.x, i.y);
    }

    removeGroundItem(i: GroundItem){
        i.action = EMsgMapItemAction.Delete;
        this.groundItems.remove(i.uid);
        this.players.map(p => {
            if (distanceBetween(p.character.x, p.character.y, i.x, i.y) <= GAME_SCREEN_SIZE + 10){
                p.character.sendGroundItemSpawn(i);
            }
        });

        if (this.ground)
            this.ground.setValidLocation(EMapElementType.ITEM, i.x, i.y);
    }

    addNpc(npc: NPC){
        this.npcs.add(npc.uid, npc);
        this.players.map(p => npc.sendSpawn(p));
        if (this.ground)
            this.ground.setInvalidLocation(EMapElementType.NPC, npc.x, npc.y);
    }

    removeNpc(npc: NPC){
        this.npcs.remove(npc.uid);
        this.players.map(p => npc.removeSpawn(p));
        if (this.ground)
            this.ground.setValidLocation(EMapElementType.NPC, npc.x, npc.y);
    }

    removeAllNpc(){
        this.npcs.map(npc => {
            this.npcs.remove(npc.uid);
            this.players.map(p => npc.removeSpawn(p));
            
            if (this.ground)
                this.ground.setValidLocation(EMapElementType.NPC, npc.x, npc.y);
        });
    }


    async loadGround(base: number){
        try {
            let path = Core.dmaps.get(base);
            const gm = `${process.cwd()}/${path}`;
            if (existsSync(gm)){
                let data = '';
                let stream = createReadStream(gm, {
                    encoding: 'binary'
                });

                await new Promise((resolve, reject) => {
                    stream.on('data', chunk => data += chunk);
                    stream.on('error', (err) => console.log(err));
                    stream.on('close', () => {
                        resolve(true);
                    });
                    stream.on('end', () => {
                        try {
                            let ptr = 268;
                            let gmBuffer = Buffer.from(data, 'binary');

                            let width = gmBuffer.readUInt32LE(ptr); ptr += 4;
                            let height = gmBuffer.readUInt32LE(ptr); ptr += 4;
                            this.ground = new MapGround(width, height);

                            for (let y = 0; y < height; y++){
                                for (let x = 0; x < height; x++){
                                    let walkable = gmBuffer.readUInt16LE(ptr) === 0; ptr += 2;
                                    if (walkable){
                                        this.ground.setInvalidLocation(EMapElementType.NONE, x, y);
                                    }
                                    ptr += 4;
                                }
                                ptr += 4;
                            }

                            let portals = gmBuffer.readUInt32LE(ptr); ptr += 4;

                            for (let pi = 0; pi < portals; pi++){
                                let px = gmBuffer.readUInt32LE(ptr) - 1; ptr += 4;
                                let py = gmBuffer.readUInt32LE(ptr) - 1; ptr += 4;
                                let id = gmBuffer.readUInt32LE(ptr); ptr += 4;

                                this.portals.add(id, {
                                    portalX: px,
                                    portalY: py
                                });

                                for (let x = 0; x < 3; x++){
                                    for (let y = 0; y < 3; y++){
                                        if (py + y < height && px + x < width){
                                            this.ground.setInvalidLocation(EMapElementType.PORTAL, x, y);
                                        }
                                    }
                                }
                            }
                            data = '';
                            stream.close();
                            stream.destroy();
                        } catch (err){
                            console.log(err);
                            reject(err);
                        }
                    });
                });
            } else {
                console.error(`DMap ${path} not found!`);
            }
        } catch (err){
            console.log(err);
            console.log("Failed to load DMap.")
        }
    }


}