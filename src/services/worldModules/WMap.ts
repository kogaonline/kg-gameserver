import Map from "../../game/map";
import { ITEM_GROUND_TIME } from "../../settings";

export function groundItems(now: number, map: Map){
    map.groundItems.map(i => {
        if (now > (i.timestamp + (ITEM_GROUND_TIME * 1000))){
            map.removeGroundItem(i);
        }
    });
}