import Character from "../../game/character";
import { EAction } from "../../enums/EAction";
import { EStatusEffect } from "../../enums/EStatusEffect";
import Core from "../../core";
import handleInteraction from "../../game/interaction/handle";

export function autoAttack(now: number, character: Character) {
    let agiCoefx = (character.agility * 5) + character.speed;

    if (character.lastAttack) {
        if (now > character.timeOf.lastAttack + (1200 - agiCoefx))
            handleInteraction(character.lastAttack, character);
    }
}

export function stamina(now: number, character: Character) {
    if (character.stamina < 150) {
        if (now > character.timeOf.staminaRecharge + 2000) {
            let add = 5;
            if (character.action === EAction.Sit)
                add = 10;
            character.stamina += add;
            character.timeOf.staminaRecharge = now;
        }
    }
}

export function flashingName(now: number, character: Character) {
    if (character.hasStatus(EStatusEffect.FlashingName)) {
        if (now > new Date(character.timeOf.flashingName).addSeconds(60).valueOf()) {
            character.removeStatus(EStatusEffect.FlashingName);
        }
    }
}

export function pkPointDiminishes(now: number, character: Character) {
    if (now > new Date(character.timeOf.pkPointDiminished).addMinutes(10).valueOf()) {
        if (character.pkPoints > 0)
            character.pkPoints--;
        character.timeOf.pkPointDiminished = now;
    }
}

export function xpCircle(now: number, character: Character) {
    if (now > new Date(character.timeOf.xpCircleList).addSeconds(5).valueOf()) {
        if (character.xpcircle < 100)
            character.xpcircle++;

        if (character.xpcircle >= 100) {
            character.addStatus(EStatusEffect.XPList);
            character.timeOf.xpList = now;
            character.xpcircle = 0;
        }

        character.timeOf.xpCircleList = now;
    }
    if (now > new Date(character.timeOf.xpList).addSeconds(20).valueOf()) {
        character.removeStatus(EStatusEffect.XPList);
    }
}

export function attrStatus(now: number, character: Character) {
    let spellIds = Object.keys(character.timeOf.spell);
    spellIds.map(sid => {
        let type = Number.parseInt(sid);
        if (now >= (character.timeOf.spell[type] + (character.xpKillCount * 1000))) {
            let info = Core.magicInfo.get(character.valueOf.spell[type]);
            if (info) {
                character.removeStatus(info.status);
                character.removeMagicAttrValue(info.type);
                character.xpKillCount = 0;
            }
        }
    });
}