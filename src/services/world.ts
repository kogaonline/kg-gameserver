import Core from "../core";
import Player from "../game/player";
import Map from "../game/map";

import { 
    stamina, 
    flashingName, 
    pkPointDiminishes, 
    xpCircle, 
    attrStatus, 
    autoAttack 
} from './worldModules/WCharacter';
import { groundItems } from './worldModules/WMap';

export default class WorldService {
    static characterHandlerTime: number = 0;
    static characterHandlerTimeMax: number = 0;

    constructor() {
        this.start();
    }

    start() {
        this.registerServiceHandler(200, this.characterHandler);
        this.registerServiceHandler(1000, this.mapHandler);
    }

    registerServiceHandler(time: number, callback: () => void) {
        setInterval(callback, time);
    }

    characterHandler() {
        let now = Date.now();

        Core.playerPool.map((_player: Player) => {
            let start = process.hrtime();
            let { character } = _player

            stamina(now, character);
            flashingName(now, character);
            pkPointDiminishes(now, character);
            xpCircle(now, character);
            attrStatus(now, character);
            autoAttack(now, character);

            let end = process.hrtime(start);
            let endns = end[1];
            WorldService.characterHandlerTime = endns;
            if (endns > WorldService.characterHandlerTimeMax){
                WorldService.characterHandlerTimeMax = endns;
                console.warn(`Character World Handler took ${endns / 1000000}ms.`);
            }
        });
    }

    mapHandler() {
        let now = Date.now();

        Core.maps.map((map: Map) => {
            groundItems(now, map);
        });
    }
}

