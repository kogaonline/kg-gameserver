import Core from "../core";
import Sequelize, { Model } from 'sequelize';
import accounts from "../database/sequelize/models/accounts";
import cq_user from "../database/sequelize/models/cq_user";
import cq_deluser from "../database/sequelize/models/cq_deluser";
import cq_item from "../database/sequelize/models/cq_item";
import cq_itemtype from "../database/sequelize/models/cq_itemtype";
import cq_magic from "../database/sequelize/models/cq_magic";
import cq_magictype from "../database/sequelize/models/cq_magictype";
import cq_monstertype from "../database/sequelize/models/cq_monstertype";
import cq_map from "../database/sequelize/models/cq_map";
import cq_action from "../database/sequelize/models/cq_action";
import cq_npc from "../database/sequelize/models/cq_npc";
import cq_levexp from "../database/sequelize/models/cq_levexp";
import cq_weapon_skill from "../database/sequelize/models/cq_weapon_skill";
import RMaps from "../database/map";
import RItemType from "../database/itemtype";
import RMagicType from "../database/magicType";
import RItem from "../database/item";
import RMonsterType from "../database/monstertype";
import RAction from "../database/action";
import RNPC from "../database/npc";
import RLevExp from "../database/levexp";

const config = require('../database/sequelize/config/config');

export default class DBService {
    static MapsTable = RMaps;
    static ItemsTable = RItem;
    static ItemTypesTable = RItemType;
    static MagicsTypeTable = RMagicType;

    static Models: {
        accounts: any,
        cq_user: any,
        cq_deluser: any,
        cq_magic: any,
        cq_magictype: any,
        cq_item: any,
        cq_itemtype: any,
        cq_map: any,
        cq_monstertype: any,
        cq_action: any,
        cq_npc: any,
        cq_levexp: any,
        cq_weapon_skill: any,
    } = null;

    static async init() {
        let connection = new Sequelize(config);
    
        let models: any = {
            accounts,
            cq_user,
            cq_deluser,
            cq_item,
            cq_itemtype,
            cq_magic,
            cq_magictype,
            cq_map,
            cq_monstertype,
            cq_action,
            cq_npc,
            cq_levexp,
            cq_weapon_skill,
        };

        this.Models = models;

        ((Object as any).values(models) as any[])
            .map((model: any) => model.init(connection))
            .map((model: any) => model.associate && model.associate(connection.models));


        await this.loadUIDs();
        await RMaps.load();
        await RItemType.load();
        await RMagicType.load();
        await RMonsterType.load();
        await RAction.load();
        await RNPC.load();
        await RLevExp.load();
    }

    static getResults<T = any>(sql: any){
        let results: T[] = [];

        if (Array.isArray(sql)){
            if (sql){
                sql.map((row: any) => {
                    results.push(Object.assign({}, row.dataValues));
                });
            }
        } else {
            if (sql)
                results.push(Object.assign({}, sql.dataValues));
        }

        return results as T[];
    }

    static getFirstResult<T = any>(sql: any){
        let results = this.getResults<T>(sql);
        if (Array.isArray(results))
            return results[0];
        return Object.assign({}, results);
    }

    static async findByIdDecorator<T>(model: any, id: number, include: any[] = []): Promise<T>{
        try {
            if (!Array.isArray(include))
                include = [include];
            let obj = await model.findOne({ include, where: { id } });
            return DBService.getFirstResult<T>(obj);
        } catch (err) {
            throw new Error(`Failed to get by ID at ${ model ? model.getTableName() : 'unknown_model'} -> ${err.message}`);
        }
    }

    static async findByUidDecorator<T>(model: any, uid: number, include: any[] = []): Promise<T>{
        try {
            if (!Array.isArray(include))
                include = [include];
            let obj = await model.findOne({ include, where: { uid } });
            return DBService.getFirstResult<T>(obj);
        } catch (err) {
            throw new Error(`Failed to get by UID at ${ model ? model.getTableName() : 'unknown_model'} -> ${err.message}`);
        }
    }

    static async loadUIDs() {
        let uq = await this.Models.cq_user.findOne({ order: [['uid', 'DESC']] });
        let iq = await this.Models.cq_item.findOne({ order: [['id', 'DESC']] });
        let mq = await this.Models.cq_magic.findOne({ order: [['id', 'DESC']] });

        Core.charaterUIDCounter = (!uq || uq.uid < 1000000) ? 1000000 : uq.uid;
        Core.itemUIDCounter = (!iq || iq.id < 1) ? 0 : iq.id;
        Core.magicUIDCounter = (!mq || mq.id < 1) ? 0 : mq.id;

        console.log(`Item UID Counter: ${Core.itemUIDCounter++}`)
        console.log(`Magic UID Counter: ${Core.magicUIDCounter++}`)
        console.log(`Character UID Counter: ${Core.charaterUIDCounter++}`);
    }
}