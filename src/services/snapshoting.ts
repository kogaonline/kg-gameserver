import Core from "../core";
import { writeFileSync, readFileSync, existsSync } from "fs";
import * as Settings from './../settings';

function writeTmpFile(content:any, filename: string){
    writeFileSync(`dist/tmp/${filename}.tmp`, content);
}

function readTmpFile(filename: string){
    let path = `dist/tmp/${filename}.tmp`;
    let data = existsSync(path) ? readFileSync(path) : '{}';
    let content = data.toString();

    // console.log(`Reading data from ${filename}.tmp: `);
    // console.log(content);

    return content.length >= 2 ? content : '{}';
}

export function loadSnapshot(){
    if (Settings.DEV_MODE){
        console.log('Loading snapshot...');

        loadCore();
        loadPlayers();
    }
}

export function createSnapshot(){
    //console.log('Creating snapshot...');

    snapshotCore();
    snapshotPlayers();

    if (process.platform != "win32")
        process.exit(0);
}

function snapshotCore(){
    let core: {
        [ley: string]: any;
    } = {};

    core.itemUID = Core.itemUIDCounter;
    let data = JSON.stringify(JSON.decycle(core));
    writeTmpFile(data, `core`);
}

function snapshotPlayers(){
    let players: {
        [id: number] : any;
    } = { };

    Core.playerPool.map((p) => {
        let player = Object.assign({}, p);
        if (player.id)
            players[player.id] = player;
    });

    let data = JSON.stringify(JSON.decycle(players));
    writeTmpFile(data, `players`);
    assureData(players);
}

function assureData(pdata: any){
    let data = readTmpFile(`players`);
    let _players = JSON.parse(data);
    for (var pid in _players){
        //console.log(`Verifying id ${pid}.`);
        if (!pdata[pid]){
            console.log(`Data is corrupted.`);
        }
    }
}

function loadCore() {
    let data = readTmpFile(`core`);
    let _core = JSON.retrocycle(JSON.parse(data));
}

function loadPlayers(){
    let data = readTmpFile(`players`);
    let _players = JSON.retrocycle(JSON.parse(data));
    for (var pid in _players){
        let player = _players[pid];
        Core.statedPlayerPool.add(player.id, player);
    }
}