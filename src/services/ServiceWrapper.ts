const child_process = require('child_process');

export default class ServiceWrapper {
    workerProcess: any = null;
        
    exec(script: string, args: any[], onData?: (json: any) => void, onErr?: (json: any) => void){
        this.workerProcess = child_process.spawn('node', [script, ...args]);  

        this.workerProcess.stdout.on('data', function (data: any) { 
            let obj = Buffer.from(data.toString(), 'base64').toString('utf8');
            let json = JSON.parse(obj);

            if (onData)
                onData(json);
        });  

        this.workerProcess.stderr.on('data', function (data: string) {
            if (onErr)
                onErr(data);
        });  

        this.workerProcess.on('close', function (code: number) {  
             console.log('child process exited with code ' + code);  
        }); 
    }
}