import Player from "../../../game/player"
import MsgTalk from "../gamepackets/MsgTalk"
import { EMsgTalkType } from "../../../enums/EMsgTalkType"
import { EUpdateType } from "../../../enums/EUpdateType"
import { EStatusEffect } from "../../../enums/EStatusEffect"
import MsgName from "../gamepackets/MsgName"
import { EStringType } from "../../../enums/EStringType"
import MsgItemInfo from "../gamepackets/MsgItemInfo"
import { EItemMode } from "../../../enums/EItemAction"
import { EItemPosition } from "../../../enums/EItemPosition"
import MsgUserAttrib from "../gamepackets/MsgUserAttrib"
import IUpdate from "../../../interfaces/IUpdate"
import Core from "../../../core"
import MsgInteract from "../gamepackets/MsgInteract"
import { getRandom } from "../../../logical/kernel"
import { EInteractionActivationType } from "../../../enums/EInteractionActivationType"
import MsgWeaponSkill from "../gamepackets/MsgWeaponSkill"
import MsgMagicInfo from "../gamepackets/MsgMagicInfo"
import MagicTypeTable from "../../../database/magicType";
import Character from "../../../game/character"
import IndexedElement from "../../../components/indexedElement"
import IMapElement from "../../../interfaces/IMapElement"
import MsgAction from "../gamepackets/MsgAction"
import { EMsgActionType } from "../../../enums/EMsgActionType"
import MsgWalk from "../gamepackets/MsgWalk"
import { EPostCmdType } from "../../../enums/EPostCmdType"
import { EActionTaskType } from "../../../enums/EActionTaskType"
import { EAction } from "../../../enums/EAction"
import ServerAction from "../../../base/serverActions"
import RAction from "../../../database/action"
import RNPC from "../../../database/npc"
import App from "../../../"
import RMonsterType from "../../../database/monstertype"
import { EInteractionType } from "../../../enums/EInteractionType"
const bn = require(`bignum`);

export async function handleMsgTalk(msg: MsgTalk, player: Player) {

    // TODO: Logs and [PM] Logs

    if (!await performedCommand(msg, player)) {
        switch (msg.type) {
            case EMsgTalkType.Talk: {
                player.character.sendToScreen(msg, false)
                break
            }
        }
    }
}

async function performedCommand(msg: MsgTalk, player: Player) {
    try {
        if (msg.message.startsWith(`/`)) {
            let command = msg.message.split(` `)[0].substring(1).toLowerCase();
            let args = msg.message.split(` `).slice(1);
            let nargs = getNumbers(args);

            switch (command) {
                case "reloadmonsters": {
                    await RMonsterType.load();
                    App.npcserver.restart();
                    break;
                }

                case "reloadnpc": {
                    if (assureArgs(1, args, player)) {
                        let reload = async (nargs: any, player: Player) => {
                            if (nargs[0] === 0){
                                await RNPC.load();
                                player.sendMsg(`Reloaded ${Core.npcs.size} npcs.`, EMsgTalkType.TopLeft);
                            } else {
                                await RNPC.loadNPC(nargs[0]);
                                player.sendMsg(`Reloaded npc ${nargs[0]}.`, EMsgTalkType.TopLeft);
                            }

                            Core.playerPool.map(p => {
                                p.screen.clearAll();
                                p.screen.reload();
                            });
                        }

                        reload(nargs, player);
                    }
                    break;
                }

                case "finishtask": {
                    player.character.finishActionTask();
                    break;
                }
                case "canceltask": {
                    player.character.cancelActionTask('A test with spaces');
                    break;
                }
                case "task": {
                    if (assureArgs(3, args, player)){
                        player.character.startActionTask(nargs[0], nargs[1], 5, 0, 0, args[2]);
                    }
                    break;
                }

                case "test": {
                    player.sendMsg("Commands are working!");
                    break;
                }

                case "xp": {
                    player.character.timeOf.xpCircleList = Date.now() - 5000;
                    player.character.xpcircle = 100;
                    break;
                }

                case "quit": {
                    player.character.sendPostCmd(EPostCmdType.CloseClient);
                    break;
                }

                case "pm":
                case "gui": {
                    player.character.sendPostCmd(EPostCmdType.PMWindow);
                    break;
                }

                case "openwindow": {
                    if (assureArgs(2, args, player)) {
                        let open = new MsgAction(true);
                        open.uid = player.character.uid;
                        open.id = EMsgActionType.OpenDialog;
                        open.param = nargs[0];
                        open.param2 = nargs[1];

                        player.send(open);
                    }
                    break;
                }

                case "open": {
                    if (assureArgs(4, args, player)) {
                        for (let i = nargs[0]; i < nargs[0] + nargs[1]; i++) {
                            let open = new MsgAction(true);
                            open.uid = player.character.uid;
                            open.x = player.character.x;
                            open.y = player.character.y;
                            open.map = player.character.mapId;
                            open.id = i;
                            open.param = nargs[2];
                            open.param2 = nargs[3];

                            player.send(open);
                        }
                    }
                    break;
                }

                case "postcmd": {
                    if (assureArgs(2, args, player)) {
                        for (let i = nargs[0]; i < nargs[0] + nargs[1]; i++) {
                            let open = new MsgAction(true);
                            open.uid = player.character.uid;
                            open.id = EMsgActionType.PostCommand;
                            open.map = player.character.mapId;
                            open.x = player.character.x;
                            open.y = player.character.y;
                            open.param = i;
                            player.send(open);
                        }
                    }
                    break;
                }

                case "monstereffect": {
                    if (assureArgs(2, args, player)) {
                        let action = new MsgAction(true);
                        action.uid = nargs[0];
                        action.id = nargs[1];
                        action.map = player.character.mapId;
                        // action.param = nargs[2];
                        // action.param2 = nargs[3];
                        player.send(action);
                    }
                    break;
                }

                case "move": {
                    if (assureArgs(2, args, player)) {
                        let element = player.screen.elements[nargs[0]] as Character;

                        if (element){
                            let data = new MsgWalk();
                            data.uid = element.uid;
                            data.map = player.character.mapId;
                            data.type = 1;
                            data.direction = nargs[1];

                            element.move(data);
                            player.character.sendToScreen(data, true);
                        }
                    }
                    break;
                }

                case "remove": {
                    if (assureArgs(1, args, player)) {
                        let data = new MsgAction(true);
                        data.uid = nargs[0];
                        data.id = EMsgActionType.RemoveEntity;
                        player.send(data);
                        //player.screen.remove(this);
                    }
                    break;
                }

                case "reloadscreen":
                case "clearscreen": {
                    player.screen.clearAll();
                    player.screen.reload();
                    break;
                }

                case "spawn": {
                    if (assureArgs(1, args, player)) {
                        let uid = nargs[0];
                        let p = Core.playerPool.get(uid);
                        let m = player.character.map.monsters.get(uid);

                        if (p)
                            player.character.sendSpawn(p.character);
                        if (m)
                            player.character.sendSpawn(m);

                    }
                    break;
                }

                case "mob":
                case "monster": {
                    if (assureArgs(2, args, player)) {
                        let body = nargs[0];

                        for (let i = 0; i < nargs[1]; i++) {
                            let monster = new Character(null, true, false);
                            monster.name = `Body ${body}`;
                            monster.body = body;
                            monster.hp = monster.maxhp = 1;
                            monster.level = 1;
                            monster.mapId = player.character.mapId;
                            monster.x = player.character.x + i;
                            monster.y = player.character.y;
                            monster.uid = Core.monsterUIDCounter++;

                            player.character.map.monsters.add(monster.uid, monster);
                            player.character.sendToScreen(monster.getSpawnPacket());
                            body++;
                        }
                    }
                    break;
                }

                case "monsterdie": {
                    let m = player.screen.entities.toArray()[0];
                    m.hp = 0;

                    let attack = new MsgInteract();
                    attack.type = EInteractionType.Kill;
                    attack.sender = player.character.uid;
                    attack.target = m.uid;
                    attack.x = m.x;
                    attack.y = m.y;
                    attack.ko = 1;

                    player.character.sendToScreen(attack, true);

                    m.addStatus(EStatusEffect.Dead);
                    m.addStatus(EStatusEffect.FadeAway);
                    break;
                }

                case "pk":
                case "pkp":
                case "pkpoints": {
                    if (assureArgs(1, args, player))
                        player.character.pkPoints = nargs[0];
                    break;
                }

                case "revive": {
                    if (args.length > 0) {
                        switch (args[1]) {
                            case "all": {
                                Core.playerPool.map(p => {
                                    p.character.revive();
                                });
                                break;
                            }
                        }
                        break;
                    } else {
                        player.character.revive();
                        break;
                    }
                }

                case "enemy": {
                    let nplayer = new Player(null, true);
                    let character = new Character(nplayer, false, false);
                    nplayer.character = character;
                    let uid = Core.charaterUIDCounter++;
                    nplayer.id = uid;

                    character.prepare({
                        "uid": uid,
                        "accountId": uid,
                        "name": `E${uid}`,
                        "hairStyle": 617,
                        "class": 15,
                        "money": 100,
                        "cps": 100,
                        "cpsBound": 1000,
                        "treasurePoints": 0,
                        "body": 1003,
                        "face": 1,
                        "level": 150,
                        "strength": 500,
                        "agility": 500,
                        "vitality": 500,
                        "spirit": 500,
                        "attrPoints": 0,
                        "hp": nargs.length > 0 ? nargs[0] : 1000,
                        "mana": 25,
                        "mapId": player.character.mapId,
                        "x": player.character.x,
                        "y": player.character.y,
                        "pkPoints": 0,
                        "experience": 0,
                        "quizPoints": 0,
                        "prevMapId": 0,
                        "reborn": 0,
                        "firstClass": 0,
                        "secondClass": 0,
                        "firstRebornLevel": 0,
                        "secondRebornLevel": 0,
                        "spouse": "",
                        "whPassword": "",
                        "whMoney": 0,
                        "enlightenPoints": 0,
                        "heavenBlessing": 0,
                        "luckTime": 0,
                        "enlightments": 0,
                        "enlightmentTime": 0,
                        "guildId": 0,
                        "guildRank": 0,
                        "guildMoneyDonation": 0,
                        "guildCpsDonation": 0,
                        "vipLevel": 0,
                        "virtuePoints": 0,
                        "prevX": 0,
                        "prevY": 0,
                        "clanId": 0,
                        "clanDonation": 0,
                        "clanRank": 0,
                        "subclass": 0,
                        "subclassLevel": 0,
                        "studyPoints": 0,
                        "lastLogin": 0,
                        "title": 0,
                        "firstBuy": 0,
                        "country": 0,
                        "flower": 0,
                        "namechange": "",
                        "namechangeCount": 0,
                        "racePoints": 0,
                        "multipleExp": 0,
                        "multipleExpTimes": 0,
                        "onlineTrainingExp": 0,
                        "blessedHuntingExp": 0,
                        "royalPoints": 0,
                        "business": 0,
                        statusFlag: 0,
                    });

                    Core.playerPool.add(nplayer.id, nplayer);
                    player.character.map.addPlayer(nplayer);
                    player.character.sendToScreen(nplayer.character.getSpawnPacket());

                    break;
                }

                case "clearinventory": {
                    player.inventory.clear();
                    break;
                }

                case "mapeffect": {
                    if (assureArgs(1, args, player)) {
                        let buffer = Buffer.alloc(28 + 8);
                        buffer.writeUInt16LE(28, 0);
                        buffer.writeUInt16LE(1101, 2);
                        buffer.writeUInt32LE(Date.now() / 1000, 4);
                        buffer.writeUInt32LE(player.character.uid, 8);
                        buffer.writeUInt32LE(nargs[0], 12);
                        buffer.writeUInt16LE(player.character.x, 16);
                        buffer.writeUInt16LE(player.character.y, 18);
                        buffer.writeUInt16LE(10, 22);


                        player.send(buffer);
                    }
                }

                case "removeitem": {
                    let removed = false;
                    player.inventory.items.map((item) => {
                        if (item && !removed) {
                            player.inventory.remove(item);
                            removed = true;
                            return;
                        }
                    });
                    break;
                }

                case "awarditem": {
                    if (assureArgs(1, args, player)) {
                        let item = new MsgItemInfo(true);
                        item.id = nargs[0];
                        item.mode = EItemMode.Default;
                        item.position = EItemPosition.Inventory;

                        if (args.length > 1)
                            item.plus = nargs[1];

                        if (args.length > 2)
                            item.bless = nargs[2];

                        if (args.length > 3)
                            item.enchant = nargs[3];

                        if (args.length > 4)
                            item.socketOne = nargs[4];

                        if (args.length > 5)
                            item.socketTwo = nargs[5];

                        if (args.length > 6)
                            item.data = nargs[6];

                        if (args.length > 7)
                            item.position = nargs[7];

                        item.durability = item.maxDurability = 65535;
                        player.inventory.add(item);
                    }
                    break;
                }

                case "effect": {
                    if (assureArgs(1, args, player)) {
                        let str = new MsgName(true, EStringType.RoleEffect);
                        str.uid = player.character.uid;
                        str.addString(args[0]);
                        str.send(player);
                    }
                    break;
                }

                case "map": {
                    let path = Core.dmaps.get(player.character.map.base);
                    player.sendMsg(`MapID: ${player.character.map.id} | Base: ${player.character.map.base} | Path: ${path}`);
                }

                case "tele":
                case "teleport":
                case "chgmap": {
                    if (assureArgs(3, args, player)) {
                        player.character.teleport(nargs[0], nargs[1], nargs[2]);
                    }
                    break;
                }

                case "gl": {
                    if (player.character.hasStatus(EStatusEffect.TopGuildLeader))
                        player.character.removeStatus(EStatusEffect.TopGuildLeader);
                    else
                        player.character.addStatus(EStatusEffect.TopGuildLeader);
                    break;
                }

                case "teststatus": {
                    if (assureArgs(1, args, player)) {
                        let status = bn(1).shiftLeft(nargs[0]);
                        let _hasStatus = player.character.hasStatus(status);
                        player.sendMsg(`Status ${nargs[0]} ${_hasStatus ? '' : 'in'}active.`);
                    }
                    break;
                }

                case "addstatus": {
                    if (assureArgs(1, args, player)) {
                        let status = bn(1).shiftLeft(nargs[0]);
                        player.character.addStatus(status);
                    }
                    break;
                }

                case "delstatus":
                case "removestatus": {
                    if (assureArgs(1, args, player)) {
                        let status = bn(1).shiftLeft(nargs[0]);
                        player.character.removeStatus(status);
                    }
                    break;
                }

                case "addstatus2": {
                    if (assureArgs(1, args, player)) {
                        let status = bn(1).shiftLeft(nargs[0]);
                        player.character.addStatus2(status);
                    }
                    break;
                }

                case "delstatus2":
                case "removestatus2": {
                    if (assureArgs(1, args, player)) {
                        let status = bn(1).shiftLeft(nargs[0]);
                        player.character.removeStatus2(status);
                    }
                    break;
                }

                case "addstatus3": {
                    if (assureArgs(1, args, player)) {
                        let status = bn(1).shiftLeft(nargs[0]);
                        player.character.addStatus3(status);
                    }
                    break;
                }

                case "delstatus3":
                case "removestatus3": {
                    if (assureArgs(1, args, player)) {
                        let status = bn(1).shiftLeft(nargs[0]);
                        player.character.removeStatus3(status);
                    }
                    break;
                }

                case "mana":
                case "mp": {
                    player.character.mana = player.character.maxmana;
                    break;
                }

                case "life":
                case "hp": {
                    player.character.hp = player.character.maxhp;
                    break;
                }

                case "maxhp": {
                    player.character.calculateMaxHp();
                    player.sendMsg(`You hp is ${player.character.hp}/${player.character.maxhp}.`);
                    break;
                }

                case "maxmana": {
                    player.character.calculateMaxHp();
                    player.sendMsg(`You mana is ${player.character.mana}/${player.character.maxmana}.`);
                    break;
                }

                case "lev":
                case "level": {
                    if (assureArgs(1, args, player))
                        player.character.level = nargs[0];
                    break;
                }

                case "vitality":
                case "vit": {
                    if (assureArgs(1, args, player))
                        player.character.vitality = nargs[0];
                    break;
                }

                case "agility":
                case "agi": {
                    if (assureArgs(1, args, player))
                        player.character.agility = nargs[0];
                    break;
                }

                case "spirit":
                case "spi": {
                    if (assureArgs(1, args, player))
                        player.character.spirit = nargs[0];
                    break;
                }

                case "strength":
                case "str": {
                    if (assureArgs(1, args, player))
                        player.character.strength = nargs[0];
                    break;
                }

                case "superman": {
                    player.character.strength += 500;
                    player.character.agility += 500;
                    player.character.spirit += 500;
                    player.character.vitality += 500;

                    player.character.hp = player.character.maxhp;
                    player.character.mana = player.character.maxmana;
                    break;
                }

                case "prof":
                case "profession":
                case "pro":
                case "class": {
                    if (assureArgs(1, args, player))
                        player.character.class = nargs[0];
                    break;
                }

                case "money":
                case "awardmoney": {
                    if (assureArgs(1, args, player))
                        player.character.money = nargs[0];
                    break;
                }

                case "emoney":
                case "awardemoney": {
                    if (assureArgs(1, args, player))
                        player.character.cps = nargs[0];
                    break;
                }

                case "bmoney":
                case "awardbmoney": {
                    if (assureArgs(1, args, player))
                        player.character.cpsBound = nargs[0];
                    break;
                }

                case "update": {
                    let update = new MsgUserAttrib();
                    update.uid = player.character.uid;

                    if (assureArgs(4, args, player)) {
                        let u: IUpdate = {
                            type: nargs[0],
                            value1: nargs[1],
                            value2: nargs[2],
                            value3: nargs[3]
                        };

                        update.addUpdate(u);
                    } else if (assureArgs(2, args, player)) {
                        let u: IUpdate = {
                            type: nargs[0],
                            value1: 0,
                            value2: 0,
                            value3: 0
                        };

                        update.addUpdate(u);
                    }

                    update.send(player);
                    break;
                }

                case "chkitems": {
                    let data: string[] = [];
                    player.inventory.items.map((item) => {
                        data.push(`${item.id}(${item.uid})`);
                    });

                    player.sendMsg(data.join(`::`));
                    break;
                }

                case "spawn": {
                    let data = player.character.getSpawnPacket();

                    data[225] = 0;

                    player.character.sendToScreen(data);
                    break;
                }

                case "save": {
                    player.save();
                    break;
                }

                case "face": {
                    if (assureArgs(1, args, player)) {
                        player.character.face = nargs[0];
                        player.character.sendToScreen(player.character.getSpawnPacket());
                    }
                    break;
                }

                case "reloadaction": {
                    if (assureArgs(1, args, player)) {
                        let reload = async (nargs: any, player: Player) => {
                            if (nargs[0] === 0){
                                await RAction.load();
                                player.sendMsg(`Reloaded ${Core.actions.size} actions.`, EMsgTalkType.TopLeft);
                            } else {
                                await RAction.loadAction(nargs[0]);
                                player.sendMsg(`Reloaded action ${nargs[0]}.`, EMsgTalkType.TopLeft);
                            }
                        }

                        reload(nargs, player);
                    }
                    break;
                }

                case "action": {
                    if (assureArgs(1, args, player)) {
                        ServerAction.executeAction(nargs[0], player.character);
                    }
                    break;
                }

                case "removespawn": {
                    player.character.removeSpawn();
                    break;
                }

                case "online": {
                    let msg: string[] = [];
                    Core.playerPool.map((_player) => {
                        msg.push(`${_player.character.name}(${_player.character.uid})`);
                    });

                    player.sendMsg(`Me: ${player.character.uid}`);
                    player.sendMsg(msg.join(`,`));
                    break;
                }

                case "attack": {
                    if (assureArgs(1, args, player)) {
                        setInterval(() => {
                            let attack = new MsgInteract();
                            attack.sender = player.character.uid;
                            attack.value = getRandom(500, 999);
                            attack.type = 2;
                            attack.addStatus(EInteractionActivationType.StudyPoints);
                            //attack.effectValue = 999;
                            attack.responseValue = 0;

                            Core.playerPool.map((_player) => {
                                if (_player.character.uid === nargs[0]) {
                                    attack.target = _player.character.uid;
                                    attack.x = _player.character.x;
                                    attack.y = _player.character.y;
                                }
                            });

                            player.character.sendToScreen(attack);

                        }, 1000);
                    }
                    break;

                }

                case "attackrange": {
                    if (assureArgs(1, args, player)) {
                        player.character.attackRange = nargs[0];
                    }
                    break;
                }

                case "sp": {
                    if (assureArgs(1, args, player)) {
                        player.character.stamina = nargs[0];
                    }
                    break;
                }

                case "weaponskill":
                case "awardwskill": {
                    if (assureArgs(4, args, player)) {
                        let wskill = new MsgWeaponSkill();
                        wskill.type = nargs[0];
                        wskill.level = nargs[1];
                        wskill.experience = nargs[2];
                        wskill.reqExperience = nargs[3];

                        player.character.addWeaponSkill(wskill);
                    }
                    break;
                }

                case "magic":
                case "awardmagic": {
                    if (assureArgs(4, args, player)) {
                        let magic = new MsgMagicInfo();
                        magic.type = nargs[0];
                        magic.level = nargs[1];
                        magic.experience = nargs[2];
                        magic.upgradeLevel = nargs[3];

                        player.character.addMagic(magic);
                    }
                    break;
                }

                case "reloadmagics": {
                    await MagicTypeTable.load();
                    player.sendMsg(`Magic types loaded.`, EMsgTalkType.TopLeft);
                    break;
                }

                case "invincible": {
                    player.character.invincible = !player.character.invincible;
                    if (player.character.invincible)
                        player.sendMsg(`You are invincible.`);
                    else
                        player.sendMsg(`You are not invincible.`);
                    break;
                }
            }

            return true
        }
    } catch (err) {
        console.error(err)
    }
    return false
}

function assureArgs(argCount: number, args: string[], player: Player) {
    if (args.length < argCount) {
        player.sendMsg(`Invalid args count for this command. It need ${argCount} args!`)
        return false
    }

    return true
}

function getNumbers(args: string[]) {
    let nargs: number[] = []

    if (args.length === 0)
        return nargs

    for (let i = 0; i < args.length; i++) {
        try {
            nargs[i] = Number.parseInt(args[i], 10)
        } catch (err) {
            nargs[i] = 0
        }
    }

    return nargs
}