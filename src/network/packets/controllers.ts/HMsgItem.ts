import Player from "../../../game/player";
import MsgItem from "../gamepackets/MsgItem";
import { EItemActionType } from "../../../enums/EItemAction";
import Equipment from "../../../game/equipment";
import { EItemPosition } from "../../../enums/EItemPosition";
import { cnRes } from "../../../res/cnRes";
import { EMapElementType } from "../../../enums/EMapElementType";
import MsgMapItem from "../gamepackets/MsgMapItem";
import { EMsgMapItemAction } from "../../../enums/EMsgMapItemAction";
import Core from "../../../core";
import { EActionTaskType } from "../../../enums/EActionTaskType";
import { EAction } from "../../../enums/EAction";

export async function handleMsgItem(msg: MsgItem, player: Player) {
    switch (msg.action) {
        case EItemActionType.EquipItem: {
            equipItem(msg, player);
            break;
        }

        case EItemActionType.UnequipItem: {
            unequipItem(msg, player);
            break;
        }

        case EItemActionType.Ping: {
            player.send(msg);
            break;
        }

        case EItemActionType.DropItem: {
            dropItem(msg, player);
            break;
        }

        default: {
            player.sendMsg(`Received MsgItem with Action: ${msg.action}.`);
            break;
        }
    }
}

function equipItem(msgItem: MsgItem, player: Player) {
    let itemID = msgItem.uid;
    let itemPos = msgItem.param1;

    let item = player.inventory.getUID(itemID);
    if (item) {
        let pos = Equipment.getPosition(item.id);

        if (itemPos === EItemPosition.LeftWeapon && pos === EItemPosition.RightWeapon) {
            if (player.equipment.occupied(pos)) {
                if (player.equipment.occupied(itemPos))
                    player.equipment.remove(itemPos);
                player.equipment.add(itemPos, item);
            }
            else
                player.equipment.add(pos, item);
        }

        if (pos === itemPos) {
            if (player.equipment.occupied(pos))
                player.equipment.remove(pos);

            if (itemPos === EItemPosition.LeftWeapon) {
                if (player.equipment.occupied(EItemPosition.RightWeapon))
                    player.equipment.add(pos, item);
                else
                    player.sendMsg(cnRes.STR_EQUIPMENT_RIGHT_HAND_FIRST);
            } else
                player.equipment.add(pos, item);
        }
    } else {
        player.sendMsg(`Unable to find item(${itemID}).`);
    }
}

function unequipItem(msgItem: MsgItem, player: Player) {
    let itemID = msgItem.uid;

    let item = player.equipment.getUID(itemID);
    if (item) {
        player.equipment.remove(item.position);
    } else {
        player.sendMsg(`Unable to find item(${itemID}).`);
        player.equipment.remove(msgItem.param1);
    }
}

function dropItem(msgItem: MsgItem, player: Player){
    let itemID = msgItem.uid;

    let item = player.inventory.getUID(itemID);

    if (item){
        player.inventory.removeUID(itemID);
        let map = player.character.map;
        if (map){
            let validPlace = map.ground.validLocation(EMapElementType.ITEM, player.character.x, player.character.y);
            if (validPlace)
                map.addGroundItem(item, player.character.x, player.character.y);
            else {
                let nextValidPlace = map.ground.getNextValid(EMapElementType.ITEM, player.character.x, player.character.y);
                if (nextValidPlace)
                    map.addGroundItem(item, nextValidPlace.x, nextValidPlace.y);
            }
        }
    }
}

export async function handleMsgMapItem(msg: MsgMapItem, player: Player) {
    switch (msg.action) {
        case EMsgMapItemAction.Pick: {
            pickupItem(msg, player);
            break;
        }
    }
}

export function pickupItem(msgMapItem: MsgMapItem, player: Player){
    let itemID = msgMapItem.uid;
    let map = player.character.map;
    if (map){
        if (map.groundItems.contains(itemID)){
            let item = map.groundItems.get(itemID);
            let info = Core.itemInfo.get(item.type);
            player.sendMsg(`Trying to get item ${item.uid} at ${item.x}, ${item.y}`);
            if (info && info.rateLv === 5){
                player.character.startActionTask(EActionTaskType.Realize, EAction.Kneel, 5, 0, 0, 'Opening...', () => {
                    if (map.groundItems.contains(itemID)){
                        map.removeGroundItem(item);

                        // TODO: execute item action
                    } else {
                        player.character.cancelActionTask('Too late! It has gone.')
                    }
                });
            } else {
                if (item.valueItem){
                    if (item.moneyValue){
                        map.removeGroundItem(item);
                        player.character.money += item.moneyValue;
                        player.sendMsg(cnRes.STR_MONEY_PICKUP.format(item.moneyValue));
                    }
                } else {
                    map.removeGroundItem(item);
                    item.info.justCreated = false;
                    player.inventory.add(item.info);
                    let info = Core.itemInfo.get(item.info.id);
                    player.sendMsg(cnRes.STR_ITEM_PICKUP.format(info.name));
                }
            }
        }
    }
}