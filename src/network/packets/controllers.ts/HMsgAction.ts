import Player from "../../../game/player";
import MsgAction from "../gamepackets/MsgAction";
import { EMsgActionType } from "../../../enums/EMsgActionType";
import MsgMapInfo from "../gamepackets/MsgMapInfo";
import CharacterTable from "../../../database/character";
import { cnRes } from "../../../res/cnRes";
import { EAction } from "../../../enums/EAction";
import { EMapElementType } from "../../../enums/EMapElementType";

export async function handleMsgAction(data: MsgAction, player: Player) {
    switch (data.id) {
        case EMsgActionType.SendLocation: {
            setLocation(data, player);
            break;
        }

        case EMsgActionType.CompleteLogin:
        case EMsgActionType.SendItems: {
            completeLogin(data, player);
            break;
        }

        case EMsgActionType.ChangePKMode: {
            changePkMode(data, player);
            break;
        }

        case EMsgActionType.Jump: {
            jump(data, player);
            break;
        }

        case EMsgActionType.ChangeDirection: {
            changeDirection(data, player);
            break;
        }

        case EMsgActionType.DeleteCharacter: {
            deleteCharacter(data, player);
            break;
        }

        case EMsgActionType.Revive: {
            revive(data, player);
            break;
        }

        case EMsgActionType.ChangeAction: {
            changeAction(data, player);
            break;
        }

        case EMsgActionType.QueryEntity: {
            entityQuery(data, player);
            break;
        }

        case EMsgActionType.QueryStatInfo: {
            player.send(data)
            break;
        }

        default: {
            player.sendMsg(`Requested MsgAction with Subtype ${data.id}.`);
            console.error(`Requested MsgAction with Subtype ${data.id}.`);
            break;
        }
    }
}

function setLocation(data: MsgAction, player: Player) {
    // player.character.setLocation(717, 547);

    /*data.map = player.character.map.id;
    data.x = player.character.x;
    data.y = player.character.y;
    player.send(data);*/

    let end = process.hrtime(player.loginhr);
    let endns = end[1];
    console.warn(`Login took ${endns / 1000000}ms.`);

    player.character.setLocation(data.x, data.y);

    //player.character.sendSpawn(player);

    let mapInfo = new MsgMapInfo();
    mapInfo.id = player.character.map.id;
    mapInfo.baseId = player.character.map.base;

    player.send(mapInfo);

}

function changePkMode(data: MsgAction, player: Player) {
    player.character.pkMode = data.param;
    player.send(data);
}

export async function completeLogin(data: MsgAction, player: Player) {
    player.sendMsg(`Completing login...`);
    player.sendDate();

    await player.inventory.loadItems();
    player.character.fullyLoaded = true;

    player.character.hp = player.character.hp;
    player.character.mana = player.character.mana;
    player.character.pkPoints = player.character.pkPoints;

    player.screen.clearAll();
    player.screen.reload();
}

function jump(packet: MsgAction, player: Player) {
    player.character.action = EAction.Jump;

    let _x = player.character.x;
    let _y = player.character.y;

    let nX = packet.aParam5;
    let nY = packet.aParam6;

    const { facing } = packet;
    player.character.facing = facing;

    if (player.character.map.ground &&
        player.character.map.ground.validLocation(EMapElementType.PLAYER, nX, nY)) {
        player.character.setLocation(nX, nY);
        player.character.map.ground.setInvalidLocation(EMapElementType.PLAYER, player.character.x, player.character.y);
        player.character.map.ground.setValidLocation(EMapElementType.PLAYER, _x, _y);
        player.character.sendToScreen(packet);
        player.screen.clean();// player.screen.clearAll();
        player.screen.reload(null);
    } else {
        player.character.teleport(player.character.mapId, _x, _y);
    }

    player.character.cancelActionTask();
    player.character.lastAttack = null;
    player.character.action = EAction.None;
}

function changeDirection(packet: MsgAction, player: Player) {
    player.character.facing = packet.facing;
    player.character.sendToScreen(packet);
}

function deleteCharacter(packet: MsgAction, player: Player) {
    let pin = packet.param;
    if (CharacterTable.deleteCharacter(pin.toString(), player)) {
        player.sendMsg(cnRes.STR_ROLE_DELETE_SUCCESS);
        player.disconnect();
    } else {
        player.sendMsg(cnRes.STR_ROLE_DELETE_FAILURE);
    }

    console.log(JSON.stringify(packet));
}

function revive(packet: MsgAction, player: Player) {
    let date = Date.now();
    let timeLeft = player.character.timeOf.death + 20000 - date;

    if (timeLeft <= 0) {
        player.character.findFirstLocation();
        player.character.revive();

        player.screen.clearAll();
        player.screen.reload(packet);
    } else {
        player.sendMsg(`Wait ${timeLeft}ms to revive.`)
    }
}

function changeAction(packet: MsgAction, player: Player) {
    player.character.action = EAction.Sit;
    player.character.sendToScreen(player.character.getSpawnPacket());
    player.send(packet);
}

function entityQuery(data: MsgAction, player: Player) {
    player.sendMsg(`Querying Entity...`);

    player.screen.clearAll();
    player.screen.reload();
    player.send(data);
}