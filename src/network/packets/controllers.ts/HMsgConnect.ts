import MsgConnect from "../gamepackets/MsgConnect";
import Player from "../../../game/player";
import MsgTalk from "../gamepackets/MsgTalk";
import { EMsgTalkType } from "../../../enums/EMsgTalkType";
import AccountTable from "../../../database/account";
import CharacterTable from "../../../database/character";
import { EAccountStatus } from "../../../enums/EAccountStatus";
import Character from "../../../game/character";
import MsgUserInfo from "../gamepackets/MsgUserInfo";
import Core from "../../../core";
import { cnRes } from "../../../res/cnRes";
import { IAccountRow } from "../../../database/sequelize/models/accounts";
import { NPCServerController } from "../npcserver/npcscontroller";
import MsgAction from "../gamepackets/MsgAction";
import { EMsgActionType } from "../../../enums/EMsgActionType";

export async function handleNewConnection(conn: MsgConnect, player: Player) {
    try {
        let msg = cnRes.NEW_ROLE;
        let account: IAccountRow = await AccountTable.getById(conn.id);

        player.account = account;

        if (!account.playerId || account.playerId === 0)
            return player.send(new MsgTalk(msg, '', cnRes.ALLUSERS, EMsgTalkType.Dialog));

        if (account.status === EAccountStatus.Banned) {
            msg = cnRes.STR_REGISTER_ACCOUNT_BANNED;
        }

        player.character = new Character(player, false, false);

        let load = await CharacterTable.getById(account.playerId);
        if (!load)
            return player.send(new MsgTalk(msg, '', cnRes.ALLUSERS, EMsgTalkType.Dialog));

        await player.character.prepare(load);
        await CharacterTable.loadExtras(player);

        msg = cnRes.ANSWER_OK;

        if (Core.playerPool.contains(player.character.uid))
            await Core.playerPool.get(player.character.uid).disconnect();
        Core.playerPool.add(player.character.uid, player);

        player.loginhr = process.hrtime();
        player.send(new MsgTalk(msg, cnRes.SYSTEM, cnRes.ALLUSERS, EMsgTalkType.Dialog));
        player.send(new MsgUserInfo(player));
        player.character.finalizeLoad();

        let initializeData = new MsgAction(true);
        initializeData.map = player.character.mapId;
        initializeData.x = player.character.x;
        initializeData.y = player.character.y;
        initializeData.id = EMsgActionType.SendLocation;
        initializeData.uid = player.character.uid;
        player.send(initializeData.toArray());

        console.log(`${player.character.name}(${player.id}) has logged in.`);
        player.playerAction = 10;

        NPCServerController.sendPlayerInfo();
    } catch (err) {
        console.log(err);
    }
}