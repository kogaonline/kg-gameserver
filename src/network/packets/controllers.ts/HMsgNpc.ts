import MsgNpc from "../gamepackets/MsgNpc";
import Player from "../../../game/player";
import ServerAction from "../../../base/serverActions";
import { EMsgNpcActionType } from "../../../enums/ENPCType";
import Core from "../../../core";
import { EMsgTalkType } from "../../../enums/EMsgTalkType";

export function handleMsgNpc(npc: MsgNpc, player: Player){
    if (player.character.isSpecial)
        player.sendMsg(`NPC ID: ${npc.uid}`, EMsgTalkType.TopLeft);

    switch (npc.action){
        case EMsgNpcActionType.Activate: {
            let _npc = Core.npcs.get(npc.uid);
            if (_npc){
                ServerAction.executeAction(_npc.task0, player.character);
            }
        }
    }
}