import MsgTaskDialog from "../gamepackets/MsgTaskDialog";
import Player from "../../../game/player";
import { EMsgTaskDialogType } from "../../../enums/EMsgTaskDialogType";
import ServerAction from "../../../base/serverActions";

export function handleTaskDialog(dialog: MsgTaskDialog, player: Player){
    switch (dialog.type){
        case EMsgTaskDialogType.Confirm:
        case EMsgTaskDialogType.Answer: {
            if (dialog.input.length > 0){
                player.iters['text'] = dialog.input;
            }

            let action_id = player.taskDialogOptions[dialog.option];
            player.taskDialogOptions = [0];
            ServerAction.executeAction(action_id, player.character);
            break;
        }
        case EMsgTaskDialogType.Edit: {
            console.log(dialog.option);
            break;
        }
    }
}