import Player from "../../game/player";
import { cnRes } from "../../res/cnRes";

import CharacterTable from "../../database/character";

import { EMsgTalkType } from "../../enums/EMsgTalkType";

import { handleNewConnection } from "./controllers.ts/HMsgConnect";
import { handleMsgAction, completeLogin } from "./controllers.ts/HMsgAction";
import { handleMsgTalk } from "./controllers.ts/HMsgTalk";
import { handleMsgItem, handleMsgMapItem } from "./controllers.ts/HMsgItem";
import { handleTaskDialog } from "./controllers.ts/HMsgTaskDialog";
import { handleMsgNpc } from "./controllers.ts/HMsgNpc";
import handleInteraction from "../../game/interaction/handle";

import MsgConnect from "./gamepackets/MsgConnect";
import MsgAction from "./gamepackets/MsgAction";
import MsgTalk from "./gamepackets/MsgTalk";
import MsgWalk from "./gamepackets/MsgWalk";
import MsgItem from "./gamepackets/MsgItem";
import MsgNpc from "./gamepackets/MsgNpc";
import MsgRegister from "./gamepackets/MsgRegister";
import MsgInteract from "./gamepackets/MsgInteract";
import MsgMapItem from "./gamepackets/MsgMapItem";
import MsgTaskDialog from "./gamepackets/MsgTaskDialog";

const enum packetType {
    MsgRegister = 1001,
    MsgTalk = 1004,
    MsgItem = 1009,
    MsgInteract = 1022,
    MsgConnect = 1052,
    MsgMapItem = 1101,
    MsgTaskStatus = 1134,
    MsgTaskDetailInfo = 1135,
    MsgNpc = 2031,
    MsgTaskDialog = 2032,
    MsgWalk = 10005,
    MsgAction = 10010,
};

export class GamePacketController {
    public static handle(buffer: Buffer, player: Player) {
        let id = buffer.readUInt16LE(2);
        let length = buffer.readUInt16LE(0);

        let msg = `Packet ${id} with ${length} bytes`;
        if (id > 11000 || length > 2048) {
            console.log(msg);
            if (player.playerAction < 10)
                player.disconnect();

            return;
        }

        switch (id) {
            case packetType.MsgRegister: {
                let register = new MsgRegister();
                register.construct(buffer);
                CharacterTable.create(register, player).then((response) => {
                    player.send(new MsgTalk(response, cnRes.SYSTEM, cnRes.ALLUSERS, EMsgTalkType.Popup));
                }).catch((err) => {
                    console.error(err);
                });
                break;
            }
            case packetType.MsgTalk: {
                let talk = new MsgTalk('');
                talk.construct(buffer);
                handleMsgTalk(talk, player);
                break;
            }
            case packetType.MsgItem: {
                let msg = new MsgItem(false);
                msg.construct(buffer);
                handleMsgItem(msg, player);
                break;
            }
            case packetType.MsgMapItem: {
                let msg = new MsgMapItem();
                msg.construct(buffer);
                handleMsgMapItem(msg, player);
                break;
            }
            case packetType.MsgInteract: {
                let interaction = new MsgInteract();
                interaction.construct(buffer);
                handleInteraction(interaction, player.character);
                break;
            }
            case packetType.MsgConnect: {
                let conn: MsgConnect = new MsgConnect();
                conn.construct(buffer);
                handleNewConnection(conn, player);
                break;
            }
            case packetType.MsgTaskStatus: {
                completeLogin(null, player);
                break;
            }
            case packetType.MsgWalk: {
                let data: MsgWalk = new MsgWalk();
                data.construct(buffer);
                player.character.move(data);
                break;
            }
            case packetType.MsgAction: {
                let data: MsgAction = new MsgAction(false);
                data.construct(buffer);
                handleMsgAction(data, player);
                break;
            }

            case packetType.MsgTaskDialog: {
                let dialog = new MsgTaskDialog(0, '');
                dialog.construct(buffer);
                handleTaskDialog(dialog, player);
                break;
            }

            case packetType.MsgNpc: {
                let npc = new MsgNpc();
                npc.construct(buffer);
                handleMsgNpc(npc, player);
                break;
            }

            default: {
                if (length > 0){
                    console.log(msg);
                    player.sendMsg(msg);
                }
                break;
            }
        }
    }
}