export default class BufferWriter {
    static WriteUInt16(arg:number, offset:number, buffer:Buffer)
    {
        if (!buffer || offset > buffer.length-1) return
        for (var i = 0; i < 2; i++) {       
            buffer[offset+i] = arg >> 0 + i * 8
        }
    }
    static WriteUInt32(arg:number, offset:number, buffer:Buffer)
    {
        if (!buffer || offset > buffer.length-1) return
        if (buffer.length >= offset + 4)
        {
            buffer[offset] = arg;
            buffer[offset + 1] = (arg >> 8);
            buffer[offset + 2] = (arg >> 16);
            buffer[offset + 3] = (arg >> 24);
        }
    }
    static WriteInt32(arg:number, offset:number, buffer:Buffer)
    {
        for (var i = 0; i < 4; i++) {       
            buffer[offset+i] = arg >> 0 + i * 8
        }
    }

    static WriteStringList(list:Array<string>, offset: number, buffer: Buffer){
        if (!list || !buffer || offset > buffer.length -1)
            return;

        buffer[offset] = list.length;
        offset++;

        for (let x = 0; x < list.length; x++){
            let str = list[x];
            buffer[offset] = str.length & 0xFF;
            buffer.write(str, offset+1);
            offset += 1 + str.length;
        }
    }

}