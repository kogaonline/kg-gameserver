import IPacket from "../../../interfaces/IPacket";
import Player from "../../../game/player";
import { EMsgActionType } from "../../../enums/EMsgActionType";

export default class MsgData implements IPacket {
    private buffer: Buffer;

    constructor(){
        this.buffer = Buffer.alloc(40 + 8);
        this.buffer.writeUInt16LE(40, 0);
        this.buffer.writeUInt16LE(1033, 2);
    }

    construct(buffer: Buffer){
        this.buffer = buffer;
    }

    get action(): EMsgActionType {
        return this.buffer.readUInt32LE(8);
    }

    set action(val: EMsgActionType) {
        this.buffer.writeUInt32LE(val, 8);
    }

    get year(): number {
        return this.buffer.readUInt32LE(12);
    }

    set year(val: number) {
        this.buffer.writeUInt32LE(val, 12);
    }

    get month(): number {
        return this.buffer.readUInt32LE(16);
    }

    set month(val: number) {
        this.buffer.writeUInt32LE(val, 16);
    }

    get dayOfYear(): number {
        return this.buffer.readUInt32LE(20);
    }

    set dayOfYear(val: number) {
        this.buffer.writeUInt32LE(val, 20);
    }

    get day(): number {
        return this.buffer.readUInt32LE(24);
    }

    set day(val: number) {
        this.buffer.writeUInt32LE(val, 24);
    }

    get hour(): number {
        return this.buffer.readUInt32LE(28);
    }

    set hour(val: number) {
        this.buffer.writeUInt32LE(val, 28);
    }

    get minute(): number {
        return this.buffer.readUInt32LE(32);
    }

    set minute(val: number) {
        this.buffer.writeUInt32LE(val, 32);
    }

    get second(): number {
        return this.buffer.readUInt32LE(36);
    }

    set second(val: number) {
        this.buffer.writeUInt32LE(val, 36);
    }

    toArray(): Buffer {
        return this.buffer;
    }

    send(player: Player){
        player.send(this.buffer);
    }
}