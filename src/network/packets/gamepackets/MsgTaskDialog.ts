import IPacket from "../../../interfaces/IPacket";
import Player from "../../../game/player";
import { EMsgTaskDialogType } from "../../../enums/EMsgTaskDialogType";

enum offsets {
    timestamp = 4,
    uid = 8,
    avatar = 12,
    option = 14,
    type = 15,
    strings = 16,
    stringLen = 17
}

export default class MsgTaskDialog implements IPacket {
    private buffer: Buffer;

    constructor(type: EMsgTaskDialogType, text: string, option: number = 0, maxLenOrAvatar: number = 0) {
        this.buffer = Buffer.alloc(21 + text.length + 8);
        this.buffer.writeUInt16LE(21 + text.length, 0);
        this.buffer.writeUInt16LE(2032, 2);
        this.type = type;

        if (option)
            this.option = option;
        if (maxLenOrAvatar)
            this.avatar = maxLenOrAvatar;

        this.buffer[offsets.strings] = type === EMsgTaskDialogType.Create ? 1 : 1;
        this.buffer[offsets.stringLen] = text.length;
        this.buffer.write(text, offsets.stringLen + 1);
    }

    get timestamp(): number {
        return this.buffer.readUInt32LE(offsets.timestamp);
    }
    set timestamp(val: number) {
        this.buffer.writeUInt32LE(val, offsets.timestamp);
    }

    get uid(): number {
        return this.buffer.readUInt32LE(offsets.uid);
    }
    set uid(val: number) {
        this.buffer.writeUInt32LE(val, offsets.uid);
    }

    get avatar(): number {
        return this.buffer.readUInt16LE(offsets.avatar);
    }
    set avatar(val: number) {
        this.buffer.writeUInt16LE(val, offsets.avatar);
    }

    get option(): number {
        return this.buffer[offsets.option];
    }
    set option(val: number) {
        this.buffer[offsets.option] = val;
    }

    get type(): EMsgTaskDialogType {
        return this.buffer[offsets.type];
    }
    set type(val: EMsgTaskDialogType) {
        this.buffer[offsets.type] = val;
    }

    get input(): string {
        let inputSize = this.buffer[offsets.stringLen];
        let _input = this.buffer.toString('ascii', offsets.stringLen + 1, offsets.stringLen + 1 + inputSize);
        return _input;
    }

    construct(buffer: Buffer){
        this.buffer = buffer;
    }

    toArray(): Buffer {
        return this.buffer;
    }

    send(player: Player){
        player.send(this.buffer);
    }
}