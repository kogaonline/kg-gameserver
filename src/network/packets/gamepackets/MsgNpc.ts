import IPacket from "../../../interfaces/IPacket";
import Player from "../../../game/player";
import {EMsgNpcActionType} from "../../../enums/ENPCType"

enum offsets {
    timestamp = 4,
    uid = 8,
    data = 12,
    action = 16,
    sort = 18,
}

export default class MsgNpc implements IPacket {
    private buffer: Buffer;

    constructor() {
        this.buffer = Buffer.alloc(20 + 8);
        this.buffer.writeUInt16LE(20, 0);
        this.buffer.writeUInt16LE(2031, 2);
    }

    get timestamp(): number {
        return this.buffer.readUInt32LE(offsets.timestamp);
    }
    set timestamp(val: number) {
        this.buffer.writeUInt32LE(val, offsets.timestamp);
    }

    get uid(): number {
        return this.buffer.readUInt32LE(offsets.uid);
    }
    set uid(val: number) {
        this.buffer.writeUInt32LE(val, offsets.uid);
    }

    get data(): number {
        return this.buffer.readUInt32LE(offsets.data);
    }
    set data(val: number) {
        this.buffer.writeUInt32LE(val, offsets.data);
    }

    get action(): EMsgNpcActionType {
        return this.buffer.readUInt16LE(offsets.action);
    }
    set action(val: EMsgNpcActionType) {
        this.buffer.writeUInt16LE(val, offsets.action);
    }

    get sort(): number {
        return this.buffer.readUInt16LE(offsets.sort);
    }
    set sort(val: number) {
        this.buffer.writeUInt16LE(val, offsets.sort);
    }
    
    construct(buffer: Buffer){
        this.buffer = buffer;
    }

    toArray(): Buffer {
        return this.buffer;
    }

    send(player: Player){
        player.send(this.buffer);
    }
}