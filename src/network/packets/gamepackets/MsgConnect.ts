import IPacket from "../../../interfaces/IPacket";
import Player from "../../../game/player";
import { EMsgConnectType } from "../../../enums/EMsgConnectType";

export default class MsgConnect implements IPacket {
    private buffer: Buffer;

    construct(buffer: Buffer){
        this.buffer = buffer;
    }

    get id(): number {
        return this.buffer.readUInt32LE(4);
    }

    set id(val: number) {
        this.buffer.writeUInt32LE(val, 4);
    }

    get type(): EMsgConnectType{
        return this.buffer[8];
    }

    set type(val: EMsgConnectType){
        this.buffer[8] = val;
    }

    toArray(): Buffer {
        return this.buffer;
    }

    send(player: Player){
        player.send(this.buffer);
    }
}