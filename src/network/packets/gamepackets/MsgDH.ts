import * as Settings from '../../../settings';
import { randomBytes, Hash, getDiffieHellman } from 'crypto';
// import { printBuffer } from '../../../logical/kernel';
import GameCryptography from '../../cryptography/gameCryptography';
import DiffieHellman from '../../cryptography/diffieHellman';

export default class DHKey {
    _keyExchange: DiffieHellman;
    _serverIv: Buffer;
    _clientIv: Buffer;

    handleResponse(pubKey: string) {
        let bi = require('node-biginteger');
        let pkey = bi.fromString(pubKey, 16);
        let a = bi.fromString('112504424144827073538649700363328748050738259306688644008324964685869091213141', 10);
        let p = bi.fromString(Settings.DH_KEY_P, 16);
        let s = pkey.modPow(a, p);

        return s.toBuffer();
    }

    getPacket(): Buffer {
        let packet: Buffer;

        this._serverIv = Buffer.alloc(8);
        this._clientIv = Buffer.alloc(8);
        let P: string = Settings.DH_KEY_P;
        let G: string = Settings.DH_KEY_G;

        this._keyExchange = new DiffieHellman(P, G);
        packet = this.generatePacket(P, G, this._keyExchange.GenerateRequest());

        return packet;
    }

    generatePacket(P: string, G: string, publicKey: string): Buffer {
        const _P = Settings.DH_KEY_P.length;
        const _G = Settings.DH_KEY_G.length;
        const _S = publicKey.length;

        let PL: number = 11;
        let JL: number = 12;

        let pad = randomBytes(PL);
        let junk = randomBytes(JL);

        let length = 47 + _P + _G + _S + 12 + 8 + 8;
        let buffer = Buffer.alloc(length);

        buffer.writeInt32LE(length - PL, 11);
        buffer.writeUInt32LE(JL, 15);
        junk.copy(buffer, 19);

        buffer.writeUInt32LE(8, 31);
        this._serverIv.copy(buffer, 35);
        buffer.writeUInt32LE(8, 43);
        this._clientIv.copy(buffer, 47);

        buffer.writeUInt32LE(_P, 55);
        buffer.write(Settings.DH_KEY_P, 59);
        buffer.writeUInt32LE(_G, 59 + _P);
        buffer.write(Settings.DH_KEY_G, 63 + _P);

        buffer.writeUInt32LE(_S, 63 + _P + _G);
        buffer.write(publicKey, 67 + _P + _G);

        buffer.write(Settings.DH_KEY_S, 67 + _P + _G + _S);
        return buffer;
    }

    handleClientKey(pubKey: string, crypto: GameCryptography) {
        try {
            this._keyExchange.handleResponse(pubKey);

            let data = Buffer.from(this._keyExchange.toBytes());
            // console.log('this is old data');
            // printBuffer(data);
            let len = data.length;
            for (let i = 0; i < data.length; i++) {
                if (data[i] === 0) {
                    len = i;
                    break;
                }
            }

            let ndata = Buffer.alloc(len);
            data.copy(ndata, 0, 0, len);
            data = ndata;
            // console.log('this is new data');
            // printBuffer(data);

            var hash = require('crypto');
            let md5 = hash.createHash('md5');
            let frun = Buffer.alloc(32);

            let n = Buffer.from(md5.update(data).digest(), 'hex');
            // printBuffer(n);

            n.copy(frun, 0);
            n.copy(frun, 16);

            // printBuffer(frun);
            n = Buffer.from(frun.toString('hex'));
            md5 = hash.createHash('md5');
            n = md5.update(n).digest().copy(frun, 16);
            let key = Buffer.from(frun.toString('hex'));

            //key = Buffer.from([50, 51, 51, 101, 51, 51, 56, 102, 50, 51, 101, 55, 57, 100, 51, 54, 52, 100, 55, 100, 100, 55, 97, 97, 97, 48, 49, 54, 51, 52, 97, 57, 49, 102, 53, 99, 53, 98, 98, 55, 53, 97, 102, 51, 50, 97, 101, 48, 57, 102, 99, 48, 56, 101, 98, 50, 99, 56, 52, 54, 102, 56, 99, 50]);
            // console.log(`Server Key and IVs:`);
            // printBuffer(key);
            // printBuffer(this._clientIv);
            // printBuffer(this._serverIv);

            crypto.setKey(key);
            crypto.setIVs(this._clientIv, this._serverIv);

            return crypto;
        }
        catch (err) {
            console.log(err);
            return null;
        }
    }
}