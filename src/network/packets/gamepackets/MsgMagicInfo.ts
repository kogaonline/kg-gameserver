import IPacket from "../../../interfaces/IPacket"
import Player from "../../../game/player"
import Core from "../../../core"

export default class MsgMagicInfo implements IPacket {
    private buffer: Buffer
    uid: number = 0

    constructor() {
        this.uid = Core.magicUIDCounter++
        this.buffer = Buffer.alloc(40)
        this.buffer.writeUInt16LE(32, 0)
        this.buffer.writeUInt16LE(1103, 2)
    }

    construct(buffer: Buffer) {
        this.buffer = buffer
    }

    toArray(): Buffer {
        return this.buffer
    }

    send(player: Player) {
        player.send(this.buffer)
    }

    get type(): number {
        return this.buffer.readUInt16LE(12)
    }
    set type(val: number) {
        this.buffer.writeUInt16LE(val, 12)
    }

    get level(): number {
        return this.buffer[16]
    }
    set level(val: number) {
        this.buffer[16] = val
    }

    get experience(): number {
        return this.buffer.readUInt32LE(8)
    }
    set experience(val: number) {
        this.buffer.writeUInt32LE(val, 8)
    }

    get upgradeLevel(): number {
        return this.buffer[24]
    }
    set upgradeLevel(val: number) {
        this.buffer[24] = val
        this.buffer[28] = val
    }
}