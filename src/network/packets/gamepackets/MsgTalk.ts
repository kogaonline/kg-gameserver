import IPacket from "../../../interfaces/IPacket";
import Player from "../../../game/player";
import { EMsgTalkType } from "../../../enums/EMsgTalkType";
import BufferWriter from "../BufferWriter";

export default class MsgTalk implements IPacket {
    private buffer: Buffer;

    message: string;
    from: string;
    to: string;
    type: EMsgTalkType;
    color: number = 0xFFFF0000;
    mesh: number = 0;
    unknown: string;

    constructor(msg: string, from: string = "SYSTEM", to: string = "SYSTEM", type: EMsgTalkType = EMsgTalkType.Talk){
        this.message = msg;
        this.to = to;
        this.from = from;
        this.type = type;
    }

    construct(buffer: Buffer){
        this.type = buffer.readUInt32LE(12);
        this.mesh = buffer.readUInt32LE(24);
        this.from = buffer.slice(30, 30 + buffer[29]).toString();

        let _to = 31 + this.from.length;
        this.to = buffer.slice(_to, _to + buffer[_to-1]).toString();

        let _c = 32 + this.from.length + this.to.length;
        this.unknown = buffer.slice(_c, _c + buffer[_c-1]).toString();

        let _msg = 33 + this.from.length + this.to.length + this.unknown.length;
        this.message = buffer.slice(_msg, _msg + buffer[_msg-1]).toString();
    }

    toArray(): Buffer {
        this.buffer = Buffer.alloc(45 + this.from.length + this.to.length + this.message.length);
        this.buffer.writeUInt16LE(this.buffer.length - 8, 0);
        this.buffer.writeUInt16LE(1004, 2);
        // this.buffer.writeUInt32LE(this.color, 8);
        this.buffer.writeUInt32LE(this.type, 12);
        this.buffer.writeUInt32LE(this.mesh, 24);
        BufferWriter.WriteStringList([
            this.from,
            this.to, 
            "",
            this.message
        ], 28, this.buffer);

        return this.buffer;
    }

    send(player: Player){
        player.send(this.toArray());
    }
}