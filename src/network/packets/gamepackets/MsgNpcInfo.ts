import IPacket from "../../../interfaces/IPacket";
import Player from "../../../game/player";
import {ENPCType} from "../../../enums/ENPCType"

enum offsets {
    timestamp = 4,
    uid = 8,
    id = 12,
    x = 16,
    y = 18,
    lookface = 20,
    type = 22,
    role = 24,
    names = 26
}

export default class MsgNpcInfo implements IPacket {
    private buffer: Buffer;

    constructor() {
        this.buffer = Buffer.alloc(28 + 8);
        this.buffer.writeUInt16LE(28, 0);
        this.buffer.writeUInt16LE(2030, 2);
    }

    get timestamp(): number {
        return this.buffer.readUInt32LE(offsets.timestamp);
    }
    set timestamp(val: number) {
        this.buffer.writeUInt32LE(val, offsets.timestamp);
    }

    get uid(): number {
        return this.buffer.readUInt32LE(offsets.uid);
    }
    set uid(val: number) {
        this.buffer.writeUInt32LE(val, offsets.uid);
    }

    get id(): number {
        return this.buffer.readUInt32LE(offsets.id);
    }
    set id(val: number) {
        this.buffer.writeUInt32LE(val, offsets.id);
    }

    get x(): number {
        return this.buffer.readUInt16LE(offsets.x);
    }
    set x(val: number) {
        this.buffer.writeUInt16LE(val, offsets.x);
    }

    get y(): number {
        return this.buffer.readUInt16LE(offsets.y);
    }
    set y(val: number) {
        this.buffer.writeUInt16LE(val, offsets.y);
    }

    get lookface(): number {
        return this.buffer.readUInt16LE(offsets.lookface);
    }
    set lookface(val: number) {
        this.buffer.writeUInt16LE(val, offsets.lookface);
    }

    get type(): ENPCType {
        return this.buffer.readUInt16LE(offsets.type);
    }
    set type(val: ENPCType) {
        this.buffer.writeUInt16LE(val, offsets.type);
    }

    get role(): number {
        return this.buffer.readUInt16LE(offsets.role);
    }
    set role(val: number) {
        this.buffer.writeUInt16LE(val, offsets.role);
    }

    get name(): string {
        let len = this.buffer[offsets.names + 1];
        let val = this.buffer.toString('default', offsets.names + 2, len);
        return val;
    }
    set name(val: string) {
        this.buffer[offsets.names] = 1;
        this.buffer[offsets.names + 1] = val.length;

        let _buff = Buffer.alloc(36 + val.length);
        this.buffer.copy(_buff, 0, 0);
        _buff.write(val, offsets.names + 2);
        this.buffer = _buff;
        this.buffer.writeUInt16LE(28 + val.length, 0);
    }

    construct(buffer: Buffer){
        this.buffer = buffer;
    }

    toArray(): Buffer {
        return this.buffer;
    }

    send(player: Player){
        player.send(this.buffer);
    }
}