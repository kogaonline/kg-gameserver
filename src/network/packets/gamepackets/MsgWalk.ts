import IPacket from "../../../interfaces/IPacket";
import Player from "../../../game/player";
import { EDirection } from "../../../enums/EDirection";

const offsets = {
    direction: 4,
    uid: 8, 
    type: 12, 
    time: 16,
    map: 20
};

export default class MsgWalk implements IPacket {
    private buffer: Buffer;

    constructor(){
        this.buffer = Buffer.alloc(32);
        this.buffer.writeUInt16LE(24, 0);
        this.buffer.writeUInt16LE(10005, 2);
    }

    construct(buffer: Buffer){
        this.buffer = buffer;
    }

    toArray(): Buffer {
        return this.buffer;
    }

    send(player: Player){
        player.send(this.buffer);
    }

    get direction(): EDirection {
        return this.buffer.readUInt32LE(offsets.direction);
    }
    set direction(val: EDirection){
        this.buffer.writeUInt32LE(val, offsets.direction);
    }

    get uid(): number {
        return this.buffer.readUInt32LE(offsets.uid);
    }
    set uid(val: number){
        this.buffer.writeUInt32LE(val, offsets.uid);
    }

    get type(): number {
        return this.buffer.readUInt32LE(offsets.type);
    }
    set type(val: number){
        this.buffer.writeUInt32LE(val, offsets.type);
    }

    get map(): number {
        return this.buffer.readUInt32LE(offsets.map);
    }
    set map(val: number){
        this.buffer.writeUInt32LE(val, offsets.map);
    }

}