import IPacket from "../../../interfaces/IPacket";
import Player from "../../../game/player";

export default class MsgRegister implements IPacket {
    private buffer: Buffer;

    name: string;
    body: number;
    profession: number;
    id: number;

    construct(buffer: Buffer){
        this.buffer = buffer;
        this.name = buffer.toString("ascii", 24, 40).replace(/\0/g, ``);
        this.body = buffer.readUInt16LE(72);
        this.profession = buffer.readUInt16LE(74);
        this.id = buffer.readUInt32LE(76);
    }

    toArray(): Buffer {
        return this.buffer;
    }

    send(player: Player){
        player.send(this.buffer);
    }
}