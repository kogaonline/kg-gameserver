import IPacket from "../../../interfaces/IPacket";
import Player from "../../../game/player";
import { EStringType } from "../../../enums/EStringType";
import IndexedElement from "../../../components/indexedElement";

export default class MsgName implements IPacket {
    private buffer: Buffer;
    private strings: IndexedElement<string>;
    private strLen: number = 0;

    constructor(isNew: boolean, type: EStringType){
        if (isNew){
            this.buffer = Buffer.alloc(23);
            this.type = type;
            this.strings = new IndexedElement<string>();
        }
    }

    construct(buffer: Buffer){
        this.buffer = buffer;
    }

    toArray(): Buffer {
        this.prepare();
        return this.buffer;
    }

    prepare(){
        let len = 15 + this.strLen;
        let x = this.positionX;
        let y = this.positionY;
        let type = this.type;
        let uid = this.uid;


        this.buffer = Buffer.alloc(len + 8);
        this.buffer.writeUInt16LE(len, 0);
        this.buffer.writeUInt16LE(1015, 2);
        this.type = type;
        this.positionX = x;
        this.positionY = y;
        this.uid = uid;
        this.writeStrings();
    }

    send(player: Player){
        this.prepare();
        player.send(this.buffer);
    }

    addString(val: string){
        this.strings.add(this.strings.size, val);
        this.strLen += val.length;
    }

    writeStrings(){
        let strs: string[] = [];
        this.strings.map((str) => {
            strs.push(str);
        });

        this.buffer.writeStringArray(strs, 13);
    }

    get type(): EStringType {
        return this.buffer[12];
    }

    set type(val: EStringType){
        this.buffer[12] = val;
    }

    get uid() : number {
        return this.buffer.readUInt32LE(8);
    }
    set uid(val: number) {
        this.buffer.writeUInt32LE(val, 8);
    }

    get positionX() : number {
        return this.buffer.readUInt16BE(8);
    }

    set positionX(val: number){
        this.buffer.writeUInt16LE(val, 8);
    }

    get positionY() : number {
        return this.buffer.readUInt16BE(10);
    }

    set positionY(val: number){
        this.buffer.writeUInt16LE(val, 10);
    }
}