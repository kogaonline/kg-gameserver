import IPacket from "../../../interfaces/IPacket";
import Player from "../../../game/player";
import { EItemMode, EItemColor } from "../../../enums/EItemAction";
import Core from "../../../core";
import { EItemPosition } from "../../../enums/EItemPosition";
import { IItem } from "../../../database/item";

enum offsets {
    uid = 4,
    id = 8,
    dura = 12,
    maxdura = 14,
    mode = 16,
    position = 18,
    socketProgress = 20,
    socketOne = 24,
    socketTwo = 25,
    unknown1 = 26,
    data = 28,
    unknown2 = 30,
    unknown3 = 32,
    plus = 33,
    bless = 34,
    bound = 35,
    enchant = 36,
    nextRed = 34,
    nextBlue = 36,
    nextGreen = 40,
    suspicious = 44,
    lock = 46,
    color = 48,
    plusProgress = 52,
    inscribed = 56,
    stackSize = 68,

}

export default class MsgItemInfo implements IPacket {
    private buffer: Buffer;

    justCreated: boolean;

    constructor(isNew: boolean) {
        this.justCreated = isNew;
        if (isNew) {
            this.buffer = Buffer.alloc(84 + 8);
            this.buffer.writeUInt16LE(84, 0);
            this.buffer.writeUInt16LE(1008, 2);
            this.mode = EItemMode.Default;
            this.uid = Core.itemUIDCounter++;
        }
    }

    construct(buffer: Buffer) {
        this.buffer = buffer;
    }

    toArray(): Buffer {
        return this.buffer;
    }

    send(player: Player) {
        player.send(this.buffer);
    }

    load(item: IItem) {
        this.uid = item.id;
        this.id = item.type;
        this.mode = EItemMode.Default;
        this.bless = item.bless;
        this.plus = item.magic3;
        this.data = item.magic1;
        this.bound = item.bound;
        this.enchant = item.enchant;
        this.durability = item.durability;
        this.maxDurability = item.max_durability;
        this.lock = item.anti_monster;
        this.color = item.color;
        this.stackSize = item.stack;
        this.socketOne = item.gem1;
        this.socketTwo = item.gem2;
        this.socketProgress = item.progress_gem;
        this.plusProgress = item.progress_lev;
        this.position = item.position;

        this.justCreated = false;
    }

    get uid(): number {
        return this.buffer.readUInt32LE(offsets.uid);
    }
    set uid(val: number) {
        this.buffer.writeUInt32LE(val, offsets.uid);
    }

    get id(): number {
        return this.buffer.readUInt32LE(offsets.id);
    }
    set id(val: number) {
        this.buffer.writeUInt32LE(val, offsets.id);
    }

    get mode(): EItemMode {
        return this.buffer.readUInt16LE(16);
    }
    set mode(val: EItemMode) {
        this.buffer.writeUInt16LE(val, 16);
    }

    get durability(): number {
        return this.buffer.readUInt16LE(offsets.dura);
    }
    set durability(val: number) {
        this.buffer.writeUInt16LE(val, offsets.dura);
    }

    get maxDurability(): number {
        return this.buffer.readUInt16LE(offsets.maxdura);
    }
    set maxDurability(val: number) {
        this.buffer.writeUInt16LE(val, offsets.maxdura);
    }

    get position(): EItemPosition {
        return this.buffer.readUInt16LE(offsets.position);
    }
    set position(val: EItemPosition) {
        this.buffer.writeUInt16LE(val, offsets.position);
    }

    get socketProgress(): number {
        return this.buffer.readUInt32LE(offsets.socketProgress);
    }
    set socketProgress(val: number) {
        this.buffer.writeUInt32LE(val, offsets.socketProgress);
    }

    get socketOne(): number {
        return this.buffer[offsets.socketOne];
    }
    set socketOne(val: number) {
        this.buffer[offsets.socketOne] = val;
    }

    get socketTwo(): number {
        return this.buffer[offsets.socketTwo];
    }
    set socketTwo(val: number) {
        this.buffer[offsets.socketTwo] = val;
    }

    get data(): number {
        return this.buffer.readUInt32LE(offsets.data);
    }
    set data(val: number) {
        this.buffer.writeUInt32LE(val, offsets.data);
    }

    get plus(): number {
        return this.buffer[offsets.plus]
    }
    set plus(val: number) {
        this.buffer[offsets.plus] = val;
    }

    get bless(): number {
        return this.buffer[offsets.bless]
    }
    set bless(val: number) {
        this.buffer[offsets.bless] = val;
    }

    get bound(): number {
        return this.buffer[offsets.bound]
    }
    set bound(val: number) {
        this.buffer[offsets.bound] = val;
    }

    get enchant(): number {
        return this.buffer[offsets.enchant]
    }
    set enchant(val: number) {
        this.buffer[offsets.enchant] = val;
    }

    get suspicious(): number {
        return this.buffer[offsets.suspicious]
    }
    set suspicious(val: number) {
        this.buffer[offsets.suspicious] = val;
    }

    get lock(): number {
        return this.buffer[offsets.lock]
    }
    set lock(val: number) {
        this.buffer[offsets.lock] = val;
    }

    get color(): EItemColor {
        return this.buffer.readUInt32LE(offsets.color);
    }
    set color(val: EItemColor) {
        this.buffer.writeUInt32LE(val, offsets.color);
    }

    get plusProgress(): EItemColor {
        return this.buffer.readUInt32LE(offsets.plusProgress);
    }
    set plusProgress(val: EItemColor) {
        this.buffer.writeUInt32LE(val, offsets.plusProgress);
    }

    get inscribed(): number {
        return this.buffer.readUInt16LE(offsets.inscribed);
    }
    set inscribed(val: number) {
        this.buffer.writeUInt16LE(val, offsets.inscribed);
    }

    get stackSize(): number {
        return this.buffer.readUInt16LE(offsets.stackSize);
    }
    set stackSize(val: number) {
        this.buffer.writeUInt16LE(val, offsets.stackSize);
    }

    maxStackSize: number = 0;

    isTwoHanded(): boolean {
        return this.id.toString().startsWith('5');
    }
}