import IPacket from "../../../interfaces/IPacket";
import Player from "../../../game/player";
import { EMsgMapItemAction } from "../../../enums/EMsgMapItemAction";

enum offsets {
    timestamp = 4,
    itemUid = 8,
    itemType = 12,
    x = 16,
    y = 18,
    color = 20,
    action = 22
}

export default class MsgMapItem implements IPacket {
    private buffer: Buffer;

    constructor() {
        this.buffer = Buffer.alloc(32 + 8);
        this.buffer.writeUInt16LE(32, 0);
        this.buffer.writeUInt16LE(1101, 2);
    }

    construct(buffer: Buffer){
        this.buffer = buffer;
    }

    get timestamp(): number {
        return this.buffer.readUInt32LE(offsets.timestamp);
    }
    set timestamp(val: number) {
        this.buffer.writeUInt32LE(val, offsets.timestamp);
    }

    get uid(): number {
        return this.buffer.readUInt32LE(offsets.itemUid);
    }
    set uid(val: number) {
        this.buffer.writeUInt32LE(val, offsets.itemUid);
    }

    get type(): number {
        return this.buffer.readUInt32LE(offsets.itemType);
    }
    set type(val: number) {
        this.buffer.writeUInt32LE(val, offsets.itemType);
    }

    get x(): number {
        return this.buffer.readUInt32LE(offsets.x);
    }
    set x(val: number) {
        this.buffer.writeUInt32LE(val, offsets.x);
    }

    get y(): number {
        return this.buffer.readUInt32LE(offsets.y);
    }
    set y(val: number) {
        this.buffer.writeUInt32LE(val, offsets.y);
    }

    get color(): number {
        return this.buffer.readUInt32LE(offsets.color);
    }
    set color(val: number) {
        this.buffer.writeUInt32LE(val, offsets.color);
    }

    get action(): EMsgMapItemAction {
        return this.buffer.readUInt32LE(offsets.action);
    }
    set action(val: EMsgMapItemAction) {
        this.buffer.writeUInt32LE(val, offsets.action);
    }

    toArray(): Buffer {
        return this.buffer;
    }

    send(player: Player){
        player.send(this.buffer);
    }
}