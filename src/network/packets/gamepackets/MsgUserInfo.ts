import IPacket from "../../../interfaces/IPacket";
import Player from "../../../game/player";

export default class MsgUserInfo implements IPacket {
    player: Player;

    constructor(player: Player){
        this.player = player;
    }

    construct(){

    }

    send(player: Player) {
        player.send(this.toArray());
    }

    toArray(): Buffer {
        const {character} = this.player;
        const _uid        = 8,
            _mesh       = 14,
            _hair       = 18,
            _money      = 20,
            _cps        = 24,
            _exp        = 28,
            // 36 - 47
            _virtue     = 48,
            _str        = 56,
            _agi        = 58,
            _vit        = 60,
            _spi        = 62,
            _atr        = 64,
            _mana       = 68,
            _hp         = 70,
            _pk         = 72,
            _lev        = 74,
            _class      = 75,
            _fclass     = 76,
            _sclass     = 77,
            _reborn     = 79,
            _qps        = 81,
            _eps        = 89,
            _vip        = 90,
            _title      = 101,
            _cpsBound   = 103,
            _subclass   = 107,
            _subclassLv = 108,
            _racePoints = 116,
            _country    = 120,
            _listlen    = 122,
            _namelen    = 123,
            _name       = 124,
            _spousel    = 125,
            _spouse     = 126;

        let data: Buffer = Buffer.alloc(136 + character.name.length + character.spouse.length + 2);

        data.writeUInt16LE(data.length - 8, 0);
        data.writeUInt16LE(1006, 2);

        data.writeUInt32LE(character.uid, _uid);
        data.writeUInt32LE(character.getMesh(), _mesh);
        data.writeUInt16LE(character.hairStyle, _hair);

        data.writeUInt32LE(character.money, _money);
        data.writeUInt32LE(character.cps, _cps);
        data.writeUInt64LE(character.experience, _exp);
        
        data.writeUInt64LE(character.virtuePoints, _virtue);
        data.writeUInt16LE(character.strength, _str);
        data.writeUInt16LE(character.agility, _agi);
        data.writeUInt16LE(character.vitality, _vit);
        data.writeUInt16LE(character.spirit, _spi);
        data.writeUInt16LE(character.attrPoints, _atr);

        data.writeUInt16LE(character.pkPoints, _pk);

        data.writeUInt32LE(character.hp, _hp);
        data.writeUInt32LE(character.mana, _mana);

        data[_lev] = character.level;
        data[_class] = character.class;
        data[_fclass] = character.firstClass;
        data[_sclass] = character.secondClass;
        data[_reborn] = character.reborn;

        data[_country] = character.country;

        data.writeUInt32LE(character.quizPoints, _qps);
        data.writeUInt32LE(character.enlightenPoints, _eps);
        data.writeUInt32LE(character.vipLevel, _vip);
        data.writeUInt16LE(character.title, _title);
        data.writeUInt32LE(character.cpsBound, _cpsBound);

        data.writeStringArray([character.name, '', character.spouse], _listlen);

        return data;
    }
}