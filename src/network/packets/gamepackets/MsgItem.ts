import IPacket from "../../../interfaces/IPacket";
import Player from "../../../game/player";
import { EItemActionType } from "../../../enums/EItemAction";

enum offsets {
    uid = 8,
    param1 = 12,
    id = 16,
    timestamp = 20,
    param2 = 24,
    param3 = 28,
    param4 = 32,

    alternativeEquipment = 12,
    head = 36,
    necklace = 40,
    armor = 44,
    rWeapon = 48,
    lWeapon = 52,
    ring = 56,
    bottle = 60,
    boots = 64,
    garment = 68,
    rAccessory = 72,
    lAccessory = 76,
    steedArmor = 80,
    steedTalisman = 84,
}

export default class MsgItem implements IPacket {
    private buffer: Buffer;

    constructor(isNew: boolean){
        if (isNew){
            this.buffer = Buffer.alloc(88 + 8);
            this.buffer.writeUInt16LE(88, 0);
            this.buffer.writeUInt16LE(1009, 2);
        }
    }

    construct(buffer: Buffer){
        this.buffer = buffer;
    }

    toArray(): Buffer {
        return this.buffer;
    }

    send(player: Player){
        player.send(this.buffer);
    }

    get uid(): number {
        return this.buffer.readUInt32LE(offsets.uid);
    }
    set uid(val: number) {
        this.buffer.writeUInt32LE(val, offsets.uid);
    }

    get action(): EItemActionType {
        return this.buffer.readUInt32LE(offsets.id);
    }
    set action(val: EItemActionType) {
        this.buffer.writeUInt32LE(val, offsets.id);
    }

    get timestamp(): number {
        return this.buffer.readUInt32LE(offsets.timestamp);
    }
    set timestamp(val: number) {
        this.buffer.writeUInt32LE(val, offsets.timestamp);
    }

    get param1(): number {
        return this.buffer.readUInt32LE(offsets.param1);
    }
    set param1(val: number) {
        this.buffer.writeUInt32LE(val, offsets.param1);
    }

    get param2(): number {
        return this.buffer.readUInt32LE(offsets.param2);
    }
    set param2(val: number) {
        this.buffer.writeUInt32LE(val, offsets.param2);
    }

    get param3(): number {
        return this.buffer.readUInt32LE(offsets.param3);
    }
    set param3(val: number) {
        this.buffer.writeUInt32LE(val, offsets.param3);
    }

    get param4(): number {
        return this.buffer.readUInt32LE(offsets.param4);
    }
    set param4(val: number) {
        this.buffer.writeUInt32LE(val, offsets.param4);
    }

    get alternativeEquipment(): number {
        return this.buffer[offsets.alternativeEquipment];
    }
    set alternativeEquipment(val: number) {
        this.buffer[offsets.alternativeEquipment] = val;
    }

    get head(): number {
        return this.buffer.readUInt32LE(offsets.head);
    }
    set head(val: number) {
        this.buffer.writeUInt32LE(val, offsets.head);
    }

    get armor(): number {
        return this.buffer.readUInt32LE(offsets.armor);
    }
    set armor(val: number) {
        this.buffer.writeUInt32LE(val, offsets.armor);
    }

    get necklace(): number {
        return this.buffer.readUInt32LE(offsets.necklace);
    }
    set necklace(val: number) {
        this.buffer.writeUInt32LE(val, offsets.necklace);
    }

    get ring(): number {
        return this.buffer.readUInt32LE(offsets.ring);
    }
    set ring(val: number) {
        this.buffer.writeUInt32LE(val, offsets.ring);
    }

    get rightWeapon(): number {
        return this.buffer.readUInt32LE(offsets.rWeapon);
    }
    set rightWeapon(val: number) {
        this.buffer.writeUInt32LE(val, offsets.rWeapon);
    }

    get leftWeapon(): number {
        return this.buffer.readUInt32LE(offsets.lWeapon);
    }
    set leftWeapon(val: number) {
        this.buffer.writeUInt32LE(val, offsets.lWeapon);
    }

    get boots(): number {
        return this.buffer.readUInt32LE(offsets.boots);
    }
    set boots(val: number) {
        this.buffer.writeUInt32LE(val, offsets.boots);
    }

    get bottle(): number {
        return this.buffer.readUInt32LE(offsets.bottle);
    }
    set bottle(val: number) {
        this.buffer.writeUInt32LE(val, offsets.bottle);
    }

    get garment(): number {
        return this.buffer.readUInt32LE(offsets.garment);
    }
    set garment(val: number) {
        this.buffer.writeUInt32LE(val, offsets.garment);
    }

}