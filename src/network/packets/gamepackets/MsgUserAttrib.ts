import IPacket from "../../../interfaces/IPacket";
import Player from "../../../game/player";
import IUpdate from "../../../interfaces/IUpdate";

export default class MsgUserAttrib implements IPacket {
    private buffer: Buffer;
    uid: number;

    construct(buffer: Buffer) {
        this.buffer = buffer;
    }

    toArray(): Buffer {
        let len = 40 + this.updates.length * 24;
        let packet = Buffer.alloc(len + 8);

        packet.writeUInt16LE(len, 0);
        packet.writeUInt16LE(10017, 2);
        packet.writeUInt32LE(Date.now() / 1000, 4);

        packet.writeUInt32LE(this.uid, 8);
        packet.writeUInt32LE(this.updates.length, 12);

        let offset = 16;
        for (let i = 0; i < this.updates.length; i++) {
            let update = this.updates[i];
            packet.writeUInt32LE(update.type, offset);
            packet.writeUInt64LE(update.value1, offset + 4);
            packet.writeUInt64LE(update.value2, offset + 12);
            packet.writeUInt64LE(update.value3, offset + 20);
            offset += 24;
        }

        return packet;
    }

    send(player: Player) {
        player.send(this.toArray());
    }

    updates: IUpdate[] = [];

    addUpdate(update: IUpdate) {
        this.updates.push(update);
    }




}