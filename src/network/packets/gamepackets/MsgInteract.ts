import IPacket from "../../../interfaces/IPacket";
import Player from "../../../game/player";
import { EInteractionActivationType } from "../../../enums/EInteractionActivationType";
import { EInteractionType } from "../../../enums/EInteractionType";
const bn = require(`bignum`);

export default class MsgInteract implements IPacket {
    private buffer: Buffer;
    private activationFlag: number;

    weaponTypeR: number;
    weaponTypeL: number;
    magic: number;

    constructor(){
        this.buffer = Buffer.alloc(44);
        this.buffer.writeUInt16LE(36, 0);
        this.buffer.writeUInt16LE(1022, 2);

        this.activationFlag = 0;
    }

    construct(buffer: Buffer){
        this.buffer = buffer;
    }

    toArray(): Buffer {
        this.getActivation();
        return this.buffer;
    }

    send(player: Player){
        this.getActivation();
        player.send(this.buffer);
    }

    getActivation(){
        this.buffer.writeUInt32LE(this.activationFlag, 36);
        return this.activationFlag;
    }

    addStatus(activationFlag: EInteractionActivationType){
        let aux = bn(this.activationFlag).or(activationFlag);
        aux = Number.parseInt(aux.toString(10), 10);
        this.activationFlag = aux;
    }

    hasStatus(activationFlag: EInteractionActivationType){
        let aux = this.activationFlag & ~activationFlag;

        // console.log(aux.toString(), this.statusFlag.toString());
        let hasStatus = !(aux === this.activationFlag);
        return hasStatus;
    }

    removeStatus(activationFlag: EInteractionActivationType){
        if (this.hasStatus(activationFlag)){
            let aux = this.activationFlag & ~activationFlag;
            this.activationFlag = aux;
        }
    }

    get sender(): number {
        return this.buffer.readUInt32LE(12);
    }
    set sender(val: number){
        this.buffer.writeUInt32LE(val, 12);
    }

    get target(): number {
        return this.buffer.readUInt32LE(16);
    }
    set target(val: number){
        this.buffer.writeUInt32LE(val, 16);
    }

    get x(): number {
        return this.buffer.readUInt16LE(20);
    }
    set x(val: number){
        this.buffer.writeUInt16LE(val, 20);
    }

    get y(): number {
        return this.buffer.readUInt16LE(22);
    }
    set y(val: number){
        this.buffer.writeUInt16LE(val, 22);
    }

    get type(): EInteractionType {
        return this.buffer.readUInt32LE(24);
    }
    set type(val: EInteractionType){
        this.buffer.writeUInt32LE(val, 24);
    }

    get value(): number {
        return this.buffer.readUInt32LE(28);
    }
    set value(val: number){
        this.buffer.writeUInt32LE(val, 28);
    }

    get ko(): number {
        return this.buffer.readUInt32LE(30);
    }
    set ko(val: number){
        this.buffer.writeUInt32LE(val, 30);
    }

    get responseValue(): number {
        return this.buffer.readUInt32LE(32);
    }
    set responseValue(val: number){
        this.buffer.writeUInt32LE(val, 32);
    }

    /*get effectValue(): number {
        return this.buffer.readUInt32LE(40);
    }
    set effectValue(val: number){
        this.buffer.writeUInt32LE(val, 40);
    }*/
}