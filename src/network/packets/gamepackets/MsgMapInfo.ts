import IPacket from "../../../interfaces/IPacket";
import Player from "../../../game/player";

const offsets = {
    id: 4,
    baseId: 8,
    status: 12,
    weather: 20,
}

export default class MsgMapInfo implements IPacket {
    private buffer: Buffer;

    constructor(){
        this.buffer = Buffer.alloc(24);
    }

    construct(buffer: Buffer){
        this.buffer = buffer;
    }

    send(player: Player) {
        player.send(this.toArray());
    }

    toArray(): Buffer {
        return this.buffer;
    }

    get id(): number {
        return this.buffer.readUInt32LE(offsets.id);
    }
    set id(val: number){
        this.buffer.writeUInt32LE(val, offsets.id);
    }

    get baseId(): number {
        return this.buffer.readUInt32LE(offsets.baseId);
    }
    set baseId(val: number){
        this.buffer.writeUInt32LE(val, offsets.baseId);
    }

    get status(): number {
        return this.buffer.readUInt32LE(offsets.status);
    }
    set status(val: number){
        this.buffer.writeUInt32LE(val, offsets.status);
    }

    get weather(): number {
        return this.buffer.readUInt32LE(offsets.weather);
    }
    set weather(val: number){
        this.buffer.writeUInt32LE(val, offsets.weather);
    }
}