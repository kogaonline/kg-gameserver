import IPacket from "../../../interfaces/IPacket";
import Player from "../../../game/player";
import IndexedElement from "../../../components/indexedElement";
import Character from "../../../game/character";

interface MagicEffect {
    receiver: number;
    value: number;
    hit: boolean;
    activation: number;
    x: number;
    y: number;
}

export class EffectTarget {
    targetUID: number;
    damage: number;
    hit: boolean;
    X: number;
    Y: number;
    activationFlag: number;
}

export default class MsgMagicEffect implements IPacket {

    private buffer: Buffer;
    public targets: IndexedElement<EffectTarget>;

    magicId: number;

    constructor() {
        this.buffer = Buffer.alloc(28);
        this.buffer.writeUInt16LE(20, 0);
        this.buffer.writeUInt16LE(1105, 2);

        this.targets = new IndexedElement();
    }

    construct(buffer: Buffer) {
        this.buffer = buffer;
    }

    toArray(): Buffer {
        let _buffer = Buffer.alloc(61 + this.targets.size * 32)
        _buffer.writeUInt16LE(_buffer.length - 8, 0);
        _buffer.writeUInt16LE(1105, 2);
        _buffer.writeUInt32LE(this.sender, 4);
        _buffer.writeUInt16LE(this.x, 8);
        _buffer.writeUInt16LE(this.y, 10);
        _buffer.writeUInt16LE(this.id, 12);
        _buffer.writeUInt16LE(this.level, 14);

        _buffer.writeUInt32LE(this.targets.size, 17);
        this.targets.map((et, i) => {
            let pos = 20 + (i * 32);

            _buffer.writeUInt32LE(et.targetUID, pos + 0);
            _buffer.writeUInt32LE(et.damage, pos + 4);
            _buffer[pos + 8] = et.hit ? 1 : 0;
            _buffer.writeUInt64LE(et.activationFlag ? et.activationFlag : 0, pos + 12);
            _buffer.writeUInt32LE(et.X, pos + 20);
            _buffer.writeUInt32LE(et.Y, pos + 24);

        });

        return _buffer;
    }

    get sender(): number {
        return this.buffer.readUInt32LE(4);
    }
    set sender(val: number) {
        this.buffer.writeUInt32LE(val, 4);
    }

    get x(): number {
        return this.buffer.readUInt16LE(8);
    }
    set x(val: number) {
        this.buffer.writeUInt16LE(val, 8);
    }

    get y(): number {
        return this.buffer.readUInt16LE(10);
    }
    set y(val: number) {
        this.buffer.writeUInt16LE(val, 10);
    }

    get id(): number {
        return this.buffer.readUInt16LE(12);
    }
    set id(val: number) {
        this.buffer.writeUInt16LE(val, 12);
    }

    get level(): number {
        return this.buffer.readUInt16LE(14);
    }
    set level(val: number) {
        this.buffer.writeUInt16LE(val, 14);
    }

    addTarget(effectTarget: EffectTarget) {
        if (this.targets.size < 32)
            this.targets.add(effectTarget.targetUID, effectTarget);
    }

    send(player: Player) {
        player.send(this.buffer);
    }
}