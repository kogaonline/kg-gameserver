import IPacket from "../../../interfaces/IPacket";
import Player from "../../../game/player";
import { EMsgActionType } from "../../../enums/EMsgActionType";
import { EDirection } from "../../../enums/EDirection";

const offsets = {
    uid: 8,
    param: 12,
    param2: 16,
    timestamp: 20,
    data24: 24,
    id: 24,
    param3: 26,
    facing: 26,
    aParam5: 12,
    aParam7: 32,
    aParam6: 14,
    aParam1: 28,
    aParam2: 30,
    aParam3: 36,
    aParam4: 30,
    map: 12,
    textLength: 42
}

export default class MsgAction implements IPacket {
    private buffer: Buffer;

    constructor(create: boolean, text: string = "") {
        if (create) {
            this.buffer = Buffer.alloc(text.length ? (54 + text.length) : 50);

            this.buffer.writeUInt16LE(this.buffer.length - 8, 0);
            this.buffer.writeUInt16LE(10010, 2);
            // this.buffer.writeUInt32LE(Date.now()/1000, 4);

            if (text.length){
                this.buffer[offsets.textLength - 1] = 1;
                this.buffer[offsets.textLength] = text.length & 0xFF;
                this.buffer.write(text, offsets.textLength + 1);
            }
        }
    }

    construct(buffer: Buffer) {
        this.buffer = buffer;
    }

    send(player: Player) {
        player.send(this.toArray());
    }

    toArray(): Buffer {
        return this.buffer;
    }

    get uid(): number {
        return this.buffer.readUInt32LE(offsets.uid);
    }
    set uid(val: number) {
        this.buffer.writeUInt32LE(val, offsets.uid);
    }

    get hash(): number {
        return this.buffer.readUInt32LE(4);
    }
    set hash(val: number) {
        this.buffer.writeUInt32LE(val, 4);
    }

    get timestamp(): number {
        return this.buffer.readUInt32LE(offsets.timestamp);
    }
    set timestamp(val: number) {
        this.buffer.writeUInt32LE(val, offsets.timestamp);
    }

    get id(): EMsgActionType {
        return this.buffer.readUInt16LE(offsets.id);
    }
    set id(val: EMsgActionType) {
        this.buffer.writeUInt16LE(val, offsets.id);
    }

    get param(): number {
        return this.buffer.readUInt32LE(offsets.param);
    }
    set param(val: number) {
        this.buffer.writeUInt32LE(val, offsets.param);
    }

    get param2(): number {
        return this.buffer.readUInt32LE(offsets.param2);
    }
    set param2(val: number) {
        this.buffer.writeUInt32LE(val, offsets.param2);
    }

    get param3(): number {
        return this.buffer.readUInt16LE(offsets.param3);
    }
    set param3(val: number) {
        this.buffer.writeUInt16LE(val, offsets.param3);
    }

    get aParam1(): number {
        return this.buffer.readUInt16LE(offsets.aParam1);
    }
    set aParam1(val: number) {
        this.buffer.writeUInt16LE(val, offsets.aParam1);
    }

    get aParam2(): number {
        return this.buffer.readUInt16LE(offsets.aParam2);
    }
    set aParam2(val: number) {
        this.buffer.writeUInt16LE(val, offsets.aParam2);
    }

    get x(): number {
        return this.buffer.readUInt16LE(offsets.aParam1);
    }
    set x(val: number) {
        this.buffer.writeUInt16LE(val, offsets.aParam1);
    }

    get y(): number {
        return this.buffer.readUInt16LE(offsets.aParam2);
    }
    set y(val: number) {
        this.buffer.writeUInt16LE(val, offsets.aParam2);
    }

    get map(): number {
        return this.buffer.readUInt32LE(offsets.map);
    }
    set map(val: number) {
        this.buffer.writeUInt32LE(val, offsets.map);
    }

    get aParam5(): number {
        return this.buffer.readUInt16LE(offsets.aParam5);
    }
    set aParam5(val: number) {
        this.buffer.writeUInt16LE(val, offsets.aParam5);
    }

    get aParam3(): number {
        return this.buffer.readUInt32LE(offsets.aParam3);
    }
    set aParam3(val: number) {
        this.buffer.writeUInt32LE(val, offsets.aParam3);
    }

    get aParam6(): number {
        return this.buffer.readUInt16LE(offsets.aParam6);
    }
    set aParam6(val: number) {
        this.buffer.writeUInt16LE(val, offsets.aParam6);
    }

    get facing(): EDirection {
        return this.buffer.readUInt16LE(offsets.facing);
    }
    set facing(val: EDirection) {
        this.buffer.writeUInt16LE(val, offsets.facing);
    }
}