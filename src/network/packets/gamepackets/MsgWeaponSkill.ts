import IPacket from "../../../interfaces/IPacket";
import Player from "../../../game/player";

export default class MsgWeaponSkill implements IPacket {
    private buffer: Buffer;
    public uid = 0;

    constructor(){
        this.buffer = Buffer.alloc(28);
        this.buffer.writeUInt16LE(20, 0);
        this.buffer.writeUInt16LE(1025, 2);
    }

    construct(buffer: Buffer){
        this.buffer = buffer;
    }

    toArray(): Buffer {
        return this.buffer;
    }

    send(player: Player){
        player.send(this.buffer);
    }

    get type(): number {
        return this.buffer.readUInt16LE(4);
    }
    set type(val: number){
        this.buffer.writeUInt16LE(val, 4);
    }

    get level(): number {
        return this.buffer[8];
    }
    set level(val: number){
        this.buffer[8] = val;
    }

    get experience(): number {
        return this.buffer.readUInt32LE(12);
    }
    set experience(val: number){
        this.buffer.writeUInt32LE(val, 12);
    }

    get reqExperience(): number {
        return this.buffer.readUInt32LE(16);
    }
    set reqExperience(val: number){
        this.buffer.writeUInt32LE(val, 16);
    }
}