import IPacket from "../../../interfaces/IPacket";
import Player from "../../../game/player";

enum offsets {
    timestamp = 4,
}

export default class Msg implements IPacket {
    private buffer: Buffer;

    constructor() {
        this.buffer = Buffer.alloc(20 + 8);
        this.buffer.writeUInt16LE(20, 0);
        this.buffer.writeUInt16LE(0, 2);
    }

    get timestamp(): number {
        return this.buffer.readUInt32LE(offsets.timestamp);
    }
    set timestamp(val: number) {
        this.buffer.writeUInt32LE(val, offsets.timestamp);
    }

    construct(buffer: Buffer){
        this.buffer = buffer;
    }

    toArray(): Buffer {
        return this.buffer;
    }

    send(player: Player){
        player.send(this.buffer);
    }
}