import INpcPacket from "../../../interfaces/INpcPacket";
import NPCServerSocket from "../../sockets/npcSocket";

export default class NpcMonsterInfo implements INpcPacket {
    private buffer: Buffer;

    constructor(){
        this.buffer = Buffer.alloc(6);
        this.buffer.writeUInt16LE(6, 0);
        this.buffer.writeUInt16LE(1001, 2);
    }

    construct(buffer: Buffer){
        this.buffer = buffer;
    }

    get size(): number {
        return this.buffer.readUInt16LE(4);
    }

    set size(val: number) {
        this.buffer.writeUInt16LE(val, 4);
    }


    writeData(data: any[]){
        let datastr = JSON.stringify(data);
        this.size = datastr.length;

        let newbuffer = Buffer.alloc(6 + datastr.length);
        this.buffer.copy(newbuffer, 0);

        // newbuffer.writeUInt16LE(data.length, 4);
        newbuffer.write(datastr, 6, datastr.length, 'utf-8');
        this.buffer = newbuffer;
    }

    toArray(): Buffer {
        return this.buffer;
    }

    send(npcserver: NPCServerSocket){
        npcserver.send(this);
    }
}