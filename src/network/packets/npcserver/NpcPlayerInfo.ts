import INpcPacket from "../../../interfaces/INpcPacket";
import NPCServerSocket from "../../sockets/npcSocket";
import Player from "../../../game/player";
import IndexedElement from "../../../components/indexedElement";

interface IPlayer {
    uid: number;
    mapid: number;
    x: number;
    y: number;
    level: number;
    alive: boolean;
}

export default class NpcPlayerInfo implements INpcPacket {
    private buffer: Buffer;
    private players: IndexedElement<IPlayer>;

    constructor() {
        this.players = new IndexedElement();
        this.buffer = Buffer.alloc(8);
        this.buffer.writeUInt16LE(6, 0);
        this.buffer.writeUInt16LE(2001, 2);
    }

    construct(buffer: Buffer) {
        this.buffer = buffer;
    }

    get size(): number {
        return this.buffer.readUInt32LE(4);
    }

    set size(val: number) {
        this.buffer.writeUInt32LE(val, 4);
    }

    addPlayer(player: Player) {
        let p: IPlayer = {
            uid: player.character.uid,
            mapid: player.character.mapId,
            x: player.character.x,
            y: player.character.y,
            level: player.character.level,
            alive: !player.character.dead
        };

        this.players.add(p.uid, p);
    }

    prepare() {
        this.size = this.players.size;

        let buff: Buffer = Buffer.alloc(8 + (this.players.size * 14));
        this.buffer.copy(buff, 0);

        this.players.map((p, i) => {
            let n = 14 * i;

            buff.writeUInt32LE(p.uid, 0 + (n + 8));
            buff.writeUInt32LE(p.mapid, 4 + (n + 8));
            buff.writeUInt32LE(p.x, 8 + (n + 8));
            buff.writeUInt32LE(p.y, 10 + (n + 8));
            buff.writeUIntLE(p.level, 12 + (n + 8), 1);
            buff.writeUIntLE(p.alive ? 1 : 0, 13 + (n + 8), 1);
        });

        this.buffer = buff;
    }

    toArray(): Buffer {
        return this.buffer;
    }

    send(npcserver: NPCServerSocket) {
        this.prepare();
        npcserver.send(this);
    }
}