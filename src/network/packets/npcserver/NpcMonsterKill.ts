import INpcPacket from "../../../interfaces/INpcPacket";
import NPCServerSocket from "../../sockets/npcSocket";

export default class NpcMonsterKill implements INpcPacket {
    private buffer: Buffer;

    constructor(){
        this.buffer = Buffer.alloc(12);
        this.buffer.writeUInt16LE(1006, 2);
    }

    construct(buffer: Buffer){
        this.buffer = buffer;
    }

    get uid(): number {
        return this.buffer.readUInt32LE(4);
    }
    set uid(val: number) {
        this.buffer.writeUInt32LE(val, 4);
    }

    get mapid(): number {
        return this.buffer.readUInt16LE(8);
    }
    set mapid(val: number) {
        this.buffer.writeUInt16LE(val, 8);
    }

    toArray(): Buffer {
        return this.buffer;
    }

    send(npcserver: NPCServerSocket){
        npcserver.send(this);
    }
}