import INpcPacket from "../../../interfaces/INpcPacket";
import NPCServerSocket from "../../sockets/npcSocket";

export default class NpcMonsterMove implements INpcPacket {
    private buffer: Buffer;
    constructor(){
        this.buffer = Buffer.alloc(14);
        this.buffer.writeUInt16LE(1004, 2);
    }

    construct(buffer: Buffer){
        this.buffer = buffer;
    }

    get uid(): number {
        return this.buffer.readUInt32LE(4);
    }
    set uid(val: number) {
        this.buffer.writeUInt32LE(val, 4);
    }

    get mapid(): number {
        return this.buffer.readUInt32LE(8);
    }
    set mapid(val: number) {
        this.buffer.writeUInt32LE(val, 8);
    }

    get angle(): number {
        return this.buffer[12];
    }
    set angle(val: number) {
        this.buffer[12] = val;
    }

    toArray(): Buffer {
        return this.buffer;
    }

    send(npcserver: NPCServerSocket){
        npcserver.send(this);
    }
}