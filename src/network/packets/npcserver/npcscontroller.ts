import App from "../../../";
import Core from "../../../core";
import { getRandom } from "../../../logical/kernel";
import Character from "../../../game/character";
import IndexedElement from "../../../components/indexedElement";
import { EMapElementType } from "../../../enums/EMapElementType";
import NpcMonsterMove from "./NpcMonsterMove";
import NpcMonsterInfo from "./NpcMonsterInfo";
import NpcMonsterGen from "./NpcMonsterGen";
import MsgWalk from "../gamepackets/MsgWalk";
import NpcPlayerInfo from "./NpcPlayerInfo";
import NpcMonsterAttack from "./NpcMonsterAttack";
import MsgInteract from "../gamepackets/MsgInteract";
import { EInteractionType } from "../../../enums/EInteractionType";
import handleInteraction from "../../../game/interaction/handle";

const enum packetType {
    NpcMonsterInfo = 1001,
    NpcMonsterGen = 1002,
    NpcMonsterPos = 1003,
    NpcMonsterMove = 1004,
    NpcMonsterAttack = 1005,
    NpcMonsterKill = 1006,

    NpcPlayerInfo = 2001,

};

export class NPCServerController {
    static handlerTime: number = 0;
    static handlerTimeMax: number = 0;

    static rhandlerTime: number = 0;
    static rhandlerTimeMax: number = 0;

    public static handle(buffer: Buffer) {
        let start = process.hrtime();

        let id = buffer.readUInt16LE(2);
        let length = buffer.readUInt16LE(0);

        let msg = `NPCS Packet ${id} with ${length} bytes`;

        switch (id) {
            case packetType.NpcMonsterGen: {
                let gen = new NpcMonsterGen();
                gen.construct(buffer);
                this.generateMonster(gen);
                break;
            }

            case packetType.NpcMonsterMove: {
                let move = new NpcMonsterMove();
                move.construct(buffer);
                this.moveMonster(move);
                break;
            }

            case packetType.NpcMonsterAttack: {
                let atk = new NpcMonsterAttack();
                atk.construct(buffer);
                this.monsterAttack(atk);
                break;
            }

            default: {
                console.log(msg);
                break;
            }
        }

        let end = process.hrtime(start);
        let endns = end[1];
        NPCServerController.handlerTime = endns;
        if (endns > NPCServerController.handlerTimeMax){
            NPCServerController.handlerTimeMax = endns;
            console.warn(`NPCS Controller (${id}) took ${endns / 1000000}ms.`);
        }
    }

    public static resetMonsterData() {
        Core.maps.map(map => {
            map.monsters.map(m => {
                m.map.removeMonster(m);
                m.removeSpawn();
            });
            map.monsters = new IndexedElement();
        });
    }

    public static sendPlayerInfo() {
        let data = new NpcPlayerInfo();
        Core.playerPool.map(p => {
            if (!p.character.dead)
                data.addPlayer(p);
        });

        data.send(App.npcserver);

        console.log('Sent Player Info to NPC Server');
    }

    public static sendMonsterInfo() {
        let data = new NpcMonsterInfo();
        data.writeData(Core.monsterInfo.toArray());
        data.send(App.npcserver);
        // console.log('Sent Monster Info to NPC Server');
    }

    public static moveMonster(move: NpcMonsterMove) {
        // console.log(`Receive monster ${move.uid} move in angle ${move.angle}.`);
        let map = Core.maps.get(move.mapid);
        if (map) {
            if (map.monsters.contains(move.uid)) {
                let monster = map.monsters.get(move.uid);
                if (monster && !monster.dead) {
                    let data = new MsgWalk();
                    data.uid = move.uid;
                    data.map = move.mapid;
                    data.type = 1;
                    data.direction = move.angle;

                    monster.move(data);
                }
            }
        }
    }

    public static monsterAttack(attack: NpcMonsterAttack) {
        // console.log(`Receive monster ${attack.uid} attack to target ${attack.target} in angle ${attack.angle}.`);
        let map = Core.maps.get(attack.mapid);
        if (map) {
            if (map.monsters.contains(attack.uid)) {
                let monster = map.monsters.get(attack.uid);
                if (monster && monster.monsterType && !monster.dead) {
                    if (monster.monsterType.magic_type) {
                        // magic attack
                    } else {
                        let mattack = new MsgInteract();
                        mattack.sender = monster.uid;
                        mattack.target = attack.target;
                        mattack.type = EInteractionType.Attack;
                        mattack.x = monster.x;
                        mattack.y = monster.y;

                        handleInteraction(mattack, monster);
                    }
                }
            }
        }
    }

    public static generateMonster(gen: NpcMonsterGen) {
        //console.log(`Received generation at ${gen.mapid},${gen.x},${gen.y}, of type ${gen.type} with uid ${gen.uid}.`);
        let map = Core.maps.get(gen.mapid);
        if (map && map.ground) {
            let validPlace = map.ground.getNextValid(EMapElementType.MONSTER, gen.x, gen.y);
            if (validPlace) {
                let monstertype = Core.monsterInfo.get(gen.type);
                if (monstertype) {
                    let start = process.hrtime();
                    let monster = new Character(null, true, false);
                    monster.uid = gen.uid;
                    monster.monsterType = monstertype
                    monster.name = monstertype.name;
                    monster.body = monstertype.body;
                    monster.maxhp = monstertype.life;
                    monster.hp = monstertype.life;
                    monster.level = monstertype.level;
                    monster.mapId = gen.mapid;
                    monster.setLocation(validPlace.x, validPlace.y);
                    monster.facing = getRandom(1, 7);

                    let end = process.hrtime(start);
                    let endns = end[1];
                    NPCServerController.rhandlerTime = endns;
                    if (endns > NPCServerController.rhandlerTimeMax){
                        NPCServerController.rhandlerTimeMax = endns;
                        console.warn(`NPCS Controller Monster Creation took ${endns / 1000000}ms.`);
                    }

                    monster.attackRange = monstertype.range;
                    monster.minAttack = monstertype.min_attack;
                    monster.maxAttack = monstertype.max_attack;
                    monster.defense = monstertype.defense;
                    monster.speed = monstertype.speed;

                    monster.recalculateBP();

                    //if (Core.monsterPool.contains(monster.uid))
                    //    Core.monsterPool.remove(monster.uid);
                    //Core.monsterPool.add(monster.uid, monster);
                    
                    if (map.monsters.contains(monster.uid))
                        map.removeMonster(monster);
                    map.addMonster(monster);
                    map.players.map(player => {
                        player.screen.reload();
                        player.character.sendToScreen(monster.getSpawnPacket());
                    });

                    monster.sendSpawnEffect();
                }
            }
        }
    }
}