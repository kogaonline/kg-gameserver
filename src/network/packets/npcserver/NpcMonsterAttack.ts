import INpcPacket from "../../../interfaces/INpcPacket";
import NPCServerSocket from "../../sockets/npcSocket";

export default class NpcMonsterAttack implements INpcPacket {
    private buffer: Buffer;
    constructor(){
        this.buffer = Buffer.alloc(18);
        this.buffer.writeUInt16LE(1005, 2);
    }

    construct(buffer: Buffer){
        this.buffer = buffer;
    }

    get uid(): number {
        return this.buffer.readUInt32LE(4);
    }
    set uid(val: number) {
        this.buffer.writeUInt32LE(val, 4);
    }

    get mapid(): number {
        return this.buffer.readUInt32LE(8);
    }
    set mapid(val: number) {
        this.buffer.writeUInt32LE(val, 8);
    }

    get target(): number {
        return this.buffer.readUInt32LE(12);
    }
    set target(val: number) {
        this.buffer.writeUInt32LE(val, 12);
    }

    get angle(): number {
        return this.buffer[16];
    }
    set angle(val: number) {
        this.buffer[16] = val;
    }

    toArray(): Buffer {
        return this.buffer;
    }

    send(npcserver: NPCServerSocket){
        npcserver.send(this);
    }
}