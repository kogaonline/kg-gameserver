import INpcPacket from "../../../interfaces/INpcPacket";
import NPCServerSocket from "../../sockets/npcSocket";

export default class NpcMonsterGen implements INpcPacket {
    private buffer: Buffer;

    constructor(){
        this.buffer = Buffer.alloc(22);
        this.buffer.writeUInt16LE(1002, 2);
    }

    construct(buffer: Buffer){
        this.buffer = buffer;
    }

    get uid(): number {
        return this.buffer.readUInt32LE(4);
    }
    set uid(val: number) {
        this.buffer.writeUInt32LE(val, 4);
    }

    get mapid(): number {
        return this.buffer.readUInt32LE(8);
    }
    set mapid(val: number) {
        this.buffer.writeUInt32LE(val, 8);
    }

    get type(): number {
        return this.buffer.readUInt32LE(12);
    }
    set type(val: number) {
        this.buffer.writeUInt32LE(val, 12);
    }

    get x(): number {
        return this.buffer.readUInt16LE(16);
    }
    set x(val: number) {
        this.buffer.writeUInt16LE(val, 16);
    }

    get y(): number {
        return this.buffer.readUInt16LE(18);
    }
    set y(val: number) {
        this.buffer.writeUInt16LE(val, 18);
    }

    toArray(): Buffer {
        return this.buffer;
    }

    send(npcserver: NPCServerSocket){
        npcserver.send(this);
    }
}