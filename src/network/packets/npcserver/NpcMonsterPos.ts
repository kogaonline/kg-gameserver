import INpcPacket from "../../../interfaces/INpcPacket";
import NPCServerSocket from "../../sockets/npcSocket";

export default class NpcMonsterPos implements INpcPacket {
    private buffer: Buffer;

    constructor(){
        this.buffer = Buffer.alloc(16);
        this.buffer.writeUInt16LE(1003, 2);
    }

    construct(buffer: Buffer){
        this.buffer = buffer;
    }

    get uid(): number {
        return this.buffer.readUInt32LE(4);
    }
    set uid(val: number) {
        this.buffer.writeUInt32LE(val, 4);
    }

    get mapid(): number {
        return this.buffer.readUInt16LE(8);
    }
    set mapid(val: number) {
        this.buffer.writeUInt16LE(val, 8);
    }

    get x(): number {
        return this.buffer.readUInt16LE(12);
    }
    set x(val: number) {
        this.buffer.writeUInt16LE(val, 12);
    }

    get y(): number {
        return this.buffer.readUInt16LE(14);
    }
    set y(val: number) {
        this.buffer.writeUInt16LE(val, 14);
    }

    toArray(): Buffer {
        return this.buffer;
    }

    send(npcserver: NPCServerSocket){
        npcserver.send(this);
    }
}