import ServerSocket from './serverSocket';
import * as net from 'net';
import * as Settings from '../../settings';
import PacketQueue from '../../components/packetQueue';
import Player from '../../game/player';
import GameCryptography from '../cryptography/gameCryptography';
import { printBuffer } from '../../logical/kernel';

class PClient {
    id: number;
    player: Player;
    socket: net.Socket;
    private ssocket: net.Socket;
    private squeue: PacketQueue;
    private queue: PacketQueue;

    preconnected: boolean = false;
    connecting: boolean = false;
    connected: boolean = false;
    reconnectTimer: any;

    constructor(socket: net.Socket) {
        let _this = this;

        this.socket = socket;
        this.ssocket = new net.Socket();
        this.ssocket.on("close", () => {
            _this.preconnected = false;
            _this.connecting = false;
            _this.connected = false;
            console.log("Broken connection to Game Server...");
        });
        this.doConnect();

        this.ssocket.on('data', (data: Buffer) => this.recv(data));

        this.queue = new PacketQueue();
        this.squeue = new PacketQueue();

        this.startMonitoring();
    }

    startMonitoring() {
        this.reconnectTimer = setInterval(() => {
            if (!this.connected && !this.connecting && !this.preconnected) {
                this.doConnect(true, (connected: boolean) => {
                    if (connected) {
                        this.send(null);
                        this.recv(null);
                    } else {
                        console.log(`[${this.id}] Reconnected failed!`);
                        this.connecting = false;
                    }
                })
            }
        }, 1000);
    }

    doConnect(reconnect: boolean = false, cb?: any) {
        if (!this.connecting) {
            this.connecting = true;

            let _this = this;
            this.ssocket.connect(Settings.DS_PORT, '127.0.0.1', () => {
                _this.preconnected = true;
                if (reconnect)
                    console.log(`Reconnecting player (${this.id}).`);

                _this.connecting = false;

                let id = _this.id;

                if (id && id != 0) {

                } else {
                    id = Date.now();
                    _this.id = id;
                }

                let buffer = Buffer.alloc(12);
                buffer.write(`spid`, 0);

                if (reconnect)
                    buffer.write(`rpid`, 0);

                buffer.writeUInt64LE(id, 4);

                _this.send(buffer);
                console.log(`============`);
                console.log(`Sent PID ${id}`);
                console.log(`============`);

                if (cb) {
                    cb(true);
                }
            }).once("error", () => cb ? cb(false) : false);
        }
    }

    disconnect(kill?: boolean) {
        try {
            clearInterval(this.reconnectTimer);
            if (this.ssocket) {
                this.ssocket.end();
                this.ssocket.destroy();
                this.ssocket = null;
            }

            if (kill && this.socket) {
                this.socket.end();
                this.socket.destroy();
                this.socket = null;
            }
        } catch (err) {
            console.log(err);
        }
    }

    send(buffer: Buffer) {
        try {
            if (buffer) {
                this.squeue.enqueue(buffer);
                console.log(`Adding packet to the queue. (${this.squeue.length})`);
            }

            if (this.connected && !this.connecting) {
                while (this.squeue.canDequeue()) {
                    let packet = this.squeue.dequeue();
                    if (packet) {
                        // console.log(`-> Sending data to gameserver...`);
                        // printBuffer(packet);
                        this.ssocket.write(packet);
                        //console.log(`-> Sent data to gameserver...`);
                    } else {
                        console.log(`Packet is null...`);
                    }
                }
            } else {
                if (this.preconnected) {
                    while (this.squeue.canDequeue()) {
                        let packet = this.squeue.dequeue();
                        if (packet) {
                            let spid = packet.toString('ascii', 0, 4);
                            if (spid === "spid" || spid === "rpid")
                                this.ssocket.write(packet);
                        }
                    }
                }
            }
        } catch (err) {
            console.log(err);
            this.disconnect();
        }
    }

    recv(buffer: Buffer) {
        try {
            if (buffer) {
                let spid = buffer.toString('ascii', 0, 4);
                if (spid === `kill`) {
                    console.log('Received kill command!');
                    this.disconnect(true);
                    return;
                }

                if (spid === `cpid`) {
                    this.connecting = false;
                    this.connected = true;
                    console.log(`============`);
                    console.log(`[${this.id}] Reconnected successfully!`);
                    console.log(`Packets on the queue: ${this.squeue.length}`);
                    console.log(`============`);
                    return;
                }

                this.queue.enqueue(buffer);
            }

            if (this.connected) {
                while (this.queue.canDequeue()) {
                    let packet: Buffer = this.queue.dequeue();
                    // console.log(`<- Sending data to client...`);
                    // printBuffer(packet);
                    this.socket.player.send(packet);
                    //console.log(`<- Sent data to client...`);
                }
            }
        } catch (err) {
            console.log(err);
            this.disconnect();
        }
    }
}

export default class PoolServerSocket {
    socket: ServerSocket;

    constructor() {
        this.socket = new ServerSocket((socket: net.Socket) => {
            this.connected(socket);
        })

        this.socket.disconnect = (err: any, socket: any) => {
            this.disconnected(socket);
        }

        this.socket.receiveData = (data: Buffer, socket: net.Socket) => {
            this.handleData(data, socket);
        }
    }

    start(port: number) {
        this.socket.start(port);
        console.log(`Socket Pool Started on port ${port}.`)
    }

    connected(socket: net.Socket) {
        let client = new PClient(socket);

        let player = new Player(client.socket);
        let data = player.dKeyExchange.getPacket();
        player.send(data);

        client.connected = true;
        socket.client = client;
        client.player = player;

        socket.player = player;
    }

    disconnected(socket: net.Socket) {
        socket.client.connected = false;
        console.log('Broken connection with Socket Pool...');
        socket.client.disconnect();
    }

    handleData(buffer: Buffer, socket: net.Socket) {
        try {
            let player: Player = socket.player;

            if (!player.exchanged) {
                player.playerAction = 0;

                let serverKey = Buffer.from(Settings.SERVER_KEY)
                let crypto = new GameCryptography(serverKey)

                let otherData = Buffer.alloc(buffer.length);
                buffer.copy(otherData, 0, 0);

                otherData = crypto.decrypt(otherData);

                let plus = false
                let pos = 0

                let a = otherData[otherData.length - 140];
                let b = otherData.length >= 176 ? otherData.readInt32LE(otherData.length - 176) : 26

                if (a === 128) {
                    pos = buffer.length - 140
                    buffer = player.cryptography.decrypt(buffer);
                } else if (b === 128) {
                    pos = buffer.length - 176
                    plus = true
                    buffer = player.cryptography.decrypt(buffer, buffer.length - 36);
                }

                let length = buffer.readInt32LE(pos)
                pos += 4
                if (length != 128) {
                    console.log('Ops! Something wrong...');
                    player.disconnect();
                    return;
                }

                let pubKey = Buffer.alloc(128)
                for (let i = 0; i < length; i++ , pos++) {
                    pubKey[i] = buffer[pos]
                }

                let pubKeyStr = pubKey.toString()
                player.cryptography = player.dKeyExchange.handleClientKey(pubKeyStr, crypto)

                if (plus) {
                    let pdata = Buffer.alloc(36);
                    buffer.copy(pdata, 0, buffer.length - 36);
                    this.processData(pdata, player, true);
                }

                player.exchanged = true;
            } else {
                this.processData(buffer, player);
            }
        } catch (err) {
            console.error(err);
        }
    }

    processData(buffer: Buffer, player: Player, login?: boolean) {
        let data = player.cryptography.decrypt(buffer);
        let pclient: PClient = player.socket.client;
        pclient.send(data);
    }
}

let pserver = new PoolServerSocket();
pserver.start(Settings.GS_PORT);
