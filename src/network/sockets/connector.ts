import * as net from 'net'
import IPacket from '../../interfaces/IPacket';

export default class Connector {
    _socket: net.Socket

    constructor(socket :net.Socket){
        this._socket = socket
    }

    sendBuffer(buffer:Buffer){
        this._socket.write(buffer)
    }

    send(data:IPacket){
        this.sendBuffer(data.toArray())
    }
}