import ServerSocket from './serverSocket';
import * as net from 'net';
import * as Settings from '../../settings';
import Player from '../../game/player';
import GameCryptography from '../cryptography/gameCryptography';
import { printBuffer } from '../../logical/kernel';
import { GamePacketController } from '../packets/controller';
import Core from '../../core';
import PacketQueue from '../../components/packetQueue';

export default class GameServerSocket {
    socket: ServerSocket;

    constructor() {
        if (Settings.DEV_MODE) {
            this.socket = new ServerSocket((socket: net.Socket) => {
                this._connected(socket);
            });

            this.socket.disconnect = (err: any, socket: net.Socket) => {
                this._disconnected(socket);
            };

            this.socket.receiveData = (data: Buffer, socket: net.Socket) => {
                this._handleData(data, socket);
            };

            this.socket.error = (err: any, socket: net.Socket) => {
                console.log('[GS] Connect error:');
                console.log(err);
                this.disconnected(socket);
            };
        } else {
            this.socket = new ServerSocket((socket: net.Socket) => {
                socket.setNoDelay(true);
                socket.setKeepAlive(true);
                
                this.connected(socket);
            });

            this.socket.disconnect = (err: any, socket: net.Socket) => {
                this.disconnected(socket);
            };

            this.socket.receiveData = (data: Buffer, socket: net.Socket) => {
                if (!socket.destroyed && socket.writable)
                    GameServerSocket.handleData(data, socket);
                    /*if (socket.player){
                        socket.player.recqueue.enqueue(data);
                    }*/
            };

            this.socket.error = (err: any, socket: net.Socket) => {
                console.log('[GS] Connect error:');
                console.log(err);
            };
        }
    }

    start(port: number) {
        this.socket.start(port);
    }

    connected(socket: net.Socket) {
        let player: Player = new Player(socket);
        let data = player.dKeyExchange.getPacket();
        player.send(data);
        socket.player = player;
    }

    async disconnected(socket: net.Socket) {
        if (!socket)
            return;
        if (socket.player && socket.player.character) {
            Core.playerPool.remove(socket.player.character.uid);
            await socket.player.disconnect();
            console.log(`Player ${socket.player.character.name} disconnected.`);
        }

        if (socket.player.id) {
            console.log(`Disconnected id ${socket.player.id}.`);
        }

        socket.end();
        socket.destroy();

        socket = null;
    }

    static handleData(buffer: Buffer, socket: net.Socket) {
        let player: Player = socket.player;

        if (!player.exchanged) {
            player.playerAction = 0;

            // console.log(`Exchanging...`);
            // printBuffer(buffer);

            // console.log(JSON.stringify(player.cryptography));

            let serverKey = Buffer.from(Settings.SERVER_KEY);
            let crypto = new GameCryptography(serverKey);

            let otherData = Buffer.alloc(buffer.length);
            buffer.copy(otherData, 0, 0);

            otherData = crypto.decrypt(otherData);

            let plus = false;
            let pos = 0;

            let a = otherData[otherData.length - 140];
            let b = otherData.length >= 176 ? otherData.readInt32LE(otherData.length - 176) : 26;

            // console.log(`a: ${a}, b: ${b}, l: ${otherData.length}`);
            if (a === 128) {
                pos = buffer.length - 140;
                buffer = player.cryptography.decrypt(buffer);
            } else if (b === 128) {
                pos = buffer.length - 176;
                plus = true;
                buffer = player.cryptography.decrypt(buffer, buffer.length - 36);
            }

            let length = buffer.readInt32LE(pos);
            pos += 4;

            if (length != 128) {
                console.log('Ops! Something wrong...');
                player.disconnect();
                return;
            }

            let pubKey = Buffer.alloc(128);
            for (let i = 0; i < length; i++, pos++) {
                pubKey[i] = buffer[pos];
            }

            let pubKeyStr = pubKey.toString();
            player.cryptography = player.dKeyExchange.handleClientKey(pubKeyStr, player.cryptography);
            // console.log(pubKeyStr);

            // console.log(JSON.stringify(player.cryptography));

            if (plus) {
                let pdata = Buffer.alloc(36);
                buffer.copy(pdata, 0, buffer.length - 36);
                this.processData(pdata, player);
            }

            player.exchanged = true;
        } else {
            this.processData(buffer, player);
        }

    }

    static processData(buffer: Buffer, player: Player, bypass: boolean = false) {
        try {
            //printBuffer(buffer)
            let data = bypass ? buffer : player.cryptography.decrypt(Buffer.from(buffer));
            //printBuffer(data)

            if (player.playerAction > 0) {
                let len = data.readUInt16LE(0) + 8;
                let packet = Buffer.alloc(len);
                data.copy(packet, 0, 0);
                data = packet;
            } else player.playerAction = 1;

            //player.queue.enqueue(data);

            if (data.length){
                //while (player.queue.canDequeue()) {
                    //let packet: Buffer = player.queue.dequeue();
                    let packet = data;
                    try {
                        GamePacketController.handle(packet, player);
                    } catch (err) {
                        let id = packet.readUInt16LE(2);
                        console.log(`Error with packet id: ${id}`);
                        console.log(err);
                        player.disconnect();
                    }
                //}
            }

        } catch (err) {
            console.error(err);
        }
    }

    _connected(socket: net.Socket) {
        let player: Player = new Player(socket, true);
        socket.player = player;
    }

    _disconnected(socket: net.Socket) {
        if (socket.player && socket.player.character) {
            //Core.playerPool.remove(socket.player.id);
            socket.player.disconnect();
            console.playerlog(`Player ${socket.player.character.name} disconnected.`);
        }

        if (socket.player.id) {
            console.playerlog(`Disconnected id ${socket.player.id}.`);
            socket.end();
            socket.destroy();
        }
    }

    _handleData(buffer: Buffer, socket: net.Socket) {
        let player: Player = socket.player;
        player.recqueue.enqueue(buffer);

        if (player.exchanged) {
            if (player.character && !player.character.player) {
                player.character.player = player;
                socket.player = player;
            }

            while (player.recqueue.canDequeue()) {
                let packet = null;
                if (player.bkpqueue.canDequeue())
                    packet = player.bkpqueue.dequeue();
                else packet = player.recqueue.dequeue();

                let spid = packet.toString('ascii', 0, 4);
                if (spid === `spid` || spid === `rpid`)
                    continue;
                //this.processData(packet, player, true);
            }
        } else {
            let packet: any = false;
            while (player.recqueue.canDequeue()) {
                packet = player.recqueue.dequeue();
                let spid = packet.toString('ascii', 0, 4);
                console.playerlog(`ESPID-${spid}`);
                if (spid === `spid`) {
                    player.exchanged = true;
                    let id = buffer.readUInt64LE(4);
                    player.id = id;
                }

                if (spid === `rpid`) {
                    player.exchanged = true;
                    let id = buffer.readUInt64LE(4);
                    player.id = id;
                    console.playerlog(`Reconnecting id ${id}...`);

                    let found = false;
                    Core.statedPlayerPool.map((p) => {
                        if (p.id === id) {
                            player.loadFromSnapshot(p);
                            socket.player = player;
                            player.character.player = player;

                            if (player && player.character && player.character.player) {
                                console.playerlog(`Reconnected with ${player.character.name} (${player.id}).`);
                                Core.playerPool.add(player.id, player);

                                player.screen.clearAll();
                                player.screen.reload();

                                let cpid = Buffer.alloc(12);
                                cpid.write(`cpid`, 0);
                                player.send(cpid);
                                found = true;
                            } else {
                                console.playerlog(`Reconnected with error to ${player.character.name} (${player.id}).`);
                                found = true;
                            }
                        }
                    })

                    if (!found) {
                        console.log(`Player ${player.id} not found in the pool.`);
                        let kill = Buffer.alloc(12);
                        kill.write(`kill`, 0);
                        player.send(kill);
                    }

                } else {
                    player.bkpqueue.enqueue(packet);
                }
            }
        }
    }
}