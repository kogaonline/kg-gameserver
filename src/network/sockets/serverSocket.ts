import * as net from 'net';

export default class ServerSocket {
    server: net.Server;
    receiveData: any;
    disconnect: any;
    error: any;

    port: number;
    connect: any;

    connections: net.Socket[];

    constructor(connect: any) {
        this.connect = connect;
        this.enable(connect);
    }

    enable(connect: any){
        this.connections = [];
        let serverSocket = this;
        this.server = new net.Server(
            function (socket: net.Socket) {
                serverSocket.connections.push(socket);
                connect(socket);

                socket.setNoDelay(true);
                socket.setMaxListeners(255);

                socket.on('close', (err) => {
                    if (serverSocket.disconnect)
                        serverSocket.disconnect(err, socket);
                });

                socket.on('data', (data) => {
                    if (serverSocket.receiveData)
                        serverSocket.receiveData(data, socket);
                });

                socket.on('error', (err) => {
                    if (serverSocket.error)
                        serverSocket.error(err, socket);
                    else
                        console.log(err);
                });
            }
        );
    }

    start(port: number) {
        this.port = port;
        this.server.listen(port);
    }

    restart(){
        this.server.close();
        this.server.removeAllListeners();
        this.connections.map((socket) => {
            socket.destroy();
        });

        this.enable(this.connect);
        this.start(this.port);
    }
}
