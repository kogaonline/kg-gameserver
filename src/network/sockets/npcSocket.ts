import ServerSocket from './serverSocket';
import * as net from 'net';
import IPacket from '../../interfaces/IPacket';
import { NPCServerController } from '../packets/npcserver/npcscontroller';
import PacketQueue from '../../components/packetQueue';

export default class NPCServerSocket {
    socket: ServerSocket;
    server: net.Socket;

    queue: PacketQueue;
    recqueue: PacketQueue;

    constructor() {
        this.queue = new PacketQueue();
        this.recqueue = new PacketQueue();

        this.socket = new ServerSocket((socket: net.Socket) => {
            socket.setNoDelay(true);
            this.server = socket;
            this.connected(socket);
        });

        this.socket.disconnect = (err: any, socket: net.Socket) => {
            this.disconnected(socket);
        };

        this.socket.receiveData = (data: Buffer, socket: net.Socket) => {
            //this.processData(data);
            this.recqueue.enqueue(data);
        };

        this.socket.error = (err: any, socket: net.Socket) => {
            console.log('[NPC] Connect error:');
            console.log(err);
            socket.destroy();
            socket.end();
        };

        // start send & receive
        this.startReceiver();
        this.startSender();
    }

    async startSender(){
        let _this = this;
        setInterval(() => {
            if (_this.queue.canDequeue()){
                let _buffer = _this.queue.dequeue();
                _this.server.write(_buffer);
            }
        }, 25);
    }

    async startReceiver(){
        let _this = this;
        setInterval(() => {
            if (_this.recqueue.length > 50)
                console.log(`NPC Rec Queue Size: ${_this.recqueue.length}`);
            if (_this.recqueue.canDequeue()){
                try {
                    let _buffer = _this.recqueue.dequeue();
                    NPCServerController.handle(_buffer);
                } catch (err) {
                    console.error(err);
                }
            }
        }, 5);
    }

    sendBuffer(buffer: Buffer){
        if (this.server && this.server.writable){
            // this.server.write(buffer);
            this.queue.enqueue(buffer);
        }
    }

    send(buffer: IPacket){
        this.sendBuffer(buffer.toArray());
    }

    start(port: number) {
        this.socket.start(port);
    }

    connected(socket: net.Socket) {
        console.log(`NPC Server Connected!`);
        NPCServerController.resetMonsterData();
        NPCServerController.sendMonsterInfo();
        NPCServerController.sendPlayerInfo();
    }

    disconnected(socket: net.Socket) {
        if (!socket)
            return;

        socket.end();
        socket.destroy();
    }

    restart(){
        this.socket.restart();
    }

    async processData(buffer: Buffer) {
        try {
            NPCServerController.handle(buffer);
        } catch (err) {
            console.error(err);
        }
    }
}