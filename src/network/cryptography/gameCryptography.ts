import CAST128 from './cast128';

export default class GameCryptography {
    cast5: CAST128;

    constructor(key: Buffer) {
        this.cast5 = new CAST128();
        this.cast5.generateKey(key);
    }

    encrypt(buffer: Buffer) {
        let _buffer = this.cast5.Encrypt(buffer);
        return Buffer.from(_buffer);
    }

    decrypt(buffer: Buffer, length?: number) {
        let _buffer = this.cast5.Decrypt(buffer, length);
        return Buffer.from(_buffer);
    }

    setKey(key: Buffer) {
        this.cast5.generateKey(key);
    }

    setIVs(encryptIV: Buffer, decryptIV: Buffer) {
        this.cast5.setIVs(encryptIV, decryptIV);
    }
}