import { printBuffer } from "../../logical/kernel";

const BigInteger = require('big-integer');

export default class DiffieHellman {
    private p: any = BigInteger();
    private g: any = BigInteger();
    private a: any = BigInteger();
    private b: any = BigInteger();
    private s: any = BigInteger();
    private A: any = BigInteger();
    private B: any = BigInteger();

    public GetKey() { return this.s; }
    public GetRequest() { return this.A; }
    public GetResponse() { return this.A; }

    public get Key() {
        return this.s.toString(16);
    }

    public toString() {
        return this.s.toString(16);
    }

    public toBytes() {
        return Buffer.from(this.s.toArray(256).value);
    }

    constructor(p: string, g: string) {
        this.p = BigInteger(p, 16);
        this.g = BigInteger(g, 16);
    }

    public GenerateRequest(): string {
        this.a = BigInteger("77636349957643691723518303548337053708925539441528301179587492709950810107459");
        this.A = this.g.modPow(this.a, this.p);

        return this.A.toString(16);
    }

    public handleResponse(publicKey: string) {
        this.B = new BigInteger(publicKey, 16);
        this.s = this.B.modPow(this.a, this.p);
    }

}