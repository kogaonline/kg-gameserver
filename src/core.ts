import ConsoleHandler from "./libs/console";
import Player from "./game/player";
import Map from "./game/map";
import IndexedElement from "./components/indexedElement";
import DBService from "./services/database";
import { IItemComposeInfo } from "./interfaces/IItemInfo";
import { IMagicType } from "./database/magicType";
import WorldService from "./services/world";
import { IItemType } from "./database/itemtype";
import { INpc } from "./database/npc";
import Character from "./game/character";
import { IMonsterType } from "./database/monstertype";
import { ILevExp } from "./database/levexp";
import ServerAction from "./base/serverActions";

export default class Core {
    static statedPlayerPool: IndexedElement<Player> = new IndexedElement();
    static playerPool: IndexedElement<Player> = new IndexedElement();
    static monsterInfo: IndexedElement<IMonsterType> = new IndexedElement();
    //static monsterPool: IndexedElement<Character> = new IndexedElement();
    static maps: IndexedElement<Map> = new IndexedElement();
    static dmaps: IndexedElement<string> = new IndexedElement();
    static actions: IndexedElement<ServerAction> = new IndexedElement();
    static npcs: IndexedElement<INpc> = new IndexedElement();
    static levexp: IndexedElement<ILevExp> = new IndexedElement();

    static magicInfo: IndexedElement<IMagicType> = new IndexedElement();
    static itemInfo: IndexedElement<IItemType> = new IndexedElement();
    static itemComposeInfo: IndexedElement<IItemComposeInfo> = new IndexedElement();

    static itemUIDCounter = 0;
    static charaterUIDCounter = 0;
    static monsterUIDCounter = 400000;
    static magicUIDCounter = 0;

    static worldService: WorldService = new WorldService();

    static async prepareDatabase() {
        console.log('Preparing Database...');
        await DBService.init();

        // await DBService.MapsTable.load()
        // await DBService.ItemsTable.loadItemInfo()
        // await DBService.MagicsTypeTable.load()
        // await DBService.loadUIDs()

        return true;
    }

    static setupConsole() {
        ConsoleHandler.setup();
    }
}