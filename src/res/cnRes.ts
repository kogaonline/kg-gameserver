export enum cnRes {
    ANSWER_OK = "ANSWER_OK",
    ALLUSERS = "ALLUSERS",
    NEW_ROLE = "NEW_ROLE",
    SYSTEM = "SYSTEM",

    STR_REGISTER_INVALID_NAME = "The chosen name is invalid!",
    STR_REGISTER_INVALID_NAME_CHARACTERS = "The chosen name contains invalid characters!",
    STR_REGISTER_NAME_TAKEN = "Sorry, but the name is already taken!",
    STR_REGISTER_UNSUCCESSFULL = "Unable to create character!",
    STR_REGISTER_ACCOUNT_BANNED = "You are banned!",

    STR_ROLE_DELETE_SUCCESS = "Character deleted. Please relogin!",
    STR_ROLE_DELETE_FAILURE = "Character can not be deleted. Verify your password and try again!",

    STR_INVENTORY_FULL = "Your inventory is full!",
    STR_EQUIPMENT_UNEQUIP_INVENTORY_FULL = "Your inventory is full! Can't unequip this item.",
    STR_EQUIPMENT_RIGHT_HAND_FIRST = "You need to equip your right hand weapon first.",

    STR_YOU_ALREADY_DIE = "You are dead.",

    STR_USE_MAGIC_LEVEL_NOT_ENOUGH = "You don't have the minimum level to use this magic.",
    STR_USE_MAGIC_SELF_FORBIDDEN = "You can't use this magic on yourself.",

    STR_LOCATION_INVALID = "You can't move there.",
    STR_MONEY_PICKUP = "You've picked up %d silver.",
    STR_ITEM_PICKUP = "You've picked up %s.",
    STR_ITEM_PICKUP_TOO_LATE = "Too late. The item has disapeared.",
}