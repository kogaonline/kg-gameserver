import ServerAction from "../serverActions";
import Character from "../../game/character";
import MsgTaskDialog from "../../network/packets/gamepackets/MsgTaskDialog";
import { EMsgTaskDialogType } from "../../enums/EMsgTaskDialogType";

export function ACTION_MENUTEXT(action: ServerAction, character: Character): boolean {
    character.player.taskDialogs.push(new MsgTaskDialog(EMsgTaskDialogType.Text, action.param));
    return true;
}

export function ACTION_MENULINK(action: ServerAction, character: Character): boolean {
    let size = character.player.taskDialogOptions.length;

    character.player.taskDialogs.push(new MsgTaskDialog(EMsgTaskDialogType.Link, action.args[0], size));
    character.player.taskDialogOptions.push(action.nargs[1]);
    return true;
}

export function ACTION_MENUEDIT(action: ServerAction, character: Character): boolean {
    let size = character.player.taskDialogOptions.length;

    character.player.taskDialogs.push(new MsgTaskDialog(EMsgTaskDialogType.Edit, action.args[2], size, action.nargs[0]));
    character.player.taskDialogOptions.push(action.nargs[1]);
    return true;
}

export function ACTION_MENUPIC(action: ServerAction, character: Character): boolean {
    character.player.taskDialogs.push(new MsgTaskDialog(EMsgTaskDialogType.Picture, '', 0, action.nargs[2]));
    return true;
}

export function ACTION_MENUCREATE(action: ServerAction, character: Character): boolean {
    character.player.taskDialogs.push(new MsgTaskDialog(EMsgTaskDialogType.Create, '', 0));
    character.player.taskDialogs.map(td => td.send(character.player));

    character.player.taskDialogs = [];
    return true;
}

export function ACTION_MESSAGEBOX(action: ServerAction, character: Character): boolean {
    character.player.taskDialogOptions.push(action.data);
    new MsgTaskDialog(EMsgTaskDialogType.Confirm, action.param, 1).send(character.player);
    return true;
}