import Character from "../../game/character";
import MsgItem from "../../network/packets/gamepackets/MsgItem";
import ServerAction from "../serverActions";
import { EActionType } from "../../enums/EActionType";
import { ACTION_MENUTEXT, ACTION_MENULINK, ACTION_MENUPIC, ACTION_MENUCREATE, ACTION_MESSAGEBOX, ACTION_MENUEDIT } from "./100-199";

/**
 * Function that actually does execute an action object.
 * 
 * `Note`: Player actions will only be executed if `character` is present.
 * The same for Items and Monsters, that will require `item` and `monster` params.
 */
export default function handleActionExecution(action: ServerAction, character: Character = null, item: MsgItem = null, monster: Character = null): boolean {
    try {
        if (character){
            switch (action.type){
                case EActionType.ACTION_MENUTEXT: return ACTION_MENUTEXT(action, character);
                case EActionType.ACTION_MENULINK: return ACTION_MENULINK(action, character);
                case EActionType.ACTION_MENUPIC: return ACTION_MENUPIC(action, character);
                case EActionType.ACTION_MENUEDIT: return ACTION_MENUEDIT(action, character);
                case EActionType.ACTION_MENUCREATE: return ACTION_MENUCREATE(action, character);

                case EActionType.ACTION_MESSAGEBOX: return ACTION_MESSAGEBOX(action, character);
            }
        }
    } catch (err){
        console.error(err);
    }
    return true;
}