import { EUpdateType } from "../enums/EUpdateType";

export default interface IUpdate {
    type: EUpdateType;
    value1: number;
    value2: number;
    value3: number;
}