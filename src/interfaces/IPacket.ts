export default interface IPacket
{
    toArray(): Buffer
    construct(buffer: Buffer): void;
    send(client:any): void;
}