export default interface IPacket
{
    toArray(): Buffer
    construct(buffer: Buffer): void;
    send(server:any): void;
}