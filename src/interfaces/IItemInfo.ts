export default interface IItemInfo {
}

export interface IItemComposeInfo {
    uid: number;
    id: number;
    plus: number;
    addAgility: number;
    minAttack: number;
    maxAttack: number;
    defense: number;
    magicAttack: number;
    magicDefense: number;
    addDodge: number;
    addAccuracy: number;
    addHP: number;
    addMP: number;
    attackRange: number;
}