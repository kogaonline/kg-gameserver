import { EMapElementType } from "../enums/EMapElementType";
import Player from "../game/player";

export default interface IMapElement {
    uid: number;
    mapId: number;
    x: number;
    y: number;
    mapElementType: EMapElementType;
    player?: Player;
}