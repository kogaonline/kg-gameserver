import { ECharacterAttrType } from "../enums/ECharacterAttrType";

export default interface ICharacterAttr {
    type: ECharacterAttrType;
    value: number;
    magic: number;
    item: number;
}